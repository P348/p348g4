# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#
# COMMON CMAKE
#############

cmake_minimum_required( VERSION 2.6 )
project( p348g4 )
set( P348G4_VERSION_MAJOR 0 )
set( P348G4_VERSION_MINOR 1 )
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
    "${CMAKE_SOURCE_DIR}/assets/contrib/cmake"
    "/etc/root/cmake" )
include(assets/contrib/cmake/OutOfSource.cmake)
include(assets/contrib/cmake/option_depend.cmake)
AssureOutOfSourceBuilds()

# OPTIONS
########

# Right below are some build options that can be enabled or
# disabled by -D<option_name>=<ON|OFF> option given to CMake.
# The \option tag is used by buildtest.sh script.
#\option:
option( APRIME_CS_ROUTINES
        "Enable A' cross-section calculation routines (PDF, etc.)."
        OFF )
#\option:
option( APRIME_EVENT_GENERATOR
        "Enable A' event generator."
        OFF )
#\option:
option( GEANT4_MC_MODEL
        "Enable Geant4 MC model for the p348 experiment."
        OFF )
#\option:
option( GEANT4_DYNAMIC_PHYSICS
        "Enable run-time physlist composition."
        OFF )
#\option:
option( G4_MDL_VIS
        "Enable Geant4 visualization manager."
        OFF )
#\option:
option( G4_MDL_GUI
        "Enable Geant4 GUI."
        OFF )
#\option:
option( RPC_PROTOCOLS
        "Eneables RPC serialization protocols (via google protobuf, v.>=2.6 required)."
        OFF )
#\option:
option( ANALYSIS_ROUTINES
        "Enable analysis routines."
        OFF )
#\option
option( ALIGNMENT_ROUTINES
        "Enable alignment routines."
        OFF )
#\option:
option( EXP_RAW_DATA_READING
        "Enable raw experimental statistics readers for analysis."
        OFF )
#\option:
option( G4_APRIME_G4_PROCESS
       "Make A' mixin process."
        OFF )
# ^^^^ requires DaqDataDecoding library to be provided within CMAKE_LIBRARY_PATH
# TODO
#option( GENERATE_DOCUMENTATION
#    "Poduces a Doxygen-generated documentation for p348g4 lib."
#    ON)

option( DOC_HTML
        "Produces HTML documentation with doxygen"
        OFF )

set( DEFAULT_P348G4_CFG_PATH "../../p348g4/defaults.cfg" CACHE STRING "Default config file for entire library.")

# check for options deps
# The \opt-dep: tag is used by buildtest.sh script.
#\opt-dep:
option_depend( EXP_RAW_DATA_READING     ANALYSIS_ROUTINES )
#\opt-dep:
option_depend( G4_MDL_GUI               GEANT4_MC_MODEL )
#\opt-dep:
option_depend( G4_MDL_VIS               GEANT4_MC_MODEL )
#\opt-dep:
option_depend( GEANT4_DYNAMIC_PHYSICS   GEANT4_MC_MODEL )
#\opt-dep:
option_depend( APRIME_EVENT_GENERATOR   APRIME_CS_ROUTINES )
#\opt-dep:
option_depend( G4_APRIME_G4_PROCESS     GEANT4_MC_MODEL APRIME_CS_ROUTINES )

#
# THIRD-PARTY LIBRARIES AND PACKAGES
###################################

find_package( Goo  REQUIRED )
find_package( ROOT COMPONENTS Eve Foam Gui Ged Geom RGL EG REQUIRED )
find_package (Boost ${Boost_FORCE_VERSION} COMPONENTS program_options system thread REQUIRED )
if( RPC_PROTOCOLS )
    find_package( Protobuf REQUIRED )
    #set(PROTOBUF_GENERATE_CPP_APPEND_PATH FALSE)
    PROTOBUF_GENERATE_CPP(GPROTO_MSGS_SRCS      GPROTO_MSGS_HDRS
        assets/messages/event.proto)
    #message( STATUS '$!$' ${GPROTO_MSGS_SRCS} )  # XXX
    #message( STATUS '$@$' ${GPROTO_MSGS_HDRS} )  # XXX
endif( RPC_PROTOCOLS )
# GEANT4 (opt)
if( GEANT4_MC_MODEL )
    find_package(Geant4)
    if( G4_MDL_GUI )
        find_package(Geant4 REQUIRED ui_all vis_all)
    else( G4_MDL_GUI )
        find_package(Geant4 REQUIRED)
    endif( G4_MDL_GUI )
    include(${Geant4_USE_FILE})
endif( GEANT4_MC_MODEL )

# Qt (opt)
if( GEANT4_MC_MODEL )
    if( G4_MDL_GUI )
        set( DESIRED_QT_VERSION "4" )
        find_package(Qt)
    endif( G4_MDL_GUI )
endif( GEANT4_MC_MODEL )

# genfit (opt)
if( ANALYSIS_ROUTINES )
    # Since genfit have not yet a .cmake module, we will manually
    # specify library and include path here:
    find_library( GENFIT_LIB NAMES genfit2 HINTS ${GENFIT_LIB_PATH} )
    get_filename_component( GENFIT_LIBRARY_DIR ${GENFIT_LIB}
        DIRECTORY )
    find_path( GENFIT_INCLUDE_DIR KalmanFitter.h
        HINTS "${GENFIT_LIBRARY_DIR}/../include/" )
    if( GENFIT_LIB )
        message( STATUS "Found genfit library at " ${GENFIT_LIB} " with header at " ${GENFIT_INCLUDE_DIR} )
    else( GENFIT_LIB )
        message( WARNING "genfit library not found. You can use"
        "CMAKE_LIBRARY_PATH variable to manually specify search path." )
    endif( GENFIT_LIB )
endif( ANALYSIS_ROUTINES )

# Find DaqDataDecoding library (opt)
if( EXP_RAW_DATA_READING )
    find_library( DAQDECODING_LIB NAMES DaqDataDecoding.a DaqDataDecoding HINTS ${DDD_LIB_PATH} )
    get_filename_component( DAQDECODING_INCLUDE_DIR_HINT ${DAQDECODING_LIB}
        DIRECTORY )
    find_path( DAQDECODING_INCLUDE_DIR DaqEventsManager.h
                HINTS ${DAQDECODING_INCLUDE_DIR_HINT} )
    if( DAQDECODING_LIB )
        message( STATUS "Found DaqDataDecoding library at " ${DAQDECODING_LIB} " with header at " ${DAQDECODING_INCLUDE_DIR} )
    else( DAQDECODING_LIB )
        message( FATAL_ERROR "DaqDataDecoding library not found. You can use"
        "CMAKE_LIBRARY_PATH variable to manually specify search path." )
    endif( DAQDECODING_LIB )
    # Expat (required by DDD)
    find_package( EXPAT REQUIRED )

    # Note: this option enables look up for DATE, RFIO (aka shift, developed by CERN CASTOR team
    # at 2000-20005) libraries. RFIO library is now completly changed and included into major
    # CASTOR software facility while DATE is known to be darmant.
    # As these libs are included in CERN's Scientific Linux and probably can not be
    # redistributed, this option is dedicated out of walk-through build. One can only need them
    # when performing build on actual CERN experiments.

    find_library( RFIO_AKA_SHIFT_LIB NAMES libshift.so )
    find_library( DATE_LIBMONITOR NAMES libmonitor.so
                  HINTS /afs/cern.ch/compass/online/daq/dateV37/monitoring/Linux/ )
endif( EXP_RAW_DATA_READING )

# GSL (opt)
if( APRIME_CS_ROUTINES OR ANALYSIS_ROUTINES )
    find_package(GSL REQUIRED)
endif( APRIME_CS_ROUTINES OR ANALYSIS_ROUTINES )

site_name( BUILDER_HOSTNAME )

if( DOC_HTML )
    find_package( Doxygen REQUIRED )
endif( DOC_HTML )

mark_as_advanced( BUILDER_HOSTNAME DAQDECODING_INCLUDE_DIR DAQDECODING_LIB
    DESIRED_QT_VERSION GSL_CONFIG_EXECUTABLE
    Geant4_DIR QT_QMAKE_EXECUTABLE
    ROOT_CINT_EXECUTABLE ROOT_CONFIG_EXECUTABLE
    Goo_INCLUDE_DIRS Goo_LIBRARIES )

#
# SET-UP BUILD-TIME PARAMETERS
#############################

# Obtain versioning parameters
EXECUTE_PROCESS(COMMAND date +"%x %x"
                OUTPUT_VARIABLE BUILD_TIMESTAMP
                OUTPUT_STRIP_TRAILING_WHITESPACE )
set( P348G4_LIB_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/inc/" )

# Configure compiler -- base flags
set( CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -Wall -D_GNU_SOURCE -fexceptions -pthread -D_FILE_OFFSET_BITS=64 -DHAVE_INLINE -std=c99" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -D_GNU_SOURCE -fexceptions -pthread -D_FILE_OFFSET_BITS=64 -std=gnu++11 -Wno-c99-extensions" )

if( CMAKE_COMPILER_IS_GNUCXX )
    set( CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -rdynamic" )
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -rdynamic" )
endif( CMAKE_COMPILER_IS_GNUCXX )

set( CMAKE_C_FLAGS_DEBUG            "${CMAKE_C_FLAGS_DEBUG}     -fno-omit-frame-pointer" ) # -Wfatal-errors -fprofile-use? -fprofile-correction
set( CMAKE_CXX_FLAGS_DEBUG          "${CMAKE_CXX_FLAGS_DEBUG}   -fno-omit-frame-pointer" )
set( CMAKE_C_FLAGS_RELEASE          "${CMAKE_C_FLAGS_RELEASE}" )
set( CMAKE_CXX_FLAGS_RELEASE        "${CMAKE_CXX_FLAGS_RELEASE}" )
set( CMAKE_C_FLAGS_MINSIZEREL       "${CMAKE_C_FLAGS_MINSIZEREL}" )
set( CMAKE_CXX_FLAGS_MINSIZEREL     "${CMAKE_CXX_FLAGS_MINSIZEREL}" )
set( CMAKE_C_FLAGS_RELWITHDEBINFO   "${CMAKE_C_FLAGS_RELWITHDEBINFO}" )
set( CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}" )

# Attain configuration depending on current build dir.
string(REPLACE "/" ";" SPLITTED_PATH ${CMAKE_BINARY_DIR})
list(REVERSE SPLITTED_PATH)
list(GET SPLITTED_PATH 0 buildDir)
message( STATUS "Building in directory ${buildDir}" )
if( buildDir STREQUAL "debug" )
    message( STATUS "NOTE: debug build" )
    #\buildconf:
    set( CMAKE_BUILD_TYPE "Debug" )
elseif( buildDir STREQUAL "release" )
    message( STATUS "NOTE: release build" )
    #\buildconf:
    set( CMAKE_BUILD_TYPE "Release" )
elseif( buildDir STREQUAL "minsizerel" )
    message( STATUS "NOTE: minimized size build" )
    #\buildconf:
    set( CMAKE_BUILD_TYPE "MinSizeRel" )
elseif( buildDir STREQUAL "relwdebinfo" )
    message( STATUS "NOTE: release build with debugging info" )
    #\buildconf:
    set( CMAKE_BUILD_TYPE "RelWithDebInfo" )
else()
    message( STATUS "NOTE: custom build" )
    set( CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -DNDEBUG -O3" )
    set( CMAKE_CXX_FLAGS   "${CMAKE_CXX_FLAGS} -DNDEBUG -O3" )
    #\buildconf:
    #set( CMAKE_BUILD_TYPE "Custom" )
endif()

#
# Include directories
####################

#
# Geant4 pushes include directories with -I flag; we need, however,
# them to be included with -isystem to avoid annoying warnings. This
# tweak will move all the include dir previously pushed by third-party
# modules from usual INCLUDE_DIRECTORIES list to
# INTERFACE_SYSTEM_INCLUDE_DIRECTORIES.
get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES "")
include_directories( SYSTEM ${dirs} )
# This lines is used for dev needs. Print out all the include dirs:
#get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
#foreach(dir ${dirs})
#    message(STATUS "I-dir='${dir}'")
#endforeach()
#get_property(sdirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INTERFACE_SYSTEM_INCLUDE_DIRECTORIES)
#foreach(dir ${sdirs})
#    message(STATUS "S-dir='${dir}'")
#endforeach()

# Why does CLANG ignores this sometimes?
#SET(CMAKE_INCLUDE_SYSTEM_FLAG_CXX "-isystem ")
include_directories( "inc"
                     "rootPlugins"
                     SYSTEM ${Goo_INCLUDE_DIRS}
                     ${GSL_INCLUDE_DIRS}
                     ${Boost_INCLUDE_DIRS}
                     ${ROOT_INCLUDE_DIRS}
                     #$<$<STREQUAL:${EXP_RAW_DATA_READING},ON>:${DAQDECODING_INCLUDE_DIR}>
                     $<$<STREQUAL:${RPC_PROTOCOLS},ON>:${PROTOBUF_INCLUDE_DIRS}>
                     $<$<STREQUAL:${RPC_PROTOCOLS},ON>:${CMAKE_CURRENT_BINARY_DIR}>
                     $<$<STREQUAL:${GEANT4_MC_MODEL},ON>:${Geant4_INCLUDE_DIR}> )

if(EXP_RAW_DATA_READING)
    include_directories(SYSTEM ${DAQDECODING_INCLUDE_DIR})
endif(EXP_RAW_DATA_READING)

if( GENFIT_LIB )
    include_directories( ${GENFIT_INCLUDE_DIR} )
endif( GENFIT_LIB )

#
# Link directories
#################

link_directories( ${Goo_LIBRARY_DIR} )
# todo: crutch!
if( SUPPLEMENTARY_LINK_DIRECTORY )
    link_directories( ${SUPPLEMENTARY_LINK_DIRECTORY} )
endif( SUPPLEMENTARY_LINK_DIRECTORY )
if( APRIME_EVENT_GENERATOR OR build_eventDisplay )
    # todo: crutch!
    set( ROOT_LIBRARIES "${ROOT_LIBRARIES}" )
    # As we need multi_index lib:
    if( ${Boost_VERSION} LESS 105500 )
        message( FATAL_ERROR "Event generator requires boost library v. >=1.55.0" )
    endif( ${Boost_VERSION} LESS 105500 )
endif( APRIME_EVENT_GENERATOR OR build_eventDisplay )

if( GENFIT_LIB )
    link_directories( ${GENFIT_LIBRARY_DIR} )
endif( GENFIT_LIB )

#
# Configure sources
##################

# generic p348lib configuration
configure_file (
    "${P348G4_LIB_INCLUDE_DIR}/p348g4_config.h.in"
    "${P348G4_LIB_INCLUDE_DIR}/p348g4_config.h"
)

# Physics list aux config
if( GEANT4_MC_MODEL )
    if( NOT DEFINED Geant4_PHLIST_INCLUDE_DIR )
        set( Geant4_PHLIST_INCLUDE_DIR ${Geant4_INCLUDE_DIR} )
    endif(NOT DEFINED Geant4_PHLIST_INCLUDE_DIR )
    execute_process(COMMAND
        ${CMAKE_CURRENT_SOURCE_DIR}/assets/scripts/bash/get_phLists.sh
        ${Geant4_PHLIST_INCLUDE_DIR}
        OUTPUT_VARIABLE G4PHYSLISTS )
    # Gather physics list:
    if( G4PHYSLISTS )
        string(REPLACE "\n" "," G4PHYSLISTS ${G4PHYSLISTS})
        message( STATUS "Found pre-defined PhysLists: \n" ${G4PHYSLISTS} )
        string(REPLACE ","  ";" G4PHYSLISTS ${G4PHYSLISTS})
        foreach( phLst ${G4PHYSLISTS} )
            set( G4_PHLIST_INCLUDES_STR
                "${G4_PHLIST_INCLUDES_STR}\n# include <${phLst}.hh>" )
            set( G4_PHLIST_DEFINES_STR
                "${G4_PHLIST_DEFINES_STR}    m(${phLst}) \\\n" )
            # TODO: conditionally determine if we really need to re-generate
            # it as now each `$ make' invokation causes rebuild.
            configure_file (
                "${P348G4_LIB_INCLUDE_DIR}/g4extras/auto.in/PhysList.ihpp.in"
                "${P348G4_LIB_INCLUDE_DIR}/g4extras/auto.out/PhysList.ihpp"
            )
        endforeach( phLst )
    else( G4PHYSLISTS )
        message( SEND_ERROR "No physics list found in \
        ${Geant4_INCLUDE_DIR}. May indicate problems with \
        your Geant4 installation." )
    endif( G4PHYSLISTS )
    if( GEANT4_DYNAMIC_PHYSICS )
        # Gather particles:
        execute_process(COMMAND
            ${CMAKE_CURRENT_SOURCE_DIR}/assets/scripts/bash/get_g4Particles.sh
            ${Geant4_INCLUDE_DIR}
            OUTPUT_VARIABLE G4PARTICLES )
        if( G4PARTICLES )
            string(REPLACE "\n" "," G4PARTICLES ${G4PARTICLES})
            message( STATUS "Found pre-defined Particles: \n" ${G4PARTICLES} )
            string(REPLACE ","  ";" G4PARTICLES ${G4PARTICLES})
            foreach( particle ${G4PARTICLES} )
                set( G4_PARTICLES_INCLUDES_STR
                    "${G4_PARTICLES_INCLUDES_STR}\n# include <G4${particle}.hh>" )
                set( G4_PARTICLES_DEFINES_STR
                    "${G4_PARTICLES_DEFINES_STR}    m(${particle}) \\\n" )
                # TODO: conditionally determine if we really need to re-generate
                # it as now each `$ make' invokation causes rebuild.
                configure_file (
                    "${P348G4_LIB_INCLUDE_DIR}/g4extras/auto.in/Particles.ihpp.in"
                    "${P348G4_LIB_INCLUDE_DIR}/g4extras/auto.out/Particles.ihpp"
                )
            endforeach( particle )
        else( G4PARTICLES )
            message( SEND_ERROR "No particle definitions found in \
            ${Geant4_INCLUDE_DIR}. May indicate problems with \
            your Geant4 installation." )
        endif( G4PARTICLES )
        # Gather physics modules:
        execute_process(COMMAND
            ${CMAKE_CURRENT_SOURCE_DIR}/assets/scripts/bash/get_physics.sh
            ${Geant4_INCLUDE_DIR}
            OUTPUT_VARIABLE G4PHYSICS )
        if( G4PHYSICS )
            string(REPLACE "\n" "," G4PHYSICS ${G4PHYSICS})
            message( STATUS "Found pre-defined Physics: \n" ${G4PHYSICS} )
            string(REPLACE ","  ";" G4PHYSICS ${G4PHYSICS})
            foreach( physics ${G4PHYSICS} )
                set( G4_PHYSICS_INCLUDES_STR
                    "${G4_PHYSICS_INCLUDES_STR}\n# include <${physics}.hh>" )
                set( G4_PHYSICS_DEFINES_STR
                    "${G4_PHYSICS_DEFINES_STR}    m(${physics}) \\\n" )
                # TODO: conditionally determine if we really need to re-generate
                # it as now each `$ make' invokation causes rebuild.
                configure_file (
                    "${P348G4_LIB_INCLUDE_DIR}/g4extras/auto.in/Physics.ihpp.in"
                    "${P348G4_LIB_INCLUDE_DIR}/g4extras/auto.out/Physics.ihpp"
                )
            endforeach( physics )
        else( G4PHYSICS )
            message( SEND_ERROR "No physics definitions found in \
            ${Geant4_INCLUDE_DIR}. May indicate problems with \
            your Geant4 installation." )
        endif( G4PHYSICS )
    endif( GEANT4_DYNAMIC_PHYSICS )
endif( GEANT4_MC_MODEL )
file(GLOB PN_LOW_SRCS
        ${PROJECT_SOURCE_DIR}/src/*.c* )

# Remove this dirty things outta my compiler options!
string(REPLACE "-pedantic"              "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} )
string(REPLACE "-Wno-long-long"         "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} )
string(REPLACE "-Wno-non-virtual-dtor"  "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} )
string(REPLACE "-Wno-c99-extensions"    "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} )

# Add ROOT dictionaries
if( ANALYSIS_ROUTINES )
    ROOT_GENERATE_DICTIONARY( TrackingGroupDict
        ${PROJECT_SOURCE_DIR}/inc/alignment/TrackingGroup*
        LINKDEF alignment/TrackingGroupLinkDef.h )
endif( ANALYSIS_ROUTINES )

# TARGETS
########

file(GLOB_RECURSE P348G4_LIB_SOURCES src/*.c*)
add_library( p348g4 SHARED
        ${P348G4_LIB_SOURCES}
        $<$<STREQUAL:${RPC_PROTOCOLS},ON>:${GPROTO_MSGS_SRCS}>
        $<$<STREQUAL:${ANALYSIS_ROUTINES},ON>:TrackingGroupDict.cxx>
    )

target_link_libraries( p348g4 ${Goo_LIBRARIES} )
target_link_libraries( p348g4 ${Boost_LIBRARIES})
target_link_libraries( p348g4 ${ROOT_LIBRARIES} )
if( GEANT4_MC_MODEL )
    target_link_libraries( p348g4 ${Geant4_LIBRARIES})
    if( G4_MDL_GUI )
        target_link_libraries( p348g4 ${QT_LIBRARIES})
    endif( G4_MDL_GUI )
endif( GEANT4_MC_MODEL )
if( APRIME_CS_ROUTINES OR ANALYSIS_ROUTINES )
    target_link_libraries( p348g4 ${GSL_LIBRARIES} )
endif( APRIME_CS_ROUTINES OR ANALYSIS_ROUTINES )
if( EXP_RAW_DATA_READING )
    target_link_libraries( p348g4 ${DAQDECODING_LIB} )
    target_link_libraries( p348g4 ${EXPAT_LIBRARIES} )
endif( EXP_RAW_DATA_READING )
if( RPC_PROTOCOLS )
    #set_source_files_properties(${GPROTO_MSGS_SRCS} PROPERTIES COMPILE_FLAGS -Wno-shadow)
    target_link_libraries( p348g4 ${PROTOBUF_LIBRARIES} )
    if( RFIO_AKA_SHIFT_LIB )
        message( STATUS "Found RFIO library: "  )
        target_link_libraries( p348g4 ${RFIO_AKA_SHIFT_LIB} )
    endif(RFIO_AKA_SHIFT_LIB )
    if( DATE_LIBMONITOR )
        message( STATUS "Found DATE/libmonitor library: " ${DATE_LIBMONITOR} )
        target_link_libraries( p348g4 ${DATE_LIBMONITOR} )
    endif( DATE_LIBMONITOR )
endif( RPC_PROTOCOLS )
if( GENFIT_LIB )
    target_link_libraries( p348g4 ${GENFIT_LIB} )
endif( GENFIT_LIB )

if( DOC_HTML )
    configure_file (
        ${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile.in
        ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
    )
    add_custom_target(doc
        ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API documentation with Doxygen" VERBATIM
    )
endif( DOC_HTML )

#
# Build enabled routines
# for build_<util> options see CMakeLists.txt in utils/
#\project-subdir:
add_subdirectory( utils )

# Build ROOT plugins
#\project-subdir:
add_subdirectory( rootPlugins )
# Prepare rootrc appending some environment variables:
configure_file (
    "${PROJECT_SOURCE_DIR}/p348g4-plugins.rootrc.in"
    "${CMAKE_CURRENT_BINARY_DIR}/p348g4-plugins.rootrc"
)
