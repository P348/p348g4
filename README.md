# CERN SPS P348 Experiment {#mainpage}

Theoretical proposal and experimental setup details can be found
on the [main experiment's page](https://p-348.web.cern.ch). Please, refer to
those materials before pointing out any doubtful moments. The figure below
depicts region of interest, where the A' particle is expected to be found.
Cordinates of this plot refers to theoretical parameters — the expected mass
and the so-called «mixing parameter» (\f$\epsilon\f$) involved into modified
Weizsacker-Williams
pseudophoton cross-section expression (see proposal for details). Expected
search coverage are drawn by pink and violet lines which corresponds to
different statistics amount which is to be accumulated in practice as
experimantal data.

![](doc/own/exclusionPlotInvisible.png)

## Table of contents

  * [Current status](#current-status)
  * [Capabilities and cases of usage](#usecases)
  * [Repository structure](#repo-struct)
    * [Utilities](#utilities)
    * [Assets directory](#assets)
  * [Documentation](#documentation)
  * [Obtaining sources](#obtaining-sources)
  * [Build](#build-instructions)
    * [Build problems](#build-problems)
    * [Notes about build on CERN LXPLUS environment](#lxplus-notes)
  * [Dependencies](#dependencies)
    * [Optional dependencies and features](#opt-deps)
  * [Branching/spin-offs](#branching-notes)
  * [Third-party snippets nodes](#third-party)
  * [Bugs](#bugs)
  * [Contacts](#contacts)
  * [License](#license)
  * [References](#references)

## <a name="current-status"></a> Current Status

Project draft; only few modelling procedures are provided for a while:

  * A «geometry library» can be located at `assets/gdml` directory. Please refer to
    its internal documentation to manage how to deal with it.
  * An analysis application is currently under hard development. General
    infrastructure and few analysis routines are provided.
  * A-prime event-generation procedures are implemented, but still weren't embedded
    into Geant4-model. See `aprimeEvGen` util for details.

One can link this library against its own project in order to utilize any useful stuff
like GDML processor or data treatment routines.

## <a name="usecases"></a> Capabilities and cases of usage

This library provides scalable and reusable framework for experimental data treatment
and simulation of P348 experiment that took place on SPS accelerator in CERN. The choosen
level of generocity implies a bit more comprehensive development that could be provided,
but somehow or other, this library provides useful tools:

  * For data analysis there is an event representation format based on
  Google Protocol Buffers. This format is
  rather strict than arbitrary binary serialization, more compact then self-descriptive MLs like
  XML/JSON/YAML, but still is higly extensible. Just take a look to its
  [documentation](https://developers.google.com/protocol-buffers/).
  * For Monte-Carlo (MC) nuclear physics simulation there is some adaptor classes (TODO:docref)
  providing unified access to common features within walkthrough configuration mechanism;
  * The configuration mechanism based on [`boost::variables_map`](http://www.boost.org/doc/libs/1_54_0/doc/html/boost/program_options/variables_map.html)
  provides access to wide range of user-defined parameters in any place of the library (TODO:docref).
  * Custom event-generation and numerical analysis needed for MC-simulation of exotic events
  can be embedded seamlessly into Geant4 routines.
  * Data plotting (is provided by ROOT API in most cases).

Here is the preliminary usecases diagram providing brief description of usecases considered
during development.

![](doc/diagrams.img/OverallUCD.svg)

Use cases on the left from actors were taken into consideration, but are unrelated to
library structure: e.g. writing your own application can emplace any usecases implemented in the
library as API parts. One can [take a brief look](https://www.andrew.cmu.edu/course/90-754/umlucdfaq.html)
to the UCD notation of UML diagrams to become able to strictly interpret this picture. In short,
this structure depicts possible tasks that can be handled by the library's routines as a dependency
graph where «*includes*» means that subroutine will be invoked at least once, while «*extends*» means
that subroutine can be invoked optionally. Of course, this UCD covers only generic cases.

(TODO) These «actioins» (usecases) written in ellipse often corresponds to particular API and will be
hyperlinked to actual Doxygen-generated page in the future in order to simplify aim-specific
navigation. Even relation arrows (classified as «extend» and «include» according to UML 1.4 notation)
can refer to particular API.

(TODO) Looks a bit messy and overwhelmed. Needs to be rearranged / reduced to impove readability.

## <a name="repo-struct"></a> Repository structure

The package consists of main library combining all the related things and few applications
that utilizes them.

Entire build can be configured via CMake's options mechanism (see [build instructions](#build-instructions)).

### <a name="utilities"></a> Utilities

Library utilizing applications are located at `./utils` directory. For now, all of them are only allowed
to be invoked from current build directory (e.g. `../../p348g4.build/debug/`). Most of them probably
uses `boost::variables_map` as an application configuration endpoint, so they usually provides
a context help for command-line arguments available by `--help` option.

Some of applications, in order, can be designed as «task-oriented» — it means that actual task to evaluate
needs to be specified as an argument for `--task,-t` key. Multiple entry of `--task` values causes
sequentional invokation of "tasks". As usual, the `--printout-tasks` or similar option is available
for convinience.

For the analysis application this behaviour is enchanced with per-event level sub-tasks called
«processors». They can be specified with `-p` option.

Available utilities:

  * `aprimeEvGen` serves event-generation basics for A-prime events.
  * `mdlv` util designed for visualization, and geometry conversion operations. No beam simulation provided.
  * `p348g4mdl` serves a set of actual Monte-Carlo simulation routines.
  * `p348a` is an analysis application dynamically combining event-processors.

Note, that some of the utilities can be unstable on current revision state due to hasty and irregular
development process. See for commits containing `SBC` abbreviation (stable-build commit) for
«all-works» revision.

For the details about reuirements, dependencies, and enabling/disabling the certain routine, see the
the «[Optional dependencies section](#opt-deps)».

### <a name="assets"></a> `assets` directory

All runtime-evaluable data or large static content are placed there. This dir also contains
helpful build scripts and cmake-modules used by main build-all-the-stuff script.

## <a name="documentation"></a> Documentation

Project is documented via the [Doxygen](http://www.stack.nl/~dimitri/doxygen/) system. Currently, not all
the parts is covered by well-formed coments as development process implies frequent prototyping and
changing conventions. One can found a `Doxygen` file at `doc/` directory and generate reference.

## <a name="obtaining-sources"></a> Obtaining Sources
.
[Internal CERN](https://gitlab.cern.ch/P348/p348g4) repo contains actual state that is
updated frequently is available only for authorised staff. Open project repository
can be found at [bitbucket](https://bitbucket.org/CrankOne/p348g4), but sometime it is in
irrelevant state. In order to get full sources tree, do:

    $ git clone git@bitbucket.org:CrankOne/p348g4.git

if you have a valid SSH/RSA key set up on your machine, or
    
    $ git clone https://CrankOne@bitbucket.org/CrankOne/p348g4.git

if you're faced troubles with the first option or surely won't use SSH/RSA key set.

If you are interested in a particular branch, do:
    
    $ git branch                    # to ensure that certain branch existing
    $ git checkout branchname       # to get the branch
    $ git pull origin branchname    # to sync your local branch with the remote one

Note, that actual revision state can usually be found on `development` branch for now.

Other source-management features can be found at [git manual page](http://git-scm.com/docs/user-manual.html).

## <a name="build-instructions"></a> Build

Please, note that only out-of-source builds are allowed. It is also desirable that
last one of build subdirectory will have representative name (allowed ones:
`debug`/`release`/`minsizerel`/`relwdebinfo`) ---
our CMake script will use it as a designation.

    $ cd ..
    $ mkdir -p p348g4.build/debug  # or ./p348g4.build/release for release builds
    $ cd p348g4.build/debug
    $ cmake ../../p348g4
    $ make

In-source builds are __strictly forbidden__ as it screws up the source tree consistency.

Note, that for custom installation location for goo library, one need to specify its cmake module
location, so instead of

    $ cmake ../../p348g4

one need to specify:

    $ cmake ../../p348g4 -DCMAKE_MODULE_PATH=/path/where/goo/is/installed/share/cmake/Modules/

It is also necessary for experimental data reading to provide a valid way to DaqDataDecoding
library (supplied by CORAL, for example [here](https://gitlab.cern.ch/P348/p348-daq)). Just prefix the
above `cmake` command with e.g. `CMAKE_LIBRARY_PATH=/path/to/p348-daq/coral/src/DaqDataDecoding/src/`.

### <a name="build-problems"></a> Build problems

#### Wrong physlist entry

If you got something like

     /mySourceDir/p348g4/src/g4extras/PhysList.cpp: In function 'G4VPhysicsConstructor* p348::_static_construct_G4EmDNAPhysics_option7()':
     /mySourceDir/p348g4/inc/g4extras/auto.out/Physics.ihpp:86:7: error: 'G4EmDNAPhysics_option7' was not declared in this scope
     m(G4EmDNAPhysics_option7)

it means that our script which gathers physics list information from your Geant4 installation
brought something that is not really a physics list or physics list module. One must then to
append this name into the end of scripts search. E.g. for example above it have to take place in
`assets/contrib/get_physics.sh` at the last line (after `| \`): `grep "G4EmDNAPhysics_option7" -v`.
Then one must re-run CMake configuration.

### <a name="lxplus-notes"></a> Notes about build on CERN LXPLUS environment

Default LXPLUS environment keeps a very ancient versions of compiler and boost library, so build
of p348 lib is not possible. There are, however, a huge amount of side software on linked `/afs/` share,
so one can build this project using them.

For end-user convinience we crafted a shell script providing a common build
in `assets/bash/LXPLUS_build.sh` (note: script was formed before we introduced a Google Protocol Buffers
into the project, since it became irrelevant for a while).

## <a name="dependencies"></a> Dependencies

Only C++11-compliant compiler can build the project (GCC is preferable).

For developers or just an issue reporting, it is desired, for your system
to support liberty/bfd stack unwinding via corresponding libraries (LLVM CLANG
stack unwidning is still unimplemented in "Goo"-library).

Boost library, CERN's Geant4 and ROOT frameworks are required. Boost needs to be configured
with filesystem support, variables\_map support and, possibly, some other usually included into your
default package configuration. Geant4 needs to be configured with GDML support (note, that it requires
Apache Xerces XML®).

Generic build environment and mandatory dependencies:

 * CMake v3.2
 * CERN ROOT v6.02
 * CERN Geant v4.9
 * GSL v1.14
 * Boost v1.55
 * Google Protocol Buffers compiler, library and headers v. 2.6.3.
 * The [goo library](https://bitbucket.org/CrankOne/goo) of latest version from `master` branch.

Note, that this convinient `oneof` qualifier for Google Protocol Buffers language was introduced only
since 2.6 versions were released.

The [goo](https://bitbucket.org/CrankOne/goo) of latest version from `master` branch is required
as a light-weight basis for an application class abstraction and dynamic exception maintaining. One
can follow a [brief installation snippet](https://bitbucket.org/CrankOne/goo/overview#brief-install)
to build and (unobtrusively) install a minimal version of the library.

### <a name="opt-deps"></a> Optional dependencies and features

A particular build configuration is controlled by setting following CMake flags:

Option                   | Description                                          | Requires option(s) to be enabled               | Third-party software dependencies
------------------------ | ---------------------------------------------------- | ---------------------------------------------- | ---------------------------------
`ANALYSIS_ROUTINES`      | Data processing, filtration, plotting, etc.          | `RPC_PROTOCOLS`                                | Google Protocol Buffers
`APRIME_CS_ROUTINES`     | A-prime cross-section computation routines.          | —                                              | —
`APRIME_EVENT_GENERATOR` | A-prime event generator rotuines for MC simulations  | `APRIME_CS_ROUTINES`                           | ROOT must support TFoam
`EXP_RAW_DATA_READING`   | Reading real experimental statistics.                | `RPC_PROTOCOLS`                                | Google Protocol Buffers, DaqDataDecoding
`G4_APRIME_G4_PROCESS`   | Physics module of A-physics for modular physlist.    | `APRIME_CS_ROUTINES`, `GEANT4_DYNAMIC_PHYSICS` | —
`G4_MDL_GUI`             | Geant4 GUI integration (native).                     | `GEANT4_MC_MODEL`, `G4_MDL_VIS`                | Geant4 with GUI support
`G4_MDL_VIS`             | Geant4 visualisation manager                         | `GEANT4_MC_MODEL`                              | Geant4 with interactive sessions
`GEANT4_MC_MODEL`        | MC simulation model based on Geant4 framework.       | —                                              | Geant4 (with GDML support!)
`GEANT4_DYNAMIC_PHYSICS` | Enables modular and custom physics lists support     | —                                              | —
`RPC_PROTOCOLS`          | «universal event» structure exchanging phys. data    | —                                              | Google Protocol Buffers

Note, that Geant4 must be built with GDML support (which is implemnted via Xerces library). If one need
Qt OGL viewer supported by Geant 4.10, it may be necessary to install following Qt5 modules
(since, starting from Geant4.10.5 Qt5 support is mandatory):
   - Qt-Widgets
   - Qt-OpenGl
   - Qt-PrintSupport

Enabling or disabling particular feature can be done on build by providing a CMake-option
(via `-D...=ON/OFF` key), e.g.:

    $ cmake ../../p348g4 -DGEANT4_MC_MODEL=ON

will embed Geant4 model routines in library and will make appropriate classes available for
external usage. If one is interested in particular utility the former can be enabled in similar way:

Utility      | Option               | Required options
------------ | -------------------- | ------------------------
`aprimeEvGen`| `build_apeg_util`    | `APRIME_EVENT_GENERATOR`
`mdlv`       | `build_mdlv_util`    | `G4_MDL_GUI`
`p348g4mdl`  | `build_g4_model`     | `GEANT4_MC_MODEL`
`p348a`      | `build_evreading`    | `ANALYSIS_ROUTINES`
`NA64EvD`    | `build_eventDisplay` | `ANALYSIS_ROUTINES`, `RPC_PROTOCOLS`

For example, to build \f$A'\f$-event generator, re-configure CMake with:

    $ cmake ../../p348g4 -DAPRIME_EVENT_GENERATOR=ON -DAPRIME_CS_ROUTINES=ON -Dbuild_apeg_util=ON

Or, for viewing or editing GDML geometry without any other things:

    $ cmake ../../p348g4 -DGEANT4_MC_MODEL=ON -DG4_MDL_VIS=ON -DG4_MDL_GUI=ON -Dbuild_g4_model=ON

For the description of each util, see [Utilities](#utilities) section of this document.

Note, that basically CMake caches your options and it sometimes difficult to control what exactly
it remembered, so you can append `-LH` keys in CMake invokation command to get rid of unnecessary
dependencies.

## <a name="branching-notes"></a> Branching/spin-offs

You can always ask Renat or Anton to grant writing access to this repo. It is quite preferable
to keep all the stuff in one place in actual state. Please, remember that Git as a CVS was
designed to simplify the branching/merging mechanism and it is really good on it.

## <a name="third-party"></a> Third-party snippets nodes

There are some snippets contributed from *Goo library*, that's, in order, can be found
at [corresponding page](https://bitbucket.org/CrankOne/goo). As author still haven't
implemented something up to its useful state, here we are restricted with those
snippets only.

## <a name="bugs"></a> Bugs

As was mentioned in «current status» section all the things are under development. There
are probably no features that expected to be left as it is implemented for now.

Do not hesitate to contact me (Renat) if you're faced with troubles building the package
in different environments. It's probably won't so hard to fix them.

## <a name="contacts"></a> Contacts

Please, report me about any troubles/inconviniences found to `renat.dusaev[SPAM]cern.ch` or
`crank[SPAM]qcrypt.org`.

## <a name="license"></a> License

> Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>,
>                    Bogdan Vasilishin <togetherwithra@gmail.com>,
>                    Konstantin Sharko <gecko09@outlook.com>,
>                    Ivan Kuznetsov <i.kuznetsov.tpu@gmail.com>,
>                    Dmitriy Kirpichnikov <dmbrick@gmail.com>,
>                    Mikhail Kirsanov <mikhail.kirsanov@cern.ch>,
>                    Dipanwita Banerjee <dipanwita.banerjee@cern.ch>,
>                    Gerardo Vasquez Arenas <g.vasquez@cern.ch>
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy of
> this software and associated documentation files (the "Software"), to deal in
> the Software without restriction, including without limitation the rights to
> use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
> the Software, and to permit persons to whom the Software is furnished to do so,
> subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
> FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
> COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
> IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
> CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## <a name="references"></a> References

   - [A GDML manual](http://gdml.web.cern.ch/GDML/doc/GDMLmanual.pdf).
   - [The p348 experiment porposal](http://arxiv.org/pdf/1312.3309v1.pdf).
   - [Article «New Fixed-Target Experiments to Search for Dark Gauge Forces» / 2009](http://arxiv.org/pdf/0906.0580v1.pdf)
   - [Brief thematical overview up to the state of 2013 «Study of the discovery potential for hidden photon emission at future electron scattering fixed target experiments»](http://arxiv.org/abs/1311.5104)
gives brief considerations of A'-generation used here to perform a signal events.
   - [A Google Protocol Buffers serialization library](https://developers.google.com/protocol-buffers/).
