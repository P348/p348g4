# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


          SCRIPT_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
           BUILD_DIR := $(shell pwd)
INTERIM_DATFILES_DIR := $(BUILD_DIR)/interim_data


             GNUPLOT := gnuplot
               LATEX := context
 PREPROCESING_SCRIPT := $(SCRIPT_DIR)/preprocessPoints.py --wd $(INTERIM_DATFILES_DIR)
  STATLINES_GENERATE := $(SCRIPT_DIR)/plot3SigmaRegions.py
        STATLINES_NE := -n 2.5e9

# Sergey asked me to plot 1.8e10, 2e10, 2.5e10 at 19/09/016 (or ?e9 ???)

#
# Visible mode plots
 VISIBLE_PLOT_SCRIPT := $(SCRIPT_DIR)/buildExclusionPlotVisibleMode.gpi
  DATASETS_VISIBLE_1 := $(SCRIPT_DIR)/nData/wpd_plot_data.json
  DATASETS_VISIBLE_2 := $(SCRIPT_DIR)/nData/wpd_plot_data_k2_zoom.json
     MANSPEC_VISIBLE := $(SCRIPT_DIR)/visibleMode.py
  VISIBLE_DATASETS_1 := $(shell $(PREPROCESING_SCRIPT) -i $(DATASETS_VISIBLE_1) --listDatasets )
  VISIBLE_DATASETS_2 := $(shell $(PREPROCESING_SCRIPT) -i $(DATASETS_VISIBLE_2) --listDatasets )
  VISIBLE_DATFILES_1 := $(patsubst %, $(INTERIM_DATFILES_DIR)/%.dat, $(VISIBLE_DATASETS_1))
  VISIBLE_DATFILES_2 := $(patsubst %, $(INTERIM_DATFILES_DIR)/%.dat, $(VISIBLE_DATASETS_2))
         OUT_VISIBLE := exclusionVisible

#
# Invisible mode plots
 INVISIBLE_PLOT_SCRIPT := $(SCRIPT_DIR)/buildExclusionPlotInvisibleMode.gpi
    DATASETS_INVISIBLE := $(SCRIPT_DIR)/nData/Fig19.json $(SCRIPT_DIR)/nData/E137.json
     MANSPEC_INVISIBLE := $(SCRIPT_DIR)/invisibleMode.py
    INVISIBLE_DATASETS := $(shell $(PREPROCESING_SCRIPT) -i $(DATASETS_INVISIBLE) --listDatasets )
    INVISIBLE_DATFILES := $(patsubst %, $(INTERIM_DATFILES_DIR)/%.dat, $(VISIBLE_INDATASETS))
         OUT_INVISIBLE := exclusionInvisible

all: exclusionVisible.pdf exclusionInvisible.pdf

#
# Visible mode rules

exclusionVisible.pdf: $(BUILD_DIR)/$(OUT_VISIBLE).tex
	$(LATEX) $^

$(BUILD_DIR)/$(OUT_VISIBLE).tex: visibleModeDatfiles_1.anchor visibleModeDatfiles_2.anchor $(VISIBLE_PLOT_SCRIPT)
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='$(OUT_VISIBLE)'" $(VISIBLE_PLOT_SCRIPT)

visibleModeDatfiles_1.anchor: $(DATASETS_VISIBLE_1) $(MANSPEC_VISIBLE) | $(INTERIM_DATFILES_DIR)
	$(PREPROCESING_SCRIPT) $(foreach src,$(DATASETS_VISIBLE_1),--jsonSource $(src)) --noplot --manualSpec $(MANSPEC_VISIBLE)
	@touch $@

visibleModeDatfiles_2.anchor: $(DATASETS_VISIBLE_2) $(MANSPEC_VISIBLE) | $(INTERIM_DATFILES_DIR)
	$(PREPROCESING_SCRIPT) $(foreach src,$(DATASETS_VISIBLE_2),--jsonSource $(src)) --noplot --manualSpec $(MANSPEC_VISIBLE)
	@touch $@

#
# Invisible mode rules
exclusionInvisible.pdf: $(BUILD_DIR)/$(OUT_INVISIBLE).tex
	$(LATEX) $^

$(BUILD_DIR)/$(OUT_INVISIBLE).tex: invisibleModeDatfiles.anchor $(INVISIBLE_PLOT_SCRIPT) $(INTERIM_DATFILES_DIR)/3sigma.dat
	$(GNUPLOT) -e "datDir='$(INTERIM_DATFILES_DIR)';outFile='$(OUT_INVISIBLE)'" $(INVISIBLE_PLOT_SCRIPT)

invisibleModeDatfiles.anchor: $(DATASETS_INVISIBLE) $(MANSPEC_INVISIBLE) | $(INTERIM_DATFILES_DIR)
	$(PREPROCESING_SCRIPT) $(foreach src,$(DATASETS_INVISIBLE),--jsonSource $(src)) --noplot --manualSpec $(MANSPEC_INVISIBLE)
	@touch $@

$(INTERIM_DATFILES_DIR)/3sigma.dat: $(STATLINES_GENERATE) | $(INTERIM_DATFILES_DIR)
	$(STATLINES_GENERATE) $(STATLINES_NE) -o $@

#
# Interim data files directory
$(INTERIM_DATFILES_DIR):
	mkdir -p $(INTERIM_DATFILES_DIR)

clean:
	rm -rf $(INTERIM_DATFILES_DIR)
	rm -f visibleModeDatfiles_1.anchor
	rm -f visibleModeDatfiles_2.anchor
	rm -f invisibleModeDatfiles.anchor

.PHONY: all

#$(INTERIM_DATFILES_DIR)/*: $(SCRIPT_DIR)/nData/wpd_plot_data.json $(SCRIPT_DIR)/nData/wpd_plot_data_k2_zoom.json

