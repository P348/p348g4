# Exclusion plotting scripts

This directory contains quite important plotting data depicting the stage
where P348 experiment should act.

The exclusion plot is built in coordinates of supposed A' mass and mixing
parameter. Filled regions depicts the already covered areas (by different
experiments) and the lines depicts those that should be provided in near
future.

For particular details see comments and plot legend.

The `nData/` dir contains actual data.


## TODO

Please, keep it refreshed as new data outcomes.

