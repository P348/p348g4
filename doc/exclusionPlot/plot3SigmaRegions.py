#!/usr/bin/env python3

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from math import sqrt, log
from scipy import interpolate as interMod
import argparse, sys

# from nData/table1_III.txt
tableData = [
    [0.002,      1.3e-9 ],
    [0.005,        4e-10],
    [0.01 ,      1.5e-10],
    [0.02 ,        5e-11],
    [0.03 ,     2.58e-11],
    [0.05 ,      1.1e-11],
    [0.07 ,        5e-12],
    [0.2  ,     5.86e-13],
    [0.5  ,     6.76e-14]
]
eps0dft = 1e-4


def epsilonAt3sigma( eps0, probability, Ne ):
    return sqrt( (2*sqrt(2*log(2))*eps0*eps0)/(Ne*probability) )

def pts_for_ne( Nes, f ):
    f.write( '#me ' + "  ".join(map(str, Nes)) + '\n' )
    for mpPair in tableData:
        ma = mpPair[0]
        p = mpPair[1]
        eps = []
        for Ne in Nes:
            eps.append( epsilonAt3sigma( eps0dft, p, Ne ) )
        f.write( '%e '%ma + " ".join(map(str, eps)) + '\n' )

def extrapolated_pts_for_ne( Nes, outFile ):
    #f.write( '#me ' + "  ".join(map(str, Nes)) + '\n' )
    pts = list([])
    for Ne in Nes:
        interpolatedF = interMod.UnivariateSpline(
                            list(map(lambda p:                           p[0],        tableData)),
                            list(map(lambda p: epsilonAt3sigma( eps0dft, p[1], Ne ) , tableData)),
                            s=.1)
        k = 0
        for i in range(-3, 1):
            for j in range(1, 10):
                mass = j*10**i
                eps = float(interpolatedF(mass))
                if len(pts) > k:
                    pts[k].append( eps )
                else:
                    pts.append(list([mass, eps]))
                k += 1
    for line in pts:
        for val in line:
            outFile.write('%.3e\t'%val)
        outFile.write('\n')



if "__main__" == __name__:
    parser = argparse.ArgumentParser(description="Expected precision statistics plot.")
    parser.add_argument('-n', '--nIncedentElectrons',
                        type=float,
                        help="Order of number of incident electrons (usually 1e8 ... 1e12 => 9 ... 12)",
                        action='append',
                        nargs='+' )
    parser.add_argument('--plot', action='store_true',
                        help="Launch simple gnuplot script in X11-terminal to check out results.")
    parser.add_argument('-o', '--outFile',
                    type=argparse.FileType('w'),
                    nargs='?',
                    default=sys.stdout,
                    help='File where to write oputput.')
    args = parser.parse_args()
    #pts_for_ne( [ pow(10, x[0]) for x in args.nIncedentElectrons], args.outFile )
    extrapolated_pts_for_ne( [ x[0] for x in args.nIncedentElectrons], args.outFile )
    #if args.plot:
    #    plotStrPrefixUnfmtd = "'%s' u 1:%d"
    #    for Ne in 

