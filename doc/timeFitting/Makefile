# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

          SCRIPT_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
           BUILD_DIR := $(shell pwd)
INTERIM_DATFILES_DIR := $(BUILD_DIR)/interim_data
             SRC_DIR := $(abspath "$(SCRIPT_DIR)/../src/analysis/" )

                  CC := g++ -x c
              CFLAGS := -g -Wall -DSTANDALONE_BUILD -I../../p348g4/inc/ -lm -lgsl
                SRCS := ../../p348g4/src/analysis/sadcWF_supp.c ../../p348g4/src/analysis/sadcWF_fit.c
             GNUPLOT := gnuplot
               LATEX := context
   FITTIMEPLOTSCRIPT := $(SCRIPT_DIR)/plot_f.gpi

            NSAMPLES := 0 1 2 3 4

all: timeFittingTest.pdf

timeFittingTest.pdf : timePlots.pdf.anchor
	pdfunite $(BUILD_DIR)/timePlot0.pdf $(BUILD_DIR)/timePlot1.pdf $(BUILD_DIR)/timePlot2.pdf $(BUILD_DIR)/timePlot3.pdf $@

timePlots.pdf.anchor : timePlots.anchor
	$(LATEX) $(BUILD_DIR)/timePlot0.tex
	$(LATEX) $(BUILD_DIR)/timePlot1.tex
	$(LATEX) $(BUILD_DIR)/timePlot2.tex
	$(LATEX) $(BUILD_DIR)/timePlot3.tex

timePlots.anchor : timePlotsDat.anchor
	$(GNUPLOT) -e "suppFile='$(INTERIM_DATFILES_DIR)/timeSuppFitting_0.dat';fitFile='$(INTERIM_DATFILES_DIR)/timeFitFitting_0.dat';outFile='$(BUILD_DIR)/timePlot0'" $(FITTIMEPLOTSCRIPT)
	$(GNUPLOT) -e "suppFile='$(INTERIM_DATFILES_DIR)/timeSuppFitting_1.dat';fitFile='$(INTERIM_DATFILES_DIR)/timeFitFitting_1.dat';outFile='$(BUILD_DIR)/timePlot1'" $(FITTIMEPLOTSCRIPT)
	$(GNUPLOT) -e "suppFile='$(INTERIM_DATFILES_DIR)/timeSuppFitting_2.dat';fitFile='$(INTERIM_DATFILES_DIR)/timeFitFitting_2.dat';outFile='$(BUILD_DIR)/timePlot2'" $(FITTIMEPLOTSCRIPT)
	$(GNUPLOT) -e "suppFile='$(INTERIM_DATFILES_DIR)/timeSuppFitting_3.dat';fitFile='$(INTERIM_DATFILES_DIR)/timeFitFitting_3.dat';outFile='$(BUILD_DIR)/timePlot3'" $(FITTIMEPLOTSCRIPT)

timePlotsDat.anchor : $(BUILD_DIR)/fitting_tests | $(INTERIM_DATFILES_DIR)
	$(BUILD_DIR)/fitting_tests 0 $(INTERIM_DATFILES_DIR)/timeSuppFitting_0.dat $(INTERIM_DATFILES_DIR)/timeFitFitting_0.dat
	$(BUILD_DIR)/fitting_tests 1 $(INTERIM_DATFILES_DIR)/timeSuppFitting_1.dat $(INTERIM_DATFILES_DIR)/timeFitFitting_1.dat
	$(BUILD_DIR)/fitting_tests 2 $(INTERIM_DATFILES_DIR)/timeSuppFitting_2.dat $(INTERIM_DATFILES_DIR)/timeFitFitting_2.dat
	$(BUILD_DIR)/fitting_tests 3 $(INTERIM_DATFILES_DIR)/timeSuppFitting_3.dat $(INTERIM_DATFILES_DIR)/timeFitFitting_3.dat
	touch $@

$(BUILD_DIR)/fitting_tests : $(SRCS)
	$(CC) $(CFLAGS) $(SRCS) -o $@

#
# Interim data files directory
$(INTERIM_DATFILES_DIR):
	mkdir -p $(INTERIM_DATFILES_DIR)

