/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348G4_ANALYSIS_STATIC_CONTRAINTS_H
# define H_P348G4_ANALYSIS_STATIC_CONTRAINTS_H

# include <stdlib.h>

// Note: this names are defined in <conddb.h> header of p348-daq/p348reco/.
// If this section will become too large, it will make sense to rely into
// this code.
# define MM_size 320
# define APV_NSamples 3

# include "p348g4_detector_ids.h"

# ifdef __cplusplus
extern "C" {
# endif

const APVStripNo * p348g4_APV_strip_mapper__micromegas_joints( APVWireNo, size_t * );
const APVStripNo * p348g4_APV_strip_mapper__identity( APVWireNo, size_t * );
const APVStripNo * p348g4_APV_strip_mapper__GEM_demultiplexing_scheme( APVWireNo wn, size_t * lPtr );

# ifdef __cplusplus
}  // extern "C"
# endif

# endif  /* H_P348G4_ANALYSIS_STATIC_CONTRAINTS_H */

