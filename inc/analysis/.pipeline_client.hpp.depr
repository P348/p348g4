/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348G4_ANALYSIS_EVENT_PIPELINE_CLIENTS_H
# define H_P348G4_ANALYSIS_EVENT_PIPELINE_CLIENTS_H

# include "p348g4_config.h"

# if defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

# include <boost/array.hpp>
# include <boost/asio.hpp>
# include <boost/thread.hpp>

//using boost::asio::ip::udp;

namespace p348 {
namespace net {

class EventPipelineUDPClient {
private:
    boost::asio::io_service _ioService;
    boost::asio::ip::udp::socket * _socketPtr;
    boost::thread * _threadPtr;
    boost::asio::ip::udp::udp::endpoint _recvEndpoint;

    static void _static_fetcher( boost::asio::ip::udp::socket *,
                                 boost::asio::ip::udp::endpoint * );
public:
    EventPipelineUDPClient( const std::string & hostname );
    void start_fetching();
};  // class EventPipelineClient

}  // namespace net
}  // namespace p348

# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)
# endif  // H_P348G4_ANALYSIS_EVENT_PIPELINE_CLIENTS_H

