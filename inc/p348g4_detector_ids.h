/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**@file p348g4_detector_ids.h
 * @brief The file providing detector identification table and aux routines.
 *
 * This file defines a x-macro \ref for_all_detectors which defines a designations
 * of detectors used in experiment.
 *
 * There are detector numbers provided with DaqDataDecoding library detector
 * digest mapping mechanisms, which is, however, inconvinient to use in
 * high-level applications due to flat design of those identifiers.
 *
 * Here the union UniqueDetectorID is introduced in order to perform transparent
 * indexing of detector subsystem.
 */

# ifndef H_P348G4_INTERNAL_DETECTOR_IDENTIFIER_H
# define H_P348G4_INTERNAL_DETECTOR_IDENTIFIER_H

# include "goo_types.h"

# include <stdio.h>

/**\brief Detector major section identifier type.*/
typedef UShort  DetectorMajor;
/**\brief Detector minor section identifier type.*/
typedef UShort  DetectorMinor;
/**\brief Detector entire number containing hashable type. */
typedef UInt    DetectorSignature;
/**\brief Detector family identifier hashable type. */
typedef UShort  DetectorFamilyID;

/**\brief Strip number type for APV detectors. */
typedef unsigned short APVStripNo;
/**\brief Wire number type for APV detectors. */
typedef unsigned short APVWireNo;

/**\brief Mapping function type, for certain APV detector and wire number
 * returning strip number(s)
 *
 * For disjoint readouts, returning pointer will refer to single value. For
 * joint ones, the pointer returned refers to array which length is written
 * in value pointed by last argument.
 *
 * Last argument can be NULL. In this case length won't be written there.
 * */
typedef const APVStripNo * (*APVMapper)( APVWireNo, size_t * );

# define P38G4_MJ_DETID_OFFSET 4
/** Should expel last bits after shifting major number << P38G4_MJ_DETID_OFFSET contains
 * family number */
# define P348G4_DETID_FAMILY_MASK 0xf

/**\union UniqueDetectorID
 * \brief Unites unque detector identifier in terms of p348g4 library.
 *
 * Combines two-byte-long uniq number and two-byte number
 * representation as more descriptive field. The wholenum field
 * can be used as key value in associative containers like maps/sets/etc.
 * */
union UniqueDetectorID {
    struct {
        /**\brief Major number defines a detector digest in terms of custom code.
         * Basically, it is defined as unique ordering number
         * written in first six bits. For some detectors, like
         * ECAL or HCAL, the last two bits is used for submodule
         * definition. */
        DetectorMajor major;
        /**\brief The minor number is used to encode x/y coordinates for
         * detectors which support it. There are no dedicated fields. */
        DetectorMinor minor;
    } byNumber;
    /**\brief Number congregating full detector identification. */
    DetectorSignature wholenum;
    # ifdef __cplusplus
    UniqueDetectorID( DetectorSignature num ) : wholenum(num) {}
    # endif
};


/**@def for_all_detector_families
 * @brief Detectors major features table.
 *
 * Note, that all the features should be bitwise-shifted --- lower
 * bytes are reserved for family numbers.
 * */
# define for_all_detector_features( m )                     \
    /* ft. name|ft code|feature description */              \
    m( Subd,    0x10,   "for SADC: has transversal segmentation"  )   \
    m( Joint,   0x10,   "for APV: has joint wires"      )   \
    m( SADC,    0x20,   "is served by SADC"             )   \
    m( APV,     0x40,   "is served by APV"              )   \
    m( XYSep,   0x80,   "X/Y planes are separated"      )   \
    m( X,       0x100,  "X-plane, when X/Y are separated" ) \
    m( Y,       0x80,   "Y-plane, when X/Y are separated" ) \
    /* ... */

/**@def for_all_detector_families
 * @brief Detectors categories (families) table.
 *
 * Note, that these numbers stored in major section of unique
 * detectorID as numbers shifted by 3. Should be below 0x10 value,
 * as >=0x10 corresponds to features bitflags.
 * */
# define for_all_detector_families( m )                     \
    /* label    ,  custom code      , fam name */           \
    m( MISC     ,  0x0              ,  "Miscellaneous"  )   \
    m( BGO      ,  0x1              ,  "BGO"            )   \
    m( ECAL     ,  0x2              ,  "ECAL"           )   \
    m( HCAL     ,  0x3              ,  "HCAL"           )   \
    m( HOD      ,  0x4              ,  "Hodoscopes"     )   \
    m( MUON     ,  0x5              ,  "MuonCtrs"       )   \
    m( MuMega   ,  0x6              ,  "Micromegas"     )   \
    m( GEM      ,  0x7              ,  "GEMs"           )   \
    /*m( StrawTubs,  0xa              ,  "Straw Tubes")*/   \
    /* ... */

# if 1

# define for_all_SADC_detectors( m ) \
    m( BGO      ,(((fam_BGO  | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x6  ) \
    /* ECAL family */ \
    m( ECAL0    ,(((fam_ECAL | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x1) , 0xc  ) \
    m( ECAL1    ,(((fam_ECAL | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x2) , 0xca ) \
    m( ECALSUM  ,(((fam_ECAL | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x3) , 0x2d ) \
    /* HCAL family */ \
    m( HCAL0    ,(((fam_HCAL | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x0) , 0xcc ) \
    m( HCAL1    ,(((fam_HCAL | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x1) , 0xcd ) \
    m( HCAL2    ,(((fam_HCAL | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x2) , 0xce ) \
    m( HCAL3    ,(((fam_HCAL | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x3) , 0xcf ) \
    /* Hodoscopes family */ \
    m( HOD0X    ,(((fam_HOD  | f_SADC | f_Subd | f_XYSep | f_X )    << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x2  ) \
    m( HOD0Y    ,(((fam_HOD  | f_SADC | f_Subd | f_XYSep | f_Y )    << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x3  ) \
    m( HOD1X    ,(((fam_HOD  | f_SADC | f_Subd | f_XYSep | f_X )    << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x4  ) \
    m( HOD1Y    ,(((fam_HOD  | f_SADC | f_Subd | f_XYSep | f_Y )    << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x5  ) \
    /* Muon counters */ \
    m( MUON0    ,(((fam_MUON | f_SADC )                             << P38G4_MJ_DETID_OFFSET) | 0x0) , 0x29 ) \
    m( MUON1    ,(((fam_MUON | f_SADC )                             << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x2a ) \
    m( MUON2    ,(((fam_MUON | f_SADC )                             << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x2b ) \
    m( MUON3    ,(((fam_MUON | f_SADC )                             << P38G4_MJ_DETID_OFFSET) | 0x3) , 0x2c ) \
    /* Miscellaneous */ \
    m( ATARG    ,(((0x0      | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x0) , 0x7  ) \
    m( TRIG     ,(((0x0      | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x0  ) \
    m( VETO     ,(((0x0      | f_SADC | f_Subd )                    << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x24 ) \
    /* ... */

# define for_all_APV_detectors( m ) \
    /* Micromegas */ \
    m( MM01X    ,(((fam_MuMega | f_APV| f_Joint | f_XYSep | f_X )   << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x20 ) \
    m( MM01Y    ,(((fam_MuMega | f_APV| f_Joint | f_XYSep | f_Y )   << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x21 ) \
    m( MM02X    ,(((fam_MuMega | f_APV| f_Joint | f_XYSep | f_X )   << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x22 ) \
    m( MM02Y    ,(((fam_MuMega | f_APV| f_Joint | f_XYSep | f_Y )   << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x23 ) \
    m( MM03X    ,(((fam_MuMega | f_APV| f_Joint | f_XYSep | f_X )   << P38G4_MJ_DETID_OFFSET) | 0x3) , 0x1c ) \
    m( MM03Y    ,(((fam_MuMega | f_APV| f_Joint | f_XYSep | f_Y )   << P38G4_MJ_DETID_OFFSET) | 0x3) , 0x1d ) \
    m( MM04X    ,(((fam_MuMega | f_APV| f_Joint | f_XYSep | f_X )   << P38G4_MJ_DETID_OFFSET) | 0x4) , 0x1e ) \
    m( MM04Y    ,(((fam_MuMega | f_APV| f_Joint | f_XYSep | f_Y )   << P38G4_MJ_DETID_OFFSET) | 0x4) , 0x1f ) \
    /* GEMs (TODO: DDD codes)  */ \
    m( GM01X1__ ,((( fam_GEM | f_APV | f_XYSep | f_X )              << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x8  ) \
    m( GM01Y1__ ,((( fam_GEM | f_APV | f_XYSep | f_Y )              << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x9  ) \
    m( GM02X1__ ,((( fam_GEM | f_APV | f_XYSep | f_X )              << P38G4_MJ_DETID_OFFSET) | 0x2) , 0xa  ) \
    m( GM02Y1__ ,((( fam_GEM | f_APV | f_XYSep | f_Y )              << P38G4_MJ_DETID_OFFSET) | 0x2) , 0xb  ) \
    /* ... */

/**@def for_all_detectors
 * @brief Detectors enumeration table.
 *
 * This table indexes major detector IDs specific for each detector. Additional
 * detector indexing (like x,y in DDD) is provided in minor number.
 * See UniqueDetectorID
 *
 * Note, that native code is usually been corrected by mapping file, so native
 * DDD-code provided here are just some default ones.
 * */
# define for_all_detectors( m )                                                             \
    /* label    ,  custom code                                                  , ddd code */      \
    m( unknown  ,   0x0                                                          , -1   )   \
    for_all_SADC_detectors( m )                                                             \
    for_all_APV_detectors( m )                                                              \
    /* m( ST01Y    ,                   ) */         \
    /* m( ST02X    ,                   ) */         \
    /* m( ST02Y    ,                   ) */         \
    /* m( ST03X1   ,                   ) */         \
    /* m( ST03X2   ,                   ) */         \
    /* m( ST03X3   ,                   ) */         \
    /* m( ST03Y1   ,                   ) */         \
    /* m( ST04X1   ,                   ) */         \
    /* m( ST04X2   ,                   ) */         \
    /* m( ST04Y1   ,                   ) */         \
    /* ... */
# else

# endif
/**@def DETID_SCOPE_QUALIFIER
 * An C/C++ variable macro resolving the enum prefix. */
# ifdef __cplusplus
#   define DETID_SCOPE_QUALIFIER EnumScope::
extern "C" {
# else
#   define DETID_SCOPE_QUALIFIER enum
# endif

/**@struct EnumScope
 * @brief C-enum scope
 *
 * This struct must never be instantiated. It provides a namespace-like
 * scope for C++-code.
 * Since C doesn't support scoped enumname resolution,
 * this struct does not affect to C code.
 */
struct EnumScope {
    /**@enum DetectorFeatures
     * @brief Bitflag describing features upported by certain detector. */
    enum DetectorFeatures {
        # define EX_declare_feature_alias( name, code, description ) \
        f_ ## name = (code),
        for_all_detector_features( EX_declare_feature_alias )
        # undef EX_declare_feature_alias
    } _unused1;
    /**@enum DetectorFeatures
     * @brief Bitflag describing category by which certain detector is affilated. */
    enum ForAllDetectorFamilies {
        # define EX_declare_family_alias( name, code, verbName ) \
        fam_ ## name = (code),
        for_all_detector_families( EX_declare_family_alias )
        # undef EX_declare_family_alias
    } _unused2;
    /**@enum MajorDetectorsCode
     * @brief A major detector encoding enumeration.
     *
     * This enum is used to uniquely define a detector family
     * in terms of p348g4-library. */
    enum MajorDetectorsCode {
        # define EX_declare_alias( name, code, dddCode ) \
        d_ ## name = (code),
        for_all_detectors( EX_declare_alias )
        # undef EX_declare_alias
    } _unused3;
    /**@enum DDD_DetectorsCode
     * @brief DaqDataDecoding library detector digest codes.
     *
     * This enum's values originally comes from DaqDataDecoding
     * library detector digest encoding and used for mapping from
     * DDD detector codes into p348g4 detector codes.
     *
     * One can perform translation via compose_cell_identifier()
     * function. */
    enum DDD_DetectorsCode {
        # define EX_declare_alias( name, code, dddCode ) \
        ddd_ ## name = (dddCode),
        for_all_detectors( EX_declare_alias )
        # undef EX_declare_alias
    } _unused4;
};  /* struct EnumScope */

/** Corrects internal mapping table according to provided
 * information. */
char resolve_major_detector_number_mapping( const char *, unsigned );

/** Produces a UniqueDetectorID based on this custom code and x/y-subindexes.
 * The formers can be unused for not-subdivided detectors like veto
 * counters. */
DetectorSignature compose_cell_identifier( DetectorMajor major, uint8_t xIdx, uint8_t yIdx );
/** Extract index along X axis (came from DDD) from detector identification number. */
uint8_t detector_get_x_idx( union UniqueDetectorID );
/** Extract index along Y axis (came from DDD) from detector identification number. */
uint8_t detector_get_y_idx( union UniqueDetectorID );

/** For particular detectors inside families, return subgroup number
 * (e.g. N from HCALN or HCALN -- ECAL0, ECAL1, HCAL3, etc). */
uint8_t detector_subgroup_num( DetectorMajor major );

# define declare_flag_checking_function(name, code, description) \
uint8_t detector_is_ ## name( DETID_SCOPE_QUALIFIER MajorDetectorsCode );
for_all_detector_features( declare_flag_checking_function )
# undef declare_flag_checking_function

/** Returns textual label for detector by its p348g4-encoded major ID. */
const char * detector_name_by_code( DETID_SCOPE_QUALIFIER MajorDetectorsCode );
/** Returns textual label for detector by its DaqDecodingLibrary ID. */
const char * ddd_detector_name_by_code( DETID_SCOPE_QUALIFIER DDD_DetectorsCode );
/** Writes full unique detector label (Like ECAL0-1-2) into given buffer. */
char * snprintf_detector_name( char * buffer, size_t bufferSize, union UniqueDetectorID );
/** Returns detector family identifier number. */
DetectorFamilyID detector_family_num( DetectorMajor major );
/** Returns detector family name (BGO/ECAL/HCAL/Hodoscopes) --- useful in
 * category structures. */
const char * detector_family_name( DetectorFamilyID major );
/** Returns detector major number by name. */
DetectorMajor detector_major_by_name( const char * );
/** Returns X-detector ID for given Y-detector ID
 * (useful for detectors with X/Y decomposition) */
DetectorSignature detector_complementary_X_id( DetectorSignature );
/** Returns Y-detector ID for given X-detector ID
 * (useful for detectors with X/Y decomposition) */
DetectorSignature detector_complementary_Y_id( DetectorSignature );


/** Converts DDD detector encoding number to p348g4's. */
DETID_SCOPE_QUALIFIER MajorDetectorsCode det_code_ddd_to_p348( DETID_SCOPE_QUALIFIER DDD_DetectorsCode );

/** Prints formatted mapping table in file provided by C-descriptor. */
void dump_mapping_table( FILE * );

/** Auxilliary function to test all routines provided in
 * \ref p348g4_detector_ids.h */
int test_detector_table( FILE * );

/** Returns mapper specific for particular detector. */
APVMapper apv_get_mapper_for( union UniqueDetectorID );

# ifdef __cplusplus
}  /* extern "C" */
# endif

# endif  /* H_P348G4_INTERNAL_DETECTOR_IDENTIFIER_H */

