/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_G4_P348_EXTRAS_H
# define H_G4_P348_EXTRAS_H

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL

# include <Geant4/G4UImessenger.hh>

class G4UIdirectory;
class G4UIcmdWithoutParameter;

namespace p348 {
namespace extras {

/// Visibility styles. See GDMLAux/gdml_aux_visStyles.cpp for
/// implementation details.
void apply_styles_selector( const std::string & );

//
// This projects extras.
class ExtrasMessenger : public G4UImessenger {
public:
    ExtrasMessenger();
    ~ExtrasMessenger();
public:
    void SetNewValue(G4UIcommand * command, G4String newValues);
    G4String GetCurrentValue(G4UIcommand * command);
private: //commands
    G4UIdirectory             * _extrasDirectory;
    G4UIcmdWithoutParameter   * _renewStylesCmd;

};

}  // namespace extras
}  // namespace p348

# endif // GEANT4_MC_MODEL

# endif  // H_G4_P348_EXTRAS_H

