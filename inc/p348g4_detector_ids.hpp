/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348G4_DETECTOR_IDENTIFIERS_MAPPER_CLASS_H
# define H_P348G4_DETECTOR_IDENTIFIERS_MAPPER_CLASS_H

# include "p348g4_config.h"

# include "app/analysis.hpp"
# include "app/app.h"
# include "analysis/apvConstraints.h"
# include "analysis/xform.tcc"
# include "p348g4_detector_ids.h"

# include <unordered_map>

namespace p348 {
namespace aux {

/**@class DetectorMapping
 * @brief Class maintaining all the detectors numerical ID mappings.
 *
 * This singleton keeps information about major mapping schemes between
 * numerical detector IDs (in terms of CORAL/DDD) and p348g4 library,
 * names, APV strips mapping, etc. std::unordered_map is fastest
 * containers when it comes to lookup.
 *
 * todo: Since here we have a lot of repeated data implementing look-up
 * procedures by variaous criteria, one need consider using of boost::multiindex
 * container or (and?) even connection with database(s) used in experiment.
 * todo: Currently, only planar squares is supported for APV mapping.
 * */
class DetectorMapping {
public:
    enum CorrectionResult : char {
        CORRECTION_NO_PREDEFINED = -1,
        CORRECTION_KEPT = 0,
        CORRECTION_SIMPLE = 1,
        CORRECTION_SWAPPED = 2,
    };
    typedef aux::XForm<2, float> APVBoundaries;
private:
    static DetectorMapping * _self;
    /// Dynamic info --- can be updated with correct_entry().
    std::unordered_map<std::string, unsigned>               _nameToDDDCode;
    /// Dynamic info --- can be updated with correct_entry().
    std::unordered_map<unsigned, DetectorMajor>             _dddCodeToMajor;

    /// Static info --- can not be corrected.
    std::unordered_map<std::string, DetectorMajor>          _nameToMajor;
    /// Static info --- can not be corrected.
    std::unordered_map<DetectorMajor, std::string>          _majorToName;

    std::unordered_map<unsigned, std::string>               _unmapped;


    std::unordered_map<DetectorMajor, APVMapper> _apvStripMappers;
    std::unordered_map<APVMapper, APVBoundaries> _apvBoundaries;

    size_t _correctionCnt_kept,
           _correctionCnt_simple,
           _correctionCnt_swapped,
           _correctionCnt_unknown
        ;
    /// Internal aux method --- raises nonUniq, when first argument is false.
    void _assert_id_collision( bool, uint8_t, const char * );

    DetectorMapping();
public:

    /// Singleton instance ref acquizition.
    static DetectorMapping & self() {
        if( !_self ) { _self = new DetectorMapping(); }
        return *_self;
    }

    /// Corrects mapping to DDD (CORAL) entries.
    char correct_entry( const char *, unsigned );

    /// Returns major code by name string.
    EnumScope::MajorDetectorsCode major_by_name( const char * ) const; 

    /// Returns major code by its DDD code.
    EnumScope::MajorDetectorsCode major( EnumScope::DDD_DetectorsCode ) const;

    /// Returns name string by its major code.
    const char * name_by_major( EnumScope::MajorDetectorsCode ) const;

    /// (lib) Associates APV mapping function automatically
    /// obtaining its boundaries (for norming).
    void register_apv_mapper( const std::string &, APVMapper );

    /// Returns associated mapping function.
    APVMapper apv_strip_mapper_for( DetectorSignature ) const;

    /// Prints detector table to provided stream instance.
    void dump_table( std::ostream & );

    /// Returns appropriate XForm for APV referenced by its detectors major
    /// number.
    const APVBoundaries & apv_boundaries( EnumScope::MajorDetectorsCode ) const;
};  // class DetectorMapping

}  // namespace aux
}  // namespace p348


# endif  // H_P348G4_DETECTOR_IDENTIFIERS_MAPPER_CLASS_H

