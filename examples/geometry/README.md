## Running the template processor to perform TGDML -> GDML rendering

The `buildUpGeometry.py` script has a usage snippet at the `__main__` block performing
the template processing of `.tgdml` template files to true GDML format:

    $ buildUpGeometry.py --templates-dir=../../p348g4/assets/gdml/ \
                         --output-dir=./geometry.gdml/ \
                         ../../p348g4/assets/gdml/00_root.tgdml

It will create then the follwoing structure (depending on placement layout information):

    $ tree ./geometry.gdml/
    ./geometry.gdml/
    ├── 00_root.gdml
    ├── BGO
    │   └── BGO.gdml
    ├── ECAL
    │   └── ecal_segm_nopaper.gdml
    └── hodoscope
        └── hodoscope.gdml

that further can be parsed by Geant4. Note that the default ROOT's GDML
parser has quite a restricted support of GDML, so can not be directly
used to that geometry.

## Setting up a virtual environment

Python has a virtual environment (venv) conceptions allowing one too keep the
host system clear from the variaous third-party software. It becomes extremely
important considering the CERN's lxplus structure.

Setting up virtual environment:

    $ virtualenv --no-site-packages -ppython2.7 --prompt="(venv)\n" ./venv

It may be quite convinient for future usage to turn it then to relocatable
environment:

    $ virtualenv --relocatable ./venv

One can then activate venv:

    $ . ./venv/bin/activate

The active virtual environment session will be then indicated in console by
`(venv)` prefix in currently active console. To install all required packages
one can simply feed `requirements.txt` file to `pip` util under virtual
environment:

    (venv)
    $ pip install -r ../../p348g4/requirements.txt

To deactivate it, just enter the `deactivate` command.

# Writing the code

## Compatibility 2 vs 3

Since ROOT still has no bindings with Python 3 we are using a style compatible
with both versions of python interpreter. Please, consult with
[recommendations article](http://python-future.org/compatible_idioms.html)
describing how to write such code on Python.

