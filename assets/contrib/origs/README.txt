DO NOT re-distribute content of this directory using public sources. We have
no licensing and re-distribution permissions for all the content of this
directory.

This directory contains contributed third-party codes that is not maintained
via any VCS by its authors or aren't available at public sources.

Just keepeing them here as we got it.

aprimeDK.c  --  is original Dmitry Kirpichnikov's A'-production event generator
                written following the http://arxiv.org/abs/0906.0580 article.
Micromegas_from_Di
            --  reconstruction code and geometry descriptions for Micromegas
            detectors originally submitted by Dipanwita Banerjee, June 2016.

LYSO_from_Gerardo
            -- geometrical descriptions of LYSO detector kindly submitted by
            Gerardo Vasquez, June 2016.

