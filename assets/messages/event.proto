syntax = "proto3";
/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package p348.events;

// Common
////////

message UniformHistogram {
    string label = 1;

    message Axis {
        double min = 1;
        double max = 3;
        uint32 nbins = 5;
        string name = 7;
    }

    repeated Axis axes      = 7;
    repeated double bins    = 9 [packed=true];
}

message PMTStatistics {
    uint32 detector_id = 1;
    UniformHistogram time_profile = 2;
    double edep = 3;
    double resp = 4;
    // ...
}

// Event display
///////////////

//message EventPipelineSettings {
//    bool persistant = 1;
//    uint64 stackDepth = 3;
//    // ...
//}

message EventDisplayMessage {
    repeated DetectorSummary summary = 1;
}

message DetectorSummary {
    uint32 detectorID = 1;
    oneof payload {
        EnergyDeposition eDep = 3;
        ClustersArray points = 5;
        DetectorsGroup subGroup = 7;
    }
}

message DetectorsGroup {
    repeated DetectorSummary summary = 3;
    EnergyDeposition maxDep = 5;
}

message TrackPoint {
    float x = 1;
    float y = 3;
    float amplitude = 5;
}

message EnergyDeposition {
    float amplitude = 1;
}

// Simulation
////////////

message SimulatedEvent {
    repeated PMTStatistics pmt_stats = 1;
    // ...
}


// Real
//////

message SADC_suppInfo {
    // Calculated by sadcWFAlign processor
    bool badZero1 = 1;
    bool badZero2 = 2;
    SADC_ModelParameters fittingModel = 10;
    float pedestal1 = 21;
    float pedestal2 = 22;
    // Calculated by sadcGetSuppInfo processor
    int32 absMaxNBin = 24;
    float absMaxVal = 26;
    float mean = 28;
    float weightedMean = 30;
    float mpv = 31;
    float sum = 32;
    float stdDeviation = 34;
    float reliability = 36;
    float timeDispersion = 38;
}

// Calculated by sadcFit processor
message SADC_ModelParameters {
    message MoyalModelParameters {
        float p1 = 1;
        float p2 = 2;
        float p3 = 3;
    }
    MoyalModelParameters moyal = 1;
}

message SADC_profile {
    uint32 detectorID = 1;
    repeated float samples = 2;
    // TODO: computed part that shouldn't be found
    /// in source data.
    SADC_suppInfo suppInfo = 5;
}

message APV_rawData {
    uint32 wireno = 1;
    //bool isJoint = 2;  // true for Micromegas raw APV digits
    repeated float amplitudeSamples = 3;
}

message APV_Cluster {
    TrackPoint trackPoint = 1;
    float xyRatio = 3;
    // FMT: { (a_{x0}/a_{x2}), (a_{y0}/a_{y2}),
    //        (a_{x1}/a_{x2}), (a_{y1}/a_{y2}) }
    repeated float ratios = 5;
}

message ClustersArray {
    repeated APV_Cluster clusters = 1;
    // ...
}

message APV_sampleSet {
    uint32 detectorID = 1;
    repeated APV_rawData rawData = 3;

    // TODO: computed part that shouldn't be found
    /// in source data.
    ClustersArray clusters = 5;
    EnergyDeposition maxDep = 7;
}

message ExperimentalEvent {
    bool is_physical = 1;
    enum NonPhysEventType {
        unknown         = 0;
        RunStart        = 1;
        RunEnd          = 2;
        RunFilesStart   = 3;
        RunFilesEnd     = 4;
        BurstStart      = 5;
        BurstEnd        = 6;
        Calibration     = 7;
        FormatError     = 8;
    }
    repeated SADC_profile sadc_data = 5;
    repeated APV_sampleSet apv_data = 7;
    NonPhysEventType nonPhysEventType = 9;


    uint32 spillNo = 10;
    uint32 runNo = 13;
    uint32 evNoInRun = 15;
    uint32 evNoInSpill = 17;
    uint32 triggerNo = 19;
    uint32 errorCode = 21;
    uint32 trigger = 23;
    bytes timeStruct = 25;
    uint32 timeNum = 27;
}

// Event
///////

message Event {
    oneof uevent {
        SimulatedEvent simulated = 1;
        ExperimentalEvent experimental = 5;
    }
    EventDisplayMessage eventDisplayMessage = 8;
}

message MulticastMessage {
    message SenderStatusMessage {
        enum SenderStatus {
            UNKNOWN = 0;
            OPERATING = 1;
            IDLE = 2;
            MALFUNCTION = 3;
            QUENCHING = 4;
        }
        SenderStatus senderStatus = 1;
    }

    oneof Payload {
        Event event = 1;
        SenderStatusMessage status = 2;
    }
}

