//#define _XOPEN_SOURCE
//#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>

#include "ecal.h"
#ifdef DAQ
#include "event.h"
#include "monitor.h"
#include "attributesHandler.h"
#include "subEvent.h"
#endif /*DAQ*/
#ifdef VMEserver
int BaseHvc = BaseHVC;
int FireLed;

//extern "C" unsigned short ntohs(unsigned short data);
extern "C" int vme_read(int vmeaddr, unsigned short *data);
extern "C" int vme_write(int vmeaddr, unsigned short data);
extern "C" int vme_start(const char *server);
extern "C" int vme_map(int baseaddr,int window_sz);
#endif /*VMEserver*/
#ifdef ROOTAP
#include <TApplication.h>
#include <TSystem.h>
#include <TROOT.h>          // needed for root in general
#include <TH1.h>            // definition of histograms
#include <TH2.h>
#include <TF1.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TPad.h>   
#include <TStyle.h>
#include "TFrame.h"
#include "TRandom.h"
#include "TProfile.h"
#include <TGraph.h>   
#include <TLatex.h>
TFile *file;
  
  TH1F* 	histoTrPed[n_trig];
  TH1F* 	histoTrLed[n_trig];
  TH2S* 	histoTrSmpl[n_trig];
  TH1F* 	histoBgoPed[n_bgo];
  TH1F* 	histoBgoSign[n_bgo];
  TH2S* 	histoBgoSmpl[n_bgo];
  TH1F* 	histoPed[n_cells];
  TH1F* 	histoLed[n_cells];
//  TH1F* 	histoTime[n_cells];
  TH2S* 	histoSmpl[n_cells];
  TGraph* 	graphHvdep[n_cells];
  
  TCanvas* 	hvsetHst = NULL;
#endif /* ROOTAP */

#include <gtk/gtk.h>
#include <gdk/gdk.h>

//char *strptime(const char *s, const char *format, struct tm *tm);
//#define DEBUG1
//#define DEBUG2
#define FATAL_ERROR (int) 0x6

char VMEinit = FALSE;
static gint processing_id = 0;
static gint root_id = 0;
static char measure,start_measure = FALSE;
static char end_measure = TRUE;
static int chadc;
time_t modtime = 0;
time_t t, request;
static char offline = FALSE;
static char NewChunk = TRUE;

GtkWidget *fbutton[n_buttons];
GtkWidget *ec_cell[n_cells];
GtkWidget *message_button;
GtkWidget *button_trigger;
GtkWidget *button_100V;
//GtkStyle new_style;
//GtkStyle *newstyle = &new_style;
GtkStyle *newstyle;
static 
GtkSpinButton *spinner;
static GtkWidget *label_electron;
static GtkWidget *label_cur;
static GtkWidget *label_ref;
static GtkWidget *label_ratio;
GtkWidget *label_data_source;
GtkWidget *lbl_Stat;
GtkWidget *sel_phys;
GtkWidget *sel_cal;    
char ttl_stat[200];

struct slinkHeaderStruct {
  unsigned int ev_size : 16;
  unsigned int src_id  : 10;
  unsigned int ev_type :  5;
  unsigned int err     :  1;
  unsigned int event_nr: 20; 
  unsigned int spill_nr: 11;
  unsigned int stat    :  1; 
  unsigned int status_nr: 8;  
  unsigned int tcs_err :  8; 
  unsigned int nr_err  :  8;
  unsigned int format_nr: 8;
};
short map_ms[n_msmod*n_msinp];

static struct {
  short  srcId;
  char port;
  char chnl;
  short hvadd;
} mapcc[n_cells];
static struct {
  short  srcId;
  char port;
  char chnl;
} maptrg[n_trig];
static struct {
  short  srcId;
  char port;
  char chnl;
} mapbgo[n_bgo];
short SrcErr[NbID];
short SrcCells[NbID];
static struct {
 char sel;
 char clicked;
 char enable;
 int x;
 int y;
 int z;
 int xyz;
} first_corner;

typedef struct _actions actions;
struct _actions{
	char *label;
   	char sel;
	void (*func) (GtkWidget *, void *);
	char *tooltip;
};
actions func_buttons[n_buttons];

static struct {
  char select;
  int statistic;
  short Hvcode;
  short Hvcal;
  short Refcode;
  float Ped;
//  float Time;
  float Led;
  float LedRms;
  float LedRef;
  float RefRms;
  float ElectronCal;
  float ElectronRes;
  short Pnbins;
  float minPbin;
  float Pbin;
  short Lnbins;
  float minLbin;
  float Lbin;
} cell[n_cells];

static struct {
  float electron;
  float electron_statistic;
  float led;
  int led_statistic;
} calibration[n_cells];

int cells_on;
static struct {
  short xyz;
  float ph;
} word[n_cells];

typedef struct _OptionPar OptionPar;
struct _OptionPar {
  int statistic;
  float tolerance;
  int electron_peak;
  int minph;
  int maxph;
  int minres;
  int maxres;
  short Pnbins;
  float minPbin;
  float Pbin;
  short Lnbins;
  float minLbin;
  float Lbin;
  float maxfactor;
  float signcut;
  float scale;
};
char selected[n_cells], processed[n_cells], updated[n_cells];
short Cur_Ph[n_cells];
float flPh, Disp, SumPh[n_cells], SumPhQ[n_cells];
float Led[n_cells][nphv];
float Time[n_cells][nphv];
int statistic = 0;
int eventsTot = 0;
int eventsCal = 0;
int eventsPhys = 0;
int errEvents = 0;
typedef struct _OptionsSpin OptionsSpin;
struct _OptionsSpin {
  GtkSpinButton *statistic;
  GtkSpinButton *tolerance;
  GtkSpinButton *electron_peak;
  GtkSpinButton *minph;
  GtkSpinButton *maxph;
  GtkSpinButton *minres;
  GtkSpinButton *maxres;
  GtkSpinButton *maxfactor;
  GtkSpinButton *Pnbins;
  GtkSpinButton *minPbin;
  GtkSpinButton *Pbin;
  GtkSpinButton *Lnbins;
  GtkSpinButton *minLbin;
  GtkSpinButton *Lbin;
  GtkSpinButton *signcut;
  GtkSpinButton *scale;
};
#define FATAL_ERROR (int) 0x6
int errorFlag;
int errorLoc[32];
char LATCH = FALSE;
char phys_yes = FALSE;
int trigger_mask = 0;
int trigger_pattern;
char expert = FALSE;
char cosmic = FALSE;
char HVcheck = TRUE;
char sel_calibr = TRUE;
char ESPARSE = FALSE;
char SPARSE = FALSE;
char TUNED = FALSE;
char MTUNED = FALSE;
int SrcId = 0;
int nosample = 0;
int  n_smpl = 0;
int  mn_smpl = 0;
int n_sum;
int tot_chan;
static int msmpl_wd;
struct msadc_hd {
  unsigned int ev_nr  : 12; // local event number
  unsigned int blk_sz : 12; // block size
  unsigned int mode   : 1;  // data mode: '1' sparse, '0' latch all
  unsigned int id     : 2;  // ADC ID
  unsigned int port   : 3;  // HGeSiCA port
  unsigned int key    : 2;  // not used; should be '11'
};
struct msadc_chhd {
  unsigned int offset	:12; // sample offset value
  unsigned int n_u	:3;
  unsigned int rcd	:1;  //reduced coding 
  unsigned int samples	:8;  // samples
  unsigned int nw	:1;  // not used; should be 0
  unsigned int sup	:1;
  unsigned int ch_nr	:4;  // channel number
  unsigned int key	:2;  // not used; should be '01' 
};
struct msadc_data12 {
  unsigned int val_0  : 12; // sample value 0
  unsigned int val_1  : 12; // sample value 1
  unsigned int n_u    : 6; //  not used
  unsigned int key    : 2;  // not used; should be '01' in the first word, '10' in others
};
struct msadc_data6 {
  unsigned int val_0  : 6; // sample value 0
  unsigned int val_1  : 6; // sample value 1
  unsigned int val_2  : 6; // sample value 0
  unsigned int val_3  : 6; // sample value 1
  unsigned int val_4  : 6; // sample value 0
  unsigned int key    : 2;  // not used; should be '01' in the first word, '10' in others
};

struct msadc_integral {
  unsigned int val    : 16; // sample integral value 
  unsigned int samples: 9;  // samples
  unsigned int n_u    : 1;  // not used; should be 0
  unsigned int ch_nr  : 4;  // channel number
  unsigned int key    : 2;  // not used; should be '01' 
};
#ifdef DAQ
//char *monitorTableYes[  ] = {"CAL","yes","27","CAL","yes", "29", "CAL","yes","30", 0 };
char *monitorTableYes[  ] = {(char*)"CAL",(char*)"yes", 0 };
//char *monitorTableAll[  ] = {"CAL","all","27","CAL","all", "29", "CAL","all","30", 0 };
char *monitorTablePhys[] ={(char*)"PHY",(char*)"yes",0};
#endif	
OptionPar parameters;

static int tune_xyz[n_cells];
static short LedRef = 0;

static gint check_id = 0;
static gint hv_id = 0;
static gint read_id = 0;
static int tune_id = 0;
static int step_id = 0;
static int running_id = 0;
static int count = 0;
static char flash = TRUE;
static char cancel_flag = FALSE;
static int code_step;
static int flash_id = 0;
static int count_tn;
static short tune_code;
static char hard_problems = 0;
static int pmhvon = n_cells;
char proc_events(void);
char vme_access(void);
void adc_init(void);
void set_code(int xyz, short code);
void sliv(void);
void zero_reset(GtkWidget *widget,
                          gpointer data);
static int set_id = 0, set_done=0;
static short hvcode;
int  curr_cell;
GdkColor IQ_current_color;


int xyz2lin(int x,int y,int z) {
  return ((((x*n_row)+y)*n_sec)+z);
}
void lin2xyz(int xyz,int* x,int* y,int* z) {
  *x = xyz/(n_row*n_sec);
  *y = xyz%(n_row*n_sec)/n_sec;
  *z = xyz%n_sec;
}
void AIQ_set_color (GtkWidget *widget, GtkStateType state, char *color, int lab)
{
   GdkColor gcolor;
   gboolean a;
   if (newstyle) gtk_style_unref( newstyle);
   newstyle = gtk_style_copy (widget->style);
   if (!gdk_color_parse (color,&gcolor)) {
      g_print ("_AIQ_set_color: Undefined color '%s'\n",color);
      gtk_style_unref (newstyle);
   }
   else {
      if (lab) {
        (newstyle->fg[state]).red = gcolor.red;
        (newstyle->fg[state]).green = gcolor.green;
        (newstyle->fg[state]).blue = gcolor.blue;
        a=gdk_color_alloc (gtk_widget_get_colormap(widget),
	                   &(newstyle->fg[state]));
      }
      else {
        (newstyle->bg[state]).red = gcolor.red;
        (newstyle->bg[state]).green = gcolor.green;
        (newstyle->bg[state]).blue = gcolor.blue;
        a=gdk_color_alloc (gtk_widget_get_colormap(widget),
	                   &(newstyle->bg[state]));
      }
      if (a) {
        gtk_widget_set_style (widget,newstyle);
      }
      else {
        g_print ("_AIQ_set_color:  Can not allocate colormap\n");
      }
   }
}
static int message_on = FALSE;
void remove_message(void)
{
  gtk_label_set_text
	  (GTK_LABEL(GTK_BUTTON(message_button)->child),"");
  gtk_widget_hide (message_button);
  message_on = FALSE;
}

void error_message(char* data)
{
  AIQ_set_color (GTK_WIDGET(message_button), GTK_STATE_NORMAL,(char *)"red",0);
  gtk_label_set_text
	  (GTK_LABEL(GTK_BUTTON(message_button)->child),data);
  gtk_widget_show (message_button);
  message_on = TRUE;
}
void update_statistic(int Statistic,int ncells)
{
int ind;
  ind = sprintf(ttl_stat,"eventsTot: %d eventsCal: %d eventsPhys: %d Statistic: %5d, ecal: %4d cells  SrcId err",eventsTot,eventsCal,eventsPhys,Statistic,ncells);
  for (int id = 0;id <NbID;id++) {
    ind += sprintf(&ttl_stat[ind]," %d: %d",id+MIN_SrcId,SrcErr[id]);
  }
  ind += sprintf(&ttl_stat[ind],"\n cells: ");
  for (int id = 0;id <NbID;id++) {
    ind += sprintf(&ttl_stat[ind]," %d ",SrcCells[id]);
  }
  gtk_label_set_text (GTK_LABEL(lbl_Stat),ttl_stat);  
}

void ecal_button_pressed( GtkWidget *widget,
               gpointer   data )
{
  char c;
  int x,y,z,xyz;
    sscanf(GTK_LABEL(GTK_BUTTON(widget)->child)->label,"%1d%1c%1d%1c%1d",&x,&c,&y,&c,&z);
    xyz = xyz2lin(x,y,z);
    if (!cell[xyz].select) {
      cell[xyz].select = TRUE;
      AIQ_set_color (GTK_WIDGET(widget), GTK_STATE_NORMAL,(char *)"green",0);
    }
    else {
    	cell[xyz].select = FALSE;
	   AIQ_set_color (GTK_WIDGET(widget), GTK_STATE_NORMAL,(char *)"red",0);
    }
}

gint delete_event( GtkWidget *widget,
                   GdkEvent  *event,
                   gpointer   data )
{
#ifdef ROOTAP
  if (root_id) gtk_idle_remove(root_id);
//  hvsetHst->Modified();
//  hvsetHst->Update();
  file->Write("",TObject::kOverwrite);
#endif
    gtk_main_quit ();
    return(FALSE);
}
/**********************************************************************/
void color_cells( GtkWidget *widget, char *ccolor)
{
	AIQ_set_color (GTK_WIDGET(widget), GTK_STATE_NORMAL,(char *)ccolor,0);
}
void sens_fbutton(char sens) {
  for (int i = 4; i < 7; i++) gtk_widget_set_sensitive (fbutton[i], sens);
  gtk_widget_set_sensitive (fbutton[5], expert&sens);
  for (int i = k_tune_selection; i < n_buttons; i++) gtk_widget_set_sensitive (fbutton[i], expert&sens);
/*  if (!sens) {
    for (int i= 41; i< 55; i++) 
	       gtk_widget_set_sensitive (fbutton[i], FALSE);
  }
  else {
    if (sens != -1) {
//      gtk_widget_set_sensitive (fbutton[5], TRUE);
      for (int i = 20; i < 23; i++) gtk_widget_set_sensitive (fbutton[i], TRUE);
    }
    if (!processing_id) for (int i = 45; i < 54; i++) gtk_widget_set_sensitive (fbutton[i], expert);
  }*/
}	      
void color_buttons(void) {
  short n_sel=0;
  for (int xyz=0; xyz<n_cells; xyz++) {
    if (!cell[xyz].select) continue;
    n_sel++;
  }
  if (n_sel == n_pmts || n_sel == 0) {
    if (n_sel == n_pmts)  AIQ_set_color (GTK_WIDGET(fbutton[0]), GTK_STATE_NORMAL,(char *)"green",0);
    else  AIQ_set_color (GTK_WIDGET(fbutton[0]), GTK_STATE_NORMAL,(char *)"red",0);
    for (int xyz=0; xyz<n_cells; xyz++) {
      if (ec_cell[xyz]) gtk_widget_set_style (ec_cell[xyz],newstyle);
    }  
  }
  else {
//  g_print("n_sel %d  ld00 %d ld01 %d\n",n_sel,func_buttons[1].sel,func_buttons[2].sel);
    AIQ_set_color (GTK_WIDGET(fbutton[0]), GTK_STATE_NORMAL,(char *)"green",0);
    for (int xyz=0; xyz<n_cells; xyz++) {
      if (!ec_cell[xyz]) continue;
      if (cell[xyz].select) gtk_widget_set_style (ec_cell[xyz],newstyle);
    }  
    AIQ_set_color (GTK_WIDGET(fbutton[0]), GTK_STATE_NORMAL,(char *)"red",0);
    for (int xyz=0; xyz<n_cells; xyz++) {
        if (!ec_cell[xyz]) continue;
        if (!cell[xyz].select) gtk_widget_set_style (ec_cell[xyz],newstyle);
    }  
  }
}
void select_all( GtkWidget *widget,
               gpointer   data )
{
int i;
  func_buttons[0].sel = !func_buttons[0].sel;
//  for (i=1;i<33;i++) {
//    func_buttons[i].sel = func_buttons[0].sel;
//  }
  for (int xyz=0; xyz<n_cells; xyz++) {
    if (mapcc[xyz].srcId != -1) cell[xyz].select = func_buttons[0].sel;
    else cell[xyz].select = FALSE;
  }
  color_buttons();
}

void read_map_file(void) {
FILE *ecal_map = NULL;
int i,ind,gesId,n_chip,chip_ch,dev,x,y,z,xyz,xf,yf,zf,n,sadc,fiadc,chnl,n_cell,gsinch,srcId,adc_chan,port,mapped=0;
int hvmdl,hvport,hvsad,ldgen;
char c;
  for (xyz=0; xyz<n_cells; xyz++) {
    mapcc[xyz].srcId = -1;
  }
  for (xyz=0; xyz<n_trig; xyz++) {
    maptrg[xyz].srcId = -1;
  }
  for (xyz=0; xyz<n_bgo; xyz++) {
    mapbgo[xyz].srcId = -1;
  }
  for (n = 0; n < n_msmod*n_msinp; n++) {
    map_ms[n] = -1;
  }
  if (!(ecal_map = fopen ("/online/detector/maps/2015.xml/Trigger.xml","r"))) {
    g_print("can't open mapping file for Trigger msadc's\n");
    exit (1);
  }
  while((ind = fscanf (ecal_map,"TRIG %d %d %d %d\n",&gesId,&port,&adc_chan,&x)) != EOF) {
    if (ind == 4) {
      gsinch = ((((gesId - MIN_SrcId)<<3) + port)<<6) + adc_chan;
      chnl = gsinch%n_msinp;
      xyz = x+n_cells;
      if (map_ms[gsinch] != -1) {
        g_print("ERROR: double mapping of srcId %3d port %02d chnl %02d gsinch %3d  -> map %3d, first: %3d\n",
                 gesId,port,adc_chan,xyz,map_ms[gsinch]);
      }
      else if (maptrg[x].srcId != -1) {
        g_print("ERROR second mapping to trig %1d srcId %3d port %02d chnl %02d, old SrcId %3d port/chn %02d/%02d\n",
        x,gesId,port,adc_chan,maptrg[x].srcId,maptrg[x].port,maptrg[x].chnl);
      }
      else {
//        xyz = x+n_cells;
        maptrg[x].srcId = gesId;;
        maptrg[x].port = port;
        maptrg[x].chnl = adc_chan;
        map_ms[gsinch] = xyz;
//	g_print("SrcId %d port %d chnl %d gsinch %d -> xys %d\n",gesId,port,adc_chan,gsinch,xyz);
        mapped++;
      }
    }
    else {
//      g_print("ind = %d ",ind);
      c=getc(ecal_map);
      if (c == '<') {
//        g_print("%c",c);
        do {
          c=getc(ecal_map);
//		  g_print("%c",c);
        } while (c!='>');
      }
      else {
        while (c!='\n') {
          c=getc(ecal_map);
//	      g_print("%c",c);
        } 
      }
    }
  }
  fclose(ecal_map);
  if (!(ecal_map = fopen ("/online/detector/maps/2015.xml/BGO.xml","r"))) {
    g_print("can't open mapping file for BGO msadc's\n");
    exit (1);
  }
  while((ind = fscanf (ecal_map,"BGO %d %d %d %d\n",&gesId,&port,&adc_chan,&x)) != EOF) {
    if (ind == 4) {
      gsinch = ((((gesId - MIN_SrcId)<<3) + port)<<6) + adc_chan;
      chnl = gsinch%n_msinp;
      xyz = x+(n_cells+n_trig);
      if (map_ms[gsinch] != -1) {
        g_print("ERROR: double mapping of srcId %3d port %02d chnl %02d gsinch %3d  -> map %3d, first: %3d\n",
                 gesId,port,adc_chan,xyz,map_ms[gsinch]);
      }
      else if (mapbgo[x].srcId != -1) {
        g_print("ERROR second mapping to bgo %1d srcId %3d port %02d chnl %02d, old SrcId %3d port/chn %02d/%02d\n",
        x,gesId,port,adc_chan,mapbgo[x].srcId,mapbgo[x].port,mapbgo[x].chnl);
      }
      else {
//        xyz = x+n_cells;
        mapbgo[x].srcId = gesId;;
        mapbgo[x].port = port;
        mapbgo[x].chnl = adc_chan;
        map_ms[gsinch] = xyz;
//	g_print("gsinch %d -> xys %d\n",gsinch,xyz);
        mapped++;
      }
    }
    else {
//      g_print("ind = %d ",ind);
      c=getc(ecal_map);
      if (c == '<') {
//        g_print("%c",c);
        do {
          c=getc(ecal_map);
//		  g_print("%c",c);
        } while (c!='>');
      }
      else {
        while (c!='\n') {
          c=getc(ecal_map);
//	      g_print("%c",c);
        } 
      }
    }
  }
  fclose(ecal_map);
  if (!(ecal_map = fopen ("/online/detector/maps/2015.xml/ECAL.xml","r"))) {
    g_print("can't open mapping file for sadc's\n");
    exit (1);
  }
  while((ind = fscanf (ecal_map,"ECAL%d %d %d %d %d %d\n",&z,&gesId,&port,&adc_chan,&x,&y)) != EOF) {
    if (ind == 6) {
      gsinch = ((((gesId - MIN_SrcId)<<3) + port)<<6) + adc_chan;
//      sadc = gsinch/n_msinp;
      chnl = gsinch%n_msinp;
      xyz = xyz2lin(x,y,z);
      if (map_ms[gsinch] != -1) {
        lin2xyz(map_ms[gsinch],&xf,&yf,&zf);
        g_print("ERROR: double mapping of srcId %3d port %02d chnl %02d gsinch %3d  -> x-y-z %1d-%1d-%1d, first: %1d-%1d-%1d\n",
                 gesId,port,adc_chan,x,y,z,xf,yf,zf);
      }
      else if (mapcc[xyz].srcId != -1) {
        g_print("ERROR second mapping to cell x-y-z %1d-%1d-%1d srcId %3d port %02d chnl %02d, old SrcId %3d port/chn %02d/%02d\n",
        x,y,z,gesId,port,adc_chan,mapcc[xyz].srcId,mapcc[xyz].port,mapcc[xyz].chnl);
      }
      else {
        xyz = xyz2lin(x,y,z);
        mapcc[xyz].srcId = gesId;;
        mapcc[xyz].port = port;
        mapcc[xyz].chnl = adc_chan;
        map_ms[gsinch] = xyz;
        mapped++;
      }
    }
    else {
//      g_print("ind = %d ",ind);
      c=getc(ecal_map);
      if (c == '<') {
//        g_print("%c",c);
        do {
          c=getc(ecal_map);
//		  g_print("%c",c);
        } while (c!='>');
      }
      else {
        while (c!='\n') {
          c=getc(ecal_map);
//	      g_print("%c",c);
        } 
      }
    }
  }
  fclose(ecal_map);
  if (!(ecal_map = fopen ("ECALhv.xml","r"))) {
    g_print("can't open mapping file for hv system\n");
    exit (1);
  }
  while((ind = fscanf (ecal_map,"%d %d %d %d %d %d\n",&x,&y,&z,&hvmdl,&hvport,&hvsad)) != EOF) {
    if (ind == 6) {
      if (mapcc[xyz].srcId == -1) continue;
      xyz = xyz2lin(x,y,z);
      mapcc[xyz].hvadd = ((((hvmdl<<3)+hvport)<<4)+hvsad)<<1;
//      g_print(" x %d y %d  z %d hvadd = %x\n",x,y,z,mapcc[xyz].hvadd);
//      mapcc[xyz].ldgen = ldgen;
//      fibers[ldgen] += 1;
    }
    else {
//      g_print("ind = %d ",ind);
      c=getc(ecal_map);
      if (c == '<') {
//	g_print("%c",c);
	do {
	  c=getc(ecal_map);
//		  g_print("%c",c);
	} while (c!='>');
      }
      else {
	while (c!='\n') {
	  c=getc(ecal_map);
//	      g_print("%c",c);
	}
      }
    }
  }   
  fclose(ecal_map);
  if (mapped != n_all) {
    g_print("MAPPING PROBLEMS, %d cells mapped, expected %d\n",mapped,n_all);
/*    i = 0;
    for (x=0;x<n_col;x++) {
      for (y=0;y<n_row;y++) {
        if (mapcc[xyz].srcId != -1) continue;
        g_print("%02d/%02d%c",x,y,++i%10 == 0 ? '\n' : ' ');
      }
    } */
  }
}
void read_selection( GtkWidget *widget,
               gpointer   data )
{
int xyz;
int status;
  if (data != NULL) {
    for (xyz=0;xyz<n_cells;xyz++){
      processed[xyz] = selected[xyz] = cell[xyz].select;
    }
  }
  for (xyz=0;xyz<n_cells;xyz++){
     if (processed[xyz]) {
         cell[xyz].statistic = 0;
//         SumPh[xyz] = 0.;
//	 SumPhQ[xyz] = 0.;
         cell[xyz].Ped = 0.;
         cell[xyz].Led = 0.;
	 cell[xyz].LedRms = 0.;
       }
  }
  cancel_flag = FALSE;
  if (!offline) {
    time(&modtime);
//    modtime+=2;     // add 2 second
  }
  eventsCal = 0;
  eventsPhys = 0;
  eventsTot = 0;
  statistic = 0;
  for (int i=0; i<NbID;i++) {
    SrcErr[i] = 0;
  }
  update_statistic(statistic,0);
#ifdef DAQ
  sens_fbutton(FALSE);
  if (!processing_id) {
//      status = monitorDeclareTableWithAttributes( monitorTableAll ); 
    processing_id = gtk_idle_add ((GtkFunction) proc_events, NULL);
  }
#else
  error_message("DAQ is not available");
#endif /*DAQ*/
  return;
}    


void  set_default(void) {
int xyz;
  parameters.statistic	= 100;
  parameters.tolerance  = 1.10;
  parameters.electron_peak = 100;
  parameters.minph = 0;
  parameters.maxph = 4095;
  parameters.minres = 01;
  parameters.maxres = 100;
  parameters.Pnbins = 100;
  parameters.minPbin =  0.;
  parameters.Pbin = 2.;
  parameters.Lnbins = 100;
  parameters.minLbin =  0.;
  parameters.Lbin = 10.;
  parameters.maxfactor = 1.05;
  parameters.signcut = 5.;
  parameters.scale = 1.;
  for (xyz=0;xyz<n_cells;xyz++){
    cell[xyz].select	   = 1;
    cell[xyz].statistic  = 0;
    cell[xyz].Ped        = 50.;
//    cell[xyz].Time       = 260.;
    cell[xyz].Led        = 100.;
    cell[xyz].LedRms     = 1.;
    cell[xyz].LedRef     = 100.;
    cell[xyz].RefRms     = 1.;
    cell[xyz].ElectronCal= 2000.;
    cell[xyz].ElectronRes= 1.;
    cell[xyz].Pnbins     = 100;
    cell[xyz].minPbin    = 0.;
    cell[xyz].Pbin       = 5.;
    cell[xyz].Lnbins     = 100;
    cell[xyz].minLbin    = 0.;
    cell[xyz].Lbin       = 40.;
    cell[xyz].Hvcode     = 0;
    cell[xyz].Hvcal      = 800;
    cell[xyz].Refcode    = 800;
  }
}	

void  read_ref_file (GtkObject *entry)
{
FILE *ecal_open;
GString *filename;
int ind,x,y;
char c;  
short Hvcode, Hvcal, Refcode;
float Ped;
//float Time;
float Led;
float LedRms;
float LedRef;
float RefRms;
float ElectronCal;
float ElectronRes;
float Pbin,Lbin;
short MinLed, MaxLed, limit, Pnbins, Lnbins;
  if (entry) {
    filename = g_string_new (gtk_entry_get_text(GTK_ENTRY(entry)));
    gtk_label_set_text
	     (GTK_LABEL(GTK_BUTTON(message_button)->child),(char *)"  ");
    gtk_widget_set_sensitive(message_button, FALSE);
    if (!(ecal_open=fopen (filename->str,"r"))) {
      g_string_prepend (filename,"can't open file: ");
      gtk_label_set_text
         (GTK_LABEL(GTK_BUTTON(message_button)->child),filename->str);
      gtk_widget_set_sensitive(message_button, TRUE);
      g_string_free (filename,TRUE);
      return;
    }
  }
  else {
    filename=g_string_new("ec_hvset.las");
     if (!(ecal_open=fopen (filename->str,"r"))) {
      g_print ("can't open file: %s \n",filename->str);
      g_string_free (filename,TRUE);
      return;
    }
  }	
//  g_print("restore parameters from file : %s \n",filename->str);
  fscanf(ecal_open,"%d %f %d %d %d %d %d %f %hd %f %f %hd %f %f %f %f\n",
  		&parameters.statistic,
		&parameters.tolerance,
		&parameters.electron_peak,
		&parameters.minph,
		&parameters.maxph,
		&parameters.minres,
		&parameters.maxres,
		&parameters.maxfactor,
		&parameters.Pnbins, 
		&parameters.minPbin,
		&parameters.Pbin,   
		&parameters.Lnbins, 
		&parameters.minLbin,
		&parameters.Lbin,
		&parameters.signcut,
		&parameters.scale);
  for (int xyz=0;xyz<n_cells;xyz++){
    ind = fscanf (ecal_open," %hd %hd %hd %f %f %f %f %f %f %f %hd %f %hd %f\n"
         ,&Hvcode
         ,&Hvcal
         ,&Refcode
	 ,&Ped 
	 ,&Led 
	 ,&LedRms
	 ,&LedRef
	 ,&RefRms
	 ,&ElectronCal
	 ,&ElectronRes
//	 ,&Time
	 ,&Pnbins
	 ,&Pbin
	 ,&Lnbins
	 ,&Lbin);
    if (ind == EOF) break;
    if (cell[xyz].select) {
      cell[xyz].Hvcode 	= Hvcode;
      cell[xyz].Hvcal 	= Hvcal;
      cell[xyz].Refcode	= Refcode;
      cell[xyz].Led 	= Led;
      cell[xyz].LedRms 	= LedRms;
      cell[xyz].LedRef	= LedRef;
      cell[xyz].RefRms	= RefRms;
      cell[xyz].ElectronCal	= ElectronCal;
      cell[xyz].ElectronRes	= ElectronRes;
//	cell[xyz].Time 	= Time;
      cell[xyz].Ped 	= Ped;
      cell[xyz].Pnbins	= Pnbins;
      cell[xyz].Pbin	= Pbin;
      cell[xyz].minPbin	= parameters.minPbin;
      cell[xyz].Lnbins	= Lnbins;
      cell[xyz].Lbin	= Lbin;
      cell[xyz].minLbin	= parameters.minLbin;
    }
  }
  fclose(ecal_open);
  g_string_free (filename,TRUE);

}	

void restore_status( GtkWidget *widget,
               gpointer   data )
{
  static GtkWidget *window = NULL;
  GtkWidget *box1;
  GtkWidget *box2;
  GtkWidget *entry;
  GtkWidget *button;
  GtkWidget *separator;
  if (!window) {
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

    gtk_signal_connect (GTK_OBJECT (window), "destroy",
			GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			&window);

    gtk_window_set_title (GTK_WINDOW (window), "restore status from file");
    gtk_container_set_border_width (GTK_CONTAINER (window), 0);

    box1 = gtk_vbox_new (FALSE, 0);
    gtk_container_add (GTK_CONTAINER (window), box1);
    gtk_widget_show (box1);


    box2 = gtk_vbox_new (FALSE, 10);
    gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
    gtk_box_pack_start (GTK_BOX (box1), box2, TRUE, TRUE, 0);
    gtk_widget_show (box2);



    entry = gtk_entry_new ();
/*      gtk_signal_connect(GTK_OBJECT(entry), "activate",
		       GTK_SIGNAL_FUNC(enter_callback),
		       entry);
*/
    gtk_entry_set_text (GTK_ENTRY (entry), "ec_hvset.las");
    gtk_editable_select_region (GTK_EDITABLE (entry), 0, 5);
    gtk_box_pack_start (GTK_BOX (box2), entry, TRUE, TRUE, 0);
    gtk_widget_show (entry);

    button = gtk_button_new_with_label ("read");
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			       GTK_SIGNAL_FUNC(read_ref_file),
			       GTK_OBJECT (entry));
    gtk_box_pack_start (GTK_BOX (box2), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

    button = gtk_button_new_with_label ("cancel");
    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			       GTK_SIGNAL_FUNC(gtk_widget_destroy),
			       GTK_OBJECT (window));
    gtk_box_pack_start (GTK_BOX (box2), button, TRUE, TRUE, 0);
    gtk_widget_show (button);

  }
  if (!GTK_WIDGET_VISIBLE (window))
    gtk_widget_show (window);
  else
    gtk_widget_destroy (window);
}

static char passwd_ok = FALSE;

void check_passwd(GtkWidget *widget, GtkObject *passwd)
{
 passwd_ok = !strncmp(gtk_entry_get_text(GTK_ENTRY(passwd)),PASSWD,10);
}


void  save_ref_to_file (GtkObject *entry)
{
FILE *ecal_open;
GString *filename;
int xyz;
  filename = g_string_new (gtk_entry_get_text(GTK_ENTRY(entry)));
  gtk_label_set_text
		(GTK_LABEL(GTK_BUTTON(message_button)->child),(char *)"  ");
  gtk_widget_set_sensitive(message_button, FALSE);
  if (!passwd_ok) {
    gtk_label_set_text
	(GTK_LABEL(GTK_BUTTON(message_button)->child),(char *)"wrong password");
	gtk_widget_set_sensitive(message_button, TRUE);
    return;
  }
    
  if (!(ecal_open=fopen (filename->str,"w"))) {
    g_string_prepend (filename,"can't open file: ");
    gtk_label_set_text
	(GTK_LABEL(GTK_BUTTON(message_button)->child),filename->str);
	gtk_widget_set_sensitive(message_button, TRUE);
    g_string_free (filename,TRUE);
    return;
  }
  fprintf(ecal_open,"%d %5.2f %d %d %d %d %d %5.2f %hd %4.1f %4.1f %hd %4.1f %4.1f %5.1f %5.2f\n",
	  parameters.statistic,
	  parameters.tolerance,
	  parameters.electron_peak,
	  parameters.minph,
	  parameters.maxph,
	  parameters.minres,
	  parameters.maxres,
	  parameters.maxfactor,
	  parameters.Pnbins, 
	  parameters.minPbin,
	  parameters.Pbin,   
	  parameters.Lnbins, 
	  parameters.minLbin,
	  parameters.Lbin,
	  parameters.signcut,
	  parameters.scale);		
  for (xyz=0;xyz<n_cells;xyz++){
    fprintf (ecal_open,"%4d %4d %4d %4.1f %6.1f %5.1f %6.1f %5.1f %5.1f %4.1f %hd %4.1f  %hd %4.1f\n",
	     cell[xyz].Hvcode,
	     cell[xyz].Hvcal,
	     cell[xyz].Refcode,
	     cell[xyz].Ped,     
	     cell[xyz].Led,     
	     cell[xyz].LedRms,     
	     cell[xyz].LedRef,    
	     cell[xyz].RefRms,    
	     cell[xyz].ElectronCal,
	     cell[xyz].ElectronRes,
       //      cell[xyz].Time,
	     cell[xyz].Pnbins, 
	     cell[xyz].Pbin, 
	     cell[xyz].Lnbins,
	     cell[xyz].Lbin);
  }
  fclose(ecal_open);
  g_string_free (filename,TRUE);

}	

void save_status( GtkWidget *widget,
               gpointer   data )
{
  static GtkWidget *window = NULL;
  GtkWidget *box1;
  GtkWidget *box2;
  GtkWidget *entry;
  GtkWidget *passwd;
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *separator;
  if (!window) {
      window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

      gtk_signal_connect (GTK_OBJECT (window), "destroy",
			  GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			  &window);

      gtk_window_set_title (GTK_WINDOW (window), "save status to file");
      gtk_container_set_border_width (GTK_CONTAINER (window), 0);

      box1 = gtk_vbox_new (FALSE, 0);
      gtk_container_add (GTK_CONTAINER (window), box1);
      gtk_widget_show (box1);


      box2 = gtk_vbox_new (FALSE, 10);
      gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
      gtk_box_pack_start (GTK_BOX (box1), box2, TRUE, TRUE, 0);
      gtk_widget_show (box2);


      label = gtk_label_new ("file name");
      gtk_box_pack_start (GTK_BOX (box2), label, TRUE, TRUE, 0);
      gtk_widget_show (label);

      entry = gtk_entry_new ();
/*      gtk_signal_connect(GTK_OBJECT(entry), "activate",
			 GTK_SIGNAL_FUNC(enter_callback),
			 entry);
*/
      gtk_entry_set_text (GTK_ENTRY (entry), "ec_hvset.las");
      gtk_editable_select_region (GTK_EDITABLE (entry), 0, 5);
      gtk_box_pack_start (GTK_BOX (box2), entry, TRUE, TRUE, 0);
      gtk_widget_show (entry);

      label = gtk_label_new ("password");
      gtk_box_pack_start (GTK_BOX (box2), label, TRUE, TRUE, 0);
      gtk_widget_show (label);

      passwd = gtk_entry_new ();
      gtk_entry_set_text (GTK_ENTRY (passwd), "");
      gtk_editable_select_region (GTK_EDITABLE (passwd), 0, 5);
      gtk_box_pack_start (GTK_BOX (box2), passwd, TRUE, TRUE, 0);
      gtk_entry_set_visibility(GTK_ENTRY (passwd),FALSE);
      gtk_widget_show (passwd);

      button = gtk_button_new_with_label ("save");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC(check_passwd),
				 GTK_OBJECT (passwd));
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC(save_ref_to_file),
				 GTK_OBJECT (entry));
      gtk_box_pack_start (GTK_BOX (box2), button, TRUE, TRUE, 0);
      gtk_widget_show (button);

      button = gtk_button_new_with_label ("close");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC(remove_message),
				 NULL);
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC(gtk_widget_destroy),
				 GTK_OBJECT (window));
      gtk_box_pack_start (GTK_BOX (box2), button, TRUE, TRUE, 0);
      gtk_widget_show (button);
    }

  if (!GTK_WIDGET_VISIBLE (window))
    gtk_widget_show (window);
  else
    gtk_widget_destroy (window);
}
#ifdef OLDFORMATS
void  read_calib_file (GtkObject *entry)
{
FILE *ecal_open;
GString *filename;
int ind,x,y,z,ev_stat,ld_stat;
float MeanPeak,SigmaPeak,Statistic,Coeff,SigmaCoeff,Led,Ped;
char c;

  filename = g_string_new (gtk_entry_get_text(GTK_ENTRY(entry)));
  gtk_label_set_text
		(GTK_LABEL(GTK_BUTTON(message_button)->child),(char *)"  ");
  gtk_widget_set_sensitive(message_button, FALSE);
  if (!(ecal_open=fopen (filename->str,"r"))) {
    g_string_prepend (filename,"can't open file: ");
    gtk_label_set_text
	(GTK_LABEL(GTK_BUTTON(message_button)->child),filename->str);
	gtk_widget_set_sensitive(message_button, TRUE);
    g_string_free (filename,TRUE);
    return;
  }
//   Kolosov format 
#ifndef joujou
  for (ind=0;ind<4;) {
	  if ((c=getc(ecal_open))=='\n') ind++;
		g_print("%c",c);
  }
  for (xyz=0;xyz<n_cells;xyz++){
    ind = fscanf (ecal_open," %d %d %d %f %f %f %f %f",
    &x,&y,&z,&MeanPeak,&SigmaPeak,&Statistic,&Coeff,&SigmaCoeff);
//			g_print("x %d y %d mu %f rms %f\n",cx,cy,MeanPeak,finp[1]);
    if (xyz == xyz2lin(x,y,z)) {
      if (Statistic < 1.) continue; //statistic cut
      calibration[xyz].electron = BeamEnergy/Coeff;
      cell[xyz].ElectronCal = calibration[xyz].electron;
      calibration[xyz].electron_statistic = Statistic;
      if (cell[xyz].select) cell[xyz].LedRef = cell[xyz].Led;
      calibration[xyz].led	=  cell[xyz].LedRef;
    }
    if (ind == EOF) break;
  }

#else
// joujou format
  for (ind=0;ind<1;) {
    if ((c=getc(ecal_open))=='\n') ind++;
    g_print("%c",c);
  }
  for (int xyz=0;xyz<n_cells;xyz++){
      ind = fscanf (ecal_open," %d %d %d %f %f %f %d %d",
      &x,&y,&z,&Ped,&MeanPeak,&Led,&ev_stat,&ld_stat);
 //			g_print("x %d y %d cal %55.1f\n",cx,cy,MeanPeak);
      if (xyz == xyz2lin(x,y,z)) {
        calibration[xyz].electron 	= MeanPeak;
        cell[xyz].ElectronCal		= MeanPeak;
        calibration[xyz].electron_statistic = ev_stat;
        if (cell[xyz].select) cell[xyz].LedRef = cell[xyz].Led;
        calibration[xyz].led = cell[xyz].LedRef;
        calibration[xyz].led_statistic	= ld_stat;
      }
      if (ind == EOF) break;
  }
#endif
  fclose(ecal_open);
  g_string_free (filename,TRUE);

}	
#endif // OLDFORMATS
void  read_calib_file (GtkObject *entry)
{
FILE *ecal_calib;
GString *filename;
int ind,x,y,z,xyz,ev_stat,ld_stat;
float MeanPeak,SigmaPeak,Statistic,Coeff,SigmaCoeff,Led,Ped;
char c;
  filename = g_string_new (gtk_entry_get_text(GTK_ENTRY(entry)));
  gtk_label_set_text
		(GTK_LABEL(GTK_BUTTON(message_button)->child),(char *)"  ");
  gtk_widget_set_sensitive(message_button, FALSE);
  if (!(ecal_calib=fopen (filename->str,"r"))) {
    g_string_prepend (filename,"can't open file: ");
    gtk_label_set_text
	(GTK_LABEL(GTK_BUTTON(message_button)->child),filename->str);
	gtk_widget_set_sensitive(message_button, TRUE);
    g_string_free (filename,TRUE);
    return;
  }
  while((ind = fscanf (ecal_calib,"ECP%d__ %d %d %f\n",&z,&x,&y,&MeanPeak)) != EOF) {
    if (ind == 4) {
      xyz = xyz2lin(x,y,z);
      calibration[xyz].electron 	= MeanPeak;
      cell[xyz].ElectronCal		= MeanPeak;
      calibration[xyz].electron_statistic = 1000;
    }
    else {
//      g_print("ind = %d ",ind);
      c=getc(ecal_calib);
      if (c == '<') {
//        g_print("%c",c);
        do {
          c=getc(ecal_calib);
//		  g_print("%c",c);
        } while (c!='>');
      }
      else {
        while (c!='\n') {
          c=getc(ecal_calib);
//	      g_print("%c",c);
        } 
      }
    }
  }
  fclose(ecal_calib);
  g_string_free (filename,TRUE);
}	
void read_calibration( GtkWidget *widget,
               gpointer   data )
{
  static GtkWidget *window = NULL;
  GtkWidget *box1;
  GtkWidget *box2;
  GtkWidget *entry;
  GtkWidget *button;
  GtkWidget *separator;
  if (!window) {
      window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

      gtk_signal_connect (GTK_OBJECT (window), "destroy",
			  GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			  &window);

      gtk_window_set_title (GTK_WINDOW (window), "read calibration file");
      gtk_container_set_border_width (GTK_CONTAINER (window), 0);

      box1 = gtk_vbox_new (FALSE, 0);
      gtk_container_add (GTK_CONTAINER (window), box1);
      gtk_widget_show (box1);


      box2 = gtk_vbox_new (FALSE, 10);
      gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
      gtk_box_pack_start (GTK_BOX (box1), box2, TRUE, TRUE, 0);
      gtk_widget_show (box2);



      entry = gtk_entry_new ();
      gtk_entry_set_text (GTK_ENTRY (entry), "/online/detector/calibrs/2015.xml/ECAL_CALIBR.xml");
      gtk_editable_select_region (GTK_EDITABLE (entry), 0, 5);
      gtk_box_pack_start (GTK_BOX (box2), entry, TRUE, TRUE, 0);
      gtk_widget_show (entry);

      button = gtk_button_new_with_label ("read");
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC(read_calib_file),
				 GTK_OBJECT (entry));
      gtk_box_pack_start (GTK_BOX (box2), button, TRUE, TRUE, 0);
      gtk_widget_show (button);

      button = gtk_button_new_with_label ("cancel");
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC(gtk_widget_destroy),
				 GTK_OBJECT (window));
      gtk_box_pack_start (GTK_BOX (box2), button, TRUE, TRUE, 0);
      gtk_widget_show (button);

    }

  if (!GTK_WIDGET_VISIBLE (window))
    gtk_widget_show (window);
  else
    gtk_widget_destroy (window);
}
void make_new_reference( GtkWidget *widget,
               gpointer   data )
{
int i, x,y,xyz,ihv, ihv1, ihv_1, HvNew, dv;
float factor, LedHv,LedNew, cdstep;
float dhvcd_dL,dL;
//  int factor_type =  (int )data - 52; //number of new_ref button: 0  use calibration data, 1 - use choosen factor
//  g_print("factor_type %d\n",factor_type);
//  factor = parameters.scale;
  for (xyz=0;xyz<n_cells;xyz++){
    if (!cell[xyz].select) continue;
    cell[xyz].Hvcode = cell[xyz].Hvcal = cell[xyz].Refcode;
//    if (factor_type == 0) {
      if(cell[xyz].ElectronCal < 1. || calibration[xyz].electron_statistic < 2.) continue;
      factor = parameters.electron_peak /cell[xyz].ElectronCal;
//    }
    cell[xyz].LedRef = cell[xyz].Led*factor;
//    g_print("chn %d Led %6.1f  factor %5.2f new Led %6.1f\n",i,cell[xyz].Led,factor,cell[xyz].LedRef);
    if (fabs(1.-factor)*100. <parameters.tolerance) continue;
    if (Led[xyz][nphv-1] < 10.) continue;
    cdstep = hvstep;
    ihv = (cell[xyz].Refcode - min_hvcode)/hvstep;
    if (ihv < 0) ihv = 0;
    if (ihv > nphv-1) ihv = nphv-2;
    ihv1 = ihv+1;
    if (ihv1 == nphv-1) cdstep = 3.;
    LedHv = (Led[xyz][ihv1] - Led[xyz][ihv])/cdstep*(cell[xyz].Refcode - (min_hvcode + (ihv * hvstep)));
    LedHv+=Led[xyz][ihv];
    LedNew = LedHv * factor;
    for (ihv=0; ihv<nphv; ihv++) {
      if (Led[xyz][ihv] < LedNew) continue;
      if (ihv == (nphv-1)) {
        HvNew = max_hvcode;
        break;
      }
      if (ihv == 0) {
        HvNew = min_hvcode;
        break;
      }
      ihv_1 = ihv -1;
      dL = Led[xyz][ihv] - Led[xyz][ihv_1];
      if (dL < 3.) {
        HvNew = min_hvcode + (ihv_1*hvstep);
        break;
      }
      cdstep = hvstep;
//      if (ihv == nphv-1) cdstep = 3.;
      dhvcd_dL = cdstep/(Led[xyz][ihv] - Led[xyz][ihv_1]);
      HvNew = min_hvcode + (ihv_1*hvstep) + (int)((LedNew-Led[xyz][ihv_1])*dhvcd_dL);
      break;
     }
     if (HvNew > max_hvcode) HvNew = max_hvcode;
     cell[xyz].Hvcode = HvNew;
     g_print("chn %d Refcode %d LedHv %6.1f Cal %5.1f  factor %5.2f LedNew %5.1f HvcodeNew %d\n",
             xyz,cell[xyz].Refcode,LedHv,cell[xyz].ElectronCal,factor,LedNew,HvNew);
  }
}

void make_new_LedRef( GtkWidget *widget,
               gpointer   data )
{
float factor;
int factor_type =  *(int* )data - 22; //number of new_ref button: 0  use calibration data, 1 - use choosen factor
  factor = parameters.scale;
  g_print("factor_type %d\n",factor_type);
  for (int xyz=0;xyz<n_cells;xyz++){
      if (!cell[xyz].select) continue;
      cell[xyz].Hvcode = cell[xyz].Hvcal = cell[xyz].Refcode;
      if (factor_type == 0) {
        if(cell[xyz].ElectronCal < 1. || calibration[xyz].electron_statistic < 2.) continue;
        factor = parameters.electron_peak /cell[xyz].ElectronCal;
      }
      cell[xyz].LedRef = cell[xyz].Led*factor;
//      g_print("xyz %d  Led %f factor %f Ref %f\n",xyz,cell[xyz].Led, factor,cell[xyz].LedRef );
    }
}
static int ihv;
static gint PhHv(gpointer dummy) {
  int x,y,z,xyz,j,k, sadcm;
//  int xc,yc,dc,xm = -1,ym = -1,dm = 3;
  float hv[nphv], Hvdep[nphv];
  char histoname[20];
  char done;
  FILE *dependence_report;
  if (!cancel_flag) {
    if (processing_id) return TRUE;  // processing in progress, coninue measuremens
    if (measure) { //processing is finished, save result of the last measurement
      done = TRUE;
      for (xyz=0; xyz<n_cells; xyz++) {
	if(!processed[xyz]) continue;
	Led[xyz][ihv] = cell[xyz].Led;
//	Time[xyz][ihv] = cell[xyz].Time;
	if (done && (cell[xyz].Ped + cell[xyz].Led < 4090.)) done = FALSE;
      }
      if (done) {
	for (xyz=0; xyz<n_cells; xyz++) {
	  if(!processed[xyz]) continue;
	  for (j=ihv+1; j<nphv; j++) {
	    Led[xyz][j] = cell[xyz].Led;
//	    Time[xyz][j] = cell[xyz].Time;
	  }
	}
      }
    }
    if (hvcode < max_hvcode) {
      hvcode += hvstep;
      if (hvcode > max_hvcode) hvcode = max_hvcode;
      ihv++;
      for (xyz=0; xyz<n_cells; xyz++) {
        if(processed[xyz]) {
	  set_code(xyz,hvcode);
	}
      }
    }
    else {
      if (measure) {
	for (xyz=0; xyz<n_cells; xyz++) {
	  if(processed[xyz]) {
	    set_code(xyz,0);
	    updated[xyz] = 1;
	    processed[xyz] = 0;
	  }
	}
	sliv();
//	for (xyz=0; xyz<n_cells; xyz++) cell[xyz].select = 0;
	measure = FALSE;
      }
      if (chadc < 15) {
	ihv = 0;
	hvcode = min_hvcode;
	while (!measure && (++chadc < 16)) {
          for (xyz=chadc;xyz<n_cells;xyz+=16) {
	      if (selected[xyz]) {
        	measure = TRUE;
        	processed[xyz] = 1;
        	AIQ_set_color (ec_cell[xyz], GTK_STATE_NORMAL,(char *)"violet",0);
		set_code(xyz,hvcode);
	      }
	  }
	}
      }
    }
    if (measure) {
      g_print("Measure point ihv %2i hvcode %3i chadc %2i\n",ihv,hvcode,chadc);
      time(&modtime);
      modtime++;
      read_selection(NULL,NULL);
      return TRUE;
    }
    g_print("measure FALSE, chadc %2i\n",chadc);
  }
  dependence_report = fopen ("hv_dependence","a");
  for (int ihv=0; ihv<nphv ;ihv++) hv[ihv] = min_hvcode + ihv*hvstep;
  for (xyz=0; xyz<n_cells; xyz++) {
    if (!updated[xyz]) continue;
    lin2xyz(xyz,&x,&y,&z);
    sprintf(histoname,"HvDep_%1i-%1i-%1i",x,y,z);
    graphHvdep[xyz]= (TGraph*) file->Get(histoname);
    if (graphHvdep[xyz] != NULL)  delete graphHvdep[xyz];
    for (int ihv=0;ihv<nphv; ihv++) Hvdep[ihv] = Led[xyz][ihv]/(Led[xyz][nphv-1]+0.0001);
    graphHvdep[xyz]= new TGraph(nphv,hv,Hvdep);
    graphHvdep[xyz]->SetTitle(histoname);
    fprintf(dependence_report," chn %3d  %1d-%1d-%1d\n",xyz,x,y,z);
    for (j=0; j<nphv; j++) fprintf(dependence_report," %6.1f",Led[xyz][j]);
    fprintf(dependence_report,"\n");
//    for (j=0; j<nphv; j++) fprintf(dependence_report," %6.1f",Time[xyz][j]);
    fprintf(dependence_report,"\n");
  }
  fclose(dependence_report);
  sens_fbutton(TRUE);
  return FALSE;
}

void hv_dep( GtkWidget *widget,
               gpointer   data )
{
int xyz,chn_start;
  zero_reset(NULL,NULL);
  chadc = -2;
  hvcode = max_hvcode;
  measure = FALSE;
  cancel_flag = 0;
  for (xyz=0; xyz<n_cells;xyz+=16) {
      for(chn_start=0; chn_start<16; chn_start++) {
	if ( cell[xyz+chn_start].select) {
          chadc = chn_start -1;
	  break;
	}
      }
    if (chadc != -2) break;           
  }
  for (xyz=0; xyz<n_cells;xyz++) {
    selected[xyz] = cell[xyz].select;
    updated[xyz] = 0;
  }
  sens_fbutton(FALSE);
  hv_id = gtk_timeout_add (500,(GtkFunction) PhHv,NULL);
}
static gint RdOne(gpointer dummy) {
  int xyz;
  if (!cancel_flag) {
    if (processing_id) return TRUE;
    if (measure) {
      for (xyz=0; xyz<n_cells;xyz++) {
	if(selected[xyz]) {
	  set_code(xyz,0);
	}
      }
      sliv();
      measure = FALSE;
    }
    if (chadc < 15) {
      while (!measure && ++chadc < 16) {
        for (xyz=0;xyz<n_cells;xyz+=16) {
	    if (selected[xyz+chadc]) {
              measure = TRUE;
              processed[xyz+chadc] = 1;
              AIQ_set_color (ec_cell[xyz+chadc], GTK_STATE_NORMAL,(char *)"violet",0);
	      set_code(xyz+chadc,cell[xyz+chadc].Refcode);
	  }
	}
      }
    }
    if (measure) {
      time(&modtime);
      modtime++;
      read_selection(NULL,NULL);
      return TRUE;
    }
  }
  for (xyz=0; xyz<n_cells;xyz++) {
      if(selected[xyz]) {
	processed[xyz] = 1;
        AIQ_set_color (ec_cell[xyz], GTK_STATE_NORMAL,(char *)"green",0);
      }
  }
  sens_fbutton(TRUE);
  return FALSE;
}
void read_one( GtkWidget *widget,
               gpointer   data )
{
int xyz,chn_start;
  zero_reset(NULL,NULL);
  chadc = -2;
  measure = FALSE;
  cancel_flag = 0;
  for (xyz=0; xyz<n_cells;xyz+=16) {
    for(chn_start=0; chn_start<16; chn_start++) {
      if ( cell[xyz+chn_start].select) {
        chadc = chn_start -1;
	break;
      }
    }
    if (chadc != -2) break;           
  }
  for (xyz=0; xyz<n_cells;xyz++) {
    selected[xyz] = cell[xyz].select;
  }
  g_print("chadc = %i\n",chadc);
  sens_fbutton(FALSE);
  read_id = gtk_timeout_add (500,(GtkFunction) RdOne,NULL);
}

#ifdef ROOTAP
void mod_histos(int nhisto) {
int x,y,z;
char histoname[46];
  int strt=0,fin=n_cells;
  if(nhisto != n_cells) {strt = nhisto; fin = nhisto+1;}
  for (int xyz=strt;xyz<fin;xyz++) {
    if(cell[xyz].select || nhisto != n_cells) {
      if (cell[xyz].Pnbins != parameters.Pnbins || cell[xyz].minPbin != parameters.minPbin
        || cell[xyz].Pbin != parameters.Pbin) {
        if (nhisto == n_cells) {
          cell[xyz].Pnbins = parameters.Pnbins;
          cell[xyz].minPbin = parameters.minPbin;
          cell[xyz].Pbin = parameters.Pbin;
        }
        histoPed[xyz]->Delete();
	lin2xyz(xyz,&x,&y,&z);
        sprintf(histoname,"PED_%1i-%1i-%1i",x,y,z);
        histoPed[xyz]= new TH1F(histoname,"", cell[xyz].Pnbins,
                                             cell[xyz].minPbin,
                                             cell[xyz].minPbin+(cell[xyz].Pnbins*cell[xyz].Pbin));
      }
      if (cell[xyz].Lnbins != parameters.Lnbins || cell[xyz].minLbin != parameters.minLbin
        || cell[xyz].Lbin != parameters.Lbin) {
        if (nhisto == n_cells) {
          cell[xyz].Lnbins = parameters.Lnbins;
          cell[xyz].minLbin = parameters.minLbin;
          cell[xyz].Lbin = parameters.Lbin;
        }
        histoLed[xyz]->Delete();
	lin2xyz(xyz,&x,&y,&z);
        sprintf(histoname,"LED_%1i-%1i-%1i",x,y,z);
        histoLed[xyz]= new TH1F(histoname,"", cell[xyz].Lnbins,
                                             cell[xyz].minLbin,
                                             cell[xyz].minLbin+(cell[xyz].Lnbins*cell[xyz].Lbin));
      }
    }
  }
}
#endif /* ROOTAP */

void accept_parameters( GtkWidget *widget,
               OptionsSpin   *set_options_spin )
{
  parameters.statistic =
               gtk_spin_button_get_value_as_int(set_options_spin->statistic);
  parameters.tolerance =
               gtk_spin_button_get_value_as_float(set_options_spin->tolerance);
  parameters.electron_peak =
               gtk_spin_button_get_value_as_int(set_options_spin->electron_peak);
  parameters.minph =
               gtk_spin_button_get_value_as_int(set_options_spin->minph);
  parameters.maxph=
               gtk_spin_button_get_value_as_int(set_options_spin->maxph);
  parameters.minres =
               gtk_spin_button_get_value_as_int(set_options_spin->minres);
  parameters.maxres =
               gtk_spin_button_get_value_as_int(set_options_spin->maxres);
  parameters.maxfactor =
               gtk_spin_button_get_value_as_float(set_options_spin->maxfactor);
  parameters.Pnbins =
               gtk_spin_button_get_value_as_int(set_options_spin->Pnbins);
  parameters.minPbin=
               gtk_spin_button_get_value_as_float(set_options_spin->minPbin);
  parameters.Pbin =
               gtk_spin_button_get_value_as_float(set_options_spin->Pbin);
  parameters.Lnbins =
               gtk_spin_button_get_value_as_int(set_options_spin->Lnbins);
  parameters.Lbin =
               gtk_spin_button_get_value_as_float(set_options_spin->Lbin);
//  parameters.minLbin =
//               gtk_spin_button_get_value_as_float(set_options_spin->minLbin);
  parameters.signcut =
               gtk_spin_button_get_value_as_float(set_options_spin->signcut);
  parameters.scale =
               gtk_spin_button_get_value_as_float(set_options_spin->scale);

  gtk_label_set_text
		(GTK_LABEL(GTK_BUTTON(message_button)->child),(char *)"  ");
  gtk_widget_set_sensitive(message_button, FALSE);
  if ((parameters.minph > parameters.maxph) || (parameters.minres > parameters.maxres)) {
    gtk_label_set_text
	(GTK_LABEL(GTK_BUTTON(message_button)->child),(char *)"wrong parameters, check");
	gtk_widget_set_sensitive(message_button, TRUE);
  }
#ifdef ROOTAP
  mod_histos(n_cells);
#endif
}

void set_options( GtkWidget *widget,
              gpointer   data )
{
  OptionsSpin *set_options_spin;
  static GtkWidget *window = NULL;
  GtkWidget *fixed;
  GtkWidget *button;
  GtkWidget *label;
  GtkAdjustment *adj;
  
    set_options_spin = (OptionsSpin *) malloc(sizeof(OptionsSpin));
  
    if (!window)
    {
      window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
      
      gtk_signal_connect (GTK_OBJECT (window), "destroy",
			  GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			  &window);
      gtk_window_set_title (GTK_WINDOW (window),"Set parameters");
      
      fixed = gtk_fixed_new ();
      gtk_widget_set_usize (fixed, 200,510);
      gtk_container_add (GTK_CONTAINER (window), fixed);
      gtk_widget_show (fixed);
      
      label = gtk_label_new ("Minimum  ph");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,10);
       

      label = gtk_label_new ("Maximum  ph");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,40);

      label = gtk_label_new ("Minimum res");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,70);

      label = gtk_label_new ("Maximum res");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,100);
      
      label = gtk_label_new ("Factor Cur/Ref");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,130);
      
      label = gtk_label_new ("Statistic");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,160);
      
      label = gtk_label_new ("Tolerance %");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,190);
      
      label = gtk_label_new ("Electron Peak");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,220);

      label = gtk_label_new ("#bins Ped.h");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,250);
       

      label = gtk_label_new ("low bin Ped");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,280);

      label = gtk_label_new ("bin  sz ped");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,310);

      label = gtk_label_new ("#bins Led.h");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,340);

      label = gtk_label_new ("bin sz Led");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,370);

      label = gtk_label_new ("Signal   Cut");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,400);
            
      label = gtk_label_new ("Scale  factor");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,430);
      
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.minph, 0.0, 4095.0,
						  1.0, 10.0, 0.0);
      set_options_spin->minph = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->minph), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->minph),120,10);
     
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.maxph, 0.0, 9999.0,
						  1.0, 10.0, 0.0);
      set_options_spin->maxph = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->maxph), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->maxph),120,40);
      
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.minres, 0.0, 4095.0,
						  1.0, 10.0, 0.0);
      set_options_spin->minres = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->minres), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->minres),120,70);
      
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.maxres, 0.0, 4095.0,
						  1.0, 10.0, 0.0);
      set_options_spin->maxres = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->maxres), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->maxres),120,100);
      
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.maxfactor, 1.0, 100.0,
						  0.1, 1.0, 0.0);
      set_options_spin->maxfactor = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_spin_button_set_digits (GTK_SPIN_BUTTON(set_options_spin->maxfactor),2);
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->maxfactor), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->maxfactor),120,130);
      

      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.statistic, 0.0, 1000000.0,
						  1.0, 100.0, 0.0);
      set_options_spin->statistic = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->statistic), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->statistic),120,160);

      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.tolerance, 1.0, 100.0,
						  1.0, 10.0, 0.0);
      set_options_spin->tolerance = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->tolerance), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->tolerance),120,190);

      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.electron_peak, 0.0, 3000.0,
						  1.0, 10.0, 0.0);
      set_options_spin->electron_peak = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->electron_peak), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->electron_peak),120,220);

      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.Pnbins, 20.0, 1000.0,
						  1.0, 10.0, 0.0);
      set_options_spin->Pnbins = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->Pnbins), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->Pnbins),120,250);
     
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.minPbin, 0., 1000.0,
						  1., 10.0, 0.0);
      set_options_spin->minPbin = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->minPbin), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->minPbin),120,280);
      
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.Pbin, 0.1, 10.0,
						  0.1, 1.0, 0.0);
      set_options_spin->Pbin = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_spin_button_set_digits (GTK_SPIN_BUTTON(set_options_spin->Pbin),1);
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->Pbin), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->Pbin),120,310);
      
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.Lnbins, 20.0, 4000.0,
						  1.0, 10.0, 0.0);
      set_options_spin->Lnbins = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->Lnbins), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->Lnbins),120,340);
      
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.Lbin, 0.1, 10.0,
						  1.0, 10.0, 0.0);
      set_options_spin->Lbin = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_spin_button_set_digits (GTK_SPIN_BUTTON(set_options_spin->Lbin),1);
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->Lbin), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->Lbin),120,370);

      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.signcut, 0.0, 100.0,
						  1.0, 10.0, 0.0);
      set_options_spin->signcut = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->signcut), 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->signcut),120,400);

      adj = (GtkAdjustment *) gtk_adjustment_new ((float) parameters.scale, 0.1, 10.0,
						  1.0, 10.0, 0.0);
      set_options_spin->scale = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
      gtk_widget_set_usize (GTK_WIDGET(set_options_spin->scale), 70, 0);
      gtk_spin_button_set_digits (GTK_SPIN_BUTTON(set_options_spin->scale),2);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(set_options_spin->scale),120,430);


      button = gtk_button_new_with_label ("   Close    ");
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (gtk_widget_destroy),
				 GTK_OBJECT (window));
      gtk_widget_show(button);				 
      gtk_fixed_put(GTK_FIXED (fixed), button,20,470);
      button = gtk_button_new_with_label ("  Accept  ");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (accept_parameters),
				 set_options_spin);
      gtk_fixed_put(GTK_FIXED (fixed), button,110,470);
      gtk_widget_show(button);				 
    }
    if (!GTK_WIDGET_VISIBLE (window))
       gtk_widget_show_all (window);
    else  {
      gtk_widget_destroy (window);
      free (set_options_spin);
    }
/*      g_print("set_options options\n");*/
}


void print_results( GtkWidget *widget,
               gpointer   output )
{
FILE *ecal_open;
GString *filename;
int x,y;
  filename = g_string_new ("ecal.res");
  if (!(ecal_open=fopen (filename->str,"w"))) {
/*	g_print("print results\n");*/
	return;
  }
  fprintf(ecal_open,"%s",output);
  fclose(ecal_open);
//  system("xprint -p 888-1014-hp -h -o portrait ecal.res");
  system("lpr ecal.res");
}
static char *output=NULL;

void close_show( GtkWidget *widget,
                 GtkWidget *window)
{
char sel_color = FALSE;
  for (int xyz=0;xyz<n_cells;xyz++) {
      if(cell[xyz].select) {
        if (!sel_color) {
          sel_color=TRUE;  
	       AIQ_set_color (GTK_WIDGET(ec_cell[xyz]), GTK_STATE_NORMAL,(char *)"green",0);
        }
        else {
          gtk_widget_set_style (ec_cell[xyz],newstyle);
        }
    }
  }
//  g_print("free %x\n",output);
  free(output);
  gtk_widget_destroy(window);
}
void show_status( GtkWidget *widget,
               gpointer   data )
{
  static GtkWidget *window = NULL;
  GtkWidget *box1;
  GtkWidget *box2;
  GtkWidget *hbox;
  GtkWidget *button;
  GtkWidget *check;
  GtkWidget *separator;
  GtkWidget *table;
  GtkWidget *vscrollbar;
  GtkWidget *text;
  GdkFont *fixed_font;
  char *ccolor;
  int x,y,z,l=0;
  char print;
  float RLedRef;
  float MeanPhSel = 0., MeanRefSel = 0., MeanElSel = 0.;
  float MeanPhDsp = 0., MeanRefDsp = 0., MeanElDsp = 0.;
  int  nSel = 0, nDsp = 0;
//  time_t t;
//  time_t time(time_t *t);
//  char *ctim(const time_t *timep);
  
  if (!window) {
      window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
      gtk_widget_set_usize (window, 780, 550);
      gtk_window_set_policy (GTK_WINDOW(window), TRUE, TRUE, FALSE);  
      gtk_signal_connect (GTK_OBJECT (window), "destroy",
			  GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			  &window);
      gtk_window_set_title (GTK_WINDOW (window), "ECAL STATUS");
      gtk_container_set_border_width (GTK_CONTAINER (window), 0);


      box1 = gtk_vbox_new (FALSE, 0);
      gtk_container_add (GTK_CONTAINER (window), box1);
      gtk_widget_show (box1);


      box2 = gtk_vbox_new (FALSE, 10);
      gtk_container_set_border_width (GTK_CONTAINER (box2), 10);
      gtk_box_pack_start (GTK_BOX (box1), box2, TRUE, TRUE, 0);
      gtk_widget_show (box2);


      table = gtk_table_new (2, 2, FALSE);
      gtk_table_set_row_spacing (GTK_TABLE (table), 0, 2);
      gtk_table_set_col_spacing (GTK_TABLE (table), 0, 2);
      gtk_box_pack_start (GTK_BOX (box2), table, TRUE, TRUE, 0);
      gtk_widget_show (table);
      /* Create the GtkText widget */
      text = gtk_text_new (NULL, NULL);
/*      gtk_text_set_editable (GTK_TEXT (text), TRUE);*/
      gtk_table_attach (GTK_TABLE (table), text, 0, 1, 0, 1,
			GtkAttachOptions(GTK_EXPAND | GTK_SHRINK | GTK_FILL),
			GtkAttachOptions(GTK_EXPAND | GTK_SHRINK | GTK_FILL), 0, 0);
      gtk_widget_show (text);

      /* Add a vertical scrollbar to the GtkText widget */
      vscrollbar = gtk_vscrollbar_new (GTK_TEXT (text)->vadj);
      gtk_table_attach (GTK_TABLE (table), vscrollbar, 1, 2, 0, 1,
			GtkAttachOptions(GTK_FILL), GtkAttachOptions(GTK_EXPAND | GTK_SHRINK | GTK_FILL), 0, 0);
      gtk_widget_show (vscrollbar);
      fixed_font = gdk_font_load ("-misc-fixed-medium-r-*-*-*-140-*-*-*-*-*-*");

      gtk_widget_realize (text);

      /* Freeze the text widget, ready for multiple updates */
     gtk_text_freeze (GTK_TEXT (text));

     output = (char *)malloc(100*(n_cells+6));
     time(&t);
     if (output) {
       l += sprintf(output," ECAL LED %s\n",ctime(&t));
       l += sprintf((output+l),"x y z       ADC   Ped     Led   RMS    Ref  Factor   Elcal Hvcl HvRf HVcd\n\n");
       for (int xyz=0;xyz<n_cells;xyz++){
	   if (cell[xyz].select) {
	     nSel++;
	     flPh = cell[xyz].Led;
	     flPh = ((flPh > 0.) ? flPh : 0.5);
	     Disp = cell[xyz].LedRms - (flPh*flPh);
	     Disp = ((Disp > 0.) ? (sqrt(Disp)) : 0.);
	     MeanPhSel += flPh;
	     MeanRefSel += cell[xyz].LedRef;
	     MeanElSel += cell[xyz].ElectronCal;
	     print = flPh <= parameters.minph || flPh >= parameters.maxph;
	     print = print || Disp < parameters.minres || Disp > parameters.maxres;
             if (cell[xyz].Led < 1) RLedRef = -99.99;
             else {
               if (cell[xyz].LedRef > 0) {
 	         RLedRef =  (float)cell[xyz].Led /(float) cell[xyz].LedRef;
        	 if (RLedRef > 99.99) RLedRef = 99.99;
        	 if (RLedRef < 1.) RLedRef = -1./RLedRef;
               }
               else 
        	 RLedRef = 99.99;
             }
             if (fabs(RLedRef) > parameters.maxfactor) {
               if (RLedRef > 0.) ccolor = (char *)"violet";
               else ccolor = (char *)"yellow";
               print = TRUE;
             }
             else ccolor = (char *)"green";
	     if (print) {
	       lin2xyz(xyz,&x,&y,&z);
               l += sprintf((output+l),"%1d-%1d-%1d",x,y,z);
/*               if (mapcc[xyz].srcId <MIN_SrcId) l += sprintf((output+l),"    %02d-%02d ",mapcc[xyz].port,mapcc[xyz].chnl);
               else */ l += sprintf((output+l)," %3d-%02d-%02d",mapcc[xyz].srcId,mapcc[xyz].port,mapcc[xyz].chnl);
//               l += sprintf((output+l)," %5.1f  %6.1f %5.1f  %6.1f %6.1f %7.2f  %6.1f %4d %4d %4d\n",
               l += sprintf((output+l)," %5.1f  %6.1f %5.1f %6.1f %7.2f  %6.1f %4d %4d %4d\n",
	    		  cell[xyz].Ped,flPh, Disp, cell[xyz].LedRef,
		             /*cell[xyz].Time,*/RLedRef, cell[xyz].ElectronCal,cell[xyz].Hvcal,cell[xyz].Refcode,cell[xyz].Hvcode);
      /*         l += sprintf((output+l),"%02d %02d  %4d  %4d %5d  %4d %6.2f  %4d\n",
	    	             x, y, cell[xyz].Led, cell[xyz].LedRms, cell[xyz].LedRef,
		             cell[xyz].RefRms,RLedRef, cell[xyz].ElectronCal);
      */
               nDsp++;
               MeanPhDsp += cell[xyz].Led;
	       MeanRefDsp += cell[xyz].LedRef;
	       MeanElDsp += cell[xyz].ElectronCal;
               color_cells(ec_cell[xyz],ccolor);
             }
           } 
       }
       if (nSel) {
         MeanPhSel  /= nSel;
	 MeanRefSel /= nSel;
	 MeanElSel  /= nSel;
       }
       l += sprintf((output+l),"\n Ch Sel   %5d        %6.1f        %6.1f                 %6.1f Cur/Ref %6.2f\n",
       		nSel,MeanPhSel, MeanRefSel, MeanElSel,MeanPhSel/(MeanRefSel+0.00001) );
       if (nDsp) {
         MeanPhDsp  /= nDsp;
	 	 MeanRefDsp /= nDsp;
	     MeanElDsp  /= nDsp;
       }
       l += sprintf((output+l),"\n Ch Dsp   %5d        %6.1f        %6.1f                 %6.1f Cur/Ref %6.2f\n",
       		nDsp,MeanPhDsp, MeanRefDsp, MeanElDsp,MeanPhDsp/(MeanRefDsp+0.00001) );
       gtk_text_insert (GTK_TEXT (text), fixed_font, &text->style->black, NULL,
		   output, -1);
     }
     hbox = gtk_hbutton_box_new ();
     gtk_box_pack_start (GTK_BOX (box2), hbox, FALSE, FALSE, 0);
     gtk_widget_show (hbox);
     button = gtk_button_new_with_label ("close");
     gtk_widget_show (button);
/*     gtk_signal_connect_object (GTK_OBJECT (window), "clicked",
				 GTK_SIGNAL_FUNC(close_show),
				 output);*/
     gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC(close_show),
				 GTK_OBJECT (window));
     gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
     button = gtk_button_new_with_label ("print");
     gtk_widget_show (button);
     gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (print_results),
				 output);
     gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);


   }
   if (!GTK_WIDGET_VISIBLE (window))
     gtk_widget_show (window);
   else
     gtk_widget_destroy (window);

/*	g_print("show status\n");*/
}
void exit_ecal( GtkWidget *widget,
               gpointer   data )
{
#ifdef ROOTAP
  hvsetHst->Modified();
  hvsetHst->Update();
  file->Write("",TObject::kOverwrite);
#endif
	g_print("ecal finished\n");
	gtk_main_quit ();
}


static void running_flash(gpointer label)
{
   if ((count%1000) != 1) return;
   if (flash) {
	gtk_label_set_text
		(GTK_LABEL(GTK_BUTTON(message_button)->child),(char *)label);
	gtk_widget_set_sensitive(message_button, TRUE);
   }
   else	{
/*	gtk_label_set_text
		(GTK_LABEL(GTK_BUTTON(message_button)->child),(char *)"  ");*/
	gtk_widget_set_sensitive(message_button, FALSE);
  }
  flash = !flash;
}
#ifdef ROOTAP
void SosCanvas()
{
  TSeqCollection *canvases;
  canvases = gROOT->GetListOfCanvases ();
  TCanvas *curcan = (TCanvas *) canvases->First ();
  while (curcan != NULL && curcan != hvsetHst) {
    curcan = (TCanvas *) canvases->After((TCanvas *) curcan);
  }

  if (curcan == NULL)
    if (hvsetHst != NULL) hvsetHst = NULL;
// end of check

  if (hvsetHst == NULL) {
//   create canvas
    hvsetHst = new TCanvas("hvsetHst","ECAL HVSET  Histograms",100,20,600,680);
    hvsetHst->Clear();
  }
}
#endif
gint onhvpm(void) {
  int hvon = 0;
  for (int xyz=0;xyz<n_cells;xyz++) {
      if (cell[xyz].Hvcode != 0) hvon++;      
  }
  return hvon;
}

void sliv(void) {
#ifdef FireLED
  int i;
  unsigned short busy;
//  if (pmhvon > 200) system("TCSC waitoffspill");
  for (i=0;i<10000;i++) {
    vme_write(FireLed,1);
  }
#endif
}  
void zero_reset(GtkWidget *widget,
                          gpointer data)
{
int xyz;
int hvon = 0;
int hvoff = 0;
 for (xyz=0;xyz<n_cells;xyz++) {
    if (cell[xyz].Hvcode == 0) continue;
    hvon++;    
    if (!cell[xyz].select) continue;
    hvoff++;
    set_code(xyz,0);
//    if (restore_protection(chn)) AIQ_set_color (ec_cell[xyz], GTK_STATE_NORMAL,"green",0);
  }
  pmhvon = hvon - hvoff;
  sliv();
}
static gint flash_led(gpointer data) {
#ifdef FireLED
  int i;
  unsigned short busy;
  for (i=0;i<1000;i++) {
   vme_write(FireLed,1);
  }
#endif
  count++;
  if (count%2) gtk_widget_set_sensitive(message_button, TRUE);
  else  gtk_widget_set_sensitive(message_button, FALSE);
//	if ((count < 300) && !cancel_flag) return TRUE;
  if (!cancel_flag) return TRUE;
  flash_id = 0;
  return FALSE;
}
void flashing() {
#ifdef FireLED
  int i;
  if (flash_id) return;
  count = 0;
  cancel_flag = FALSE;
  flash_id = gtk_idle_add ((GtkFunction) flash_led, NULL);
#endif 
}

void make_step( GtkWidget *widget,
                 GtkSpinButton   *step_code )
{
short new_code, old_code;
int xyz;
  code_step = gtk_spin_button_get_value_as_int(step_code);
  if (code_step != 0) {
    for (xyz=0;xyz<n_cells;xyz++) {
      if (cell[xyz].select !=0) {
	old_code = cell[xyz].Hvcode;
	new_code = old_code + code_step;
	new_code = (new_code > 0) ? new_code : 0;
	new_code = (new_code < max_hvcode) ? new_code : max_hvcode;
//	if (new_code == old_code) continue;
#ifdef DEBUG
	g_print(" set for cell[%d] Hvcode = %4d\n",xyz,new_code);
#endif
	set_code(xyz,new_code);
      }
    }
  }
}
static void
hv_code (short code)
{
int i;
  if ((code < 0) || (code > max_hvcode)) return;
  for (i=0;i<n_cells;i++) {
    if (!tune_xyz[i]) continue;
//#ifdef DEBUG
//    g_print(" set for cell[%04d] Hvcode = %4d\n",i,code);
//#endif
    set_code(i,code);
  }
}
static gint set_cell_hv(gpointer data) {
  int x,y;
  if (curr_cell >= 0) AIQ_set_color (ec_cell[curr_cell], GTK_STATE_NORMAL,(char *)"green",0);
  if ((++curr_cell == n_cells) || cancel_flag) {
    set_id = 0;
    sens_fbutton(TRUE);
    set_done=1;
    time(&modtime);
/*    pmhvon = 0;
    for (x=0;x<n_col;x++) {
      for (y=0;y<n_row;y++) {
        if (cell[xyz].Hvcode != 0) pmhvon++;      
      }
    }*/
    return FALSE;
  }
  while (!cell[curr_cell].select) {
    curr_cell++;
    if (curr_cell == n_cells) {
      time(&modtime);
      set_id = 0;
      set_done = 1;
      sens_fbutton(TRUE);
/*      pmhvon = 0;
      for (x=0;x<n_col;x++) {
        for (y=0;y<n_row;y++) {
          if (cell[xyz].Hvcode != 0) pmhvon++;      
        }
      }*/
      return FALSE;
    }
  }
  AIQ_set_color (ec_cell[curr_cell], GTK_STATE_NORMAL,(char *)"violet",0);
  if (data == NULL) set_code(curr_cell,hvcode);
  else set_code(curr_cell,cell[curr_cell].Refcode);
  set_done=0;
  return TRUE;
}
static void hv_code_sel( GtkWidget *widget,
                 GtkSpinButton   *step_code ) {
   if (set_id) return;
   cancel_flag = 0;
   hvcode = gtk_spin_button_get_value_as_int(step_code);
   if (hvcode < 0) hvcode = 0;
   if (hvcode > max_hvcode) hvcode = max_hvcode;
   curr_cell = -1;
   sens_fbutton(FALSE);
   set_id = gtk_idle_add ((GtkFunction) set_cell_hv, NULL);
} 

static void
step_change( GtkWidget *widget,
              gpointer   data )
{
  static GtkWidget *window = NULL;
  GtkWidget *fixed;
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *step_code;
  GtkAdjustment *adj;

    if (!window)
    {
      window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
      
      gtk_signal_connect (GTK_OBJECT (window), "destroy",
			  GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			  &window);
      gtk_window_set_title (GTK_WINDOW (window),"Select code / step");
      
      fixed = gtk_fixed_new ();
      gtk_widget_set_usize (fixed, 200,150);
      gtk_container_add (GTK_CONTAINER (window), fixed);
      gtk_widget_show (fixed);
      
      label = gtk_label_new ("Value/Step");
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,10);
      adj = (GtkAdjustment *) gtk_adjustment_new ((float) code_step, -1023.0, 1023.0,
						  1.0, 10.0, 0.0);
      step_code = gtk_spin_button_new (adj, 0.0, 0);
      gtk_widget_set_usize (step_code, 70, 0);
      gtk_fixed_put (GTK_FIXED (fixed), step_code,120,10);
       
      button = gtk_button_new_with_label ("LED Flashing");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (flashing),
				 NULL);
      gtk_fixed_put(GTK_FIXED (fixed), button,20,110);
      gtk_widget_show(button);
							 
      button = gtk_button_new_with_label ("   Close    ");
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (gtk_widget_destroy),
				 GTK_OBJECT (window));
      gtk_fixed_put(GTK_FIXED (fixed), button,110,110);
      gtk_widget_show(button);
							 
      button = gtk_button_new_with_label ("Make Step");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (make_step),
				 step_code);
      gtk_fixed_put(GTK_FIXED (fixed), button,110,80);
      gtk_widget_show(button);
      button = gtk_button_new_with_label ("Set  value");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (hv_code_sel),
				 step_code);
      gtk_fixed_put(GTK_FIXED (fixed), button,20,80);
      gtk_widget_show(button);				 
    }
    if (!GTK_WIDGET_VISIBLE (window))
       gtk_widget_show_all (window);
    else  {
      gtk_widget_destroy (window);
    }
}
/*
void meanVal(void) {
int x,y;
  for (int xyz=0;xyz<n_cells;xyz++){
    if (cell[xyz].select) {
      if (cell[xyz].statistic) {
	    flPh = cell[xyz].Led;
	    flPh = ((flPh > 0.) ? flPh : 0.5);
	    Disp = cell[xyz].LedRms - (flPh*flPh);
	    Disp = ((Disp > 0.) ? (sqrt(Disp)) : 0.);
	    cell[xyz].LedRms = ((flPh > 1.) ? Disp  : 0.);
      }
      else {
	cell[xyz].Led = 0.;
	cell[xyz].LedRms = 0.;
      }
    }
  }
}
*/
#ifdef DAQ
#define DESCRIPTION "ECAL HVSET , DAQ READOUT"
#define VID "1.12"
#define HEAD 3
#define TRAIL 7
char mpDaemonMainIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                        """ """ VID """ compiled """ __DATE__ """ """ __TIME__;
struct subeventStruct subevent[4*96];
char *dataBuffer=NULL;
char *dataSource;
char filename[128];
char listRunsNumbers[50];
char listruns = -1;
char onlDataSource = FALSE;
//#define firstEB 22
//#define lastEB 37
static int nruns = 0;
static int nmbEB = 0;
static int nusedEB = 16;
static int runNmb = 0, usedChunk = 0;
static int nusedrun = -1;
static char chunkFound = FALSE;
static char genlist = FALSE;
static char stageqry[128];
static char stageing = FALSE;
static char stop_proc_flag;
FILE *listRuns;


int getDataSource() {

int i, lastRun;

static int runsList[500];
static int EB[25]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//char castor[]={"/castor/cern.ch/compass/data/2006/raw/test/cdr"};
static char castor[60];
static char extention[6];
char stagein[128];
  if (!offline) return 1;
  if (listruns == 0) return 0;
  else if (listruns == -1) {
     listruns = 0;
     return 1;
  }
  else if (stageing) {
    if (system(stageqry)) return -1;
    dataSource = &stageqry[12];
    stageing = FALSE;
    return 1;
  }
  else if (!genlist) {
/* Read list of runs file and prepare list of data files */
    g_print("read ListRunNubmbers\n");
    if (listRuns = fopen(listRunsNumbers,"r")) {
      fscanf(listRuns,"%s %s\n",castor,extention);
      g_print("%s %s\n",castor,extention);
      while (1) {
        if (fscanf(listRuns,"%d",&runNmb) == EOF) break;
        if (runNmb < 0) {
          if ((runNmb = abs(runNmb)) <= (lastRun = runsList[nruns-1])) {
            g_print("*** wrong sequance in the list of runs: %d -%d\n",lastRun,runNmb);
            exit (1);
          }
          for (i = lastRun+1; i <= runNmb; i++) runsList[nruns++] = i;
        }
        else if (runNmb >= firstEB && runNmb <= lastEB) EB[nmbEB++] = runNmb;  
        else runsList[nruns++] = runNmb;
      }
      genlist = TRUE;
      runNmb = 0;  
    }
    else {
      fprintf( stderr,"can't open list of runs file %s\n",listRunsNumbers);
      exit ( 1);
    }
    if (nmbEB ) {
      g_print("EventBuilders booked: ");
      for (i = 0; i < nmbEB; i++) { printf(" %02d",EB[i]);}
      g_print("\n");
    }
    else {
      g_print("*** Error, no Event Builders booked ***\n");
      exit (1);
    }
    g_print("\n%d files in runsList:",nruns);
    for (i = 0; i < nruns; i++) {
      g_print("%s%5d",((i%10 == 0) ? "\n" : " "),runsList[i]);
    }
    g_print("\n");
  }
  dataSource = NULL;
  while (1) {
    if (++nusedEB >= nmbEB) {  /*last Event builder have been done, look for next chunk */
      nusedEB = 0;
      if (chunkFound) {
        usedChunk++;
        chunkFound = FALSE;
      }
      else runNmb = 0;
      if (runNmb == 0) { 
        if (nusedrun++ >= nruns) return 0;
        runNmb = runsList[nusedrun];
        usedChunk = 1;
        chunkFound = FALSE;
      }
    }
    if (runNmb == 0) return 0;
    sprintf(filename,"rfdir %s%02d://data/cdr/cdr%02d%03d-%06d.%s",castor,EB[nusedEB],EB[nusedEB],usedChunk,runNmb,extention);
    printf("\n");
	fflush( stdout );
    if (!system(filename)) {
      chunkFound = TRUE;
      dataSource = &filename[6];
      printf("\ndataSource: %s\n",&filename[6]);
      return (1);
    }
    else {
      return -1;
    }
  }
}



/* Setup monitoring source, parameters and characteristics */
void monitorSetup( char *ourName ) {
  int status;
  
  if ( ( status = monitorDeclareMp( (char*)DESCRIPTION """ V""" VID ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot declare MP, status: %s\n",
	     monitorDecodeError( status ) );
    exit( 1 );
  }

  if ( dataSource == NULL ) {
//    usage( ourName );		/* We must have a data source! */
    exit( 1 );
  }

  if ( ( status = monitorSetDataSource( dataSource ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot attach to monitor scheme, status: %s\n",
	     monitorDecodeError( status ) );
    exit( 1 );
  }

  if ( ( status = monitorSetSwap( FALSE, FALSE ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot set swpping mode, status: %d (0x%08x), %s",
	     status, status, monitorDecodeError( status ) );
    exit( 1 );
  }

  if ( ( status = monitorSetNowait() ) != 0 ) {
    fprintf( stderr,
	     "Cannot set Asynchronous mode, status: %s\n",
	     monitorDecodeError( status ) );
    exit( 1 );
  }


  if (sel_calibr) {
//    status = monitorDeclareTableWithAttributes( monitorTableYes );
    status = monitorDeclareTable( monitorTableYes );
//    status = monitorDeclareTable(monitorTablePhys);
    if ( status != 0 ) {
      fprintf( stderr,
	       "Error during monitoring table declaration with attributes %s, status: %d\n",
	       monitorDecodeError( status ),status );
      exit( 1 );
    }
    g_print("calibration data\n");
//    trigger_mask = 1;
  }
  else {
    trigger_mask &= 0xfe;
    if (!trigger_mask) {
      g_print("no trigger selected\n");
      error_message((char*)"no trigger selected");
    }
    status = monitorDeclareTable(monitorTablePhys);
    if ( status != 0 ) {
      fprintf( stderr,
	       "Error during monitoring table declaration  %s, status: %d\n",
	       monitorDecodeError( status ),status );
      exit( 1 );
    }
    g_print("physical events, trigger mask: %x\n",trigger_mask);
  }
/*
  status = monitorDeclareTable(monitorTablePhys);
  if ( status != 0 ) {
    fprintf( stderr,
	     "Error during monitoring table declaration  %s, status: %d\n",
	     monitorDecodeError( status ),status );
    exit( 1 );
  }
*/
} /* End of monitorSetup */


/* Get the next event (if possible) */
#define DOTS_AT 5000
int getNextEvent() {
      return monitorGetEventDynamic( (void**)&dataBuffer );
} /* End of getNextEvent */

void initDataSource( int argc, char **argv ) {
  
  if ( strcmp( argv[1], "-l" ) == 0 ) {
    sscanf( argv[2], "%s", &listRunsNumbers );
    printf("list of Runs file %s\n",listRunsNumbers);
    offline = TRUE;
    listruns = 1;
  }
  else
    dataSource = argv[1];
    if (!dataSource) return;
    if (*dataSource != '@' && *dataSource != ':') offline = TRUE;
    g_print("dataSource %s, offline %d\n",dataSource, offline); 
}

void errorRep(void) {
  char* errors[10] = {(char*)"ev_nb",(char*)"blk_sz",(char*)"ovfl",(char*)"hd_id",(char*)"hd_key",(char*)"dt_key",
		       (char*)"dtl_nz",(char*)"ch_nr",(char*)"nu",(char*)"i_cut"};
  int i;
  if(!errorFlag) return;
  printf("\nerrorFlag %x");
  for (i=0;i<11;i++)
    if (errorFlag & (1<<i)) printf(" %s wd %d ",errors[i],errorLoc[i]);
  printf("\n");
}
void clearError(void) {
  int i;
  errorFlag = 0;
  for (i=0;i<32;i++) errorLoc[i]=0;
}
void locError(int location, int nError) {
  errorFlag |= (1<<nError);
  if (!errorLoc[nError]) errorLoc[nError] = location;
}


#define f_sample_0 0
#define l_sample_0 5
#define f_sample_1 8
#define l_sample_1 31
#define f_sample_2 30
#define l_sample_2 31
static int SrcIdEventNum;
            
int decodeMSADC(int evsize, int *data_buf) {
  int err;
  int ev_type;
  int src_id;
  int ev_size;
  int stat;
  int spill_nr;
  int event_nr;
  int format_nr, nr_err, tcs_err, status_nr;
  int j,iwrd,idata,intdata,k,last_iwrd,chn,nchn,ch_nr,last_chn;
  int adcId, oldId = 0;
  int gesica, ges_mod;
  int err_fmt;
  static int chnl_wd = 1;
  int sadc_ch;
  int offset;
  int intval;
  short sample[128];
  
  int sum0 = 0, sum0_even = 0, sum0_odd = 0, sum1 = 0;
  int maxamp_even,maxamp_odd;
  int xyz;
  float ped,signal,ped_even,ped_odd;
  int ch_stat;
  bool rcd;
  struct msadc_hd *adc_hd;
  struct msadc_chhd *chhd;
  struct msadc_data12 *sample_dt12;
  struct msadc_data6 *sample_dt6;
  int indSmpl, indHdr;
  struct slinkHeaderStruct *gsc = (struct slinkHeaderStruct *)data_buf;  
//**/  g_print("gsc->src_id %d ev_size %d type %d ev_number %d\n",gsc->src_id,gsc->ev_size,gsc->ev_type,gsc->event_nr);  
  iwrd=HEAD;
  if (HEAD==3) {
    gesica = (gsc->src_id - MIN_SrcId)<<9;
  }
  last_iwrd = (evsize>>2) - TRAIL;
  if (iwrd >= last_iwrd) return cells_on; 
//  g_print("/nsadc_ch: ");
  do {    
    adc_hd = (struct msadc_hd *) &data_buf[iwrd];
/* check header format */
//    g_print("event_nr  %x adc_hd: ev_nr %x blk_sz %x mode %1d id %1d port %1d key %1d\n",event_nr,adc_hd->ev_nr,adc_hd->blk_sz,adc_hd->mode,adc_hd->id,adc_hd->port,adc_hd->key);
    if (adc_hd->ev_nr != (gsc->event_nr&0xfff))  {
    	locError(iwrd,0);   // inconsistent event number
        g_print("SrcId %d event_nr %x adc_hd->ev_nr %x id %d port %d\n",gsc->src_id,gsc->event_nr,
        adc_hd->ev_nr,adc_hd->id,adc_hd->port);
    }
    else {
      SrcId = gsc->src_id;
      SrcIdEventNum = adc_hd->ev_nr;
    }
    if (adc_hd->port < oldId) locError(iwrd,3);
    oldId = adc_hd->port;
    if (adc_hd->key < 3) locError(iwrd,4);
    if (errorFlag & FATAL_ERROR) {
      time(&t);
      g_print("SrcId %d ** Fatal error %x %s",gsc->src_id,errorFlag,ctime(&t));
//      g_print("SrcId %d ** Fatal error %x\n",gsc->src_id,errorFlag);
      break;
      return cells_on;
    }
    iwrd++;
    if (adc_hd->blk_sz == 1) continue;  // only header word present
//    g_print("adc_hd->port %d blk_size %d\n",adc_hd->port,adc_hd->blk_sz);
    idata = iwrd;
    last_chn = -1;
    ges_mod = gesica + (adc_hd->port << 6) + (adc_hd->id << 4);
    for (chn=0;chn<16;chn++) {
      chhd = (struct msadc_chhd *) &data_buf[idata];
      ch_nr = chhd->ch_nr;
      if (ch_nr <= last_chn) locError(intdata,7);
      offset = chhd->offset;
      rcd = chhd->rcd;
      if (rcd) {
        chnl_wd = chhd->samples/5;
	if (chhd->samples%5) chnl_wd+=1;
      }
      else {
        chnl_wd = chhd->samples/2;
	if (chhd->samples%2) chnl_wd+=1;       
      }
      sadc_ch = ges_mod + ch_nr;
      xyz = map_ms[sadc_ch];
//**/      g_print("idata %d ev_nr %d port %d id %d ch_nr %d sadc_ch %d xyz %d\n",idata,adc_hd->ev_nr,adc_hd->port,adc_hd->id,chhd->ch_nr,sadc_ch,xyz);
      if (xyz != -1) {
	int trig_ch = -1;
	int bgo_ch = -1;
	if (xyz >= n_cells) {
	  if (xyz < n_cells+n_trig) {
	    trig_ch = xyz - n_cells;
	  }
	  else bgo_ch = xyz - (n_cells+n_trig);
//	  g_print("trig_ch %2i xyz %3i\n",trig_ch,xyz);
	}
	if (trig_ch != -1 || ((xyz < n_cells) && processed[xyz])  || (bgo_ch != -1)) {
          if (trig_ch == -1) {
//	    if (trigger_pattern == 0 && !phys_yes)  return cells_on;
//	    else if (trigger_pattern != 0 && (( trigger_pattern & trigger_mask) == 0)) return cells_on;
	    if (sel_phys && (trigger_pattern != 0 && (( trigger_pattern & trigger_mask) == 0))) return cells_on;
//	    if((trigger_pattern & trigger_mask) == 0) return cells_on;
          }
//	  g_print("trigger_pattern %x sel_phys %x  cells_on %d\n",trigger_pattern,sel_phys,cells_on);
	  indSmpl = idata + 1;
          k = 0;
	  if (!rcd) {
            for (j=0;j<chnl_wd;j++) {
		  sample_dt12 = (struct msadc_data12 *) &data_buf[indSmpl];
		  if (sample_dt12->key != 2) { 
		    if (j > 0 || sample_dt12->key != 1) {
        	    locError(indSmpl,5);
                    g_print("rcd 0 data[%d] = %08x  key = %d \n",j,data_buf[indSmpl],sample_dt12->key);
                    }
		  }
		  sample[k++] = sample_dt12->val_0;
		  sample[k++] = sample_dt12->val_1;
		  indSmpl++;
            }
	  }
	  else {
            for (j=0;j<chnl_wd;j++) {
		  sample_dt6 = (struct msadc_data6 *) &data_buf[indSmpl];
		  if (sample_dt6->key != 2) { 
		    if (j > 0 || sample_dt6->key != 1) {
        	    locError(indSmpl,5);
                    g_print("rcd data[%d] = %08x  key = %d \n",j,data_buf[indSmpl],sample_dt6->key);
                    }
		  }
		  sample[k++] = sample_dt6->val_0 + offset;
		  sample[k++] = sample_dt6->val_1 + offset;
		  sample[k++] = sample_dt6->val_2 + offset;
		  sample[k++] = sample_dt6->val_3 + offset;
		  sample[k++] = sample_dt6->val_4 + offset;
		  indSmpl++;
            }	
	  }
          sum0_even=sum0_odd=0;
          maxamp_even=maxamp_odd=0;
          for (k=f_sample_0;k<=l_sample_0;k++) {sum0_even +=sample[k++]; sum0_odd += sample[k];}
          ped_even =float(sum0_even)*2./(l_sample_0-f_sample_0+1);
          ped_odd  =float(sum0_odd)*2./(l_sample_0-f_sample_0+1);
          ped = 0.5*(ped_even + ped_odd);
          for (k=f_sample_1;k<=l_sample_1;k++) {
	    if (sample[k] > maxamp_even) maxamp_even = sample[k];
            if (sample[++k] > maxamp_odd) maxamp_odd = sample[k];
          }
          signal = float(maxamp_even) - ped_even;
          signal = (float(maxamp_odd) - ped_odd) > signal ? (float(maxamp_odd) - ped_odd) : signal;
          if (trig_ch != -1) {
            for (k=f_sample_0;k<=l_sample_2;k++) histoTrSmpl[trig_ch]->Fill(k,(float)sample[k]);
	    histoTrPed[trig_ch]->Fill(ped_even);
            histoTrPed[trig_ch]->Fill(ped_odd);
            histoTrLed[trig_ch]->Fill(signal);
	    if (signal > trigger_thr) trigger_pattern |= (1 << trig_ch);
	    trigger_pattern &= 3;
	  }
	  else if (bgo_ch == -1) {
            for (k=f_sample_0;k<=l_sample_2;k++) histoSmpl[xyz]->Fill(k,(float)sample[k]);
            histoPed[xyz]->Fill(ped_even);
            histoPed[xyz]->Fill(ped_odd);
            histoLed[xyz]->Fill(signal);
            ch_stat = cell[xyz].statistic;
            cell[xyz].Ped = ((cell[xyz].Ped*ch_stat) + ped)/(ch_stat+1);
    //        cell[xyz].PedRms = ((cell[xyz].PedRms*ch_stat) + (ped*ped))/(ch_stat+1);
            cell[xyz].Led = ((cell[xyz].Led*ch_stat) + signal)/(ch_stat+1);
            cell[xyz].LedRms = ((cell[xyz].LedRms*ch_stat) + (signal*signal))/(ch_stat+1);
            cell[xyz].statistic +=1;
            cells_on++;
	  }
	  else {
            for (k=f_sample_0;k<=l_sample_2;k++) histoBgoSmpl[bgo_ch]->Fill(k,(float)sample[k]);
	    histoBgoPed[bgo_ch]->Fill(ped_even);
            histoBgoPed[bgo_ch]->Fill(ped_odd);
            histoBgoSign[bgo_ch]->Fill(signal);
	  }
	}
      }
      idata += chnl_wd+1;
//**/      g_print("idata-iwrd %d adc_hd->blk_sz-1 %d\n",idata-iwrd,adc_hd->blk_sz-1);
      if (idata-iwrd>=adc_hd->blk_sz-1) break;
    }
    iwrd += adc_hd->blk_sz - 1;    
  } while (iwrd < last_iwrd);
  return cells_on;
}

#define MAXLDC  5
static struct {
  short ldcnr;
  char  *ldcptr;
} ldcnp[MAXLDC];

int scanLDC(char *dataBuffer) {
  char *ptr = dataBuffer;
  int nldc = 0;
  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)dataBuffer;
  if ( TEST_SYSTEM_ATTRIBUTE( ev->eventTypeAttribute, ATTR_SUPER_EVENT ) ) {
/* Superevent */
    int totSize = ev->eventHeadSize;
    int dataSize = ev->eventSize;
    while ( totSize != dataSize ) {
      ptr =(char *) (dataBuffer + totSize );
      ev = (struct eventHeaderStruct *)ptr;
      ldcnp[nldc].ldcnr = ev->eventLdcId;
      ldcnp[nldc].ldcptr = ptr;
      nldc++;
      totSize += ev->eventSize;
    }
  }
  else {

/* Normal event */
    ldcnp[nldc].ldcnr = ev->eventLdcId;
    ldcnp[nldc].ldcptr = ptr;
    nldc++;
  }
  return nldc;
} /* End of scanLDC */

int getNextEquipment(int nldc, int * *eqptr, int *eqlen) {

  static char *ptr;

  struct equipmentHeaderStruct *subeq;
  static int subeqSize, subeqPos,eventSize,totSize;
  static int n;
  if (*eqlen == 0) {
    n = -1;
    subeqSize = subeqPos = eventSize = 0;
  }
//  g_print("subeqSize = %i subeqPos = %i eventSize %i\n",subeqSize,subeqPos,eventSize);
   if (subeqPos + subeqSize < eventSize) {
    subeqPos += subeqSize;
    ptr += subeqSize;
    subeq = (struct equipmentHeaderStruct *)ptr;
    subeqSize = subeq->equipmentSize;
    totSize += subeqSize;
    *eqlen = subeqSize;
    *eqptr = (int *) (ptr + sizeof( *subeq ));
    if (subeqSize < 0 || totSize > eventSize) {
      g_print("n_ldc %i subeqSize %i totSize  %i eventSize %i\n",n,subeqSize,totSize,eventSize);
      return (1);
    }
    return (0);
  }
  if (++n < nldc) {
    ptr = ldcnp[n].ldcptr;
    struct eventHeaderStruct *ev = (struct eventHeaderStruct *)ptr;
    totSize = ev->eventHeadSize;
    eventSize = ev->eventSize;
//    g_print("ldcptr %d sizeof( *ev ) %d ev->eventHeadSize %d eventSize %d totSize %d\n",ptr,sizeof( *ev ),ev->eventHeadSize,eventSize,totSize); 
    if (eventSize == totSize) return 1;
    ptr = ptr + ev->eventHeadSize;
    subeqPos  = sizeof( *ev );
    subeq = (struct equipmentHeaderStruct *)ptr;
    subeqSize = subeq->equipmentSize;
    totSize += subeqSize;
    *eqlen = subeqSize;
    *eqptr = (int *) (ptr + sizeof( *subeq ));
//    g_print("n_ldc %i *eqlen %i *eqptr %x ptr %x subeqSize %i\n",n,*eqlen,*eqptr,ptr,subeqSize);
    return (0);
  }
  return (1);
}
/* Process the event we have received */
int procEvent() {
  char *ptr = (char *) dataBuffer;
  int *eqptr; /* FHH */
  int eqlen = 0;
  int i,k,j,x,y;
  int src_id;
  int status;
  char led = FALSE;
  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)dataBuffer;
  int cells_src;
  for (i=0;i<NbID;i++) SrcCells[i] = 0;
 //  g_print("dataBuffer: %x\n",dataBuffer);
  if ((ev->eventType == CALIBRATION_EVENT) || (ev->eventType == PHYSICS_EVENT)) {
//    if (ev->eventType == CALIBRATION_EVENT) g_print("Type: %x TypeAttribute[0]: %x  LED_TRIGGER_MASK0 %x LED_TRIGGER_MASK1 %x \n",ev->eventType,ev->eventTypeAttribute[0],LED_TRIGGER_MASK0,LED_TRIGGER_MASK1);
    if (sel_calibr) {
      if (ev->eventType != CALIBRATION_EVENT) return 0;
//      else if (ev->eventTypeAttribute[0] != LED_TRIGGER_MASK) return 0;
    }
//    g_print("LED event\n");
    if (!sel_calibr) {
       if (ev->eventType != PHYSICS_EVENT) return 0;
       else if (trigger_mask != 0 && ((ev->eventTriggerPattern[0] & trigger_mask) == 0)) return 0;
    }
    cells_on = 0;
    trigger_pattern = 0;
    int nldc = scanLDC(dataBuffer);
//    g_print("\reventType %X nldc %d",ev->eventType,nldc);
    if (nldc == 0) return 0;
    do {
      status = getNextEquipment(nldc, &eqptr, &eqlen);
      if (status) break;
      src_id=(*eqptr >> 16 & (1<< 10)-1);
//      if (src_id == SRCID) g_print("ev->eventType:%x eqptr %x SourceId %d eqlen %d\n",ev->eventType,eqptr,src_id,eqlen);
	  
//	  exit (1);
      if (src_id < MIN_SrcId || src_id > MAX_SrcId) continue; //* not our Source Id 
//      g_print("nldc %i Source Id %i\n",nldc,src_id);
      clearError(); 
      cells_src = cells_on;
      cells_on = decodeMSADC(eqlen,eqptr);
      SrcCells[src_id-MIN_SrcId] = cells_on-cells_src;
      if(errorFlag /*& FATAL_ERROR*/) SrcErr[src_id-MIN_SrcId]++;
//          printf("src_id %d, cells_on = %d errorFlag = %x\n",src_id,cells_on,errorFlag);
//          if(errorFlag) return 0; 
    } while (!status);
  }
  return cells_on;    
} /* End of decodeEvent */
char proc_events() {
  struct eventHeaderStruct *ev;
  int  status;short errorFlag;
  time_t eventTimestamp;

  int  source;
  int x,y;
  int tryget = 0;
  char errproc;
  static char NODATA = FALSE;
  char message[100];
  if (cancel_flag) {
    processing_id = 0;
    end_measure = TRUE;
    sens_fbutton(TRUE);
    if (dataBuffer) {
      free( dataBuffer );
      g_print("free dataBuffer %x\n",dataBuffer);
      dataBuffer = NULL;
    }
    return FALSE;
  }
  if (!onlDataSource) {
    source = getDataSource();
      g_print("getDataSource: %d\n",source);
    switch (source) {
      case -1:         /* stagein in progress */
        return TRUE;
      case  0:
        gtk_widget_set_sensitive(fbutton[2], FALSE);
        error_message((char *)"dataSource absent");
        cancel_flag = TRUE;
        update_statistic(statistic,0);
//        processing_id = 0;
//        meanVal();
//        sens_fbutton(TRUE);
//        return FALSE;
         return TRUE;
      case  1:
        monitorSetup( (char *)"ECAL" );
        error_message(dataSource);
        onlDataSource = TRUE;
        break;
    }
    g_print("dataSource: %s\n",dataSource);
    time(&request);
  }
  while (tryget++ < 100) {
    if (dataBuffer != NULL) g_print("Buffer is already used: %x\n",dataBuffer);
    status = getNextEvent();
//    g_print("getNextEvent: try %d status %x dataBuffer %x\n",tryget,status,dataBuffer);
    if ( status == MON_ERR_EOF ) {
      /* End-of-file: terminate the monitoring loop */
      error_message((char *)"End of file");
      onlDataSource = FALSE;
      gtk_widget_set_sensitive(message_button, TRUE);
      usleep(1000);     
      return TRUE;
    }
    else if ( status != 0 ) {
      error_message(monitorDecodeError( status ));
      gtk_widget_set_sensitive(message_button, TRUE);
      usleep(1000);
      return TRUE;
    }
    if (dataBuffer != NULL) {
/* Event received OK */
      if (NODATA) {
         error_message(dataSource);
	 NODATA = FALSE;
      }
      errorFlag = 0;
      eventsTot++;
      ev = (struct eventHeaderStruct *)dataBuffer;
      if(ev->eventType == PHYSICS_EVENT) eventsPhys++;
      if(ev->eventType == CALIBRATION_EVENT) eventsCal++;
      eventTimestamp = ev->eventTimestamp;
      runNmb = ev->eventRunNb;
//      if (!find_adcmap(runNmb)) {
 //       error_message((char*)"Mapping for the the run is absent");
//          return TRUE;
//      }
      time(&request);
      if ((time_t)ev->eventTimestamp > modtime) {
//        g_printr("proc stat i\n",statistic);
        if(procEvent() > 0  || tune_id) {
          statistic++;
          if (errorFlag) errEvents++;
//          update_statistic(statistic,cells_on);
        }
      }
      else {
        g_print("modtime %d ev_time %d ev_size %i\r",modtime,ev->eventTimestamp,ev->eventSize);
      }
      update_statistic(statistic,cells_on);
//      g_print("modtime %d ev_time %d\n",modtime,ev->eventTimestamp);
/* If Dynamic mode, free the data buffer and invalidate the pointer */
      free( dataBuffer );
//      g_print("free dataBuffer %x\n",dataBuffer);
      dataBuffer = NULL;
      if (statistic < parameters.statistic || parameters.statistic == 0) return TRUE;
      end_measure = TRUE;
//      meanVal();
/*
      status = monitorFlushEvents();
      status = monitorDeclareTableWithAttributes( monitorTableYes );
      if ( status != 0 ) {
        fprintf( stderr,
	         "Error during monitoring table declaration with attributes %s, status: %d\n",
	         monitorDecodeError( status ),status );
      }
*/
      processing_id = 0;
      sens_fbutton(TRUE);
      return FALSE;
    }
  }
  NODATA = TRUE;
  time(&t);
  sprintf(message,"no data from %s try %d status %x  dataBuffer %x %s",dataSource,tryget,status,dataBuffer,ctime(&t));  
//  g_print("no data from %s try %d status %x  dataBuffer %x %s",dataSource,tryget,status,dataBuffer,ctime(&t));  
  error_message(message);
  sleep(3);
  if (!offline && ((time_t)t > ((time_t)request + 300))) // no data for 5 mins
  {  
     sprintf(message,"no data from %s for 5 mins, reconnect  %s",dataSource,ctime(&t));
     g_print("no data from %s for 5 mins, reconnect  %s",dataSource,ctime(&t));
     error_message(message);
     sleep(3);
     free( dataBuffer );
//      g_print("free dataBuffer %x\n",dataBuffer);
     dataBuffer = NULL;
     onlDataSource = FALSE;
  }
  return TRUE;
}
#else
char proc_events() {
  return FALSE;
}
#endif /*Daq*/  
	            
/*
 * Tuning
 */
void LedPh(void) {
int xyz;
int status;
  for (xyz=0;xyz<n_cells;xyz++){
    if (cell[xyz].select) {
      cell[xyz].statistic = 0;
//    cell[xyz].Ped = 0.;
//    cell[xyz].Time = 0.;
//	SumPh[xyz] = 0.;
//	SumPhQ[xyz] = 0.;
      cell[xyz].Led = 0.;
      cell[xyz].LedRms = 0.;
    }
  }
  statistic = 0;
  eventsTot = 0;
  update_statistic(statistic,0);
  if (!processing_id) {
//      status = monitorDeclareTableWithAttributes( monitorTableAll );
    processing_id = gtk_idle_add ((GtkFunction) proc_events, NULL);
  }
    return;
}

static gint tune_step() {
  int n,i,x,y,z;
  static char message[]={"Tuning green counters, hv_code:                               "}; 
  if (cancel_flag) {
    tune_id = 0;
    return FALSE;
  }
  count_tn++;
  if (count_tn == 1000) count_tn = 0;
  if (count_tn == 0) gtk_widget_set_sensitive(message_button, TRUE);
  if (count_tn == 500)  gtk_widget_set_sensitive(message_button, FALSE);
  if (set_id || processing_id) return TRUE;
  if (set_done) {
    set_done = 0;
    g_print("set_done read_selection\n");
    read_selection(NULL,NULL);
    return TRUE;
  }
//  g_print("check tuning\n");
  n = 0;
  for (int xyz=0;xyz<n_cells;xyz++) {
    if (!tune_xyz[xyz]) continue;
    lin2xyz(xyz,&x,&y,&z);
//    g_print("check channel %d  Led %6.1f Ref %6.1f\n",xyz,cell[xyz].Led,cell[xyz].LedRef);
    if ((cell[xyz].Led >= cell[xyz].LedRef) ||
       (100.*(cell[xyz].LedRef - cell[xyz].Led)/(float)cell[xyz].LedRef <= parameters.tolerance)
       || (tune_code == max_hvcode)) {
       tune_xyz[xyz] = 0;
       cell[xyz].Hvcode = tune_code;
//       cell[xyz].Refcode = tune_code;
       cell[xyz].select = FALSE;
       AIQ_set_color (GTK_WIDGET(ec_cell[xyz]), GTK_STATE_NORMAL,(char *)"red",0);
    }
    else n++;
  }
//  g_print("tune_code %d  -> %d counters should be tuned\n",tune_code,n);
  if ((!n) || (tune_code == max_hvcode)) {
    gtk_label_set_text
		(GTK_LABEL(GTK_BUTTON(message_button)->child),"  ");
    tune_id = 0;
    sens_fbutton(TRUE);
    return FALSE;
  }
  tune_code += TUNE_STEP;
  if (tune_code > max_hvcode) tune_code = max_hvcode;
  hv_code(tune_code);
//  g_print("tune_code %d\n",tune_code);
  time(&modtime);
  sprintf(&message[32],"%04d, %d cells to be done %s",tune_code,n,ctime(&modtime));
    gtk_label_set_text
		(GTK_LABEL(GTK_BUTTON(message_button)->child),message);
  usleep(1000000);
  read_selection(NULL,NULL);
  return TRUE;
}

static void
tune_selection (GtkWidget *widget,
		 gpointer linaddr)
{
int xyz;
short n;
    cancel_flag = FALSE;
//  g_print("tune selection: linaddr:  %d\n",linaddr);
//  if ((int)linaddr == 44) {              // is the number of "tune" button
  {  n = 0;
    for(xyz=0;xyz<n_cells;xyz++) {
      tune_xyz[xyz] = cell[xyz].select;
      n += tune_xyz[xyz];
    }
  }
  if (n) {
//    zero_reset(NULL,NULL);
    tune_code = TUNE_START_CODE;
    hvcode = tune_code;
    curr_cell = -1;
    sens_fbutton(FALSE);
    set_id = gtk_idle_add ((GtkFunction) set_cell_hv, NULL);
    tune_id = gtk_idle_add ((GtkFunction) tune_step, NULL);
    gtk_label_set_text
		  (GTK_LABEL(GTK_BUTTON(message_button)->child),"  Tuning of cells  selected ");
    g_print("tuning %d cells\n",n);
  }
} 
int curr_check;
FILE *check_report;
FILE *bad_report;
static gint CheckChn(gpointer dummy) {
  int xyz,chn,MaxCell;
  float MaxLed;
  if (cancel_flag) return FALSE;
  if (processing_id) return TRUE;
  if (curr_check >= 0) { //check result
    MaxLed = 0.0;
    MaxCell = -1;
    for (xyz=0; xyz<n_cells; xyz++) {
        if (cell[xyz].Led > MaxLed) {
        MaxLed = cell[xyz].Led;
	MaxCell = xyz;
      }
    }
    set_code(xyz,0);
    sliv();
    if (cell[xyz].Led < parameters.minph  || curr_check != MaxCell) {
      AIQ_set_color (ec_cell[xyz], GTK_STATE_NORMAL,(char *)"yellow",0);
      fprintf(bad_report,"present SrcId,port, chnl MaxCell %3d %1d %02d  %03d  expected: %3d %1d %02d  %03d\n",
        mapcc[MaxCell].srcId,mapcc[MaxCell].port,mapcc[MaxCell].chnl,MaxCell,
        mapcc[xyz].srcId,mapcc[xyz].port,mapcc[xyz].chnl,curr_check);
    }
  }
  if ((++curr_check == n_cells) || cancel_flag) {
    set_id = 0;
    sens_fbutton(TRUE);
//    set_done=1;
    fclose(check_report);
    fclose(bad_report);
    for (chn=0;chn<n_cells;chn++) {
      if (selected[chn]) cell[chn].select = TRUE;
    }
    return FALSE;
  }
  while (!selected[curr_check]) {
    curr_check++;
    if (curr_check == n_cells) {
      set_id = 0;
      set_done = 1;
      sens_fbutton(TRUE);
      fclose(check_report);
      fclose(bad_report);
      return FALSE;
    }
  }
  set_code(curr_check,max_hvcode);
  AIQ_set_color (GTK_WIDGET(ec_cell[curr_check]), GTK_STATE_NORMAL,(char *)"violet",0);
  set_done=0;
  time(&modtime);
  read_selection(NULL,NULL);
  return TRUE;  
}
void zero_reset_all(void)
{
int chn;
  for (chn=0;chn<n_cells;chn++) {
    set_code(chn,0);
    selected[chn] = cell[chn].select;
    cell[chn].select = TRUE;
  }
  sliv();
}
void com_check( GtkWidget *widget,
               gpointer   data )
{
int i;
  zero_reset_all();
  curr_check = -1;
  cancel_flag = 0;
  check_report = fopen ("status_report","w");
  bad_report = fopen ("status_bad","a");
  check_id = gtk_timeout_add (500,(GtkFunction) CheckChn,NULL);
}

static void
cancel_proc (GtkWidget *widget,
		gpointer   data)
{
  g_print("set cancel_flag\n");
  cancel_flag = TRUE;
}

/**********************************************************************/
typedef struct _Coordinates Coordinates;
struct _Coordinates {
	int x;
	int y;
	int z;
};
void cur_ref_sel( GtkWidget *widget,
               gpointer   data )
{
int xyz;
  for (xyz=0;xyz<n_cells;xyz++){
    if (cell[xyz].select) {
      cell[xyz].LedRef = cell[xyz].Led;
      cell[xyz].RefRms = cell[xyz].LedRms;
    }
  }
}
/*void msadc_map( GtkWidget *widget,
               gpointer   data )
{
  func_buttons[32].sel = !func_buttons[32].sel;
  color_buttons();
}
*/
static void
cur_ref(  GtkWidget *widget,
               Coordinates   *counter_xyz )
{  
  char reference[40];
  int x = counter_xyz->x;
  int y = counter_xyz->y;
  int z = counter_xyz->z;
  int xyz = xyz2lin(x,y,z);
  cell[xyz].LedRef = cell[xyz].Led;
  cell[xyz].Refcode = cell[xyz].Hvcode;
  sprintf(reference,"Reference:      %04d   %5.1f",cell[xyz].Refcode,cell[xyz].LedRef);
  gtk_label_set_text (GTK_LABEL(label_ref),reference);
}
static void
new_cur(  GtkWidget *widget,
               Coordinates   *counter_xyz )
{  
  char current[40];
  char ratio[40];
  int x = counter_xyz->x;
  int y = counter_xyz->y;
  int z = counter_xyz->z;
  int xyz = xyz2lin(x,y,z);
  cell[xyz].Hvcode = gtk_spin_button_get_value_as_int(spinner);
  set_code(xyz,cell[xyz].Hvcode);
  sprintf(current,"Current:          %04d   %5.1f",cell[xyz].Hvcode,cell[xyz].Led);
  sprintf(ratio,"Ratio:          %6.2f     %6.2f",float(cell[xyz].Hvcode)/(float(cell[xyz].Refcode)+0.01),
  					    cell[xyz].Led/(cell[xyz].LedRef+0.001));
  gtk_label_set_text (GTK_LABEL(label_cur),current);
  gtk_label_set_text (GTK_LABEL(label_ratio),ratio);
}
static void
new_ref(  GtkWidget *widget,
               Coordinates   *counter_xyz )
{  
  char reference[40];
  char ratio[40];
  int x = counter_xyz->x;
  int y = counter_xyz->y;
  int z = counter_xyz->z;
  int xyz = xyz2lin(x,y,z);
  cell[xyz].LedRef = gtk_spin_button_get_value_as_int(spinner);
  sprintf(reference,"Reference:      %04d   %5.1f",cell[xyz].Refcode,cell[xyz].LedRef);
  sprintf(ratio,"Ratio:          %6.2f     %6.2f",float(cell[xyz].Hvcode)/(float(cell[xyz].Refcode)+0.01),
  					    cell[xyz].Led/(cell[xyz].LedRef+0.001));
  gtk_label_set_text (GTK_LABEL(label_ref),reference);
  gtk_label_set_text (GTK_LABEL(label_ratio),ratio);
}

void draw_cell( int xyz)
{
#ifdef ROOTAP
  SosCanvas();
  hvsetHst->Clear();
  hvsetHst->Divide(2,2);
  hvsetHst->cd(1);
  histoPed[xyz]->Draw();
  hvsetHst->cd(2);
  histoLed[xyz]->Draw();
  hvsetHst->cd(3);
//  histoTime[xy]->Draw();
  graphHvdep[xyz]->SetLineColor(2);
  graphHvdep[xyz]->Draw("AL");
  hvsetHst->cd(4);
  histoSmpl[xyz]->Draw();
  hvsetHst->Modified();
  hvsetHst->Update();
#endif /*ROOTAP*/
}
static void
create_one_cell (char *cell_xyz, Coordinates *counter_xyz)
{
  static GtkWidget *window = NULL;
  GtkWidget *fixed;
/*  static GtkWidget *spinner;*/
  GtkWidget *button;
  static GtkWidget *label;
  static GtkWidget *label_new;
  GtkAdjustment *adj;
  
  char title[40];
  char control[40];
  char electron_peak[80];
  char current[40];
  char reference[40];
  char ratio[40];
  int x,y,z,chn;
  x = counter_xyz->x;
  y = counter_xyz->y;
  z = counter_xyz->z;
  int xyz = xyz2lin(x,y,z);
  short hvadd = mapcc[xyz].hvadd>>1;
//  g_print( "%s x %d y %d y %d xyz %d hvadd %x\n",cell_xyz,x,y,z,xyz,hvadd);
  sprintf(title,"Cell %s MSADC %3d-%1d-%02d\0",cell_xyz,mapcc[xyz].srcId,mapcc[xyz].port,mapcc[xyz].chnl);
  sprintf(control,"Control HV: %02d-%02d-%02d",hvadd>>8,(hvadd&0xf0)>>4,hvadd&0xf);
//   sprintf(electron_peak,"Electron Peak: %5.1f ",cell[xyz].ElectronCal);
  sprintf(electron_peak,"Calibration electron: %5.1f HVcode: %4d\nHVcode    LED",cell[xyz].ElectronCal,cell[xyz].Hvcal);
//  g_print("%s\n",electron_peak);
//  for (int i=0; i<26; i++) g_print(" %d",electron_peak[i]); g_print("\n");
  sprintf(current,"Current:          %04d   %5.1f",cell[xyz].Hvcode,cell[xyz].Led);
  sprintf(reference,"Reference:      %04d   %5.1f",cell[xyz].Refcode,cell[xyz].LedRef);
  sprintf(ratio,"Ratio:          %6.2f     %6.2f",float(cell[xyz].Hvcode)/(float(cell[xyz].Refcode)+0.01),
  					    cell[xyz].Led/(cell[xyz].LedRef+0.001));
/*  if (window) gtk_widget_destroy (window);*/
#ifdef ROOTAP
  draw_cell(xyz);
#endif
  if (!window)
    {
      window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
      
      gtk_signal_connect (GTK_OBJECT (window), "destroy",
			  GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			  &window);
      gtk_window_set_title (GTK_WINDOW (window),title);
      
      fixed = gtk_fixed_new ();
      gtk_widget_set_usize (fixed, 400,200);
      gtk_container_add (GTK_CONTAINER (window), fixed);
      gtk_widget_show (fixed);
      
      label = gtk_label_new (control);
      gtk_widget_show (label);
      gtk_fixed_put(GTK_FIXED (fixed), label,30,5);
     
      label_electron = gtk_label_new (electron_peak);
      gtk_widget_show (label_electron);
      gtk_fixed_put(GTK_FIXED (fixed), label_electron,30,25);
       
      label_cur = gtk_label_new (current);
      gtk_widget_show (label_cur);
      gtk_fixed_put(GTK_FIXED (fixed), label_cur,30,65);
     
      label_ref = gtk_label_new (reference);
      gtk_widget_show (label_ref);
      gtk_fixed_put(GTK_FIXED (fixed), label_ref,30,85);

      label_ratio = gtk_label_new (ratio);
      gtk_widget_show (label_ratio);
      gtk_fixed_put(GTK_FIXED (fixed), label_ratio,30,105);

      label_new = gtk_label_new ("New Value:");
      gtk_widget_show (label_new);
      gtk_fixed_put(GTK_FIXED (fixed), label_new,30,125);

      adj = (GtkAdjustment *) gtk_adjustment_new ((short) cell[xyz].Refcode, 0.0, 1023.0,
						  1.0, 10.0, 0.0);
      spinner = GTK_SPIN_BUTTON(gtk_spin_button_new (adj, 0.0, 0));
/*      gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner), TRUE);*/
      gtk_widget_set_usize (GTK_WIDGET(spinner), 50, 0);
      gtk_fixed_put (GTK_FIXED (fixed), GTK_WIDGET(spinner),120,125);

  
      button = gtk_button_new_with_label ("   Close    ");
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (gtk_widget_destroy),
				 GTK_OBJECT (window));
      gtk_fixed_put(GTK_FIXED (fixed), button,300,170);
      
      button = gtk_button_new_with_label (" New->HVcd ");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (new_cur),
				 counter_xyz);
      gtk_fixed_put(GTK_FIXED (fixed), button,20,170);
      button = gtk_button_new_with_label (" New->LedRef ");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (new_ref),
				 counter_xyz);
      gtk_fixed_put (GTK_FIXED (fixed), button,190,170);
      
      button = gtk_button_new_with_label (" Cur->Ref ");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (cur_ref),
				 counter_xyz);
      gtk_fixed_put (GTK_FIXED (fixed), button,110,170);
    }
   else {
      gtk_window_set_title (GTK_WINDOW (window),title);
      gtk_label_set_text (GTK_LABEL(label),control);
      gtk_label_set_text (GTK_LABEL(label_electron),electron_peak);
      gtk_label_set_text (GTK_LABEL(label_cur),current);
      gtk_label_set_text (GTK_LABEL(label_ref),reference);
      gtk_label_set_text (GTK_LABEL(label_ratio),ratio);
      gtk_spin_button_set_value (spinner,cell[xyz].Hvcode);
   }

  if (!GTK_WIDGET_VISIBLE (window))
    gtk_widget_show_all (window);
/*  else
    gtk_widget_destroy (window);*/
}
/**********************************************************************/

static gint
button_pressed (GtkWidget *widget,
		GdkEventButton *event, gpointer   data )
{
  int c;
  int x,y,z,sx,sy,sz,i,j,k;
  char col[2][6]={"red","green"};
static    
    Coordinates cell_xyz;
    Coordinates *counter_xyz = &cell_xyz;

//   g_print ("button %s, event->button = %d\n",(char *) data, event->button);
   if (event->button == 2) {
//     if (!func_buttons[1].sel) return TRUE;
//	  sscanf((char *)data,"%02d%1c%02d",&x,&c,&y);
     sscanf(GTK_LABEL(GTK_BUTTON(widget)->child)->label,"%1d%1c%1d%1c%1d",&x,&c,&y,&c,&z);
     int xyz = xyz2lin(x,y,z);
     if (!first_corner.clicked) {
       first_corner.sel = !cell[xyz].select;
       first_corner.x = x;
       first_corner.y = y;
       first_corner.z = z;
       first_corner.xyz = xyz;
       first_corner.clicked = TRUE;
       AIQ_set_color (GTK_WIDGET(ec_cell[first_corner.xyz]), GTK_STATE_NORMAL,&col[first_corner.sel][0],0);
       return TRUE;
     }
     else {
       if (z != first_corner.z) return TRUE;
       first_corner.clicked = FALSE;
       AIQ_set_color (GTK_WIDGET(ec_cell[first_corner.xyz]), GTK_STATE_NORMAL,&col[first_corner.sel][0],0);
       sx = (x >= first_corner.x) ? 1 : -1;
       sy = (y >= first_corner.y) ? 1 : -1;
       sz = (z >= first_corner.z) ? 1 : -1;
//       g_print("xf %d xl %d xs %d yf %d yl %d ys %d\n",first_corner.x,x,sx,first_corner.y,y,sy);
       for (i=first_corner.x;i!=(x+sx);i+=sx) {
         for (j=first_corner.y;j!=(y+sy);j+=sy) {
           for (k=first_corner.z;k!=(z+sz);k+=sz) {
//           g_print(" x/y: %2d/%2d\n",i,j);
//           if ((i>=HOLE_XL) && (i<=HOLE_XR) && (j>=HOLE_YD) && (j<=HOLE_YU)) continue;
             int xyz = xyz2lin(i,j,k);
             if (mapcc[xyz].srcId == n_cells) continue;
             cell[xyz].select = first_corner.sel;
//	        gtk_widget_set_style (ec_cell[i][j],newstyle);
	   }
         }       
       }
//       AIQ_set_color (GTK_WIDGET(fbutton[35]), GTK_STATE_NORMAL,"red",0);
//       func_buttons[35].sel = FALSE;
     }
     color_buttons();
     return TRUE;
   }
   if (event->button == 3) {
     sscanf(GTK_LABEL(GTK_BUTTON(widget)->child)->label,"%1d%1c%1d%1c%1d",&x,&c,&y,&c,&z);
	  cell_xyz.x = x;
	  cell_xyz.y = y;
	  cell_xyz.z = z;
//      g_print("next create_one_cell\n");
	  create_one_cell(GTK_LABEL(GTK_BUTTON(widget)->child)->label, counter_xyz);
   }
   return TRUE;
}

/**********************************************************************/
#ifdef ROOTAP
extern void InitGui();
VoidFuncPtr_t initfuncs[] = {InitGui, 0};
TROOT root("ECAL", "ECAL HVSET",initfuncs);

void init_histos() {
int x,y,z,xyz;
  float Hvdep[nphv], hv[nphv];
  char histoname[46];
    file = new TFile("hvsetHist.root","UPDATE");
    if (!file->IsOpen()) file = new TFile("hvsetHist.root","RECREATE");
    if (!file->IsOpen()) {
      g_print("Problems to open file hvsetHist.root\n");
      exit (1);
    }    
    for (int i=0;i<n_cells;i++) {histoPed[i] = histoLed[i] = NULL;
    				 histoSmpl[i] = NULL;}
    for (int i=0;i<n_cells;i++) {
      lin2xyz(i,&x,&y,&z);
      sprintf(histoname,"PED_%1i-%1i-%1i",x,y,z);
      histoPed[i]= (TH1F*) file->Get(histoname);
      if (histoPed[i] == NULL) {
   	    histoPed[i]= new TH1F(histoname,"", cell[i].Pnbins,
                                            cell[i].minPbin,
                                            cell[i].minPbin+(cell[i].Pnbins*cell[i].Pbin));
      }
    }
    for (int i=0;i<n_cells;i++) {
      lin2xyz(i,&x,&y,&z);
      sprintf(histoname,"LED_%1i-%1i-%1i",x,y,z);
      histoLed[i]= (TH1F*) file->Get(histoname);
      if (histoLed[i] == NULL) {
    	histoLed[i]= new TH1F(histoname,"", cell[i].Lnbins,
                                            cell[i].minLbin,
                                            cell[i].minLbin+(cell[i].Lnbins*cell[i].Lbin));
      }
    }
/*    for (int i=0;i<n_cells;i++) {
      x = i/n_row;
      y = i%n_row;
      sprintf(histoname,"TIME_%02i-%02i",x,y);
      histoTime[i]= (TH1F*) file->Get(histoname);
      if (histoTime[i] == NULL) {
          histoTime[i]= new TH1F(histoname,"", 300,230,290);
      }
    }
*/
    for (int i=0;i<n_cells;i++) {
      lin2xyz(i,&x,&y,&z);
      sprintf(histoname,"SMPL_%1i-%1i-%1i",x,y,z);
      histoSmpl[i]= (TH2S*) file->Get(histoname);
      if (histoSmpl[i] == NULL) {
   	    histoSmpl[i]= new TH2S(histoname,"", 30,-0.5, 29.5,1024,-0.5,4095.5);
      }
    }
    
    for (int i=0;i<n_trig;i++) {histoTrPed[i] = histoTrLed[i] = NULL;
    				 histoTrSmpl[i] = NULL;}
    for (int i=0;i<n_trig;i++) {
      sprintf(histoname,"PED_Tr%1i",i);
      histoTrPed[i]= (TH1F*) file->Get(histoname);
      if (histoTrPed[i] == NULL) {
   	    histoTrPed[i]= new TH1F(histoname,"", 1024,-0.5,1024.5);
      }
    }
    for (int i=0;i<n_trig;i++) {
      sprintf(histoname,"LED_Tr%1i",i);
      histoTrLed[i]= (TH1F*) file->Get(histoname);
      if (histoTrLed[i] == NULL) {
   	    histoTrLed[i]= new TH1F(histoname,"", 1024,-0.5,4095.5);
      }
    }
    for (int i=0;i<n_trig;i++) {
      sprintf(histoname,"SMPL_Tr%1i",i);
      histoTrSmpl[i]= (TH2S*) file->Get(histoname);
      if (histoTrSmpl[i] == NULL) {
   	    histoTrSmpl[i]= new TH2S(histoname,"", 30,-0.5, 29.5,1024,-0.5,4095.5);
      }
    }
    
    for (int i=0;i<n_bgo;i++) {histoBgoPed[i] = histoBgoSign[i] = NULL;
    				 histoBgoSmpl[i] = NULL;}
    for (int i=0;i<n_bgo;i++) {
      sprintf(histoname,"PED_Bgo%1i",i);
      histoBgoPed[i]= (TH1F*) file->Get(histoname);
      if (histoBgoPed[i] == NULL) {
   	    histoBgoPed[i]= new TH1F(histoname,"", 1024,-0.5,1024.5);
      }
    }
    for (int i=0;i<n_bgo;i++) {
      sprintf(histoname,"SIGN_Tr%1i",i);
      histoBgoSign[i]= (TH1F*) file->Get(histoname);
      if (histoBgoSign[i] == NULL) {
   	    histoBgoSign[i]= new TH1F(histoname,"", 1024,-0.5,4095.5);
      }
    }
    for (int i=0;i<n_bgo;i++) {
      sprintf(histoname,"SMPL_Bgo%1i",i);
      histoBgoSmpl[i]= (TH2S*) file->Get(histoname);
      if (histoBgoSmpl[i] == NULL) {
   	    histoBgoSmpl[i]= new TH2S(histoname,"", 30,-0.5, 29.5,1024,-0.5,4095.5);
      }
    }
    for (int ihv=0; ihv<nphv ;ihv++) hv[ihv] = min_hvcode + ihv*hvstep;
//    hv[nphv-1] = 1023.;
    for (int i=0;i<n_cells;i++) {
      lin2xyz(i,&x,&y,&z);
      for (int ihv=0;ihv<nphv; ihv++) Hvdep[ihv] = Led[i][ihv]/(Led[i][nphv-1]+0.0001);
      sprintf(histoname,"HvDep_%1i-%1i-%1i",x,y,z);
      graphHvdep[i]= (TGraph*) file->Get(histoname);
      delete graphHvdep[i];
      graphHvdep[i]= new TGraph(nphv,hv,Hvdep);
      graphHvdep[i]->SetTitle(histoname);
      
    }
    hvsetHst = new TCanvas("hvsetHst","ECAL HVSET  Histograms",100,20,600,680);
    hvsetHst->cd();
}
void read_dependence_file(void) {
  FILE *hv_dependence;
  int xyz,i,ihv,ind;
  char d,xy[6];
  if (!(hv_dependence = fopen ("hv_dependence","r"))) {
    g_print("CAN'T open dependence file\n");
    return;
  }
//  for (i=0; i<n_cells; i++) {
  for (i=0; ; i++) {
    ind = fscanf(hv_dependence,"chn %d %c %s",&xyz,&d,xy);
    if (ind == EOF) break;
//    printf(" chn %3d %c %s",chn,d,xy);
    for (ihv=0; ihv<nphv; ihv++) fscanf(hv_dependence," %f",&Led[xyz][ihv]);
    fscanf(hv_dependence,"\n");
//    for (ihv=0; ihv<nphv; ihv++) fscanf(hv_dependence," %f",&Time[xyz][ihv]);
//    fscanf(hv_dependence,"\n");
  }
}
void reset_histos( GtkWidget *widget,
               gpointer   data )
{
  for (int i=0;i<n_cells;i++){
    if (!cell[i].select) continue;
    histoPed[i]->Reset();
    histoLed[i]->Reset();
    histoSmpl[i]->Reset();
  }
}
void reset_tr_histos( GtkWidget *widget,
               gpointer   data )
{
  for (int i=0;i<n_trig;i++){
    histoTrPed[i]->Reset();
    histoTrLed[i]->Reset();
    histoTrSmpl[i]->Reset();
  }
}
void reset_bgo_histos( GtkWidget *widget,
               gpointer   data )
{
  for (int i=0;i<n_bgo;i++){
    histoBgoPed[i]->Reset();
    histoBgoSign[i]->Reset();
    histoBgoSmpl[i]->Reset();
  }
}
void draw_hist( GtkWidget *widget,
                          gpointer data)
{
  int i,minx = 100,maxx=-1,miny=100,maxy=-1;
  int x,y,z,xz,xyz,nx_histo,ny_histo;
  int type_histo,nhisto = 0;
  short histos[n_cells], histox[n_cells], histoy[n_cells]
      , histoz[n_cells], histoxyz[n_cells];
//  if (draw_id) return;
//  if (draw_id) gtk_idle_remove(draw_id);
//  if (draw_id) g_source_remove(draw_id);
  type_histo =  *(int* )data - 9; // 9 = number of draw_sample button
//  g_print("(int) data %d\n", (int) data);
//  SosCanvas();
//  hvsetHst->Clear();
  for (x=0; x<n_col; x++) {
    for (y=0; y<n_row; y++) {
      for (z=0; z<n_sec;z++) {
        xyz = xyz2lin(x,y,z);
	if (!cell[xyz].select) continue;
	histox[nhisto] = xz = x+(z*n_col);
	histoy[nhisto] = y;
	histoz[nhisto] = z;
	histos[nhisto++] = xyz;        
	if (xz < minx) minx = xz;
	if (xz > maxx) maxx = xz;
	if (y < miny) miny = y;
	if (y > maxy) maxy = y;
	if (nhisto == n_cells) break;
      }
    }
    if (nhisto == n_cells) break;
  }
  if (!nhisto) return;
  nx_histo = maxx-minx+1;
  ny_histo = maxy-miny+1;
  for (i=0; i<nhisto; i++) {
    x = histox[i] - minx;
    y = maxy - histoy[i];
    histoxyz[i] = (y * nx_histo) + x + 1;
  }
  SosCanvas();
  hvsetHst->Clear();
  hvsetHst->Divide(nx_histo,ny_histo);
  for (i=0; i<nhisto; i++) {
    hvsetHst->cd(histoxyz[i]);
    switch (type_histo) {
      case 0:
        histoSmpl[histos[i]]->Draw();
      break;
      case 1:
        histoLed[histos[i]]->Draw();
      break;
      case 2:
        histoPed[histos[i]]->Draw();
      break;
      case 3:
//        histoTime[histos[i]]->Draw();
        graphHvdep[histos[i]]->SetLineColor(2);
        graphHvdep[histos[i]]->Draw("AL");
      break;
      default:
      break;
    }
  }
  hvsetHst->Modified();
  hvsetHst->Update();
}
void draw_tr_hist( GtkWidget *widget,
                          gpointer data)
{
  int i,x;
  SosCanvas();
  hvsetHst->Clear();
  hvsetHst->Divide(n_trig,3);
  i=0;
  for (x=0; x<n_trig; x++) {
    hvsetHst->cd(++i);
    histoTrSmpl[x]->Draw();
  }
  for (x=0; x<n_trig; x++) {
    hvsetHst->cd(++i);
    histoTrLed[x]->Draw();
  }
  for (x=0; x<n_trig; x++) {
    hvsetHst->cd(++i);
    histoTrPed[x]->Draw();
  }
  hvsetHst->Modified();
  hvsetHst->Update();
}

void draw_bgo_hist( GtkWidget *widget,
                          gpointer data)
{
  int i,x;
  SosCanvas();
  hvsetHst->Clear();
  hvsetHst->Divide(n_bgo,3);
  i=0;
  for (x=0; x<n_bgo; x++) {
    hvsetHst->cd(++i);
    histoBgoSmpl[x]->Draw();
  }
  for (x=0; x<n_bgo; x++) {
    hvsetHst->cd(++i);
    histoBgoSign[x]->Draw();
  }
  for (x=0; x<n_bgo; x++) {
    hvsetHst->cd(++i);
    histoBgoPed[x]->Draw();
  }
  hvsetHst->Modified();
  hvsetHst->Update();
}
static int
ProcRoot(gpointer dummy) {
  gSystem->ProcessEvents();
  usleep(1000);
  return TRUE;
}
static int ModifyHist(gpointer dummy) {
  hvsetHst->Modified();
  hvsetHst->Update();
  return TRUE;
}
#endif /* ROOTAP */
void led_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<0);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<0));
  }
}
void rand_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<1);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<1));
  }
}
void beam_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<2);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<2));
//    g_print("mask for trigger %x\n",(0xfffffff ^ (1<<2)));
  }
}
void ecal_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<3);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<3));
  }
}
void hcal_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<4);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<4));
  }
}
void bgo_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<5);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<5));
  }
}
void phys_tr(GtkWidget *widget,gpointer inner)
{
    phys_yes = GTK_TOGGLE_BUTTON (widget)->active;
}
/*
void vetoinner_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<6);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<6));
  }
}
void halo_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<7);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<7));
  }
}
void beam_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<8);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<8));
  }
}
void inclMT_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<9);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<9));
  }
}
void ecal1_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (1<<10);
  }
  else {
    trigger_mask &= (0xfffffff ^ (1<<10));
  }
}
void random_tr(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    trigger_mask |= (3<<11);
  }
  else {
    trigger_mask &= (0xfffffff ^ (3<<11));
  }
}
*/
void check100V(GtkWidget *widget,gpointer inner)
{
  if(!expert) return;
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    HVcheck = TRUE;
  }
  else {
    HVcheck = FALSE;
  }
}
void expert_operator(GtkWidget *widget,gpointer inner)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
    expert = TRUE;
  }
  else {
    expert = FALSE;
  }
  gtk_widget_set_sensitive(button_100V, expert);
  sens_fbutton(-1);
}

/*void sel_ev_type (GtkWidget *widget,gpointer pointer)
{
  if (GTK_TOGGLE_BUTTON (widget)->active) {
//    g_print("sel cal ev\n");
    sel_calibr = TRUE;
  }
  else {
//    g_print("sel phys ev\n");
    sel_calibr = FALSE;
  }
}
*/
void sel_ev_type (GtkWidget *widget,gpointer pointer)
{
  int status;
  status = monitorLogout();
  g_print("sel_ev_type: %d logout status %d\n",GTK_TOGGLE_BUTTON (widget)->active,status);
  if (GTK_TOGGLE_BUTTON (widget)->active) {
//    g_print("sel cal ev\n");
    sel_calibr = TRUE;
    status = monitorDeclareTable(monitorTableYes);
    if ( status != 0 ) {
      fprintf( stderr,
	       "Error during monitoring table declaration with attributes %s, status: %d\n",
	       monitorDecodeError( status ),status );
      exit( 1 );
    }
    g_print("calibration data\n");    
  }
  else {
//    g_print("sel phys ev\n");
    sel_calibr = FALSE;
    if (!trigger_mask) {
      g_print("no trigger selected\n");
      error_message((char*)"no trigger selected");
    }
    status = monitorDeclareTable(monitorTablePhys);
    if ( status != 0 ) {
      fprintf( stderr,
	       "Error during monitoring table declaration  %s, status: %d\n",
	       monitorDecodeError( status ),status );
      exit( 1 );
    }
    g_print("physical events, trigger mask: %x\n",trigger_mask);
  }
}


static void
hv_code_ref (GtkWidget *widget,
                          gpointer data)
{
  for (int xyz=0; xyz<n_cells; xyz++) {
    if (!cell[xyz].select || !cell[xyz].Hvcode) continue;
    cell[xyz].Refcode = cell[xyz].Hvcode;
  }
}

static void
set_ref_code (GtkWidget *widget,
                          gpointer data)
{
int i;
  if (set_id) return;
  cancel_flag = 0;
  curr_cell = -1;
  sens_fbutton(FALSE);
  set_id = gtk_idle_add ((GtkFunction) set_cell_hv, &curr_cell);
}


static actions afunc_buttons[] = {
  {(char *)"all",FALSE, select_all,(char *)"SELECT/UNSELECT ALL CHANNELS" },
  {(char *)"set options",0,set_options,(char *)"SET PARAMETERS"},
  {(char *)"show status", 0,show_status,(char *)"SHOW CURRENT STATUS (Filtered)"},
  {(char *)"cancel",0,cancel_proc,(char *)"STOP PROCESSING (IF ANY)"},
  {(char *)"read selection",0, read_selection ,(char *)"MEASURE LEDs for selected channels" },
  {(char *)"discharge",0, zero_reset ,(char *)"PUT CURRENT HV to zero. FOR SELECTED CHANNELS\nand flash light to discharge"},
  {(char *)"set ref.code",0, set_ref_code ,(char *)"SET REF. HV CODE FOR SELECTED CHANNELS"},
  {(char *)"exit ecal", 0,exit_ecal,(char *)"EXIT PROGRAM" },
  {(char *)"reset histos",0, reset_histos ,(char *)"RESET HISTOGRAMS FOR SELECTED CHANNELS"},
  {(char *)"draw sample",0,draw_hist,(char *)"DRAW SAMPLE HISTO\nfor selection"},
  {(char *)"draw led",0,draw_hist,(char *)"DRAW LED HISTO\nfor selection"},
  {(char *)"draw ped",0,draw_hist,(char *)"DRAW PED HISTO\nfor selection"},
  {(char *)"rst BGO hist",0, reset_bgo_histos ,(char *)"RESET ALL BGO HISTOGRAMS"},
  {(char *)"draw BGO hist",0,draw_bgo_hist,(char *)"DRAW BGO HISTOS"},
  {(char *)"rst Tr hist",0, reset_tr_histos ,(char *)"RESET ALL TRIGGER HISTOGRAMS"},
  {(char *)"draw Tr hist",0,draw_tr_hist,(char *)"DRAW TRIGGERS HISTOS"},
//  {(char *)"draw time",0,draw_hist,(char *)"DRAW TIME HISTO\nfor selection"},
  {(char *)"tune selection",0, tune_selection ,(char *)"MAKE TUNING FOR SELECTED CHANNELS"},
//  {(char *)"read one",0, read_one ,(char *)"Measure Signals one by one"},
  {(char *)"check mapping",0, com_check ,(char *)"CHECK MAPPING FOR SELECTED CHANNELS"},
  {(char *)"measure dep",0, hv_dep ,(char *)"MEASURE HV DEPENDENCIES FOR SELECTED CHANNELS"},
  {(char *)"restore status",0, restore_status,(char *)"RESTORE SAVED STATUS" },
  {(char *)"save status",0, save_status ,(char *)(char *)"SAVE CURRENT STATUS & PARAMETERS"},
  {(char *)"read calibration",0, read_calibration ,(char *)"READ ELECTRON CALIBRATION FILE\n CURRENT LED -> LedRef for SELECTED"},
  {(char *)"make new LedRef",0, make_new_LedRef ,(char *)"MAKE NEW LED REFERENCIES\nFROM CALIBRATION DATA for SELECTED"},
  {(char *)"scale reference",0, make_new_LedRef ,(char *)"SCALE LEDs on factor and use as REFERENCIES\nFOR SELECTED CHANNELS"},
  {(char *)"make new HV",0, make_new_reference ,(char *)"MAKE NEW HV REFERENCIES\nFOR SELECTED CHANNELS"},
  {(char *)"hv.code->ref",0, hv_code_ref ,(char *)"PUT CURRENT HV to REF. FOR SELECTED CHANNELS"},
  {(char *)"step change",0, step_change ,(char *)"ROTATE MOTORS ON SELECTED STEP"}
};
void initVME(void)
{
#ifdef VMEserver
  if (vme_start(VMEserver)) {
    printf("vme_server %s ERROR\n",VMEserver);
    exit (1);
  }
  BaseHvc = BaseHVC;
  if (vme_map(BaseHvc,0x2000)) {
    printf("vme_map BaseHVC ERROR\n");
    exit (2);
  }
  FireLed = FireLED;
//  LedZug  = BaseLED | 2;
//  if (vme_map(FireLed,0x100)) {
//    printf("vme_map BaseLED ERROR\n");
//    exit (3);
//  }
#endif  
}

void main_panel(void) {
    GtkWidget *window;
    GtkWidget *button;
    GtkWidget *tablebox;
    GtkWidget *table;
    GtkWidget *ftable;
    GtkWidget *mainbox;
    GtkWidget *triggerbox;
    GtkWidget *separator;
    GtkWidget *label;
    GtkWidget *procedurebox;
    GtkWidget *scrolled_window;
    GtkTooltips *tooltips;
    GSList    *group; 

    static int buttons_id[n_buttons];
 
    int i,x,y,z,xyz;
    char title[6];
    for (i=0; i<n_buttons; i++) func_buttons[i] = afunc_buttons[i];
    tooltips = gtk_tooltips_new ();

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

    gtk_window_set_title (GTK_WINDOW (window), "ECAL High Voltage Control");
    gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, TRUE);

    gtk_signal_connect (GTK_OBJECT (window), "delete_event",
                        GTK_SIGNAL_FUNC (delete_event), NULL);

    gtk_container_set_border_width (GTK_CONTAINER (window), 2);
    
    mainbox = gtk_vbox_new (FALSE, 1);

/*    label = gtk_label_new ("CHANNELS SELECTION");
//   gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
    gtk_misc_set_padding (GTK_MISC (label), 10, 10);

    gtk_box_pack_start (GTK_BOX (mainbox), label, FALSE, FALSE, 0);
    gtk_widget_show (label);*/

/*    scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 10);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				    GTK_POLICY_ALWAYS,
				    GTK_POLICY_AUTOMATIC);

    gtk_box_pack_start (GTK_BOX (mainbox), 
			scrolled_window, TRUE, TRUE, 0);
    gtk_widget_show (scrolled_window);
*/
    tablebox = gtk_hbox_new (FALSE, 1);
    gtk_box_pack_start (GTK_BOX (mainbox), tablebox, TRUE, TRUE, 0);
    /* Create a n_col x n_row table */
    for (z=0;z<n_sec;z++) {
      table = gtk_table_new (n_col, n_row, FALSE);
      gtk_box_pack_start (GTK_BOX (tablebox), table, TRUE, TRUE, 0);
    
/*    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled_window), table);
    gtk_container_set_focus_hadjustment (GTK_CONTAINER (table),
					 gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (scrolled_window)));
    gtk_container_set_focus_vadjustment (GTK_CONTAINER (table),
					 gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (scrolled_window)));
*/
    /* Create  buttons */

//    for (z=0;z<n_sec;z++) {
      for (x=0;x<n_col;x++) {
	for (y=n_row-1;y>=0;y--) {
//          for (z=0; z<n_sec; z++) {
	    xyz = xyz2lin(x,y,z);
            if (mapcc[xyz].srcId == -1) continue;
	    sprintf(title,"%1d-%1d-%1d",x,y,z);	
	    ec_cell[xyz] = gtk_button_new_with_label (title);
	    gtk_signal_connect (GTK_OBJECT (ec_cell[xyz]), "clicked",
		 GTK_SIGNAL_FUNC (ecal_button_pressed), NULL);

	    gtk_signal_connect (GTK_OBJECT (ec_cell[xyz]), "button_press_event",
		 GTK_SIGNAL_FUNC (button_pressed), NULL);
	    gtk_table_attach_defaults (GTK_TABLE(table), ec_cell[xyz], (x/*+(z*n_col)*/), (x/*+(z*n_col)*/)+1, n_row-1-y, n_row-y);

	    gtk_widget_show (ec_cell[xyz]);
//	  }
	}
      }
      gtk_widget_show (table);
      if (z<n_sec-1) {
	separator = gtk_vseparator_new();
	gtk_box_pack_start (GTK_BOX (tablebox), separator, FALSE, TRUE, 5);
	gtk_widget_show (separator);
      }
    }
    gtk_widget_show (tablebox);

    separator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (mainbox), separator, FALSE, TRUE, 5);
    gtk_widget_show (separator);
    
    ftable = gtk_table_new ((n_buttons+1)/4, 4, FALSE);

    gtk_box_pack_start (GTK_BOX (mainbox), ftable, FALSE, FALSE, 0);
    i = 0;
    for (y=0;y<5;y++) {
      for (x=0;x<(n_buttons+1)/4;x++) {                                          
	     if (i == n_buttons) break;
	     fbutton[i] = gtk_button_new_with_label (func_buttons[i].label);
	     buttons_id[i] = i;
	     if (func_buttons[i].tooltip) gtk_tooltips_set_tip (tooltips, fbutton[i], func_buttons[i].tooltip, NULL);
	     if (func_buttons[i].func)
	       gtk_signal_connect (GTK_OBJECT (fbutton[i]), 
			           "clicked", 
			           GTK_SIGNAL_FUNC(func_buttons[i].func),&buttons_id[i]);
	     else
	       gtk_widget_set_sensitive (fbutton[i], FALSE);	      
//	     gtk_signal_connect (GTK_OBJECT (fbutton[i]), "button_press_event",
//	           GTK_SIGNAL_FUNC (fbutton_pressed), &buttons_id[i]);
	     gtk_table_attach_defaults (GTK_TABLE(ftable), fbutton[i], x, x+1, y  , y+1 );
	     gtk_widget_show (fbutton[i]);
	     i++;
      }	
    }
    gtk_widget_show (ftable);
    
    separator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (mainbox), separator, FALSE, TRUE, 5);
    gtk_widget_show (separator);
    triggerbox = gtk_hbox_new (FALSE, 1);
//    gtk_widget_set_usize (triggerbox, 800,50);
    sel_cal = gtk_radio_button_new_with_label (NULL,"calibration events");
    gtk_signal_connect (GTK_OBJECT (sel_cal), "clicked",
			       GTK_SIGNAL_FUNC (sel_ev_type),
			       GTK_BOX(triggerbox));
    gtk_box_pack_start (GTK_BOX (triggerbox), sel_cal, FALSE, FALSE, 0);
    gtk_widget_show (sel_cal);
    group = gtk_radio_button_group (GTK_RADIO_BUTTON (sel_cal));
    sel_phys = gtk_radio_button_new_with_label (group,"physical events");
//    gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON (sel_phys),TRUE);
    gtk_box_pack_start (GTK_BOX (triggerbox), sel_phys, FALSE, FALSE, 0);
    
    gtk_widget_show (sel_phys);
    
    button_trigger = gtk_check_button_new_with_label ("BGO");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (bgo_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger); 
 
    button_trigger = gtk_check_button_new_with_label ("HCAL");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (hcal_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger); 

    button_trigger = gtk_check_button_new_with_label ("ECAL");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (ecal_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger); 
   
    button_trigger = gtk_check_button_new_with_label ("BEAM");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (beam_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger);     

    button_trigger = gtk_check_button_new_with_label ("RAND");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (rand_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger);     

    button_trigger = gtk_check_button_new_with_label ("LED");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (led_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger);     

/*
    button_trigger = gtk_check_button_new_with_label ("PHYS");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (phys_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger);     
*/
/*
    button_trigger = gtk_check_button_new_with_label ("VetoInner");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (vetoinner_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger); 

    button_trigger = gtk_check_button_new_with_label ("Halo");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (halo_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger);     

    button_trigger = gtk_check_button_new_with_label ("Beam");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (beam_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger); 
 

    button_trigger = gtk_check_button_new_with_label ("inclMT");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (inclMT_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger); 
 
    button_trigger = gtk_check_button_new_with_label ("LargeQ2");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (ecal1_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger); 
 
    button_trigger = gtk_check_button_new_with_label ("random");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (random_tr),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger); 
    
*/
 
    button_100V = gtk_check_button_new_with_label ("check100V");
    gtk_signal_connect (GTK_OBJECT (button_100V), "clicked",
			       GTK_SIGNAL_FUNC (check100V),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_100V), TRUE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_100V, FALSE, FALSE, 0);
    gtk_widget_set_sensitive(button_100V, FALSE);
    gtk_widget_show (button_100V); 

    button_trigger = gtk_check_button_new_with_label ("expert");
    gtk_signal_connect (GTK_OBJECT (button_trigger), "clicked",
			       GTK_SIGNAL_FUNC (expert_operator),
			       NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button_trigger), FALSE);
    gtk_box_pack_start (GTK_BOX (triggerbox), button_trigger, FALSE, FALSE, 0);
    gtk_widget_show (button_trigger); 
 
 //   gtk_container_add (GTK_CONTAINER (mainbox), triggerbox);
    gtk_box_pack_start (GTK_BOX (mainbox), triggerbox, FALSE, FALSE, 0);
    gtk_widget_show (triggerbox);    


    message_button = gtk_button_new_with_label  ("  ");
/*    label = gtk_label_new ("HV tuning in progress");
    gtk_misc_set_padding (GTK_MISC (label), 10, 10);*/
    AIQ_set_color (GTK_WIDGET(message_button), GTK_STATE_NORMAL,(char *)"red",0);
/*    gtk_widget_set_sensitive(message_button, FALSE);*/
    gtk_widget_show (message_button);    
    gtk_signal_connect (GTK_OBJECT (message_button), 
			      "clicked", 
			      GTK_SIGNAL_FUNC(cancel_proc),
			      NULL);
    gtk_box_pack_start (GTK_BOX (mainbox), message_button, FALSE, FALSE, 0);

    lbl_Stat = gtk_label_new (ttl_stat);
    gtk_box_pack_start (GTK_BOX (mainbox), lbl_Stat, FALSE, FALSE, 0);
    sprintf(ttl_stat,"Statistic: %5d",0); 
    gtk_label_set_text (GTK_LABEL(lbl_Stat),ttl_stat);  
    gtk_widget_show (lbl_Stat);

    gtk_container_add (GTK_CONTAINER (window), mainbox);
    
    
    gtk_widget_show (mainbox);
    gtk_widget_show (window);
}
void set_code(int xyz, short code)
{
  int hvmpa;
  unsigned short  old_code, new_code;
  unsigned short busy, cma;
  int n,n_step;
  short step = 20;
#ifdef VMEserver

//  AIQ_set_color (ecal_cell[i], GTK_STATE_NORMAL,"violet",0);
  new_code = (unsigned short) code; 
//  old_code = cell[xyz].Hvcode;
  cma = mapcc[xyz].hvadd>>1;
  hvmpa = BaseHvc | mapcc[xyz].hvadd;

#ifdef DEBUG
  int x,y,z;
  lin2xyz(xyz,&x,&y,&z);
  g_print("cell[%1d,%1d,%1d] Hvcode = %4d VMEADD %x  address %x md: %d sa: %d cs: %d\n",x,y,z,code,hvmpa,cma,(cma>>7)&7,(cma>>4)&7,cma&0xf);
#endif  
  vme_write(hvmpa,new_code);
  n=0;
  do {
    vme_read(hvmpa,&busy);
//    g_print("busy %x\n",busy);
  } while(busy & 0x1 && n++ != 10);
  vme_read(hvmpa,&busy);
  vme_read(hvmpa,&busy);
#endif /*VMEserver*/
  cell[xyz].Hvcode = code;
//  AIQ_set_color (ecal_cell[i], GTK_STATE_NORMAL,"green",0);
#ifdef DEBUG
  g_print("set_code x %d y %d  z %d hvmpa %x code %d\n",x,y,z,hvmpa,new_code);
#endif
}
static gint check_fuses(gpointer label) {
  unsigned short busy,busy0;
  if ((!HVcheck) || set_id) return TRUE;
#ifdef VMEserver
  AIQ_set_color (message_button,GTK_STATE_NORMAL,(char*)"blue",0);
  for (int mdl=0; mdl<HV_distributors; mdl++) {
    int testaddr = BaseHvc | (((mdl<<4)+6)<<5);
//    int mdladdr = mdl<<9;
    vme_write(testaddr,0);
    vme_read(testaddr,&busy0);
//    busy0=ntohs(busy0);
    int count=0;
    do {
      vme_read(testaddr,&busy);
//      busy=ntohs(busy);
      count++;
    } while((busy & 0x1) && count < 20);
    if (!(busy&2)) {
//      g_print("hv mdl %d busy0 %x busy %x count %d\n",mdl,busy0,busy,count);
      for (int xyz=0; xyz<n_cells; xyz++) {
//          if (((mapcc[xyz].hvadd&mdladdr)== mdladdr) && ec_cell[xyz]) AIQ_set_color (ec_cell[xyz], GTK_STATE_NORMAL,"blue",0);
//          if (((mapcc[xyz].hvadd&mdladdr)== mdladdr) && ec_cell[xyz]) gtk_widget_set_style (ec_cell[xyz],newstyle);
          if (((mapcc[xyz].hvadd>>9)== mdl) && ec_cell[xyz]) gtk_widget_set_style (ec_cell[xyz],newstyle);
      }
    }
  }
  AIQ_set_color (message_button,GTK_STATE_NORMAL,(char*)"red",0);
#endif /*VMEserver*/
  return TRUE;
}
/**********************************************************************/
int main( int   argc,
          char *argv[] )
{    
  if(argc < 2)
    {
      fprintf(stdout,"USAGE: %s <@source:>\n",argv[0]);
      return -1;
    }
  read_map_file();
  initVME();
  #ifdef DAQ       
    initDataSource(argc, argv);
 //   g_print("DataSource initiated\n");
#endif
    gtk_init (&argc, &argv);
#ifdef ROOTAP    
    TApplication theApp("App", &argc, argv);
#endif /* ROOTAP */
    main_panel();
    set_default();   
    select_all(GTK_WIDGET(fbutton[0]),NULL);
    read_ref_file(NULL);
    read_dependence_file();
//    g_print("\nstart gtk_main\n");
#ifdef ROOTAP
    init_histos();
    g_print("\ninit_histos done\n");
    root_id = gtk_idle_add ((GtkFunction) ProcRoot,NULL);
    check_id=gtk_timeout_add( 60000, (GtkFunction) check_fuses,NULL);
//    mod_id = gtk_idle_add ((GtkFunction) ModifyHist,NULL);
//    mod_id = gtk_timeout_add (500,(GtkFunction) ModifyHist,NULL);
#endif
//    g_print("start main loop\n");
    sens_fbutton(TRUE);
    gtk_main ();

    return 0;
}
