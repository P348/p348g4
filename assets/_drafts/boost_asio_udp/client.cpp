//
// client.cpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

# include <iostream>
# include <boost/array.hpp>
# include <boost/asio.hpp>
# include <boost/thread.hpp>

using boost::asio::ip::udp;

void
fetcher( udp::socket * sockPtr, udp::endpoint * enpPtr ) {
    udp::socket & socket = *sockPtr;
    udp::endpoint & receiver_endpoint = *enpPtr;
    socket.open(udp::v4());

    boost::array<char, 1> send_buf  = { 'f' };
    socket.send_to(boost::asio::buffer(send_buf), receiver_endpoint);

    boost::array<char, 128> recv_buf;
    udp::endpoint sender_endpoint;
    size_t len = socket.receive_from(
                boost::asio::buffer(recv_buf), sender_endpoint);

    std::cout.write(recv_buf.data(), len);
}

int main(int argc, char* argv[]) {
    const char spinner[] = "/-\\|";
    const size_t spinnerLength = sizeof(spinner)-1;
    size_t n = 0;
    try {
        if (argc != 2) {
            std::cerr << "Usage: client <host>" << std::endl;
            return 1;
        }

        boost::asio::io_service io_service;

        udp::resolver resolver(io_service);
        udp::resolver::query query(udp::v4(), argv[1], "daytime",
                                    boost::asio::ip::resolver_query_base::flags()
                                    // ^^^ This is to prevent wrong name resolution on loopback
                                    // device in offline mode.
                                    );
        udp::endpoint receiver_endpoint = *resolver.resolve( query );

        udp::socket socket(io_service);
        
        boost::thread t( fetcher, &socket, &receiver_endpoint );
        t.detach();
        
        for(;;++n) {
            // just do other things here.
            printf( "\b%c", spinner[n%spinnerLength] );
            fflush( stdout );
            sleep( 1 );
        }
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

  return 0;
}
