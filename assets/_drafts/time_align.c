/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <stdint.h>
# include <stdio.h>
# include <stdlib.h>
# include <math.h>

const uint16_t srcSamples[] = {
    213,  191,  216,  192,     215,  189,  214,  193,
    213,  189,  215,  192,     214,  191,  214,  193,
    252,  488,  907,  1360,    1630, 1625, 1490, 1285,
    1103, 896,  750,  595,     508,  406,  372,  311
};

/* === */

static int
heuristic_find_zeroes(
        const uint16_t * samples,   uint8_t nSamples,
        float * zero1Ptr,           float * zero2Ptr,
        float threshold ) {
    double meanDelta = 0,
           cDelta,
           z1, meanZ1,
           z2, meanZ2,
           discrepancy;
    uint8_t nAccumulated;
    for( nAccumulated = 0;
         nAccumulated < nSamples;
         nAccumulated +=2 ) {
        z1 = samples[nAccumulated    ];
        z2 = samples[nAccumulated + 1];
        cDelta = 2*fabs(z1 - z2)/(z1 + z2);
        if( meanDelta ) {
            float tmpMean = meanDelta/nAccumulated;
            discrepancy = fabs(1 - fabs(tmpMean - cDelta)/tmpMean);
            if( discrepancy > threshold ) {
                printf("# stop, since %f > %f\n", discrepancy, threshold);
                break;  // stop accumulation
            }
        }
        meanDelta += cDelta;
        meanZ1 += z1;
        meanZ2 += z2;
    }
    if( !nAccumulated || nAccumulated == nSamples ) return -1;
    *zero1Ptr = meanZ1/(nAccumulated/2);
    *zero2Ptr = meanZ2/(nAccumulated/2);
    return nAccumulated;
}

/* === */

static int
find_maximum_simple( float * samples,   uint8_t nSamples,
                     float * maxXPtr,   float * maxYPtr) {
    uint8_t n = 1;
    *maxXPtr = 0;
    *maxYPtr = samples[0];
    for( ; n < nSamples; n++ ) {
        if( *maxYPtr < samples[n] ) {
            *maxYPtr = samples[n];
            *maxXPtr = n;
        }
    }
    return n;
}

int
main( int argc, char * argv[] ) {
    int (*zeroes_finder)(const uint16_t *, uint8_t, float*, float*, float) = heuristic_find_zeroes;

    const uint8_t nSamples = sizeof(srcSamples)/sizeof(uint16_t);
    float z1, z2;
    uint8_t i;

    int n = zeroes_finder( srcSamples, nSamples, &z1, &z2, 0.3 );

    if( n < 0 ) {
        fprintf( stderr, "Error.\n" );
        return EXIT_FAILURE;
    }

    printf( "# zeroes (by %d): %f, %f.\n", n, z1, z2 );
    for( i = 0; i < nSamples; i += 2 ) {
        printf( "%f\n%f\n", srcSamples[i] - z1, srcSamples[i+1] - z2 );
    }
    printf("\n");

    /* find maximum and r */

    return EXIT_SUCCESS;
}


