# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# include <stdlib.h>
# include <math.h>
# include <stdint.h>
# include <stdio.h>
# include <gsl/gsl_vector.h>
# include <gsl/gsl_rng.h>
# include <gsl/gsl_randist.h>
# include <gsl/gsl_blas.h>
# include <gsl/gsl_multifit_nlin.h>

# define USE_MOYAL

static const double srcSamples[] = {
     0, 0, -4, -1,          -2, -1, -5, -1,
     -3, -1, -5, -1,        -4, -3, -6, 33,
     188, 508, 877, 1138,   1158, 1085, 931, 783,
     620, 489, 371, 284,    211, 163, 121, 97
};

struct MoyalParameters {
    uint8_t n;
    const double * samples;
    const double * sigma;
};

# ifdef USE_MOYAL
double
moyal( double p1, double p2, double p3, double x ) {
    const double lambda = 2.22*(x - p2)/p3,
                 exp1 = exp(-lambda),
                 exp2 = exp( -(lambda + exp1)/2 )
                 ;
    return exp2*p1;
}

int
moyal_f( const gsl_vector * p,
         void * data_, 
         gsl_vector * f){
    uint8_t i;
    struct MoyalParameters * data = (struct MoyalParameters *) data_;
    const double * y = data->samples;
    const double * sigma = data->sigma;

    double p1 = gsl_vector_get(p, 0),
           p2 = gsl_vector_get(p, 1),
           p3 = gsl_vector_get(p, 2)
           ;

    for( i = 0; i < data->n; i++ ) {
        /* Model Yi = p1*exp(- (lambda + exp(-lambda))/2),
         *       lambda = 2.22*(x-p2)/p3 */
        double x        = i,
               lambda   = 2.22*( x - p2 ) / p3,
               Yi       = p1*exp(- (lambda + exp(-lambda))/2);
        gsl_vector_set( f, i, (Yi - y[i])/sigma[i] );
    }
    return GSL_SUCCESS;
}

int
moyal_df( const gsl_vector * p,
          void * data_, 
          gsl_matrix * J){
    uint8_t i;
    struct MoyalParameters * data = (struct MoyalParameters *) data_;
    const double p1 = gsl_vector_get(p, 0),
                 p2 = gsl_vector_get(p, 1),
                 p3 = gsl_vector_get(p, 2)
           ;
    for( i = 0; i < data->n; i++ ) {
        /* Jacobian matrix J(i,j) = dfi / dxj,
         * where fi = (Yi - yi)/sigma[i],
         *       Yi = A * exp(-lambda * i) + b
         * and the xj are the parameters (A,lambda,b) */
        const double x = i,
                     a = 2.22,
                     sigma = data->sigma[i],
                     lambda = a*(x - p2)/p3,
                     exp2 = exp(-lambda),
                     exp1 = exp(-(lambda + exp2)/2)
               ;
        gsl_matrix_set( J, i, 0, -exp1/sigma );
        gsl_matrix_set( J, i, 1, -(p1/(2*p3*sigma))*a*(1 - exp2)*exp1 );
        gsl_matrix_set( J, i, 2, -(p1/(2*p3*p3*sigma))*a*(x-p2)*(1 - exp2)*exp1 );
    }
    return GSL_SUCCESS;
}

int
moyal_fdf( const gsl_vector * p,
           void * data, 
           gsl_vector * f,
           gsl_matrix * J ){
    moyal_f(p, data, f);
    moyal_df(p, data, J);
    return GSL_SUCCESS;
}
# else
double
expb( double A, double lambda, double b, double x ) {
    return A * exp (-lambda * x) + b;
}

int
expb_f (const gsl_vector * x,
        void *data,
        gsl_vector * f) {
    size_t n = ((struct MoyalParameters *)data)->n;
    const double *y = ((struct MoyalParameters *)data)->samples;
    const double *sigma = ((struct MoyalParameters *) data)->sigma;

    double A = gsl_vector_get (x, 0);
    double lambda = gsl_vector_get (x, 1);
    double b = gsl_vector_get (x, 2);

    size_t i;

    for (i = 0; i < n; i++)
    {
        /* Model Yi = A * exp(-lambda * i) + b */
        double t = i;
        double Yi = A * exp (-lambda * t) + b;
        gsl_vector_set (f, i, (Yi - y[i])/sigma[i]);
    }

    return GSL_SUCCESS;
}

int
expb_df (const gsl_vector * x, void *data,
         gsl_matrix * J) {
    size_t n = ((struct MoyalParameters *)data)->n;
    const double *sigma = ((struct MoyalParameters *) data)->sigma;

    double A = gsl_vector_get (x, 0);
    double lambda = gsl_vector_get (x, 1);

    size_t i;

    for (i = 0; i < n; i++)
    {
        /* Jacobian matrix J(i,j) = dfi / dxj, */
        /* where fi = (Yi - yi)/sigma[i],      */
        /*       Yi = A * exp(-lambda * i) + b  */
        /* and the xj are the parameters (A,lambda,b) */
        double t = i;
        double s = sigma[i];
        double e = exp(-lambda * t);
        gsl_matrix_set (J, i, 0, e/s);
        gsl_matrix_set (J, i, 1, -t * A * e/s);
        gsl_matrix_set (J, i, 2, 1/s);
    }
    return GSL_SUCCESS;
}

int
expb_fdf (const gsl_vector * x, void *data,
          gsl_vector * f, gsl_matrix * J) {
    expb_f (x, data, f);
    expb_df (x, data, J);

    return GSL_SUCCESS;
}
# endif

void
print_state (size_t iter, gsl_multifit_fdfsolver * s) {
    printf ("# iter: %3u p = % 15.8e % 15.8e % 15.8e "
           " |f(x)| = %g\n",
           (unsigned) iter,
           gsl_vector_get( s->x, 0 ),
           gsl_vector_get( s->x, 1 ),
           gsl_vector_get( s->x, 2 ),
           gsl_blas_dnrm2( s->f ));
}

int
main(int argc, char * argv[]) {
    gsl_multifit_fdfsolver *s;
    int status;
    unsigned int iter = 0, i;
    const size_t n = sizeof(srcSamples)/sizeof(double);
    const size_t p = 3;
    gsl_matrix *covar = gsl_matrix_alloc( p, p );
    double sigma[n];
    struct MoyalParameters d = { n, srcSamples, sigma };
    gsl_multifit_function_fdf f;
    //double p_init[3] = { 5e3, 20, 5 };
    double p_init[3] = { 2e3, 0, 5 };
    gsl_vector_view P = gsl_vector_view_array( p_init, p );

    for( i = 0; i < n; ++i ) {
        sigma[i] = 1;
    }

    # if 0
    {  /* Testing printout for initial parameters {{{ */
        gsl_vector * tmpP = gsl_vector_alloc(3),
                   * tmpF = gsl_vector_alloc(n)
                   ;
        gsl_matrix * derivCache = gsl_matrix_alloc( n, p );
        gsl_vector_set( tmpP, 0, p_init[0] );
        gsl_vector_set( tmpP, 1, p_init[1] );
        gsl_vector_set( tmpP, 2, p_init[2] );
        # ifdef USE_MOYAL
        moyal_f( tmpP, &d, tmpF );
        moyal_df( tmpP, &d, derivCache );
        # else
        expb_f( tmpP, &d, tmpF );
        expb_df( tmpP, &d, derivCache );
        # endif
        for( i = 0; i < n; ++i ) {
            printf( "%d %f %f %f %f %f %f\n",
                    (int) i,
                    d.samples[i],
                    gsl_vector_get(tmpF, i),
                    gsl_matrix_get( derivCache, i, 0 ),
                    gsl_matrix_get( derivCache, i, 1 ),
                    gsl_matrix_get( derivCache, i, 2 ),
                    # ifdef USE_MOYAL
                    moyal(
                        p_init[0],
                        p_init[1],
                        p_init[2],
                        i )
                    # else
                    expb(
                        p_init[0],
                        p_init[1],
                        p_init[2],
                        i )
                    # endif
                );
        }
        gsl_vector_free( tmpF );
        gsl_vector_free( tmpP );
    /* }}} */ }
    # endif

    # ifdef USE_MOYAL
    f.f = &moyal_f;
    f.df = &moyal_df;
    f.fdf = &moyal_fdf;
    # else
    f.f = &expb_f;
    f.df = &expb_df;
    f.fdf = &expb_fdf;
    # endif
    f.n = n;
    f.p = p;
    f.params = &d;

    s = gsl_multifit_fdfsolver_alloc( gsl_multifit_fdfsolver_lmsder, n, p );
    gsl_multifit_fdfsolver_set( s, &f, &P.vector );
    print_state( iter, s ); 
    do {
        iter++;
        status = gsl_multifit_fdfsolver_iterate( s );
        printf("# multifit status = %s\n", gsl_strerror (status));
        print_state(iter, s);
        /*printf("XXX: %e, %e\n", s->x, s->dx );*/
        if(status)
            break;
        status = gsl_multifit_test_delta( s->dx, s->x, 1e-4, 1e-4 );
        printf("# multifit test delta status = %s\n", gsl_strerror (status));
    } while(status == GSL_CONTINUE && iter < 500);
     
    gsl_multifit_covar( s->J, 0.0, covar );

    printf("# *** Fitting exit.\n");

    # define FIT(i) gsl_vector_get(s->x, i)
    # define ERR(i) sqrt(gsl_matrix_get(covar,i,i)) 
    {
        double chi = gsl_blas_dnrm2(s->f);
        double dof = n - p;
        double c = GSL_MAX_DBL( 1, chi/sqrt(dof) ); 
     
        printf( "# chisq/dof = %g\n",  pow(chi,2.0)/dof );
     
        printf( "# p1 = %.5f +/- %.5f\n", FIT(0), c*ERR(0) );
        printf( "# p2 = %.5f +/- %.5f\n", FIT(1), c*ERR(1) );
        printf( "# p3 = %.5f +/- %.5f\n", FIT(2), c*ERR(2) );
    }

    # if 0
    {  /* Testing printout for obtained parameters {{{ */
        gsl_vector * tmpP = gsl_vector_alloc(3),
                   * tmpF = gsl_vector_alloc(n)
                   ;
        gsl_matrix * derivCache = gsl_matrix_alloc( n, p );
        gsl_vector_set( tmpP, 0, p_init[0] );
        gsl_vector_set( tmpP, 1, p_init[1] );
        gsl_vector_set( tmpP, 2, p_init[2] );
        # ifdef USE_MOYAL
        moyal_f( tmpP, &d, tmpF );
        moyal_df( tmpP, &d, derivCache );
        # else
        expb_f( tmpP, &d, tmpF );
        expb_df( tmpP, &d, derivCache );
        # endif
        for( i = 0; i < n; ++i ) {
            printf( "%d %f %f %f %f %f %f\n",
                    (int) i,
                    d.samples[i],
                    gsl_vector_get(tmpF, i),
                    gsl_matrix_get( derivCache, i, 0 ),
                    gsl_matrix_get( derivCache, i, 1 ),
                    gsl_matrix_get( derivCache, i, 2 ),
                    # ifdef USE_MOYAL
                    moyal(
                        p_init[0],
                        p_init[1],
                        p_init[2],
                        i )
                    # else
                    expb(
                        p_init[0],
                        p_init[1],
                        p_init[2],
                        i )
                    # endif
                );
        }
        gsl_vector_free( tmpF );
        gsl_vector_free( tmpP );
    /*}}} */ }
    # endif

    gsl_multifit_fdfsolver_free (s);
    gsl_matrix_free (covar);
    
    return EXIT_SUCCESS;
}

