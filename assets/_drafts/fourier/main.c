/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <stdlib.h>
# include <stdint.h>
# include <string.h>
# include <assert.h>

# include <math.h>
# include <gsl/gsl_errno.h>
# include <gsl/gsl_fft_complex.h>

# include "analysis/sadcWF_fit.h"  /* for Moyal function routines. */

const size_t NSAMPLES = 32;

/*
 * Samples generator
 */

void
generate_samples(
        union SADCWF_FittingFunctionParameters * mp,
        struct SADCWF_FittingInput * sStructPtr
    ) {
    uint8_t n;

    sStructPtr->samples = malloc( sizeof(SADCSamples)*sStructPtr->n );
    for( n = 0; n < sStructPtr->n; ++n ) {
        sStructPtr->samples[n] = moyal(mp, n);
    }
}

void
noise_samples(
        struct SADCWF_FittingInput * sStructPtr,
        double mixinFactor
    ) {
    uint8_t n;
    for( n = 0; n < sStructPtr->n; ++n ) {
        sStructPtr->samples[n] *= 
            1 - mixinFactor*(.5 - ((double)(rand()))/RAND_MAX);
    }
}

void
free_samples( struct SADCWF_FittingInput * sStructPtr ) {
    free( sStructPtr->samples );
}

/*
 * FFT decomposition
 */

struct DiscreteFourierSpectrum {
    double * data;
    size_t n;
};

# define REAL(z,i) ((z)[2*(i)])
# define IMAG(z,i) ((z)[2*(i)+1])

void
get_spectrum( struct SADCWF_FittingInput * sStructPtr,
              struct DiscreteFourierSpectrum * spectrumPtr ) {
    const uint8_t N = sStructPtr->n;
    int i;
    if( !spectrumPtr->data ) {
        spectrumPtr->data = malloc(2*sizeof(double)*N);
        spectrumPtr->n = N;
    } else {
        assert( spectrumPtr->n == N );
    }

    bzero( spectrumPtr->data, 2*sizeof(double)*N );

    for( i = 1; i <= N; i++ ) {
       REAL(spectrumPtr->data,i) = sStructPtr->samples[i];
    }

    gsl_fft_complex_radix2_forward( spectrumPtr->data, 1, N );
}

# undef REAL
# undef IMAG

/*
 * Streaming
 */

void
write_samples(  FILE * destFPtr,
                SADCSamples samples[],
                size_t n,
                char fmt ) {
    size_t i;
    switch(fmt) {
        /* ... */
        default : {
            fprintf( stderr,
                     "write_samples() got unknown output format specification '%c'."
                     " 2-column output will be used.", fmt );
        }
        case '2' : {
            fprintf( destFPtr, "# N\tAmpl.\n" );
            for( i = 0; i < n; ++i ) {
                fprintf( destFPtr, "%zu\t%e\n", i, samples[i] );
            }
        }
    };
}

void
write_spectrum( FILE * destFPtr,
                struct DiscreteFourierSpectrum * spectrumPtr,
                char fmt ) {
    size_t i;

    switch(fmt) {
        /* ... */
        case 'A' : {
            fprintf( destFPtr, "# No\tAbs\n" );
            for( i = 0; i < spectrumPtr->n; ++i ) {
                fprintf( destFPtr, "%zu\t%e\n",
                        i,
                        sqrt( spectrumPtr->data[2*i]*spectrumPtr->data[2*i] + spectrumPtr->data[2*i+1]*spectrumPtr->data[2*i+1] )
                    );
            }
        } break;
        case '2' : {
            fprintf( destFPtr, "# No\tReal\tImag\n" );
            for( i = 0; i < spectrumPtr->n; ++i ) {
                fprintf( destFPtr, "%zu\t%e\t%e\n",
                        i,
                        spectrumPtr->data[2*i],
                        spectrumPtr->data[2*i+1]
                    );
            }
        } break;
        default : {
            fprintf( stderr,
                     "write_samples() got unknown output format specification '%c'."
                     " 2-column output will be used.", fmt );
        }
        case '3' : {
            fprintf( destFPtr, "# No\tReal\tImag\tAbs\n" );
            for( i = 0; i < spectrumPtr->n; ++i ) {
                fprintf( destFPtr, "%zu\t%e\t%e\t%e\n",
                        i,
                        spectrumPtr->data[2*i],
                        spectrumPtr->data[2*i+1],
                        sqrt( spectrumPtr->data[2*i]*spectrumPtr->data[2*i] + spectrumPtr->data[2*i+1]*spectrumPtr->data[2*i+1] )
                    );
            }
        }
    };
}

/*
 * Testing routines
 */

void
subroutine_first() {
    /* This subroutine:
     *  1. Produces Moyal function profile with {3116, 19, 2} parameters
     *     and writes it into 'sourceSignal.dat'.
     *  2. Provides DFT with source and writes it into 'sourceDFTSpec.dat';
     *  3. Applies randomized noise to source function and writes
     *     resulting profile to 'uNoiseSignal.dat'
     *  4. Provides DFT with noised profile and writes it into 'uNoiseSignalDFTSpec.dat';
     *  ????
     *  PROFIT!!
     * */
    size_t i;
    struct SADCWF_FittingInput generated =  {NSAMPLES, /* ... unused */},
                               background = {NSAMPLES, /* ... unused */};
    union SADCWF_FittingFunctionParameters
              mp = {{ {3116, 19, 2 } }},
            bgMp = {{ {2000, 20, 16}, /* ... unused */}};
    struct DiscreteFourierSpectrum dftSpec = {NULL, 0};
    FILE * outf = NULL;

    /* 1: */
    generate_samples(   &mp, &generated );
    outf = fopen( "sourceSignal.dat", "w" ); {
        write_samples( outf, generated.samples, generated.n, '2' );
    } fclose( outf );
    /* 2: */
    get_spectrum( &generated, &dftSpec );
    outf = fopen( "sourceDFTSpec.dat", "w" ); {
        write_spectrum( outf, &dftSpec, '3' );
    } fclose( outf );
    /* 3: */
    noise_samples( &generated, .3 );
    outf = fopen( "uNoiseSignal.dat", "w" ); {
        write_samples( outf, generated.samples, generated.n, '2' );
    } fclose( outf );
    /* 4: */
    get_spectrum( &generated, &dftSpec );
    outf = fopen( "sourceDFTSpecNoise.dat", "w" ); {
        write_spectrum( outf, &dftSpec, '3' );
    } fclose( outf );
    /* 5: */
    generate_samples( &bgMp, &background );
    outf = fopen( "backgroundNoise.dat", "w" ); {
        write_samples( outf, background.samples, background.n, '2' );
    } fclose( outf );
    /* 6: */
    get_spectrum( &background, &dftSpec );
    outf = fopen( "backgroundDFTSpec.dat", "w" ); {
        write_spectrum( outf, &dftSpec, '3' );
    } fclose( outf );
    /* 7: */
    for( i = 0; i < NSAMPLES; ++i ) {
        generated.samples[i] += background.samples[i];
    }
    outf = fopen( "resultingSignal.dat", "w" ); {
        write_samples( outf, generated.samples, generated.n, '2' );
    } fclose( outf );
    /* 8: */
    get_spectrum( &generated, &dftSpec );
    outf = fopen( "resultingDFTSpec.dat", "w" ); {
        write_spectrum( outf, &dftSpec, '3' );
    } fclose( outf );

    free_samples( &generated );
    free_samples( &background );
}

/*
 * Entry point
 */

int
main(int argc, char * argv[]) {

    subroutine_first();

    return EXIT_SUCCESS;
}

