/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <stdint.h>
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <gsl/gsl_rng.h>
# include <gsl/gsl_randist.h>
# include <gsl/gsl_vector.h>
# include <gsl/gsl_blas.h>
# include <gsl/gsl_multifit_nlin.h>

const uint16_t srcSamples[] = {
    213,  191,  216,  192,     215,  189,  214,  193,
    213,  189,  215,  192,     214,  191,  214,  193,
    252,  488,  907,  1360,    1630, 1625, 1490, 1285,
    1103, 896,  750,  595,     508,  406,  372,  311
};

int
main(int argc, char * argv[]) {
    //const uint8_t N = sizeof(srcSamples)/sizeof(uint16_t);
    const gsl_multifit_fsolver_type * T;    // TODO
    const gsl_vector * xGuesses;            // TODO
    gsl_multifit_function * f;              // TODO
    gsl_multifit_fsolver * solver = gsl_multifit_fsolver_alloc( T, 1, 3 );
    gsl_multifit_fsolver_set( solver, f, xGuesses );

    return EXIT_SUCCESS;
}


