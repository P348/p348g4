# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


# This file contains environment settings designed to meet p348g4 library needs.
# It is recommended to use it like (./path/to/env && do whatever) or to just
# include it into another shell-scripts. Direct source-load in BASH environment
# is not recommended.


DIR_AFS_SW=/afs/cern.ch/sw
DIR_HOME=/afs/cern.ch/user/r/rdusaev
DIR_P348G4_SRC=../../p348g4
DIR_BOOST=$DIR_AFS_SW/lcg/external/Boost/1.55.0_python2.7/x86_64-slc6-gcc47-opt
DIR_GSL=$DIR_AFS_SW/lcg/external/GSL/1.14/x86_64-slc5-gcc46-opt

#
# GCC settings
GCC_VER=4.9.3
GCC_PREFIX="$DIR_AFS_SW/lcg/contrib/gcc/$GCC_VER/x86_64-slc6"
CXX_CFLAGS="-Wl,-rpath,$GCC_PREFIX/lib64" 

#
# Geant4 settings
G4_VER=4.10.1.ref05
G4_REL_VER="4-10.2.0"

#
# CMake settings
CMAKE=$DIR_AFS_SW/lcg/external/CMake/3.5.2/Linux-x86_64/bin/cmake
Boost_DIR=$DIR_BOOST/logs
Protobuf_DIR=/afs/cern.ch/user/r/rdusaev/public/3rdParty/protobuf.install/usr/local/lib/cmake/protobuf/

CMAKE_INCLUDE_PATH=$DIR_BOOST/include/Boost_1-55/boost
export LD_LIBRARY_PATH=$GCC_PREFIX/lib64:$DIR_HOME/public/3rdParty/protobuf.install/usr/local/lib64:$DIR_BOOST/lib:$DIR_GSL/lib:$LD_LIBRARY_PATH

#~/3rdParty/protobuf.install/usr/local/lib/cmake/protobuf/
