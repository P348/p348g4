#!/usr/bin/env bash
#
# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  Script to setup environment variables for Geant4 datasets # 
#    used by achitecture dependent setup scripts.

datadir=/afs/cern.ch/sw/geant4/releases/share/data

export G4LEVELGAMMADATA=${datadir}/PhotonEvaporation3.1
export G4NEUTRONXSDATA=${datadir}/G4NEUTRONXS1.4
export G4LEDATA=${datadir}/G4EMLOW6.44
export G4NEUTRONHPDATA=${datadir}/G4NDL4.5
export G4RADIOACTIVEDATA=${datadir}/RadioactiveDecay4.2
export G4ABLADATA=${datadir}/G4ABLA3.0
export G4PIIDATA=${datadir}/G4PII1.3
export G4SAIDXSDATA=${datadir}/G4SAIDDATA1.1
export G4REALSURFACEDATA=${datadir}/RealSurface1.0

