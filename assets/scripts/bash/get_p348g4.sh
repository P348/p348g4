#!/bin/sh

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# initialize some shell variables
DEPL_DIR=$(realpath .)  # current path (absolute)
GOO_DIR="$DEPL_DIR/goo.install"  # path were to install a Goo library

# clone repos
git clone git@bitbucket.org:CrankOne/p348g4.git
git clone https://bitbucket.org/CrankOne/goo

# make build directories
mkdir ./p348g4.build/debug -p
mkdir ./goo.build/debug -p

# workaround this silly CMake-script bug
touch ./goo/.git/COMMIT_EDITMSG
touch ./p348g4/.git/COMMIT_EDITMSG

# build and install goo library
cd ./goo.build/debug
cmake ../../goo #-DCMAKE_INSTALL_PREFIX="goo.install"
make
make "DESTDIR=$GOO_DIR" install
cd ../..  # get back to current dir

# run goo tests
$GOO_DIR/usr/local/bin/GooTests

# build p348g4 library with aprimeEvGen utility
cd ./p348g4.build/debug
(export PKG_CONFIG_PATH=$GOO_DIR/usr/local/share/pkgconfig/ && cmake \
        ../../p348g4 \
        -DAPRIME_CS_ROUTINES=ON \
        -Dbuild_apeg_model=ON \
        -DAPRIME_EVENT_GENERATOR=ON \
        -DCMAKE_MODULE_PATH=$GOO_DIR/usr/local/share/cmake/Modules/ \
        -Dgoo_INCLUDE_DIR=/tmp/goo.install/usr/local/include/goo/ \
        -Dgoo_LIBRARY=/tmp/goo.install/usr/local/lib/libgoo.so )
make
cd ../..  # get back to current dir
