# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

"""
This file defines actions dictionary for development purposes.
Demonstrates the design of actions class.
"""

class Scriptlet(object):
    """
    A scriptlet class providing evaluation within given context.
    """
    def __init__(self, evalStr=None):
        if evalStr:
            self.pyCode = evalStr
        else:
            self.pyCode = "locals().update(context)\n" \
                          "context['{_}'] = %s"
        self.cc = None

    def evaluate(self, context, expr, **kwargs):
        if not '_' in kwargs.keys():
            kwargs['_'] = '_'
        evalStr = self.pyCode.format(kwargs)%expr
        print( '>>Evaluating\n%s\n<<'%evalStr )
        self.cc = compile(evalStr, "/dev/shm/uniqFileNameTODO", 'exec')
        exec(self.cc, {'context' : context})
        return context[kwargs['_']]


class GDMLActionsDictionary(object):
    """
    Testing tool. Just has all the handlers necessary for action dictionary
    that can be invoked within GDML processing.
    """
    def __init__(self, printOut=True):
        self.doPrint = printOut
        self.definitions = {
            'constants' : {},
            'variables' : {},
            'quantities' : {},
            'positions' : {},
            'rotations' : {},
            'scales' : {},
            'matrices' : {},
        }
        self.commonContext = {}

    def update_definitions(self, cathegory, newDefName, **kwargs):
        tDct = self.definitions[cathegory]
        if newDefName in tDct.keys():
            print( 'Warning: re-definition of "%s" entry in %s dictionary.'%(
                newDefName, cathegory ) )
        tDct[newDefName] = kwargs

    def numerical_value(self, expr, scriptletID="default" ):
        """
        Tries to evaluate provided string to a numerical value considering all
        the known named objects.
        """
        pass

    #
    # Definitions:

    def gdml_constant(self, defDict):
        self.update_definitions( 'constants', defDict['name'], **defDict )
        if self.doPrint:
            print( 'const %s <- %s'%(defDict['name'], defDict['value']) )

    def gdml_variable(self, defDict):
        self.update_definitions( 'variables', defDict['name'], **defDict )
        if self.doPrint:
            print( '%s <- %s'%(defDict['name'], defDict['value']) )

    def gdml_quantity(self, defDict):
        self.update_definitions( 'quantities', defDict['name'], **defDict )
        if self.doPrint:
            print( '%s:%s <- %s [%s]'%(defDict['type'], defDict['name'], defDict['value'], defDict['unit']) )

    # TODO: positions
    # TODO: rotations
    # TODO: scales
    # TODO: matrices

    #
    # Materials:

    # TODO: Isotopes
    # TODO: Elements
    # TODO: Materials

    #
    # Solids (shall we dedicate a class to CSG?):

    # TODO: Box
    # TODO: Cone segment
    # TODO: Ellipsoid
    # TODO: Elliptical tube
    # TODO: Elliptical cone
    # TODO: Orb
    # TODO: Paraboloid
    # TODO: Parallelipiped
    # TODO: Polycone
    # TODO: Generic polycone
    # TODO: Polyhedron
    # TODO: Generic Polyhedron
    # TODO: Sphere
    # TODO: Torus Segment
    # TODO: Trapezoid
    # TODO: General Trapezoid
    # TODO: Hyperbolic Tube
    # TODO: Cut Tube
    # TODO: Tube Segment
    # TODO: Twisted Box
    # TODO: Twisted Trapezoid
    # TODO: Twisted	General	Trapezoid
    # TODO: Twisted	Tube Segment
    # TODO: Extruded Solid
    # TODO: Arbitrary Trapezoid
    # TODO: Tessellated	solid
    # TODO: Tetrahedron

    # TODO: Loop over solids
    # TODO: Boolean operations
    # TODO: Scaled solids

    # TODO: A LOT of paremeterised volumes...

    # TODO: setup

