/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <iostream>
# include "nsp_scorer.hpp"

# ifdef STANDALONE_BUILD

# define FLOAT_TYPE long double

int
main(int argc, char * argv[]) {
    const size_t scorerPoolSize = 10000;
    size_t length = 0;
    double factor = 1;
    unsigned long seed = 0;
    if( argc > 4 || argc < 3 ) {
        std::cerr << "Syntax:" << std::endl
                  << "\t$ " << argv[0] << " <nnodes:integer> <factor:double> [seed:integer]" << std::endl;
        return EXIT_FAILURE;
    } else {
        char * c;
        length = atoi(argv[1]);
        factor = strtod(argv[2], &c);
        if( argc >= 3 ) {
            srand(seed = atol(argv[3]));
        }
    }

    p348::aux::NotSoPrimitiveScorer<FLOAT_TYPE, size_t> sc(scorerPoolSize);
    
    FLOAT_TYPE * arr = new FLOAT_TYPE [length];
    double directSum = 0;
    for( size_t i = 0; i < length; ++i ) {
        arr[i] = rand();
        arr[i] /= RAND_MAX;
        arr[i] *= pow(factor, (RAND_MAX/2. - rand())/(RAND_MAX/4.));
        directSum += arr[i];
        sc.push_value(arr[i]);
    }

    std::cout << "  length: " << length << std::endl
              << "  factor: " << factor << std::endl
              << "    seed: " << seed << std::endl
              << " -------- " << std::endl
              << "  direct: " << directSum << std::endl
              ;
    double kahanS = kahan_sum(arr, length);
    double pairwiseSum = pairwise_sum( arr, length );
    std::cout << "   Kahan: " << kahanS << std::endl
              << "pairwise: " << pairwiseSum << std::endl
              << " -------- " << std::endl
              << "direct vs. pairwise: " << fabs( directSum - pairwiseSum ) << std::endl
              << "   direct vs. Kahan: " << fabs( directSum - kahanS ) << std::endl
              << " Kahan vs. pairwise: " << fabs( kahanS - pairwiseSum ) << std::endl
              << " -------- " << std::endl
              << "  Scorer vs pairwise: " << fabs(sc.sum() - pairwiseSum) << " ("
              << 100*fabs(sc.sum() - pairwiseSum)/pairwiseSum << "%)" << std::endl
              << "         Scorer size: " << scorerPoolSize*sizeof(FLOAT_TYPE)/1024. << "kb" << std::endl
              ;

    return EXIT_SUCCESS;
}

# endif  // STANDALONE_BUILD


