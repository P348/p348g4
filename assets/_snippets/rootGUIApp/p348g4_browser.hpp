/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348G4_APP_GUI_BROWSER_H
# define H_P348G4_APP_GUI_BROWSER_H

# include <TGClient.h>
# include <TGButton.h>
# include <TGLabel.h>
# include <TGFrame.h>
# include <TGLayout.h>
# include <TGSplitter.h>

# include "browserEntry.hpp"

class ObjectBrowsingFrame : public TGMainFrame {
protected:
    Entry * _rootCat;

    TGCompositeFrame
            *   _menuFrame,
            *   _desktopFrame,
            *       _browsingFrame,
            *           _browsingFrameSub,
            *       _tabsFrame,
            *           _tabsFrameSub,
            *   _statusBarFrame
            ;
    void _compose_menubar();
    void _compose_statusbar();
    void _compose_entity_browser();
    void _compose_entity_tree_view();
    void _compose_entity_viewport();
public:
   ObjectBrowsingFrame(const TGWindow *p, UInt_t w, UInt_t h);
   virtual ~ObjectBrowsingFrame();
   void DoSave();
   void CloseWindow();
   
   ClassDef(ObjectBrowsingFrame, 0)

   template<typename T> void insert_entry( const std::string & name, T entry );
};

// insert cathegory:
template<> void ObjectBrowsingFrame::insert_entry( const std::string & name, void * );
// insert callback:
template<> void ObjectBrowsingFrame::insert_entry( const std::string & name, Entry::Callback );

# endif  // H_P348G4_APP_GUI_BROWSER_H

