/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <TApplication.h>
# include "p348g4_browser.hpp"
# include "browserEntry.hpp"

// Object folder --- ?

// class
// draw_t
// eve_scene
// f1_t
// f2_t
// h1_t
// h2_t
// h3_t
// leaf_t
// newcanvas
// ntuple_t
// pack-empty_t / pack_t
// profile_t
// refresh
// rootdb_t
// selection_t
// sm_import_canvas
// tb_smicons
// tmacro_t
// tree_t

# if 1
Entry *
produce_entries() {
    add_entry_browser_hook<TH1F>( "detectors/statistics", _someHisto, dynamic | eventual );
}
# endif

int
main( int argc, char * argv[] ) {
    TApplication theApp("App", &argc, argv);
    new ObjectBrowsingFrame(gClient->GetRoot(), 800, 600);
    theApp.Run();
    return 0;
}

