/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * A testing application for out tiny template subst.
 */

# include <cstdlib>
# include <regex>
# include <unordered_map>
# include <sstream>
# include <list>

# include <iostream>

class TextSubst {
protected:

    //
    // Substitution entry base class
    class SubstitutionEntry {
    protected:
        virtual std::string _V_to_str( const TextSubst * ) const = 0;
    public:
        virtual ~SubstitutionEntry(){}
        std::string to_str( const TextSubst * P ) const {
                return _V_to_str( P ); }
    };

    //
    // Plain string substitution
    class PlainSubstitution : public SubstitutionEntry {
    protected:
        std::string _strValue;
        virtual std::string _V_to_str( const TextSubst * ) const override {
            return _strValue;
        }
    public:
        PlainSubstitution(
                const std::string value
            ) : _strValue(value){}
    };

    //
    // ...

    typedef std::unordered_map<std::string, SubstitutionEntry *> Dictionary;
    typedef std::list<std::string> NamesList;

    Dictionary _dict;
protected:
    std::regex _rxVar,
               _rxName;
    std::list<SubstitutionEntry *> _2rem;
    void _clear_allocated();
public:
    TextSubst() : _rxVar(  "\\$\\{([[:alnum:]_-]+)\\}" ),
                  _rxName(        "[[:alnum:]_-]+" ) {}
    virtual ~TextSubst() { _clear_allocated(); }

    /// Returns a number of performed substitutions.
    size_t subst( std::istream & is, std::ostream & os ) const;

    static NamesList get_subst_names_list(
            std::string,
            const std::regex & varrx,
            const std::regex & namrx );
    static void substitute_in_line(
            std::string & line,
            const std::regex & e,
            const std::string & dstToken );

    // Inserters

    TextSubst & operator()( const std::string & name,
                       const std::string & value ) {
        // todo: check strings for validity
        // (no '$', '(', ')' chars in value)
        auto sePtr = new PlainSubstitution( value );
        _2rem.push_back(sePtr);
        _dict[name] = sePtr;
        return *this;
    }
};

//
// Implem
//

TextSubst::NamesList
TextSubst::get_subst_names_list(
            std::string s,
            const std::regex & varrx,
            const std::regex & namrx ) {
    NamesList nl;

    // Regex for searching variables:
    for( std::smatch m;
         std::regex_search(s, m, varrx);
         s = m.suffix().str() ) {
        for(auto x: m){
            if( std::regex_match(x.str(), namrx) ) {
                nl.push_back( x.str() );
            }
        }
    }

    return nl;
}

void
TextSubst::substitute_in_line(
            std::string & line,
            const std::regex & e,
            const std::string & dstToken ) {
    std::string result;
    std::regex_replace( std::back_inserter(result),
                        line.begin(), line.end(),
                        e, dstToken );
    line = result;
}

size_t
TextSubst::subst( std::istream & is, std::ostream & os ) const {
    size_t nSubst = 0,
           nLines = 0;

    for( std::string line; std::getline(is, line); nLines++ ) { // for each line
        NamesList varNames = get_subst_names_list(
                line, _rxVar, _rxName );
        uint8_t substCtr = 0;
        do {
            for( auto varName : varNames ) {  // for each variable name found in line
                auto substPairIt = _dict.find( varName );
                if( _dict.end() == substPairIt ) {
                    throw std::runtime_error( "Couldn't resolve name \"" + varName + "\"." );
                } else {
                    substitute_in_line(
                            line,
                            std::regex("\\$\\{" + varName + "\\}"),
                            substPairIt->second->to_str( this )
                        );
                }
            }
        } while( !(varNames = get_subst_names_list(line, _rxVar, _rxName )).empty() && ++substCtr < 0xfd );
        // TODO: escaping here?
        if( substCtr >= 0xfd ) {
            throw std::runtime_error( "Circular substitution." );
        } else {
            os << line << std::endl;
        }
    }

    return nSubst;
}

void
TextSubst::_clear_allocated() {
    for( auto ptr : _2rem ) {
        delete ptr;
    }
}

//
// Entry point
//

int
main( int argc, char * argv[] ) {
    TextSubst S;
    S( "animal",   "fox" )
     ( "action",   "jumps" )
     ( "1",        "${animal}" )
     ( "foo-bar",  "${1}" );

    std::stringstream ss(
            "Quick brown ${animal} ${action} over the lazy dog.\n"
            "${1} ${1}.\n"
            "${foo-bar}. zum\n"
        );

    S.subst( ss, std::cout );

    return EXIT_SUCCESS;
}

