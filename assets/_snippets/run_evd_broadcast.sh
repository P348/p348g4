# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# This script launches an analysis application for event multicasting event (to be
# received then by event display GUI application). All the options provided to the
# script will be appended to application call.

CWD=${CWD:-.}
CFG_FILE=${CFG_FILE:-../../p348g4/defaults.cfg}
ANALYSIS_UTIL_EXEC=$CWD/utils/p348a/p348a_run

#
# The processors on pipeline will be applied to dataflow sequentially:
PIPELINE_HANDLERS=( sadcWFAlign         \
                    SimpleAPVClustering \
                    evd-ECAL            \
                    evd-HCAL            \
                    evd-MM              \
                    multicast )

#
# Data source
DATA_SOURCE=/data/disk1/Statistics/CERN/na64/2016/cdr01151-001509.dat
DATA_FORMAT=ddd

# For DDD format the mapping XML
DDD_MAP_DIR=/data/disk1/Statistics/CERN/na64/maps/2016.xml/

#
# Prepend PIPELINE_HANDLERS entries with -p option
PIPELINE_HANDLERS=( "${PIPELINE_HANDLERS[@]/#/ -p}" )

export COREDUMP_ON_SEGFAULT=enable
export ATTACH_GDB_ON_SEGFAULT=enable
#export PRESERVE_ROOT_SIGNAL_HANDLERS=enable

#
# Go!
set -x
$ANALYSIS_UTIL_EXEC --config=$CFG_FILE -A1 \
                     -i $DATA_SOURCE -F $DATA_FORMAT --ddd.map-dir=$DDD_MAP_DIR \
                     ${PIPELINE_HANDLERS[@]} \
                     --sadc-wf-alignment.enable-dynamic-correction=mean \
                     --sadc-wf-alignment.means-file-in=SADC-pedestals-01151-001509.txt \
                     --apv-simple.collect-stats=yes \
                     --evd-summary.MM.bestCluster=maxAmplitude \
                     $@

