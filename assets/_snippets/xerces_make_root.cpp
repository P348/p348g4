/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/*
 * Here I tried to compose a root gdml file by Xerces API
 * as it seems to be more self-consistant instead of preparing and
 * parsing a string template.
 *
 * However, it seems to be much more complicated to make Xerces keep
 * !ENTITY fields unresolved in <!DOCTYPE gdml [...]> header.
 *                                                  Crank (16/10/015)
 *
 */

# include <string>
# include <iostream>
# include <sstream>
# include <xercesc/util/PlatformUtils.hpp>
# include <xercesc/dom/DOM.hpp>
# include <xercesc/framework/StdOutFormatTarget.hpp>
# include <xercesc/framework/LocalFileFormatTarget.hpp>

namespace xc = xercesc;
# define XTr(w) xc::XMLString::transcode(w)

int
serializeDOM(xc::DOMNode* node) {
    XMLCh tempStr[100];
    xc::XMLString::transcode("LS", tempStr, 99);
    xc::DOMImplementation *impl = xc::DOMImplementationRegistry::getDOMImplementation(tempStr);
    xc::DOMLSSerializer* theSerializer = ((xc::DOMImplementationLS*)impl)->createLSSerializer();

    // optionally you can set some features on this serializer
    //if (theSerializer->getDomConfig()->canSetParameter(xc::XMLUni::fgDOMWRTDiscardDefaultContent, true))
    //    theSerializer->getDomConfig()->setParameter(xc::XMLUni::fgDOMWRTDiscardDefaultContent, true);

    //if (theSerializer->getDomConfig()->canSetParameter(xc::XMLUni::fgEntityString, true)) {
    //    theSerializer->getDomConfig()->setParameter(xc::XMLUni::fgEntityString, true);
    //}

    //if (theSerializer->getDomConfig()->canSetParameter(xc::XMLUni::fgDTDEntityString, true)) {
    //    theSerializer->getDomConfig()->setParameter(xc::XMLUni::fgDTDEntityString, true);
    //}
    theSerializer->getDomConfig()->setParameter( XTr("entities"), true );

    if (theSerializer->getDomConfig()->canSetParameter(xc::XMLUni::fgDOMWRTFormatPrettyPrint, true))
         theSerializer->getDomConfig()->setParameter(xc::XMLUni::fgDOMWRTFormatPrettyPrint, true);

    // StdOutFormatTarget prints the resultant XML stream
    // to stdout once it receives any thing from the serializer.
    xc::XMLFormatTarget *myFormTarget = new xc::StdOutFormatTarget();
    xc::DOMLSOutput* theOutput = ((xc::DOMImplementationLS*)impl)->createLSOutput();
    theOutput->setByteStream(myFormTarget);

    // do the serialization through DOMLSSerializer::write();
    theSerializer->write(node, theOutput);

    theOutput->release();
    theSerializer->release();
    delete myFormTarget;
    return 0;
}

xc::DOMDocument *
synthesize_root() {
    xc::DOMImplementation * domImplPtr = nullptr;
    xc::DOMDocument * doc = nullptr;
    domImplPtr = xc::DOMImplementationRegistry::getDOMImplementation(XTr("core"));
    auto docType = domImplPtr->createDocumentType(
            XTr("gdml"), NULL, NULL );

    doc = domImplPtr->createDocument(XTr("http://www.w3.org/2001/XMLSchema-instance"),
                                     XTr("xsi:gdml"),
                                     docType);
    //doc = domImplPtr->createDocument(0, XTr("gdml"), 0);

    xc::DOMElement * root = nullptr;
    root = doc->getDocumentElement();

    // Set entities.
    xc::DOMEntity * rootPathEnt = nullptr; {
        rootPathEnt = doc->createEntity( XTr("rootPath") );
        rootPathEnt->setNodeValue( XTr("SYSTEM") );
    } docType->getEntities()->setNamedItem( rootPathEnt );

    {
        // retrieve the entity - works fine
        auto foo = docType->getEntities()->getNamedItem(XTr("rootPath"));
        std::cout << foo->getNodeValue();
        for(unsigned int i = 0; i < foo->getChildNodes()->getLength(); i++) {
            std::cout << ":::" << foo->getChildNodes()->item(i) << std::endl;
        }
    }

    {   // Just a warning.
        xc::DOMComment * pDOMComment = NULL;
        pDOMComment = doc->createComment(XTr(" WARNING: Auto-generated root GDML struct. \n"
                                       "       All changes will be lost on re-generation. "));
        root->appendChild(pDOMComment);
    }

    xc::DOMElement * defines = nullptr; {   // Add a defines element (containing alias).
        defines = doc->createElement(XTr("defines"));
    } root->appendChild( defines );

    xc::DOMElement * materials = nullptr; {   // Add a materials element (containing alias).
        materials = doc->createElement(XTr("materials"));
    } root->appendChild( materials );

    xc::DOMElement * solids = nullptr; {   // Add a solids element (containing alias).
       solids = doc->createElement(XTr("solids"));
    } root->appendChild( solids );

    xc::DOMElement * structure = nullptr; {   // Add a structure element (containing alias).
        structure = doc->createElement(XTr("structure"));
    } root->appendChild( structure );

    xc::DOMElement * setup = nullptr; {   // Add a setup element.
        setup = doc->createElement(XTr("setup"));
    } root->appendChild(setup);
    //serializeDOM( doc );
    {
        XMLCh tempStr[100];
        xc::XMLString::transcode("LS", tempStr, 99);
        //xc::DOMImplementation *impl = xc::DOMImplementationRegistry::getDOMImplementation(tempStr);
        xc::DOMLSSerializer* theSerializer = ((xc::DOMImplementationLS*) domImplPtr)->createLSSerializer();

        // optionally you can set some features on this serializer
        //if (theSerializer->getDomConfig()->canSetParameter(xc::XMLUni::fgDOMWRTDiscardDefaultContent, true))
        //    theSerializer->getDomConfig()->setParameter(xc::XMLUni::fgDOMWRTDiscardDefaultContent, true);

        if (theSerializer->getDomConfig()->canSetParameter(xc::XMLUni::fgDOMWRTFormatPrettyPrint, true))
             theSerializer->getDomConfig()->setParameter(xc::XMLUni::fgDOMWRTFormatPrettyPrint, true);

        // StdOutFormatTarget prints the resultant XML stream
        // to stdout once it receives any thing from the serializer.
        xc::XMLFormatTarget *myFormTarget = new xc::StdOutFormatTarget();
        xc::DOMLSOutput* theOutput = ((xc::DOMImplementationLS*)domImplPtr)->createLSOutput();
        theOutput->setByteStream(myFormTarget);

        // do the serialization through DOMLSSerializer::write();
        theSerializer->write(doc, theOutput);

        theOutput->release();
        theSerializer->release();
        delete myFormTarget;
    }

    return doc;
}

int
main( int argc, char* argv[] ) {
    try {
        xc::XMLPlatformUtils::Initialize();
        synthesize_root();
    } catch( xc::XMLException& xmlE) {
        std::cerr << "XMLException: "
            << xc::XMLString::transcode(xmlE.getMessage()) << std::endl;
        return EXIT_FAILURE;
    } catch( xc::DOMException& domE ) {
        std::cerr << "DOMException/"
            << domE.code << ": "
            << xc::XMLString::transcode(domE.getMessage()) << std::endl;
        return EXIT_FAILURE;
    }

    xc::XMLPlatformUtils::Terminate();
    return EXIT_SUCCESS;
}
