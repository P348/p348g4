# Emerging event display facility on CERN LXPLUS host


## Existing DAQ postprocessing facility

After all data from all the detectors is collected and encoded into raw form,
it could be saved to file or transmitted by network. Actual nderlying protocol
of data transmission on most-basic (just after sockets) level is encapsulated
by RFIO library that was formely developed by CASTOR group. I was unable to
obtain sources of RFIO library. However, headers and built binary are
available as a standard package of «Scientific Linux» distributive.

For the moment the data representing serialized events can be translated to
semantical structures by DaqDecodingLibrary (DDD, originates from CORAL network).
This library also resides on abstraction layer for data reading sources, both
remote source (managed by RFIO) and local file which is provided by «monitor»
part of DATE libraries set. The DATE currently used in COMPASS (NA58)
experiment, however its development now seems to became darmant.


## Event display conception

In p348g4 lib we are separated rapid reconstruction routines and graphical
representation of them into client-server scheme. So, yes, basically there are
two subsequent servers: one does actual DAQ and our fast analysis application
uses it as a remote data source via chain of abstraction layers:
`DDD -> libmonitor.a -> libshift.so`.

Up to the moment when event display facility was needed we already
implemented event processing pipeline in the frame of analysis application
with unified events format transmitting through the run-time composed chain
of handlers (we call them «processors»). This approach is widely used
for such a kind of tasks (see, e.g. Facebook «Wiggle» framework). All that was
needed to add network-multicasting application is to add a new processor
which is tagged «broadcast» implementing asynchroneous event network
multicasting.

It is already time to introduce something not so fossil for event
representation. A well-suitable approach is to implement this data
structures in form of «Google Protocol Buffers» messages. They are
extensible and easy to manage inside of network applications. So our
multicasting processor transmittes serialized introspective
protobuf-messages.

All the actual event display application does is receiving this messages
asynchroneously, keep some of them in circular buffer and draw hits
showing some representative picture of detector state.


## Building p348g4 library dependencies on LXPLUS

It is a little bit complicated to compile modern C++11 code using linux
distributive which system environment was shaped few years ago, so thing
are not as easy as just to clone git repo and run CMake.

### Obtaining sources

It is desirable to use recent version of git to avoid some minor
incompatibilities between repository server and client. At LXPLUS
machines modern software builds are available on /afs mountpoints. To clone
p348g4 sources, one can use, for instance: 
`/cvmfs/cms.cern.ch/slc6_amd64_gcc493/cms/cmssw/CMSSW_8_0_0_pre5/external/slc6_amd64_gcc493/bin/git`.

Then, one can create usual out-of-source built directories structure `p348g4.build/debug`.

### Building dependencies:

In order to be able to just read the raw data one need only DDD library.
This library could be built without networking support that eliminates
dependency from DATE/monitoring and RFIO (shift) libs. DDD included in
NA64 «p348g4-daq» repo: `https://gitlab.cern.ch/P348/p348-daq`.

### Building RFIO (shift) library

Currently, I have not found sources of this lib. Only available options is to
use built from SLC distributive.

### Building DATE/monitor lib

On lxplus, sources of DATE lib sources can be found at location
`/afs/cern.ch/compass/online/daq/dateV371/`. This repo contains sources with
in-source build, however, the monitor library available there is 32-bit lib,
so we need to re-build it for x86_64 architecture:

   1. Copy dateV?? from /afs/compass/online/daq/dateV?? somewhere.
   2. Set in commonDefs/shellParams.Linux appropriate compiler
   3. cd to copyied directory root and run `$ DATE_ROOT=$(pwd) ./setup.sh`
   4. In `monitor/` dir do `$ DATE_ROOT=$(pwd)/.. gmake Linux/libmonitor.a` to build
      only one library we need.

### Building DDD lib with linkage against DATE/monitor

cd to `coral/src/DaqDataDecoding` dir of cloned `p348g4-daq` repo and do:
    $ ./configure --with-DATE_LIB=/afs/cern.ch/compass/online/daq/dateV37/ --with-RFIO

run `make` to yield the shared library.

### Building protobuf library

After obtaining sources from official grpotobuf-repo one can build it
via configure script (not recommended), e.g.:

    ./configure --prefix=/afs/cern.ch/user/r/rdusaev/3rdParty/protobuf-3.0.0-beta-3/protobuf.install/usr/local/

Or using the CMake system specifying installation location and particular
compiler in standard manner.

## Buidling p348g4 library



