<?xml version="1.0" encoding="UTF-8"?>

<!--
 Copyright (c) 2016 Bogdan Vasilishin <togetherwithra@gmail.com>
 Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->

<!DOCTYPE gdml [
    <!ENTITY defines    SYSTEM "../01_defines.igdml">
    <!ENTITY materials  SYSTEM "../02_materials.igdml">
    <!ENTITY solids     SYSTEM "../03_solids.igdml">
]>

<gdml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd">
    
    <define>
        &defines;

    </define>
       
    <materials>
        &materials;
    </materials>

    <solids>
        &solids;
        
        <tube name="geo_mag_vessel_Al"          rmin="mag_vessel_rmin" rmax="mag_vessel_rmax" z="mag_vessel_z" 
                                            deltaphi="mag_vessel_deltaphi" />

        <tube name="geo_mag_vessel_Vacuum"      rmin="mag_vessel_rmin" rmax="mag_vessel_Vacuum_rmax" z="mag_vessel_z"
                                            deltaphi="mag_vessel_deltaphi" />
    </solids>

    <structure>
        <volume     name="mag_vessel_Vacuum">
            <materialref    ref="G4_Galactic"/>
            <solidref       ref="geo_mag_vessel_Vacuum"/>
            <auxiliary      auxtype="style" auxvalue="Default:!hidden/Detailed:#98ddff,!surface" />
            <auxiliary      auxtype="field"   auxvalue="MagneticField:0./1.43/0."/>
        </volume>

        <volume     name="mag_vessel_Al">
            <materialref    ref="G4_Al"/>
            <solidref       ref="geo_mag_vessel_Al"/>

            <physvol>
                <volumeref      ref="mag_vessel_Vacuum"/>
                <positionref    ref="positionZero"/>
            </physvol>

            <auxiliary      auxtype="style" auxvalue="Default:!wireframe/Detailed:!wireframe" />
        </volume>
    </structure>

    <setup name="Default" version="1.0" >
        <world ref="mag_vessel_Al" />
    </setup>

    <!-- Wireframe, demonstrates, that the geometry is really segmentated -->
    <setup name="Detailed" version="1.0" >
        <world ref="mag_vessel_Al" />
    </setup>

</gdml>
