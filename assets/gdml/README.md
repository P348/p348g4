# Geometry Library

This library contains geometrical description of detectors taking part in
NA64 experiment. The organization of this directory is claimed by
[extGDML package](https://bitbucket.org/CrankOne/ext.gdml) performing all the
pre-processing operations on the GDML templates.

## Structure


Setup parts, in order, located at corresponding directories. For
simulation reasons each part can be presented in different ways. E.g.
electronic calorimeter (ECAL) actually has transversal segmentation, but
influence of such segmentation can be slightly isignificant (or can not), so
it is convinient to have both versions of this geometry.

Note: sandwich formula (Crank, 27/08/015).
Note: Forgive me, but I fix this great formula, 'cause before there was 'plus' here (Somebody, sometime)
Since some of us were unable to obtain this by themselves, I'm having to write
this note. Suppose, one has a sandwich of layers. Each layer $i$ has own width
$w_i$. The longitudal distance of sandwich from its center to center of layer
$n$, thus, can be obtained:

    d_n = ( \sum_{i < n} w_i - \sum_{j > n} w_j )/2

Please, use this dealing with sandwich-like geometries.

