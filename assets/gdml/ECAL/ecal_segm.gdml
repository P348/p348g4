<?xml version="1.0" encoding="UTF-8" ?>

<!--
 Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 Author: Renat R. Dusaev <crank@qcrypt.org>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->

<!DOCTYPE gdml [
    <!ENTITY defines    SYSTEM "../01_defines.igdml">
    <!ENTITY materials  SYSTEM "../02_materials.igdml">
    <!ENTITY solids     SYSTEM "../03_solids.igdml">
]>

<gdml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd">

    <define>
        &defines;

        <position name="paper1Offset"		x="0" y="0" z="-(PaperDepth_mm + CnvPlDepth_mm + ScnPlDepth_mm)/2" unit="mm"/>
        <position name="cnvOffset"          x="0" y="0" z="-(ScnPlDepth_mm)/2" unit="mm"/>
        <position name="paper2Offset"		x="0" y="0" z=" (PaperDepth_mm + CnvPlDepth_mm - ScnPlDepth_mm)/2" unit="mm"/>
        <position name="scnOffset"          x="0" y="0" z=" (2*PaperDepth_mm + CnvPlDepth_mm)/2" unit="mm"/>
        <position name="preshowerOffset"    x="0" y="0" z="-ECALDepth_mm/2" unit="mm"/>
        <position name="ecalOffset"         x="0" y="0" z="PreshowerDepth_mm/2" unit="mm"/>
    </define>

    <materials>
        &materials;
    </materials>

    <solids>
        &solids;

        <box name="geoECALEntire" x="SizeX"
                                  y="SizeY"
                                  z="OverallECALDepth"/>
                                  
        <box name="geoCell" x="CellSide"
                            y="CellSide"
                            z="CellDepth"/>
                            
        <box name="geoLayerX" x="SizeX"
                              y="CellSide"
                              z="CellDepth"/>
                                                         
        <box name="geoLayer" x="SizeX"
                             y="SizeY"
                             z="CellDepth"/>

        <box name="geoScnPl" x="CellSide"
                             y="CellSide"
                             z="ScnPlDepth"/>

        <box name="geoCnvPl" x="CellSide"
                             y="CellSide"
                             z="CnvPlDepth"/>
                             
        <box name="geoPaper" x="CellSide"
							 y="CellSide"
							 z="PaperDepth"/>
		
		<box name="geoPreshower" x="SizeX"
                                 y="SizeY"
                                 z="PreshowerDepth"/>
                                 
        <box name="geoECAL" x="SizeX"
                            y="SizeY"
                            z="ECALDepth"/>

		<!--Same as geoCnvPl					 
        <box name="preshowerCnvPl"  x="SizeX"
                                    y="SizeY"
                                    z="CnvPlDepth"/>
        -->
    </solids>

    <structure>
        <volume name="scnLayer">
            <materialref ref="G4_PLASTIC_SC_VINYLTOLUENE"/>
            <solidref ref="geoScnPl"/>
            <auxiliary auxtype="style" auxvalue="Default:!hidden/StandaloneCell:#333333ff,!surface/Detailed:!hidden"/>
            <auxiliary auxtype="SensDet" auxvalue="MKTriggerSD"/>
        </volume>

        <volume name="cnvLayer">
            <materialref ref="mtLead"/>
            <solidref ref="geoCnvPl"/>
            <auxiliary auxtype="style" auxvalue="Default:!hidden/StandaloneCell:#33663377,!surface/Detailed:!hidden"/>
        </volume>

		<volume name="paperLayer">
            <materialref ref="mtPaper"/>
            <solidref ref="geoPaper"/>
            <auxiliary auxtype="style" auxvalue="Default:!hidden/StandaloneCell:#eeeeeeff,!surface/Detailed:!hidden"/>
        </volume>

        <volume name="Cell">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoCell"/>
            <physvol>
				<volumeref ref="paperLayer"/>
				<positionref ref="paper1Offset"/>
            </physvol>
            <physvol>
                <volumeref ref="cnvLayer"/>
                <positionref ref="cnvOffset"/>
            </physvol>
            <physvol>
				<volumeref ref="paperLayer"/>
				<positionref ref="paper2Offset"/>
            </physvol>
            <physvol>
                <volumeref ref="scnLayer"/>
                <positionref ref="scnOffset"/>
            </physvol>
            <auxiliary auxtype="style" auxvalue="Detailed:!wireframe/Default:!hidden"/>
        </volume>

        <volume name="layerX">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoLayerX"/>
            <replicavol number="6">
				<volumeref ref="Cell"/>
				<replicate_along_axis>
					<direction x="1"/>
					<width value="CellSide_mm" unit="mm"/>
					<offset value="0" unit="mm"/>
				</replicate_along_axis>	
            </replicavol>
            <auxiliary auxtype="style" auxvalue="Detailed:!wireframe/Default:!hidden"/>
         </volume>
                
         <volume name="layer">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoLayer"/>   
			<replicavol number="6">
				<volumeref ref="layerX"/>
				<replicate_along_axis>
					<direction y="1"/>
					<width value="CellSide_mm" unit="mm"/>
					<offset value="0" unit="mm"/>
				</replicate_along_axis>	
            </replicavol>
            <auxiliary auxtype="style" auxvalue="Detailed:!wireframe/Default:!hidden"/>
        </volume>
      
        <volume name="preshower">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoPreshower"/>
            <replicavol number="16"> <!-- LongitudalPreshowerN -->
                <volumeref ref="layer"/>
                <!-- <positionref ref="preshowerCnvOffset"/> -->
                <replicate_along_axis>
                    <direction z="1"/>
                    <width  value="ScnPlDepth_mm + CnvPlDepth_mm + 2*PaperDepth_mm" unit="mm"/>
                    <offset value="0"              unit="mm"/>
                </replicate_along_axis>
            </replicavol>
            <!-- red - Preshower -->
            <auxiliary auxtype="style" auxvalue="Detailed:!wireframe/Default:!wireframe,#ff3333ee"/>
        </volume>

		<volume name="ECAL">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoECAL"/>
            <replicavol number="150"> <!-- LongitudalN -->
                <volumeref ref="layer"/>
                <!-- <positionref ref="preshowerCnvOffset"/> -->
                <replicate_along_axis>
                    <direction z="1"/>
                    <width  value="ScnPlDepth_mm + CnvPlDepth_mm + 2*PaperDepth_mm"   unit="mm"/>
                    <offset value="0"              unit="mm"/>
                </replicate_along_axis>
            </replicavol>
            <!-- blue - Main ECAL part -->
            <auxiliary auxtype="style" auxvalue="Detailed:!wireframe/Default:!wireframe,#3333ffee"/>
        </volume>
        
        <volume name="entireECAL">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoECALEntire"/>
            <physvol>
                <volumeref ref="preshower"/>
                <positionref ref="preshowerOffset"/>
            </physvol>
            <physvol>
                <volumeref ref="ECAL"/>
                <positionref ref="ecalOffset"/>
            </physvol>
            <auxiliary auxtype="style" auxvalue="Detailed:!wireframe"/>
        </volume>
    </structure>

    <!-- This one is used when embedding ECAL as a module -->
    <setup name="Default" version="1.0" >
        <world ref="entireECAL" />
    </setup>
    <!-- Wireframe, demonstrates, that the geometry is really segmentated -->
    <setup name="Detailed" version="1.0" >
        <world ref="entireECAL" />
    </setup>
    <!-- Use this one for standalone view of ECAL cell -->
    <setup name="StandaloneCell" version="1.0" >
        <world ref="layerX" />
    </setup>
</gdml>

<!--
vi:ft=xml
-->
