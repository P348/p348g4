/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// TODO: ROOT graphics suppport via Application subclass/mixin

# if 1

# include <TF1.h>
# include <TF2.h>
# include <TH1.h>
# include <TH2F.h>
# include <TLegend.h>
# include <TLegendEntry.h>
# include <Rtypes.h>
# include <TList.h>
# include <TAxis.h>
# include <TGraph2D.h>
# include <TRandom.h>
# include <TRandom3.h>
# include <TStyle.h>
# include <TVirtualPad.h>
# include <TCanvas.h>
# include <TApplication.h>

# include <goo_utility.hpp>

# include "app_aprimeEvGen.hpp"
# include "evGen/aprimeEvGen.hpp"
# include "evGen/aprimeCSCache.hpp"

// Undef it to avoid using caches container:
//# define APRIME_EVGEN_NO_CS_CACHES_TEST

# ifndef GEANT4_MC_MODEL
# ifndef APRIME_EVGEN_NO_CS_CACHES_TEST
#   warning "As option GEANT4_MC_MODEL is OFF, can not build caching container testing plot."
#   define APRIME_EVGEN_NO_CS_CACHES_TEST
# endif
# endif

# ifndef APRIME_EVGEN_NO_CS_CACHES_TEST
# include <Geant4/G4Electron.hh>
# endif

namespace aprime {

Double_t
_static_plot_chi( Double_t * x, Double_t * p ) {
    const double EBeam = p[0],
                 APMass = x[0];
    void * tungstenWS = NULL;
    init_aprime_cross_section_workspace(
            /* Z ............... */ (int) p[2], //p[2] 74,
            /* A ............... */ p[1],  //183.84,
            /* mass A', GeV .... */ APMass,
            /* E beam, GeV ..... */ EBeam,
            /* mixing factor ... */ 1e-4,
            # define default_parameter( type, txtName, dft, name, descr ) dft,
                for_all_p348lib_aprimeCS_GSL_chi_comp_parameter( default_parameter )
            # undef default_parameter
        &tungstenWS );
    double absErr, relErr;
    Double_t chiRes = chi( &absErr, &relErr, tungstenWS );
    free_aprime_cross_section_workspace( tungstenWS );
    return chiRes/(p[2]*p[2]);
}

class TLegendEntryViolator : public TLegend {
public:
    TList * get_legend_entries_TList() {
        return this->fPrimitives;
    }
};

static void  // note: reentrant block for plot_aprime_prod_cs()
_static__plot_aprime_prod_cs_on( p348::APrimeWWPDF & pdf,
                                 TGraph2D * csPlot,
                                 FILE * outf ){
    const uint8_t nPoints = 50;
    uint32_t offset = 0;
    double vec[3] = {0, 0, 0};
    double scale = 1./(nPoints-1);

    for(uint8_t nx = 0; nx < nPoints; nx++) {
        for(uint8_t nTheta = 0; nTheta < nPoints; nTheta++) {
            vec[0] = nx*scale;
            vec[1] = nTheta*scale;
            pdf.renorm( vec );
            vec[2] = pdf.density( vec[0], vec[1] );
            if(    isnan(vec[2])
                || isinf(vec[2]) ) {
                p348g4_logw( "Plotting %p on %p at {%e, %e}: NAN/INF.\n",
                             (void*) &pdf,
                             (void*) csPlot, vec[0], vec[1] );
                continue;
            }
            //std::cout << "#" << offset << ": " << x
            //          << ", " << y << ", " << z << ";" << std::endl;
            csPlot->SetPoint( offset, vec[0], vec[1], vec[2] );
            offset++;
            fprintf( outf, "%e %e %e\n", vec[0], vec[1], vec[2] );
        }
        fprintf( outf, "\n" );
    }
    if( !offset ) {
        p348g4_logw( "Couldn't plot CS at %p: all points is NAN/INF.\n", (void*) csPlot );
    }
}

static void
_static__plot_aprime_prod_cs( TVirtualPad * p, const p348::po::variables_map & ) /* {{{ */ {
    p348::APrimeWWPDF pdf(
            # define obtain_physparameter_arg( type, txtName, name, descr ) \
                    goo::app<aprime::Application>().cfg_option<type>("aprimeEvGen." txtName),
                for_all_p348lib_aprimeCS_parameters( obtain_physparameter_arg )
            # undef obtain_physparameter_arg
            # define obtain_numsparameter_arg( type, txtName, dft, name, descr ) \
                    goo::app<aprime::Application>().cfg_option<type>("extraPhysics.physicsAe.gslIntegration." txtName),
                for_all_p348lib_aprimeCS_GSL_chi_comp_parameter( obtain_numsparameter_arg )
            # undef obtain_numsparameter_arg
            nullptr
        );
    gStyle->SetPalette(1);
    TGraph2D * csPlot[3] = {
            new TGraph2D(),
            new TGraph2D(),
            new TGraph2D()
        };
    FILE * outf = NULL;
    TLegend* l = new TLegend(0.59, 0.7, 0.95, 0.9);
    p348::aux::XForm<2, Double_t> ranges;
    //l->SetTextFont(132);
    l->SetTextSize(0.035);
    p->SetLogz();
    pdf.EBeam_GeV(goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.EBeam_GeV")*1.5); {
        char outFileName[64];
        snprintf( outFileName, 64, "/tmp/aprimeCSDat-%.2e.dat", pdf.EBeam_GeV() );
        outf = fopen( outFileName, "w" );
        _static__plot_aprime_prod_cs_on(pdf, csPlot[0], outf);
        ranges.extend_with( pdf );
        csPlot[0]->SetMarkerColor(kRed);
        csPlot[0]->SetMarkerStyle(2);
        //csPlot[0]->SetMarkerSize(1.5);
        csPlot[0]->Draw("P");
        csPlot[0]->SetName( "dstr_g1" );
        l->AddEntry("dstr_g1", goo::strfmt("E_{0} = %.1e, GeV", pdf.EBeam_GeV()).c_str(), "p");
        fclose(outf);
    }

    pdf.EBeam_GeV(goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.EBeam_GeV")*1.);{
        char outFileName[64];
        snprintf( outFileName, 64, "/tmp/aprimeCSDat-%.2e.dat", pdf.EBeam_GeV() );
        outf = fopen( outFileName, "w" );
        _static__plot_aprime_prod_cs_on(pdf, csPlot[1], outf);
        ranges.extend_with( pdf );
        csPlot[1]->SetMarkerColor(kGreen);
        csPlot[1]->SetMarkerStyle(2);
        //csPlot[1]->SetMarkerSize(1.5);
        csPlot[1]->Draw("P same");
        csPlot[1]->SetName( "dstr_g2" );
        l->AddEntry("dstr_g2", goo::strfmt("E_{0} = %.1e, GeV", pdf.EBeam_GeV()).c_str(), "p");
        fclose( outf );
    }

    double eBeam = (goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.EBeam_GeV")*.1); {
        pdf.EBeam_GeV( eBeam/2. > pdf.massA_GeV() ? eBeam : pdf.massA_GeV()*2.1 );
        char outFileName[64];
        snprintf( outFileName, 64, "/tmp/aprimeCSDat-%.2e.dat", pdf.EBeam_GeV() );
        outf = fopen( outFileName, "w" );
        _static__plot_aprime_prod_cs_on(pdf, csPlot[2], outf);
        ranges.extend_with( pdf );
        csPlot[2]->SetMarkerColor(kBlue);
        csPlot[2]->SetMarkerStyle(2);
        //csPlot[2]->SetMarkerSize(1.5);
        csPlot[2]->Draw("P same");
        csPlot[2]->SetName( "dstr_g3" );
        l->AddEntry("dstr_g3", goo::strfmt("E_{0} = %.1e, GeV", pdf.EBeam_GeV()).c_str(), "p");
        fclose(outf);
    }
    # if 1  // causes segfault due to ROOT's internal obscurity
    {
        csPlot[0]->GetXaxis()->SetTitle("E_{A}/E_0");
        csPlot[0]->GetYaxis()->SetTitle("\\theta, rad");
        csPlot[0]->GetZaxis()->SetTitle("\\frac{d \\sigma}{d x d cos \\theta}, pb");
        csPlot[0]->SetTitle("A-prime PDF");
    }
    # endif

    //{  // TODO: Why, on Earth, does the ROOT ignores m specifications!!?11..
    //    printf( "%e - %e / %e - %e\n", 
    //            ranges.get_limit(DECLTYPE(ranges)::min, 0),
    //            ranges.get_limit(DECLTYPE(ranges)::max, 0),
    //            ranges.get_limit(DECLTYPE(ranges)::min, 1),
    //            ranges.get_limit(DECLTYPE(ranges)::max, 1)
    //        );
    //    std::cout << ranges << std::endl;  // XXX
    //}

    csPlot[0]->GetXaxis()->SetRangeUser(
            ranges.get_limit(DECLTYPE(ranges)::min, 0),
            ranges.get_limit(DECLTYPE(ranges)::max, 0)
        );
    csPlot[0]->GetYaxis()->SetRangeUser(
            ranges.get_limit(DECLTYPE(ranges)::min, 1),
            ranges.get_limit(DECLTYPE(ranges)::max, 1)
        );
    //csPlot[0]->GetZaxis()->SetRangeUser(1e-15, 1e-11);

    l->SetHeader("Incident e^{-} energy (E_{0})");
    l->Draw();
    p->Modified();
} /* }}} */

static void
_static__plot_chi_checks_on( TVirtualPad * p, const p348::po::variables_map & ) /* {{{ */ {
    TF1 * chiPlot = new TF1( "\\chi", _static_plot_chi, 1e-2, 2, 3 );
    TF1 * chiPlotClones[5];
    p->SetGridx(); p->SetGridy();
    p->SetLogx(); p->SetLogy();
    //chiPlot->DrawClone._creates = kFalse;
    
    chiPlot->SetTitle( "\\chi/Z^2 check" );

    chiPlot->SetParNames("beam E", "A", "Z");
    chiPlot->SetParameter(0, goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.EBeam_GeV"));
    chiPlot->SetParameter(1, goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.materialA"));
    chiPlot->SetParameter(2, goo::app<aprime::Application>().cfg_option<uint16_t>("aprimeEvGen.materialZ"));
    chiPlot->SetLineWidth(4);
    chiPlot->SetLineColor( kViolet );

    chiPlotClones[0] = (TF1 *) chiPlot->DrawClone("l");
    chiPlot->SetLineWidth(2);

    chiPlot->SetParameter(1, 183.84);
    chiPlot->SetParameter(2, 74); {
        chiPlot->SetParameter(0, .2);
        chiPlot->SetLineColor( kRed );
        chiPlotClones[1] = (TF1 *) chiPlot->DrawClone("l same");

        chiPlot->SetParameter(0, 1.);
        chiPlot->SetLineColor( kGreen );
        chiPlotClones[2] = (TF1 *) chiPlot->DrawClone("l same");

        chiPlot->SetParameter(0, 6.);
        chiPlot->SetLineColor( kBlue );
        chiPlotClones[3] = (TF1 *) chiPlot->DrawClone("l same");
    }

    chiPlot->SetParameter(1, 26.98);
    chiPlot->SetParameter(2, 13); {
        chiPlot->SetLineColor( kBlack );
        chiPlot->SetParameter(0, 6.);
        chiPlotClones[4] = (TF1 *) chiPlot->DrawClone("l same");
    }

    // crutch
    {
        char labels[][32] = {
                "<current>",
                "W_{74}^{183}, 200 MeV",
                "W_{74}^{183}, 1 GeV",
                "W_{74}^{183}, 6 GeV",
                "Al_{13}^{26}, 6 GeV",
            };
        snprintf( labels[0], 32, "%s, %d GeV",
                    goo::app<aprime::Application>().cfg_option<std::string>("aprimeEvGen.materialLabel").c_str(),
                    (int) goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.EBeam_GeV") );
        TLegendEntryViolator * lgnd = 
            reinterpret_cast<TLegendEntryViolator *>( p->BuildLegend( .2, .2, .5, .4, "Legend" ) );
        auto lst = lgnd->get_legend_entries_TList();
        TObject * obj; uint8_t lblIdx = 0;
        // Note that ROOT version > 5.34 needed
        for(TIter next(lst->begin()); (obj = next()); lblIdx++) {
            reinterpret_cast<TLegendEntry *>(obj)->SetLabel(labels[lblIdx]);
            // Setting axis labels before the computation causes segfaults...
            chiPlotClones[lblIdx]->GetXaxis()->SetTitle( "m_{A'}, GeV" );
            chiPlotClones[lblIdx]->GetYaxis()->SetTitle( "\\chi/Z^2" );
        }
    }

    p->Modified();
}  /* }}} */

static void
_static__plot_PDF( TVirtualPad * p, const p348::po::variables_map & ) /* {{{ */ {
    p348::APrimeGenerator g(
        "testGenerator",
        # define obtain_physparameter_arg( type, txtName, name, descr ) \
            goo::app<aprime::Application>().cfg_option<type>("aprimeEvGen." txtName),
            for_all_p348lib_aprimeCS_parameters( obtain_physparameter_arg )
        # undef obtain_physparameter_arg
        # define obtain_numsparameter_arg( type, txtName, dft, name, descr ) \
            goo::app<aprime::Application>().cfg_option<type>("extraPhysics.physicsAe.gslIntegration." txtName),
            for_all_p348lib_aprimeCS_GSL_chi_comp_parameter( obtain_numsparameter_arg )
        # undef obtain_numsparameter_arg
        # define obtain_tfoam_parameter( type, txtName, dft, name, descr ) \
            goo::app<aprime::Application>().cfg_option<type>("physicsAe.TFoam." txtName),
            for_all_p348_lib_TFoam_generator_parameters( obtain_tfoam_parameter )
        # undef obtain_tfoam_parameter
        nullptr
    );
    TRandom * pseRnd = new TRandom3();
    g.initialize( pseRnd );

    //char bf[1024] = "one.dat";
    //g.RootPlot2dim(bf);

    TH2F * gHist = new TH2F(
                "GeneratedAPrime",
                "Generated events",
                20,
                g.integrand().get_limit(p348::APrimeWWPDF::XForm::min, 0),
                g.integrand().get_limit(p348::APrimeWWPDF::XForm::max, 0),
                20,
                g.integrand().get_limit(p348::APrimeWWPDF::XForm::min, 1),
                g.integrand().get_limit(p348::APrimeWWPDF::XForm::max, 1) );

    p348::APrimeGenerator::Event e;
    for( size_t n = 0; n < 1e3; ++n ) {
        Double_t w = g.generate( e );
        gHist->Fill( e.byName.x, e.byName.theta, w );
        //std::cout << " w = "
        //          << g.generate( e )
        //          << " x = " << e.byName.x << ", "
        //          << " theta = " << e.byName.theta << ";" << std::endl;
    }
    Double_t mcResult, mcError;
    g.GetIntegMC( mcResult, mcError);  // get MC integral, should be one
    // mcResult= 0.060176 +- 9.21096e-05
    std::cout << " mcResult= " << mcResult << " +- " << mcError <<std::endl;
    p->SetLogz();
    gHist->Draw( "LEGO" );
    {
        gHist->GetXaxis()->SetTitle("\\E_{A}/E_0");
        gHist->GetYaxis()->SetTitle("\\theta, rad");
        gHist->GetZaxis()->SetTitle("N events");
        gHist->SetTitle("A-prime events generated example");
    }
    p->Modified();

    std::cout << "Integrand X-Form: "
              << static_cast<p348::aux::XForm<2, Double_t> >(g.integrand()) << std::endl;
    std::cout << "x, theta, density checking X-Form: "
              << g.integrand()._check1 << std::endl;
    std::cout << "x_{normed}, theta_{normed}, density checking X-Form: "
              << g.integrand()._check2 << std::endl;
    std::cout << "x_{normed, generated}, theta_{normed, generated}, density_{generated} checking X-Form: "
              << g.integrand()._check3 << std::endl;
} /* }}} */

# ifdef APRIME_EVGEN_NO_CS_CACHES_TEST
static void
_static__plot_testing_density_vs_PDF( TVirtualPad * p,
                                      const double EBeam_GeV ) {
    const size_t nBinsX = 120,
                 nBinsTheta = 120
                 ;
    p348::APrimeWWPDF pdf(
            # define obtain_physparameter_arg( type, txtName, name, descr ) \
                    goo::app<aprime::Application>().cfg_option<type>("aprimeEvGen." txtName),
                for_all_p348lib_aprimeCS_parameters( obtain_physparameter_arg )
            # undef obtain_physparameter_arg
            # define obtain_numsparameter_arg( type, txtName, dft, name, descr ) \
                    goo::app<aprime::Application>().cfg_option<type>("extraPhysics.physicsAe.gslIntegration." txtName),
                for_all_p348lib_aprimeCS_GSL_chi_comp_parameter( obtain_numsparameter_arg )
            # undef obtain_numsparameter_arg
            nullptr
        );
    pdf.EBeam_GeV( EBeam_GeV );
    // ... (plot PDF?)
    char bf[128];
    snprintf( bf, sizeof(bf), "APrimeGenerator-test-%eGeV", EBeam_GeV );
    p348::APrimeGenerator G( bf, &pdf, 
                # define obtain_tfoam_parameter( type, txtName, dft, name, descr ) \
                    goo::app<aprime::Application>().cfg_option<type>("physicsAe.TFoam." txtName),
                    for_all_p348_lib_TFoam_generator_parameters( obtain_tfoam_parameter )
                # undef obtain_tfoam_parameter
                NULL );
    TRandom * pseRnd = new TRandom3();
    G.initialize( pseRnd );
    p348::APrimeGenerator::Event e;
    for( size_t n = 0; n < 1e4; ++n ) {
        G.generate( e );
    }
    G.draw_agreement_plot( nBinsX, nBinsTheta );
    p->SetLogz();
}
# else
static void
_static__plot_caching_test_events( TVirtualPad * p[3], const double EBeam_GeV ) {
    TRandom * pseRnd = new TRandom3();
    double energies[] = {EBeam_GeV*1.4, EBeam_GeV, EBeam_GeV*.6};
    // Set tabulation for three projectile energies:
    p348::APrimeCSCaches::self()
        .projectile_energy_tabulation(
                std::set<double>(energies, energies + sizeof(energies)/sizeof(energies[0])) );
    // Set pseudo-random generator
    p348::APrimeCSCaches::self().random_generator( pseRnd );

    const double incidentCharge = G4Electron::Definition()->GetPDGCharge(),
                 incidentMass = G4Electron::Definition()->GetPDGMass()
                 ;
    uint8_t nucleusZ = goo::app<aprime::Application>().cfg_option<uint16_t>("aprimeEvGen.materialZ");
    uint16_t nucleusA = goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.materialA");

    p348::APrimeGenerator::Event event;
    // TODO: progressbar
    for( size_t nEvent = 0; nEvent < 1e6; ++nEvent ) {
        // 1. generate projectile particle energy uniformely
        double E_proj = pseRnd->Uniform(
                *p348::APrimeCSCaches::self().projectile_energy_tabulation().cbegin(),
                *p348::APrimeCSCaches::self().projectile_energy_tabulation().crbegin()
            );
        // 2. acquire suitable generator and generate event
        p348::APrimeGenerator * g = p348::APrimeCSCaches::self()
            .generator( incidentCharge, incidentMass, E_proj, nucleusA, nucleusZ );
        g->generate( event );
    }
    p348g4_log1("Generation using caches container done.\n");
    assert( 3 == p348::APrimeCSCaches::self().n_generators() );
    char bf[128];
    for( uint8_t i = 0; i < 3; ++i ) {
        p[i]->cd();
        p348::APrimeGenerator * g = p348::APrimeCSCaches::self()
            .generator( incidentCharge, incidentMass, energies[i], nucleusA, nucleusZ );
        TH2F * hst = g->draw_agreement_plot( 60, 60 );

        p348g4_log1( "Integrals for generator #%d:\n"
                     "    TFoam value ........ : %e\n"
                     "    ROOT integration ... : %e\n"
                     "    analytical ......... : %e\n"
                     //"    testing int ........ : %e\n"
                     ,
                     (int) i, g->integral_sum(),
                     g->integrand().probability(),
                     g->integrand().integral_sum_a15()
                     //,g->integrand().integral_XXX()
                     );

        // appearance
        {
            snprintf( bf, sizeof(bf),
                      "\\frac{|\\rho_{mc} - \\rho_{theor}|}{|\\rho_{mc} + \\rho_{theor}|}, "
                      "E_{proj} = %.2e GeV", energies[i] );
            hst->SetTitle( bf );
            p[i]->SetLogz();
            hst->GetXaxis()->SetTitle("E_{A}/E_0");
            hst->GetYaxis()->SetTitle("\\theta, rad");
            hst->SetStats(0);
        }
    }
}
# endif

static int
_static__generator_summary( const p348::po::variables_map & ) {
    // As we do not call APrimeProduction constructor, where A' mass and mixing
    // parameter are supposed to be set, we set it herein:
    p348::APrimeCSCaches::APrimeMass =
            goo::app<p348::AbstractApplication>().cfg_option<double>("aprimeEvGen.massA_GeV");
    p348::APrimeCSCaches::MixingConstant =
            goo::app<p348::AbstractApplication>().cfg_option<double>("aprimeEvGen.mixingFactor");
    TCanvas * cnv = new TCanvas();
    cnv->Show();
    //gSystem->ProcessEvents();
    # if 1
    //TCanvas & cnv = goo::app<aprime::Application>().canvas()
    //if((cnv = goo::app<aprime::Application>().root_canvas_ptr())){  // graphics enabled
        cnv->Divide(3,2);
        // First row: chi, PDF multiplot, generated example
        // TODO: uncomment 3 lines below
        _static__plot_chi_checks_on(          cnv->cd(1), goo::app<aprime::Application>().co() );
        _static__plot_aprime_prod_cs(         cnv->cd(2), goo::app<aprime::Application>().co() );
        _static__plot_PDF(                    cnv->cd(3), goo::app<aprime::Application>().co() );
        // Second row: \Delta = (N_{i,j} / N_0) - (\frac{d sigma_{...}}{\sigma})
        {
            const double EBeam_GeV = goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.EBeam_GeV");
            # ifdef APRIME_EVGEN_NO_CS_CACHES_TEST
            _static__plot_testing_density_vs_PDF( cnv->cd(4), EBeam_GeV*0.8 );
            _static__plot_testing_density_vs_PDF( cnv->cd(5), EBeam_GeV     );
            _static__plot_testing_density_vs_PDF( cnv->cd(6), EBeam_GeV*1.2 );
            # else
            TVirtualPad * cacheTestPads[] = { cnv->cd(4), cnv->cd(5), cnv->cd(6) };
            _static__plot_caching_test_events( cacheTestPads, EBeam_GeV );
            # endif
        }
        cnv->Show();
    //}
    # endif
    goo::app<aprime::Application>().get_TApplication().Run();
    return EXIT_SUCCESS;
}

REGISTER_APRIME_EVGEN_TASK(
    "summary",
    "Produces brief summary about generator state using ROOT GUI. "
    "Draws a canvas with 3×2 plots. First plot is dedicated for \\chi value "
    "(form factor) and can be directly compared with FIG. 10 of referencing article. "
    "The second plot depicts cross-sections calculated according to (A12) formula. "
    "Third plot demonstrates a few events generated according to cross-section, while "
    "three plots below depicts a relative error: generated distribution denisity against "
    "predicted density. In the console dump, please note three last log messages (1st lvl) about "
    "integrals calculation. Discrepancy between numerically-computed and analytically-predicted "
    "(according to (a15) formula) may indicate dangerous computation error."
    ,
    _static__generator_summary,
    nullptr )

}  // namespace aprime
# endif
