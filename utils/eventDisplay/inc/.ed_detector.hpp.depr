# ifndef H_P348G4_EVENT_DISPLAY_DETECTOR_CLASS_H
# define H_P348G4_EVENT_DISPLAY_DETECTOR_CLASS_H

/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_detector_ids.h"

# include <TVector3.h>

class TEveElement;

namespace p348 {
namespace evd {

typedef TVector3 SpatialVector;  // todo: replace with something suitable
typedef TVector3 RotationVector;

/**@brief Base detector class for event display application.
 * 
 * Incapsulates all detector handlers for geometry drawing, hit drawing,
 * representation changes, etc.
 * */
class AbstractDetector {
private:
    const std::string _detName,
                      _ctrName;
protected:
    /// Interface constructor.
    /// \param ctrName a constructor name (used to obtain placement info). Is p
    //         forwarded usually from descendant class.
    /// \param detName is this detector name provided implicitly by descendant
    //         class (user code is not supposed to change that).
    AbstractDetector( const std::string & ctrName,
                      const std::string & detName );
    /// Draws geometry of detecor (by default, based on _description member).
    virtual void _V_draw_geometry( TEveElement * sc ) = 0;
    /// Interface method --- draws a hit in detector.
    virtual void _V_draw_hit() = 0;
public:
    /// Delegates drawing to _V_draw_geometry().
    void draw_geometry( TEveElement * sc ) { _V_draw_geometry( sc ); }
    /// Delegates drawing to _V_draw_hit().
    void draw_hit() { _V_draw_hit(); }
    /// Returns detector name.
    const std::string & detector_name() { return _detName; }
    /// Returns constructor name.
    const std::string & constructor_name() { return _ctrName; }

    // Based on detector family information, produces one instance.
    //static AbstractDetector * new_detector(
    //                const DetectorDescription * dPtr );
};  // class AbstractDetector

// ...

}  // namespace evd
}  // namespace p348


# endif  // H_P348G4_EVENT_DISPLAY_DETECTOR_CLASS_H

