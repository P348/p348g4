#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link off all namespaces;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace p348;
#pragma link C++ namespace p348::evd;

#pragma link C++ defined_in "./trackingGroups/points.hpp";
#pragma link C++ class p348::evd::PointSetTrackingGroup+;
#pragma link C++ class p348::evd::PointSetTrackingGroupEditor+;

#endif

