/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348G4_FINITE_STATE_MACHINE_H
# define H_P348G4_FINITE_STATE_MACHINE_H

# include <unordered_map>

namespace p348 {
namespace aux {

template<typename StateT,
         typename ArgumentsT>
class FSM {
public:
    typedef StateT State;
    typedef ArgumentsT Arguments;
    typedef bool (*Transition)( const Arguments & );
protected:
    std::unordered_map<std::pair<State, State>, Transition> _transitions;
public:
    /// Returns true, if it is possible to switch state from current to given.
    bool can( State to ) const;
    void transition( State from, State to, const Arguments & );
    State state() const;

    /// Adds possible transition.
    void register_transition( State dest, Transition );
};  // class FSM

// implem
////////

template<typename StateT,
         typename ArgumentsT> bool
FSM<StateT, ArgumentsT>::can( State to ) const {
    return _transitions.end() == _transitions.find( std::pair<State, State>(state(), to) );
}


}  // namespace aux
}  // namespace p348

# endif  // H_P348G4_FINITE_STATE_MACHINE_H

