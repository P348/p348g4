#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;

//#pragma link C++ namespace p348;
//#pragma link C++ namespace p348::evd;
#pragma link C++ class p348::evd::DrawableTrackingGroup+;
#pragma link C++ class p348::evd::DrawableTrackingGroupGL+;
#pragma link C++ class p348::evd::DrawableTrackingGroupEditor+;
#pragma link C++ class p348::evd::aux::PointCoordinatesCache<p348::aux::HitGraphicsTraits::IsotropicPointMarkerParameters>+;
//#pragma link C++ defined_in ed_ControlGUI.h;

#endif

