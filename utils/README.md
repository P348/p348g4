# Utils

The term «util» here should be considered as an application or tool
that utilizes library's functionality in some way. We provide here
a very basic routines.

## A' generator testing application --- `aprimeEvGen`

This application preforms testing operations for A' cross section
calculation routines. Occupies the `WWACS.` part of configuration
files (for Weizs ̈acker-Williams A' cross-section).

## GDML viewer --- `mdlv`

This application is devoted to detector's geometry description. Provides
merely simple facility to parse and visualize particular detector setup.

## Event reading and statistics processing util --- `p348a`

Implements a «conveyor» approach of statistics analysis performing sequential
reading and processing of real experimental and modelled data.

## General-purpose Geant4 modelling util --- `p348g4mdl`

Implements generic Geant4-modelling utility. Higly configurable at run-time.
Currently supports only GDML geometry.

## Extensions

Any contribution to library or utilities set are kindly welcome. Only
desirable feature is to consider extension mechanism provided
by p348g4 lib, the basic idea of which is to make anything optional
in order to increase code granularity and thus to simplify code
re-using.

As we were highly concerned about it from beginning, there are some
extension points one need to consider:
    
   o Application parametrisation
   o Dictionaries
   o Compile-time configuration

### Application parametrisation

The run-time configuration is based on the [`boost::variables_map`](http://www.boost.org/doc/libs/1_54_0/doc/html/boost/program_options/variables_map.html).
The particular values in parameter set instance which are available at run
time are the result of sequential reading from the following sources
(in order of low-to-high priority):
    
   1. Default values specified in C++
   2. Values read from `defaults.cfg`
   3. Values provided via command-line.

E.g. the parameter `verbosity` can be provided at all this three sources. In
C++ source at `app/app.cpp` it is set in the following line (according to
`boost::variables_map` format in general section section):
    
    ...
    ("verbosity,v",
        po::value<int>()->default_value(1),
        "Sets verbosity level (from 0 -- silent, to 3 -- loquacious).")
    ...

Which means this parameter has type `int`, shortcut `-v` and should be set by default to `1`,
(if both config file and command line won't mention it). However,
next, the `defaults.cfg` file sets it to `3` with the following line according
to `ini`-config file notation:

    ...
    verbosity = 3
    ...

Which means, that this parameter will be set to `3` if command-line arguments
won't mention another. Now, one can provide this argument via command-line as:

    $ utilName --verbose=2

or, using shortened variant:

    $ utilName -v 2

to set it to, for example, `2`.

Any particular application in `utils` is usually inherited from
[`p348::AbstractApplication`](http://crank.qcrypt.org/p348g4-doc/html/d3/d02/classp348_1_1_abstract_application.html)
class or any of its descendants, meaning that its parameters set
is composed within several abstraction levels --- from most basic
`p348::AbstractApplication` to this particular application class.
The extension point is provided by overriding virtual
`std::vector<po::options_description> Application::_V_get_options() const`
method. One can find examples here, in `utils` directory.

It is higly recommended to provide configuration parameters to any configurable
data of your routines and applications. Do not be afraid to extend
the `defaults.cfg` file or to provide custom ones.

### Virtual constructor dictionaries

The «Dictionary» concept is a common architectural subpattern considering
the enumeration (i.a. by ASCII-string) subset of some
[virtual constructor](https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Virtual_Constructor)
instances

Its major purpose is to provide dynamic access to constructors of particular
interface implementations (one dictionary per interface). The dictionary itself
is nothing more than [singleton](https://en.wikipedia.org/wiki/Singleton_pattern)
containing a mapping array indexing constructor routines.

Possible implementation of «Dictionary» per se can be given by following template
class:

    template<typename IndexingType>
    class iDictionaryTemplate : public AbstractDictionary {
    private:
        std::unordered_map<std::string, IndexingType> _entries;
        // ... singleton stuff here
    protected:
        iDictionaryTemplate();
        // ... singleton method
        const IndexingType & operator[](const std::string & ) const;
    };  // iDictionaryTemplate

But implementing «dictionary» concept like so would overcomplicate
its possible aggregation within the Application singleton structure,
so we spell dictionary per se --- as an abstract concept. Examples:

   o The `AnalysisApplication` class contains references to
     constructors initializing abstract event reading / processing
     objects (see `inc/app/app.hpp`, that's an aggregation).
   o `SensitiveDetector` constructors database implementeted in
     class `SDDictionary` at `inc/g4extras/SensDetDict.hpp`,
     as composition.
   o GDML auxilliary info processing routines are indexed in dictionary
     which can be observed in `inc/g4extras/auxInfoProcessor.hpp`.
     Composition.

Each «Dictionary» indexing constructors usually supplied with
special macros adding an instance in dictionary upon library
initialization (i.e. at runtime). Examples:

   o For class `SensitiveDetector` there is fairly simple macro
     called `P348_G4_REGISTER_SD` which takes singular argument ---
     the class name of the particular `G4VSensitiveDetector`
     descendant which.
   o For class `AnalysisApplication` it is provided:
      - `p348_DEFINE_DATA_SOURCE_FMT_CONSTRUCTOR` defining entry
        initialization routines for particular data format,
      - `p348_REGISTER_DATA_SOURCE_FMT_CONSTRUCTOR`, adding
        data format abstraction instance constructor into app's
        dictionary.
   o The `GDML_AUXINFO_PROCESSOR_BGN` and `GDML_AUXINFO_PROCESSOR_END`
     are the macros enclosing the constructor body like

    GDML_AUXINFO_PROCESSOR_BGN( ... ) {
        // ...
    } GDML_AUXINFO_PROCESSOR_BGN( ... )

Each dictionary in p348g4 library already has at least one entry which is
a good point for one to refer to.

### Compile-time configuration

Almost everything inside this package can be disabled by options macro
providing user an ability to suit different needs without obtrusive
dependencies like Geant4 or Root frameworks (which are used in most
cases, though).

For details, see [«Optional dependencies and features» section of main
readme file](http://crank.qcrypt.org/p348g4-doc/html/#dependencies).

