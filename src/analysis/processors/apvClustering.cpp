/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# if defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

# include "app/analysis.hpp"
# include "p348g4_uevent.hpp"
# include "analysis/apvConstraints.h"
# include "app/analysis_cat_mx.tcc"
# include "app/cvalidators.hpp"
# include "p348g4_detector_ids.hpp"

# include <TH2F.h>

// Crutch to avoid name collision with gnu_dev_major
# ifdef major
# undef major
# endif

class TH2F;

namespace p348 {
namespace dprocessors {

struct SimpleAPVStatistics {
    TH2I * ccandMapPtr,
         * bestClusterMapPtr;
    TH2F * amplitudeVsRatioDistributionPtr,
         * amplitudeVsRatioDistributionMaxPtr;

    TH2F * clusterAmplitudeVsWidth;
    TH2F * clusterRatios[2];
    TH2F * hitRatios[2];

    size_t stripsConsidered,
           clustersConsidered,
           clustersFound
           ;
};

/**@class SimpleAPVClustering
 * @brief Clustering processor for APV chip.
 *
 * Search clusters in APV data obtained within strip layout. Such readout usually
 * occurs in such detectors as triple GEMs, Micromegas, etc.
 *
 * SimpleAPVClustering class implements most basic cluster search algorithm, quick
 * and dirty.
 *
 * @ingroup analysis
 * */
class SimpleAPVClustering : public AnalysisApplication::iAPVProcessor,
                            public ::p348::mixins::DetectorCatalogue<SimpleAPVStatistics *> {
public:
    typedef AnalysisApplication::iExperimantalEventProcessor Parent;
    typedef ::p348::mixins::DetectorCatalogue<SimpleAPVStatistics *> Catalogue;
    typedef AnalysisApplication::Event Event;
    typedef unsigned short StripNo;
    struct SignalOnStrip {
        float waveform[APV_NSamples];
    };
    /**@brief primitive cluster information holder.
     *
     * This struct designed only for most basic cluster information
     * storaging: x,y from [0:1] and «amplitude» values.
     */
    struct BasicClusterInfo {
        float x, y;
        float amplitude;
        float ratioAmpXY;
        float ratio02[2],
              ratio12[2];
        float widthX, widthY;
    };
    typedef std::map<StripNo, SignalOnStrip> StripsLayout;

    struct ProjectionHit {
        /// Mean weighted position in strips units
        float mwPosition;
        /// Sum of all the strip amplitudes affected by this hit.
        float amplitudeSum;
        /// Mean ratios: a0/a2, a1/a2.
        float ratios[2];
        /// Cluster width;
        StripNo width;
    };

    /// Represents particular APV detector read-out layout.
    /// Inside one event we can have a few such layouts: for
    /// GEM1, GEM2, for MuMega3, etc. This struct caches strips
    /// mapping, keeps reentrant storage of obtained clusters,
    /// maintain objects for statistical monitoring.
    struct DetectorLayout {
        /// Reentrant storage of clusters (freed by destructor).
        std::list<BasicClusterInfo> reentrantClusterStorage;
        /// Reentrant associative array depicting array (todo: may be use custom allocator here?).
        StripsLayout rLayoutX,
                     rLayoutY;
        /// Pointers on clusters only valid until _V_finalize_event_processing() call.
        std::list<BasicClusterInfo *> clusters;
        /// Ptr on per-detector statistics instance.
        SimpleAPVStatistics * statsPtr;
        /// Re-initialized every new event. Reentrant member that
        /// stores a pointer to an APV_sampleSet object inside of
        /// gprotobuf-event. Re-set at the beginning of treatment
        /// of each samples set.
        p348::events::APV_sampleSet * sampleSetPtr;

        DetectorLayout() : statsPtr(nullptr) {}
    };
private:
    const size_t _maxClusterWidth,
                 _minClusterWidth
                 ;
    const double _ratioTrust[2];
    bool _doCollectStats;
    const double _minimalAmplitudeOnStrip,
                 _minRatio02,
                 _minRatio12
                 ;
    
    std::unordered_map<DetectorSignature, DetectorLayout> _layouts;
    /// Returns reference to particular layout (mutable ref version).
    DetectorLayout & this_layout( DetectorSignature, StripsLayout ** = nullptr );
    /// Returns reference to particular layout (const ref version).
    const DetectorLayout & this_layout( DetectorSignature ) const;
protected:
    /// Should return 'false' if processing in chain should be aborted.
    bool _V_process_APV_samples( p348::events::APV_sampleSet * ) override;
    /// Called after single event processed by all the processors.
    virtual void _V_finalize_event_processing( Event * ) override;
    /// Called after all events read and source closed to cleanup statistics.
    virtual void _V_finalize() const override;
    /// Called after all events read and all processors finalized.
    virtual void _V_print_brief_summary( std::ostream & ) const override;
    /// Called after all APV raw data is processed.
    virtual bool _V_finalize_processing_apv_event( p348::events::ExperimentalEvent * ) override;

    SimpleAPVStatistics * _V_new_entry( DetectorSignature id, TDirectory * ) override;
    void _V_free_entry( DetectorSignature, SimpleAPVStatistics * ) override;

    /// Performs clusterization algorithm. List will be appended without
    /// clearing.
    virtual void _clusterize( const StripsLayout & layoutX,
                              const StripsLayout & layoutY,
                              std::list<BasicClusterInfo> & reentrantClusterStorage,
                              std::list<BasicClusterInfo *> & clusters,
                              SimpleAPVStatistics * statsPtr=nullptr );
    /// Fills uevent's APV_Cluster structure into GPB u-event.
    virtual void _fill_cluster( BasicClusterInfo &,
                               events::APV_Cluster &,
                               const aux::DetectorMapping::APVBoundaries & xf ) const;
public:
    SimpleAPVClustering( bool doAccumulateStatistics,
                         size_t maxClusterWidth,
                         size_t minClusterWidth,
                         double maxRatioTrust,
                         double minRatioTrust,
                         double minimalAmplitudeOnStrip=0.,
                         double minRatio02=1e-6,
                         double minRatio12=1e-6 );
    ~SimpleAPVClustering();
};  // class APVClustering

// Implementation
////////////////

/** This method returns reference for particular layout instance
 * correspoinding to one concrete detector based on its detector
 * signature. Internally, in SimpleAPVClustering::_detectors
 * member all the layouts are indexed by unique detector
 * identifying numbers. Since we need to summarize signals from X
 * and Y axes of detector inside one logical entity. each layout
 * is indexed by its X ID-number.
 *
 * @param[in]   dSig                numerical signature of APV detector that
 *                                  will be used as an index for particular
 *                                  layout (and insertion plane).
 * @param[out]  insertionPlanePtr   where ptr to insertion plane is to be
 *                                  written. Can be set to nullptr if
 *                                  insertion plane pointer does not matter.
 * @returns reference to layout.
 * */
SimpleAPVClustering::DetectorLayout &
SimpleAPVClustering::this_layout( DetectorSignature dSig, StripsLayout ** insertionPlanePtr ) {
    const UniqueDetectorID id(dSig);
    DetectorSignature indexingSignature = dSig;
    if( detector_is_Y( (EnumScope::MajorDetectorsCode) id.wholenum ) ) {
        indexingSignature = detector_complementary_X_id( id.wholenum );
    }
    DetectorLayout & layout = _layouts[indexingSignature];

    if( _doCollectStats && !layout.statsPtr ) {
        layout.statsPtr = consider_entry( indexingSignature );
    }

    if( insertionPlanePtr ) {
        // Insertion plane ptr is needed.
        if( detector_is_X( (EnumScope::MajorDetectorsCode) id.byNumber.major) ) {
            // it is an X-axis
            *insertionPlanePtr = &layout.rLayoutX;
        } else {
            // it is a Y-axis
            *insertionPlanePtr = &layout.rLayoutY;
        }
    }
    return layout;
}

const SimpleAPVClustering::DetectorLayout &
SimpleAPVClustering::this_layout( DetectorSignature dSig ) const {
    const UniqueDetectorID id(dSig);
    DetectorSignature indexingSignature = dSig;
    if( detector_is_Y( (EnumScope::MajorDetectorsCode) id.wholenum ) ) {
        indexingSignature = detector_complementary_X_id( id.wholenum );
    }
    auto it = _layouts.find(indexingSignature);
    if( _layouts.end() == it ) {
        emraise( notFound, "Couldn't find APV layout for detector signature 0x%x.", dSig );
    }
    return it->second;
}

/** This method scans over basic APV data. From 3 samples per strip number it
 * obtains sample with maximum amplitude (supposingly matching to avalanche
 * peak), and computes center-of-mass point.
 *
 * Note, that for APV-managed detectors with «Joint» feature there should
 * be provided wire-to-strips mapping scheme.
 */
bool
SimpleAPVClustering::_V_process_APV_samples( p348::events::APV_sampleSet * sampleSet ) {
    SignalOnStrip reentrantSignal;
    StripsLayout * insertionPlanePtr;

    // Find layout for certain detector:
    UniqueDetectorID detID( sampleSet->detectorid() );
    DetectorLayout & thisLayout = this_layout( detID.wholenum, &insertionPlanePtr );
    thisLayout.sampleSetPtr = sampleSet;

    //
    // Fill strips layouts:
    EnumScope::MajorDetectorsCode detMj = (EnumScope::MajorDetectorsCode) detID.byNumber.major;

    APVMapper map_wire2strip = apv_get_mapper_for( detID );
    for( int32_t nRawData = 0; nRawData < sampleSet->rawdata_size(); ++nRawData ) {
        const events::APV_rawData & rawData = sampleSet->rawdata(nRawData);
        assert( APV_NSamples == rawData.amplitudesamples_size() );
        for( int32_t i = 0; i < rawData.amplitudesamples_size(); ++i ) {
            reentrantSignal.waveform[i] = rawData.amplitudesamples(i);
        }
        if( !detector_is_XYSep( detMj ) ) {
            emraise( badArchitect, "Detector major 0x%x does not support per-plane segmentation. "
                     "APV processor %p relies on this information to perform clustering.",
                     (int) detID.byNumber.major, this );
        }

        APVWireNo wireNo = rawData.wireno();

        size_t nStrips = 1;
        const APVStripNo * stripNumbers = map_wire2strip( wireNo, &nStrips );
        if( _doCollectStats ) {
            thisLayout.statsPtr->stripsConsidered += nStrips;
        }
        # ifdef NDEBUG
        for( size_t nStrip = 0; nStrip < nStrips; ++nStrip ) {
            insertionPlanePtr->emplace( stripNumbers[nStrip], reentrantSignal );
        }
        # else
        if( detector_is_Joint( detMj ) ) {
            // In this case we need to obtain strip numbers which probably
            // was touched by avalanche.
            for( size_t nStrip = 0; nStrip < nStrips; ++nStrip ) {
                auto insertionResult = insertionPlanePtr->emplace( stripNumbers[nStrip], reentrantSignal );
                if( !(insertionResult.second) ) {
                    emraise( nonUniq, "Bad maping of APV's joint wires of detector 0x%x;"
                             " collision detected for %d #strip.",
                             detID.wholenum, (int) stripNumbers[nStrip] );
                }
            }
        } else {
            // In this case wire number matches directly to
            // strip number.
            assert( 1 == nStrips );
            insertionPlanePtr->emplace( *stripNumbers, reentrantSignal );
        }
        # endif
    }
    return true;
}

bool
SimpleAPVClustering::_V_finalize_processing_apv_event( p348::events::ExperimentalEvent * /*expEve*/ ) {
    for( auto & layoutIt : _layouts ) {
        DetectorLayout & thisLayout = layoutIt.second;
        if( thisLayout.rLayoutX.empty() || thisLayout.rLayoutY.empty() ) {
            # if 0
            {  //XXX
                UniqueDetectorID dID( layoutIt.first );
                std::cout << "XXX: No strips in current event for detector " 
                          << detector_name_by_code( (EnumScope::MajorDetectorsCode) dID.byNumber.major )
                          << std::endl;
            }
            # endif
            continue;
        }
        _clusterize( thisLayout.rLayoutX,
                     thisLayout.rLayoutY,
                     thisLayout.reentrantClusterStorage,
                     thisLayout.clusters,
                     thisLayout.statsPtr );

        const aux::DetectorMapping::APVBoundaries & xform
                = aux::DetectorMapping::self().apv_boundaries( (EnumScope::MajorDetectorsCode) layoutIt.first );

        BasicClusterInfo * bestCluster = nullptr;
        for( auto it = thisLayout.clusters.begin(); thisLayout.clusters.end() != it; ++it ) {
            // TODO: Fill all the cluster candidates. Has it sense? It seems to be
            // reasonable to forward only the best one.
            _fill_cluster( **it, *thisLayout.sampleSetPtr->mutable_clusters()->add_clusters(), xform );
            // TODO: elaborate selection condition;
            if( !bestCluster || bestCluster->amplitude < (*it)->amplitude ) {
                bestCluster = *it;
            }
        }
        if( _doCollectStats ) {
            // TODO: use xform here?
            thisLayout.statsPtr->clustersConsidered += thisLayout.clusters.size();
            if( bestCluster ) {
                thisLayout.statsPtr->bestClusterMapPtr->Fill(
                        bestCluster->x, bestCluster->y );
                thisLayout.statsPtr->amplitudeVsRatioDistributionMaxPtr->Fill(
                            bestCluster->ratioAmpXY, bestCluster->amplitude );
                thisLayout.statsPtr->clusterAmplitudeVsWidth->Fill(
                            bestCluster->amplitude,
                            bestCluster->widthX, bestCluster->widthY
                            );
                thisLayout.statsPtr->clusterRatios[0]->Fill(
                            bestCluster->ratio02[0], bestCluster->ratio12[0]
                            );
                thisLayout.statsPtr->clusterRatios[1]->Fill(
                            bestCluster->ratio02[1], bestCluster->ratio12[1]
                            );
                ++thisLayout.statsPtr->clustersFound;
            }
        }
        // TODO: do something with "best cluster".
    }
    return true;
}

void
SimpleAPVClustering::_clusterize( const StripsLayout & layoutX,
                                  const StripsLayout & layoutY,
                                  std::list<BasicClusterInfo> & reentrantClusterStorage,
                                  std::list<BasicClusterInfo *> & clusters,
                                  SimpleAPVStatistics * statsPtr ) {
    ProjectionHit hit;  // reentrant projection hit
    double cStripAmpl;
    std::list<ProjectionHit> centersDim[2];  // x, y
    // Note, that samples in layout maps are sorted by increasing
    // precedence, so strips adjacent in real read-out plane are
    // adjacent here as well.
    for( uint8_t i = 0; i < 2; ++i ) {
        std::list<ProjectionHit> & centers = centersDim[i];
        APVStripNo prevStrip = USHRT_MAX,
                               cStripNo;
        const StripsLayout & layout = i ? layoutX : layoutY;
        for( auto it = layout.begin(); layout.end() != it; ++it ) {
            cStripNo = it->first;
            // discard entire cluster with 0 == a[2] --- bad event.
            if( (cStripNo - prevStrip > 1 && it->second.waveform[2] > _minimalAmplitudeOnStrip)
              || &*it == &*layout.rbegin() ) {
                // next cluster:
                if( (_maxClusterWidth > hit.width)
                 && (_minClusterWidth < hit.width) ) {
                    hit.ratios[0] /= hit.width;
                    hit.ratios[1] /= hit.width;
                    hit.mwPosition /= hit.amplitudeSum;
                    if( hit.ratios[0] > _minRatio02
                     && hit.ratios[1] > _minRatio12 ) {
                        centers.push_back(hit);
                    }
                }
                if( statsPtr ) {
                    statsPtr->hitRatios[i]->Fill( hit.ratios[0], hit.ratios[1] );
                }
                cStripAmpl = 0.;
                bzero( &hit, sizeof(hit) );
            } else {
                // contigous cluster:
                // 1. Increment cluster strip counter,
                // 2. Increase integral amplitude:
                cStripAmpl = it->second.waveform[2];
                hit.ratios[0] += it->second.waveform[0] / it->second.waveform[2];
                hit.ratios[1] += it->second.waveform[1] / it->second.waveform[2];
                hit.amplitudeSum += cStripAmpl;
                hit.mwPosition += cStripAmpl*(cStripNo);
                ++hit.width;
            }
            prevStrip = cStripNo;
        }
    }
    // Here: see snippet #1
    {  // Now, build clusters:
        BasicClusterInfo clusterInfo;

        for( auto & itX : centersDim[0] ) {
            for( auto & itY : centersDim[1] ) {
                clusterInfo.ratioAmpXY = (itX.amplitudeSum - itY.amplitudeSum)
                                       / (itX.amplitudeSum + itY.amplitudeSum)
                                       ;
                clusterInfo.ratio02[0] = itX.ratios[0];
                clusterInfo.ratio02[1] = itY.ratios[0];
                clusterInfo.ratio12[0] = itX.ratios[1];
                clusterInfo.ratio12[1] = itY.ratios[1];
                clusterInfo.widthX = itX.width;
                clusterInfo.widthY = itY.width;
                // If discrepancy between amplitudes exceeds maximum
                // threshold, omit this X/Y pair and do not consider
                // it as a cluster candidate.
                if( clusterInfo.ratioAmpXY < _ratioTrust[0] 
                 || clusterInfo.ratioAmpXY > _ratioTrust[1] ) continue;
                // Otherwise, fill cluster information:
                clusterInfo.x = itX.mwPosition;
                clusterInfo.y = itY.mwPosition;
                clusterInfo.amplitude = itX.amplitudeSum + itY.amplitudeSum;
                reentrantClusterStorage.push_back( clusterInfo );
                clusters.push_back( &*reentrantClusterStorage.rbegin() );
                if( statsPtr ) {
                    statsPtr->ccandMapPtr->Fill( clusterInfo.x, clusterInfo.y );
                    statsPtr->amplitudeVsRatioDistributionPtr->Fill( clusterInfo.ratioAmpXY,
                                                                     clusterInfo.amplitude );
                }
            }
        }
    }
}

void
SimpleAPVClustering::_fill_cluster( BasicClusterInfo & clusterInfo,
                                    events::APV_Cluster & clusterMsg,
                                    const aux::DetectorMapping::APVBoundaries & xf ) const {
    float pt[] = { clusterInfo.x, clusterInfo.y };
    xf.norm( pt );
    // clusterMsg is located here:
    //  Event
    //  / experimental : ExperimentalEvent
    //  / apv_data : APV_sampleSet
    //  / clusters : ClustersArray
    //  / <i> : APV_Cluster
    clusterMsg.mutable_trackpoint()->set_x( pt[0] );
    clusterMsg.mutable_trackpoint()->set_y( pt[1] );
    clusterMsg.mutable_trackpoint()->set_amplitude( clusterInfo.amplitude );

    clusterMsg.set_xyratio( clusterInfo.ratioAmpXY );
    clusterMsg.add_ratios( clusterInfo.ratio02[0] );
    clusterMsg.add_ratios( clusterInfo.ratio02[1] );
    clusterMsg.add_ratios( clusterInfo.ratio12[0] );
    clusterMsg.add_ratios( clusterInfo.ratio12[1] );
    //p348g4_log3( "XXX set: %e %e\n", clusterInfo.x, clusterInfo.y );
}

void
SimpleAPVClustering::_V_finalize_event_processing( Event * ) {
    // todo: When using a custom allocator it will be better to not
    // physically destroy all the nodes (what clea() does), but only
    // free them in sense of allocator.
    for( auto & layoutIt : _layouts ) {
        DetectorLayout & thisLayout = layoutIt.second;
        thisLayout.rLayoutX.clear();
        thisLayout.rLayoutY.clear();
        thisLayout.clusters.clear();
        thisLayout.reentrantClusterStorage.clear();
        thisLayout.sampleSetPtr = nullptr;
    }
}

void
SimpleAPVClustering::_V_finalize() const {
}

void
SimpleAPVClustering::_V_print_brief_summary( std::ostream & os ) const {
    os << ESC_CLRGREEN "APV simple clusterization proc" ESC_CLRCLEAR ":" << std::endl;
    if( _doCollectStats ) {
        for( auto & ltRef : _layouts ) {
            UniqueDetectorID dID( ltRef.first );
            const std::string detName = detector_name_by_code(
                    (EnumScope::MajorDetectorsCode) dID.byNumber.major );
            const DetectorLayout & thisLayout = ltRef.second;
            os << "  APV-managed detector \"" << ESC_CLRGREEN << detName << ESC_CLRCLEAR << "\":" << std::endl
               << "    strips considered ........ : " << thisLayout.statsPtr->stripsConsidered << std::endl
               << "    clusters considered ...... : " << thisLayout.statsPtr->clustersConsidered << std::endl
               << "    clusters choosen ......... : " << thisLayout.statsPtr->clustersFound << std::endl
            ;
        }
    }
}

SimpleAPVClustering::SimpleAPVClustering( bool doAccumulateStatistics,
                                          size_t maxClusterWidth,
                                          size_t minClusterWidth,
                                          double maxRatioTrust,
                                          double minRatioTrust,
                                          double minimalAmplitudeOnStrip,
                                          double minRatio02,
                                          double minRatio12) :
            Catalogue("APV-clustering"),
            _maxClusterWidth( maxClusterWidth ? maxClusterWidth : USHRT_MAX ),
            _minClusterWidth( minClusterWidth ),
            _ratioTrust{ minRatioTrust, maxRatioTrust },
            _doCollectStats(doAccumulateStatistics),
            _minimalAmplitudeOnStrip(minimalAmplitudeOnStrip),
            _minRatio02( minRatio02 ),
            _minRatio12( minRatio12 ) {
    assert( _ratioTrust[0] < _ratioTrust[1] );
}

SimpleAPVClustering::~SimpleAPVClustering() {
}

SimpleAPVStatistics *
SimpleAPVClustering::_V_new_entry( DetectorSignature id, TDirectory * famDir ) {
    const aux::HistogramParameters2D & hstARP = goo::app<AnalysisApplication>()
        .cfg_option<p348::aux::HistogramParameters2D>("apv-simple.amplitude-vs-ratio"),
                                     & hstHMP = goo::app<AnalysisApplication>()
        .cfg_option<p348::aux::HistogramParameters2D>("apv-simple.cluster-candidates-map"),
                                     & hstAVW = goo::app<AnalysisApplication>()
        .cfg_option<p348::aux::HistogramParameters2D>("apv-simple.cluster-amplitude-vs-cluster-width")
        ;
    char detectorNameBuffer[64];
    snprintf_detector_name(
            detectorNameBuffer,
            sizeof(detectorNameBuffer),
            UniqueDetectorID(id) );
    assert( strcmp( "unknown", detectorNameBuffer ) );
    famDir->mkdir(detectorNameBuffer)->cd();
    char hstNameBuffer[128];
    char hstLabelBuffer[128];
    auto res = new SimpleAPVStatistics();

    # define M_form_hst_name( fmt, ... ) \
        snprintf( hstNameBuffer, sizeof(hstNameBuffer),  fmt, __VA_ARGS__ );
    # define M_form_hst_label( fmt, ... ) \
        snprintf( hstLabelBuffer, sizeof(hstLabelBuffer),  fmt, __VA_ARGS__ );

    M_form_hst_name(  "apvClusteringMap-%s", detectorNameBuffer );
    M_form_hst_label( "APV clusters candidates map (%s)", detectorNameBuffer );
    res->ccandMapPtr = new TH2I( hstNameBuffer, hstLabelBuffer,
                                hstHMP.nBins[0], hstHMP.min[0], hstHMP.max[0],
                                hstHMP.nBins[1], hstHMP.min[1], hstHMP.max[1]);
    M_form_hst_name(  "apvHitMap-%s", detectorNameBuffer );
    M_form_hst_label( "APV cluster (%s)", detectorNameBuffer );
    res->bestClusterMapPtr = new TH2I( hstNameBuffer, hstLabelBuffer,
                                hstHMP.nBins[0], hstHMP.min[0], hstHMP.max[0],
                                hstHMP.nBins[1], hstHMP.min[1], hstHMP.max[1]);

    M_form_hst_name(  "APVampVsRatio-%s", detectorNameBuffer );
    M_form_hst_label( "APV ratio vs. amplitude (%s)", detectorNameBuffer );
    res->amplitudeVsRatioDistributionPtr = new TH2F( hstNameBuffer, hstLabelBuffer,
                                hstARP.nBins[0], hstARP.min[0], hstARP.max[0],
                                hstARP.nBins[1], hstARP.min[1], hstARP.max[1] );

    M_form_hst_name(  "APVampVsRatio-best-%s", detectorNameBuffer );
    M_form_hst_label( "APV ratio vs. amplitude for best cluster (%s)", detectorNameBuffer);
    res->amplitudeVsRatioDistributionMaxPtr = new TH2F( hstNameBuffer, hstLabelBuffer,
                                hstARP.nBins[0], hstARP.min[0], hstARP.max[0],
                                hstARP.nBins[1], hstARP.min[1], hstARP.max[1] );

    M_form_hst_name(  "APVClAmplVsClWidth-%s", detectorNameBuffer );
    M_form_hst_label( "Cluster integral amplitude vs cluster width (%s)", detectorNameBuffer);
    res->clusterAmplitudeVsWidth = new TH2F( hstNameBuffer, hstLabelBuffer,
                                hstAVW.nBins[0], hstAVW.min[0], hstAVW.max[0],
                                hstAVW.nBins[1], hstAVW.min[1], hstAVW.max[1]);

    for( uint8_t i = 0; i < 2; ++i ) {
        const char coordLetter = (i ? 'X' : 'Y');
        M_form_hst_name(  "clustLatencyRatio-%s-%c", detectorNameBuffer, coordLetter );
        M_form_hst_label( "Cluster-mean latency ratios for APV (%s:%c)", detectorNameBuffer, coordLetter);
        res->clusterRatios[i] = new TH2F( hstNameBuffer, hstLabelBuffer,
                                    100, -.5, 3,
                                    100, -.5, 3 );
        M_form_hst_name(  "hitLatencyRatio-%s-%c", detectorNameBuffer, coordLetter );
        M_form_hst_label( "Latency ratios for APV (%s:%c) per projection hit", detectorNameBuffer, coordLetter);
        res->hitRatios[i] = new TH2F( hstNameBuffer, hstLabelBuffer,
                                    100, -.5, 3,
                                    100, -.5, 3 );
    }

    res->stripsConsidered
        = res->clustersConsidered
        = res->clustersFound
        = 0;

    # undef M_form_hst_name
    # undef M_form_hst_label

    return res;
}

void
SimpleAPVClustering::_V_free_entry( DetectorSignature, SimpleAPVStatistics * ptr ) {
    delete ptr;
}

// Register processor
////////////////////
p348_DEFINE_CONFIG_ARGUMENTS {
    po::options_description apvStatsParameters( "APV simple strip clustering parameters" );
    { apvStatsParameters.add_options()
        ("apv-simple.max-ratio-trust",
            po::value<double>()->default_value(1.),
            "Maximum ratio between amplitudes on X/Y strip plane to "
            "be adjointly considered as a cluster. Ratio is calculated with "
            "formula ((a_x - a_y)/(a_x + a_y)); so always lies between -1..1. "
            "By setting this parameter to +1 one can neglect "
            "this criterion completely.")
        ("apv-simple.min-ratio-trust",
            po::value<double>()->default_value(-1),
            "Maximum ratio between amplitudes on X/Y strip plane to "
            "be adjointly considered as a cluster. Ratio is calculated with "
            "formula ((a_x - a_y)/(a_x + a_y)); so always lies between -1..1. "
            "By setting this parameter to -1 one can neglect "
            "this criterion completely.")
        ("apv-simple.collect-stats",
            po::value<bool>(),
            "Do collect statistics for APV chips." )
        ("apv-simple.max-cluster-width",
            po::value<int>()->default_value(60),
            "Maximum cluster width (if set to > nWires comparision will be always true)." )
        ("apv-simple.min-cluster-width",
            po::value<int>()->default_value(0),
            "Minimum cluster width (if set to 0 or 1 comparision will be always true)." )
        ("apv-simple.amplitude-vs-ratio",
            po::value<p348::aux::HistogramParameters2D>()->default_value(
            p348::aux::HistogramParameters2D(100, -1, 1, 100, 0, 3e4) ),
            "2D histogram parameters for histograming cluster amplitude vs ratio. "
            "Ratio is always in 0..1 while upper limit of amplitude can vary.")
        ("apv-simple.cluster-candidates-map",
            po::value<p348::aux::HistogramParameters2D>()->default_value(
            p348::aux::HistogramParameters2D(320, 0, 320,  320, 0, 320) ),
            "2D histogram parameters for filling with cluster poistions.")
        ("apv-simple.cluster-amplitude-vs-cluster-width",
            po::value<p348::aux::HistogramParameters2D>()->default_value(
            p348::aux::HistogramParameters2D(1e3, 0, 3e4,  50, 0, 50) ),
            "2D histogram: cluster amplitude vs cluster width.")
        ("apv-simple.minimal-cluster-amplitude",
            po::value<double>()->default_value(0.),
            "Minimal amplitude on strip to be taken into consideration.")
        ("apv-simple.min-ratio02",
            po::value<double>()->default_value(1e-3),
            "Minimal amplitude ratio in frame: a_0/a_2.")
        ("apv-simple.min-ratio12",
            po::value<double>()->default_value(1e-3),
            "Minimal amplitude ratio in frame: a_1/a_2.")
        // ...
        ;
    }
    return apvStatsParameters;
} p348_DEFINE_DATA_PROCESSOR( SimpleAPVClustering ) {
    return new SimpleAPVClustering(
            goo::app<AnalysisApplication>().cfg_option<bool>("apv-simple.collect-stats"),
            goo::app<AnalysisApplication>().cfg_option<int>("apv-simple.max-cluster-width"),
            goo::app<AnalysisApplication>().cfg_option<int>("apv-simple.min-cluster-width"),
            goo::app<AnalysisApplication>().cfg_option<double>("apv-simple.max-ratio-trust"),
            goo::app<AnalysisApplication>().cfg_option<double>("apv-simple.min-ratio-trust"),
            goo::app<AnalysisApplication>().cfg_option<double>("apv-simple.minimal-cluster-amplitude"),
            goo::app<AnalysisApplication>().cfg_option<double>("apv-simple.min-ratio02"),
            goo::app<AnalysisApplication>().cfg_option<double>("apv-simple.min-ratio12")
        );
} p348_REGISTER_DATA_PROCESSOR(
    SimpleAPVClustering,
    "SimpleAPVClustering",
    "Primitive and fast APV clustering algorithm." )

}  // namespace dprocessors
}  // namespace p348



// Snippet #1:
    # if 0
    {  // XXX:
        UniqueDetectorID dID(0);
        for( auto & lp : _layouts ) {
            if( &(lp.second.rLayoutX) == &layoutX ) {
                dID.wholenum = lp.first;
                break;
            }
        }
        if( dID.wholenum ) {
            if( EnumScope::fam_GEM == detector_family_num( dID.byNumber.major ) ) {
                std::cout << "XXX: GEM centers: " << centersDim[0].size()
                          << "×"              << centersDim[1].size() << std::endl;  //XXX
            } else {
                std::cout << "XXX: Centers "
                          << detector_family_name( detector_family_num(dID.byNumber.major) ) << "("
                          << std::hex << dID.byNumber.major << ")" << ": "
                          << centersDim[0].size() << "×" << centersDim[1].size() << std::endl;  //XXX
            }
        }
    }
    # endif
    //std::cout << centersDim[0].size() << ", " << centersDim[1].size() << std::endl; //XXX

# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

