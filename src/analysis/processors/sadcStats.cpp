/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# if defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

# if 0
# include "app/analysis.hpp"
# include <TH1F.h>
# include <TH2F.h>
# include <TFile.h>
# include <TDirectory.h>
# include <TProfile.h>
# include <TSystem.h>
# include "p348g4_detector_ids.h"
# include "app/cvalidators.hpp"
# include "p348g4_uevent.hpp"
# include "app/analysis_cat_mx.tcc"

namespace p348 {
namespace dprocessors {
namespace aux {

struct SADC_WF_Reconstruction

class SADC_WF_ReconstructionStats : public ::p348::AnalysisApplication::iSADCProcessor {
protected:
    virtual void _V_print_brief_summary( std::ostream & os ) const override;
    virtual bool _V_process_SADC_profile_event( p348::events::SADC_profile * ) override;

    const p348::aux::HistogramParameters2D _sadcHstPars;
    bool _doCollectStats;
    TDirectory * _directory;
    std::unordered_map<unsigned, TDirectory *> _directories;
    std::unordered_map<unsigned, std::pair<TProfile *, TH2F *> > _profiles;
public:
    SADC_WF_ReconstructionStats(
        const p348::aux::HistogramParameters2D & sadcHstPars) :
                _sadcHstPars(sadcHstPars),
                _directory(nullptr) {}
};


void
SADC_WF_ReconstructionStats::_V_print_brief_summary( std::ostream & ) const {
    // ... TODO
}

bool
SADC_WF_ReconstructionStats::_V_process_SADC_profile_event( p348::events::SADC_profile * samplesPtr ) {
    p348::events::SADC_profile & sadcProf = *samplesPtr;
    UniqueDetectorID uID = { .wholenum = (UShort) sadcProf.detectorid() };
    auto it = _profiles.find( uID.wholenum );
    if( _profiles.end() == it ) {
        // Get directory:
        auto itDir = _directories.find( detector_family_num(uID.byNumber.major) );
        // if dir is not yet created, create:
        if( _directories.end() == itDir ) {
            if( !_directory ) {
                if( !gFile ) {
                    emraise( badState, "Processor requires an opened ROOT file to operate." );
                }
                gFile->cd();
                _directory = gFile->mkdir( "Statistics" );
            }
            auto insertionResult = _directories.emplace(
                    detector_family_num( uID.byNumber.major ),
                    _directory->mkdir( detector_family_name( uID.byNumber.major ) )
                );
            itDir = insertionResult.first;
        }
        itDir->second->cd();
        char detNamebf[32], namebf[64], labelbf[128];
        snprintf_detector_name( detNamebf, 32, uID );
        snprintf( namebf,  64, "%s-profile", detNamebf );
        snprintf( labelbf, 128, "SADC profile for %s detector", detNamebf );
        auto profilePtr = new TProfile(
                namebf,
                labelbf,
                _sadcHstPars.nBins[0],
                _sadcHstPars.min[0],
                _sadcHstPars.max[0] );
        snprintf( namebf,  64, "%s-dst-profile", detNamebf );
        snprintf( labelbf, 128, "SADC distribution profile for %s detector", detNamebf );
        auto dispPtr = new TH2F(
                namebf,
                labelbf,
                _sadcHstPars.nBins[0],
                _sadcHstPars.min[0],
                _sadcHstPars.max[0],
                _sadcHstPars.nBins[1],
                _sadcHstPars.min[1],
                _sadcHstPars.max[1]
                );
        it = _profiles.emplace( uID.wholenum, std::pair<TProfile *, TH2F *>( profilePtr, dispPtr) ).first;
        # if 0
        it = _profiles.emplace( detID, new TH1F * [2] ).first;
        for(uint8_t i = 0; i < 2; ++i) {
            char detNamebf[32], namebf[64], labelbf[128];
            snprintf_detector_name( detNamebf, 32, uID );
            //std::cout << std::hex << (int) uID.byNumber.major << ":"
            //          << std::hex << (int) uID.byNumber.minor << " "
            //          << " :: " << detNamebf << std::endl;
            snprintf( namebf, 64, "%s-ped%d", detNamebf, (int) i );
            snprintf( labelbf, 128, "Pedestals #%d for %s det.", (int) i, detNamebf );
            it->second[i] = new TH1F( namebf, labelbf, _hstPars.nBins, _hstPars.min, _hstPars.max );
        }
        # endif
    }
    for( UByte i = 0; i < 32; ++i ) {
        it->second.first->Fill(i,  sadcProf.samples( i ));
        it->second.second->Fill(i, sadcProf.samples( i ));
    }
    return true;
}

p348_DEFINE_CONFIG_ARGUMENTS {
    po::options_description sadcCStats( "SADC waveform statistics collector processor (sadcCStats) options" );
    { sadcCStats.add_options()
        ("sadc-stats.store-ampls-histogram",
            po::value< p348::aux::HistogramParameters2D >(),
            "Amplitude binning boundaries for time-vs-SADC histogram.")
        ;
    }
    return sadcCStats;
}
p348_DEFINE_DATA_PROCESSOR( SADC_WF_ReconstructionStats ) {
    return new SADC_WF_ReconstructionStats(
                    goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters2D>("sadc-stats.store-ampls-histogram")
            );
} p348_REGISTER_DATA_PROCESSOR( SADC_WF_ReconstructionStats,
    "sadcWFReconstruction",
    "Computes distribution parameters and fits waveforms"
    "with specified function."
)

}  // namespace aux
}  // namespace dprocessors
}  // namespace p348
# endif

# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

