/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# if defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

# if 1
# include "app/analysis.hpp"
# include "p348g4_uevent.hpp"
# include "analysis/sadcWF_supp.h"
# include "analysis/sadcWF_fit.h"
# include "app/analysis_cat_mx.tcc"
# include "app/cvalidators.hpp"
# include <TProfile.h>
# include <TH2F.h>
# include <TH1F.h>

namespace p348 {
namespace dprocessors {
namespace aux {

struct DistributionProfile {
    TProfile * _profilePtr;
        TH2F * _distributionPtr;
    DistributionProfile(
                const std::string & nameSuffix,
                const std::string & labelSuffix,
                p348::aux::HistogramParameters2D pars ) {
        _profilePtr = new TProfile(
                (nameSuffix + "-prof").c_str(),
                (labelSuffix + " profile").c_str(),
                pars.nBins[0], pars.min[0], pars.max[0] );
        _distributionPtr = new TH2F(
                (nameSuffix + "-dstr").c_str(),
                (labelSuffix + " profile distribution").c_str(),
                pars.nBins[0], pars.min[0], pars.max[0],
                pars.nBins[1], pars.min[1], pars.max[1] );
    }
    void fill( float x, float y ) {
        _profilePtr->Fill(x, y);
        _distributionPtr->Fill(x, y);
    }
};

struct ReconstructionStatistics {
    DistributionProfile absMax,
                        mean
                        ;
    TH1F * _reliabilityDistribution,
         * _stdDeviation,
         * _sumDistribution,
         * _timeDispersion
         ;

    ReconstructionStatistics(
            const std::string & detectorName,
            const p348::aux::HistogramParameters2D & maxHstmsPars,
            const p348::aux::HistogramParameters2D & meanHstmsPars,
            const p348::aux::HistogramParameters1D & relDstHstmPars,
            const p348::aux::HistogramParameters1D & stdDeviationHstPars,
            const p348::aux::HistogramParameters1D & sumDstHstPars,
            const p348::aux::HistogramParameters1D & timeDispersionPars
            ) :
               absMax("absMax" + detectorName, "Abs. max values of " + detectorName, maxHstmsPars),
               mean("mean" + detectorName, "Mean values of " + detectorName, meanHstmsPars) {
        _reliabilityDistribution = new TH1F(
                ("rel" + detectorName).c_str(), ("Reliability on " + detectorName).c_str(),
                relDstHstmPars.nBins, relDstHstmPars.min, relDstHstmPars.max );
        _stdDeviation = new TH1F(
                ("stdDev-" + detectorName).c_str(), ("Std. deviation on " + detectorName).c_str(),
                stdDeviationHstPars.nBins, stdDeviationHstPars.min, stdDeviationHstPars.max );
        _sumDistribution = new TH1F(
                ("sumDistrib-" + detectorName).c_str(), ("Sum distribution of " + detectorName).c_str(),
                sumDstHstPars.nBins, sumDstHstPars.min, sumDstHstPars.max );
        _timeDispersion = new TH1F(
                ("timeDisp-" + detectorName).c_str(), ("Time dispersion on " + detectorName).c_str(),
                timeDispersionPars.nBins, timeDispersionPars.min, timeDispersionPars.max );
    }

    void consider_exp_sadc_stats( UniqueDetectorID /*uid*/, const ::p348::events::SADC_suppInfo & sInfo ) {
        if( sInfo.absmaxnbin() || sInfo.absmaxval() ) {
            absMax.fill( sInfo.absmaxnbin(), sInfo.absmaxval() );
        }
        if( sInfo.mean() || sInfo.weightedmean() ) {
            mean.fill( sInfo.weightedmean()*32, sInfo.mpv() );
        }
        if( sInfo.reliability() ) {
            _reliabilityDistribution->Fill( sInfo.reliability() );
        }
        if( sInfo.stddeviation() ) {
            _stdDeviation->Fill( sInfo.stddeviation() );
        }
        if( sInfo.sum() ) {
            _sumDistribution->Fill( sInfo.sum() );
        }
        if( sInfo.timedispersion() ) {
            _timeDispersion->Fill( sInfo.timedispersion() );
        }
    }

    static float apply_calibration_by_max( p348::events::SADC_profile & );
};

class SADC_WF_Reconstruction : public AnalysisApplication::iSADCProcessor,
                               public ::p348::mixins::DetectorCatalogue<ReconstructionStatistics *> {
public:
    typedef AnalysisApplication::iEventProcessor Parent;
    typedef AnalysisApplication::Event Event;
protected:
    struct SADCWF_FittingInput samples;
    size_t _nConsidered,
           _nRefused_badAlgo,
           _nRefused_negativeSum,
           _nRefused_nonNumCh
           ;

    SADCWF_FittingFunction _fitting_function;
    SADCWF_FittingFunctionParameters fittingParameters;

    struct SADCWFCharacteristicParameters _chParsReentrant;

    virtual bool _V_process_SADC_profile_event( ::p348::events::SADC_profile * ) override;
    virtual void _V_print_brief_summary( std::ostream & ) const override;
    ReconstructionStatistics * _V_new_entry( DetectorSignature id, TDirectory * ) override;
    void _V_free_entry( DetectorSignature, ReconstructionStatistics * ) override;
public:
    SADC_WF_Reconstruction();
    ~SADC_WF_Reconstruction();
};  // class TestingProcessor


// IMPLEM

SADC_WF_Reconstruction::SADC_WF_Reconstruction() :
           ::p348::mixins::DetectorCatalogue<ReconstructionStatistics *>("Fitting") {
    _nConsidered
        = _nRefused_nonNumCh
        = _nRefused_badAlgo
        = _nRefused_negativeSum
        = 0;
    samples.n = 32;
    samples.samples = (SADCSamples *) malloc( samples.n*sizeof(SADCSamples) );
    samples.sigma   = (double *)      malloc( samples.n*sizeof(double) );
    ::allocate_SADCWF_cache( &_chParsReentrant, 32 );
    _fitting_function = ::moyal;  // TODO
}

SADC_WF_Reconstruction::~SADC_WF_Reconstruction() {
    free( samples.samples );
    free( samples.sigma );
    ::free_SADCWF_cache( &_chParsReentrant, 32 );
}

ReconstructionStatistics *
SADC_WF_Reconstruction::_V_new_entry( DetectorSignature id, TDirectory * famDir ) {
    char detectorNameBuffer[64];
    snprintf_detector_name(
            detectorNameBuffer,
            sizeof(detectorNameBuffer),
            UniqueDetectorID(id) );
    famDir->mkdir(detectorNameBuffer)->cd();
    return new ReconstructionStatistics(
            detectorNameBuffer,
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters2D>("sadc-rectr.abs-max-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters2D>("sadc-rectr.mean-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters1D>("sadc-rectr.reliab-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters1D>("sadc-rectr.std-dev-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters1D>("sadc-rectr.sum-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters1D>("sadc-rectr.time-disp-hst")
        );
}

void
SADC_WF_Reconstruction::_V_free_entry( DetectorSignature, ReconstructionStatistics * ptr ) {
    delete ptr;
}

void  // TODO: header for extern usage
translate_ch_pars2uevent(
            const struct SADCWFCharacteristicParameters & chPars,
            ::p348::events::SADC_suppInfo & sInfo ) {
    sInfo.set_absmaxnbin( chPars.absMaxNBin );
    sInfo.set_absmaxval( chPars.absMaxVal );
    sInfo.set_mean( chPars.mean );
    sInfo.set_weightedmean( chPars.weightedMean );
    sInfo.set_mpv( chPars.mpv );
    sInfo.set_sum( chPars.sum );
    sInfo.set_stddeviation( chPars.stdDeviation );
    sInfo.set_reliability( chPars.reliability );
    sInfo.set_timedispersion( chPars.timeDispersion );
}

bool
SADC_WF_Reconstruction::_V_process_SADC_profile_event( ::p348::events::SADC_profile * sadcProfile ){
    // initialize caches:
    //UniqueDetectorID uID {
    //    .wholenum = (UShort) sadcProfile->detectorid() };
    const uint8_t nSamples = sadcProfile->samples_size();
    assert(32 == nSamples);
    for( uint8_t nSample = 0;
         nSample < nSamples;
         ++nSample ) {
        samples.samples[nSample] = sadcProfile->samples( nSample );
    }
    //
    int rc = ::calculate_characteristics( samples.samples, samples.n, &_chParsReentrant );
    _nConsidered++;
    if( rc < 0 ) {
        if( -1 == rc ) {
            _nRefused_badAlgo++;
        } else if( -2 == rc ) {
            _nRefused_negativeSum++;
        } else if( -3 == rc ) {
            _nRefused_nonNumCh++;
        }
        return true;
    }
    auto statsPtr = this->consider_entry( sadcProfile->detectorid() );
    translate_ch_pars2uevent( _chParsReentrant, *(sadcProfile->mutable_suppinfo()) );
    statsPtr->consider_exp_sadc_stats( sadcProfile->detectorid(), sadcProfile->suppinfo() );
    // TODO: for Moyal model function only
    {
        fittingParameters.moyalPars.p[0] = _chParsReentrant.mpv;
        fittingParameters.moyalPars.p[1] = _chParsReentrant.weightedMean*samples.n;
        fittingParameters.moyalPars.p[2] = 4.44*_chParsReentrant.timeDispersion;
        for( SADCSamplesSize i = 0; i < samples.n; ++i ) {
            samples.sigma[i] = _chParsReentrant.cache->linearity[i];
        }
    }
    // fit
    ::fit_SADC_samples(
            &samples,
            _fitting_function,
            &fittingParameters,
            NULL );
    // TODO: for moyal function only
    {
        p348::events::SADC_ModelParameters & mp = *(sadcProfile->mutable_suppinfo()->mutable_fittingmodel());
        mp.mutable_moyal()->set_p1( fittingParameters.moyalPars.p[0] );
        mp.mutable_moyal()->set_p2( fittingParameters.moyalPars.p[1] );
        mp.mutable_moyal()->set_p3( fittingParameters.moyalPars.p[2] );
    }
    // TODO: use calibration data
    //statsPtr->consider_exp_sadc_stats( sadcProfile->detectorid(), sadcProfile->suppinfo() );

    return false;
}

void
SADC_WF_Reconstruction::_V_print_brief_summary( std::ostream & os ) const {
    os << ESC_CLRGREEN "sadcWFReconstruction proc" ESC_CLRCLEAR ":" << std::endl
       << "Spectra processed: " << _nConsidered << ", from them are refused:" << std::endl
       << "  by algorithmic problem ..... : " << (_nRefused_badAlgo ? 100*((double) _nRefused_badAlgo)/_nConsidered : 0.)
           << "% (" << _nRefused_badAlgo << ")"  << std::endl
       << "  by negative sum ............ : " << (_nRefused_negativeSum ? 100*((double) _nRefused_negativeSum)/_nConsidered : 0.)
           << "% (" << _nRefused_negativeSum << ")"  << std::endl
       << "  with non-numerical value ... : " << (_nRefused_nonNumCh ? 100*((double) _nRefused_nonNumCh)/_nConsidered : 0.)
           << "% (" << _nRefused_nonNumCh << ")"  << std::endl
    ;
}

p348_DEFINE_CONFIG_ARGUMENTS {
    # if 0
                    .cfg_option<p348::aux::HistogramParameters2D>("sadc-rectr.abs-max-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters2D>("sadc-rectr.mean-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters1D>("sadc-rectr.reliab-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters1D>("sadc-rectr.std-dev-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters1D>("sadc-rectr.sum-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters1D>("sadc-rectr.time-disp-hst")
    # endif
    po::options_description sadcwfAlign( "SADC waveform timing (timeRes) options" );
    { sadcwfAlign.add_options()
        ("sadc-rectr.function",
            po::value<std::string>()->default_value("moyal"),
            "Available options are: moyal / ... "
            "Model function to fit WF." )
        ("sadc-rectr.collect-stats",
            po::value<bool>(),
            "Do collect statistics." )
        ("sadc-rectr.abs-max-hst",
            po::value<p348::aux::HistogramParameters2D>()->default_value(
            p348::aux::HistogramParameters2D(160, 0, 32,  80, 0, 6000) ),
            "2D histogram parameters for histograming absolute maximums of waveforms." )
        ("sadc-rectr.mean-hst",
            po::value<p348::aux::HistogramParameters2D>()->default_value(
            p348::aux::HistogramParameters2D(160, 0, 32,  80, 0, 4000) ),
            "2D histogram parameters for histograming mean values of waveforms." )
        ("sadc-rectr.reliab-hst",
            po::value<p348::aux::HistogramParameters1D>()->default_value(
            p348::aux::HistogramParameters1D(200, 0, 1.) ),
            "1D histogram parameters for histograming \"reliability\" values." )
        ("sadc-rectr.std-dev-hst",
            po::value<p348::aux::HistogramParameters1D>()->default_value(
            p348::aux::HistogramParameters1D(200, 0, 4000) ),
            "1D histogram parameters for histograming standard deviation values." )
        ("sadc-rectr.sum-hst",
            po::value<p348::aux::HistogramParameters1D>()->default_value(
            p348::aux::HistogramParameters1D(200, 0, 5e4) ),
            "1D histogram parameters for histograming sum values." )
        ("sadc-rectr.time-disp-hst",
            po::value<p348::aux::HistogramParameters1D>()->default_value(
            p348::aux::HistogramParameters1D(200, 0, 6) ),
            "1D histogram parameters for histograming time dispersion values." )
        // ...
        ;
    }
    return sadcwfAlign;
}
p348_DEFINE_DATA_PROCESSOR( SADC_WF_Reconstruction ) {
    return new SADC_WF_Reconstruction();
} p348_REGISTER_DATA_PROCESSOR( SADC_WF_Reconstruction,
    "sadcWFReconstruction",
    "Computes distribution parameters and fits waveforms"
    "with specified function."
)

}  // namespace aux
}  // namespace dprocessors
}  // namespace p348

# endif

# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

