/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# ifdef ANALYSIS_ROUTINES

# include <stdlib.h>
# include <math.h>

# include <gsl/gsl_vector.h>
# include <gsl/gsl_rng.h>
# include <gsl/gsl_randist.h>
# include <gsl/gsl_blas.h>
# include <gsl/gsl_multifit_nlin.h>

# include "analysis/sadcWF_fit.h"

/*
 * Moyal function ********************************************************* {{{
 */

/* Model function shortcut. */
double
moyal( union SADCWF_FittingFunctionParameters * gp, double x ) {
    const double lambda = (x - gp->moyalPars.p[1])/gp->moyalPars.p[2],
                 exp1 = exp(-lambda),
                 exp2 = exp( -(lambda + exp1)/2 )
                 ;
    return exp2*gp->moyalPars.p[0];
}

/* Error calculation function --- instance to be minimized. */
int
moyal_f( const gsl_vector * p,
         void * data_, 
         gsl_vector * f){
    uint8_t i;
    struct SADCWF_FittingInput * data = (struct SADCWF_FittingInput *) data_;
    const SADCSamples * y = data->samples;
    const double * sigma = data->sigma;

    double p1 = gsl_vector_get(p, 0),
           p2 = gsl_vector_get(p, 1),
           p3 = gsl_vector_get(p, 2)
           ;

    for( i = 0; i < data->n; i++ ) {
        /* Model Yi = p1*exp(- (lambda + exp(-lambda))/2),
         *       lambda = (x-p2)/p3 */
        double x        = i,
               lambda   = ( x - p2 ) / p3,
               Yi       = p1*exp(- (lambda + exp(-lambda))/2);
        gsl_vector_set( f, i, (Yi - y[i])/sigma[i] );
    }
    return GSL_SUCCESS;
}

/* Jacobian matrix. */
int
moyal_df( const gsl_vector * p,
          void * data_, 
          gsl_matrix * J){
    uint8_t i;
    struct SADCWF_FittingInput * data = (struct SADCWF_FittingInput *) data_;
    const double p1 = gsl_vector_get(p, 0),
                 p2 = gsl_vector_get(p, 1),
                 p3 = gsl_vector_get(p, 2)
           ;
    for( i = 0; i < data->n; i++ ) {
        /* Jacobian matrix J(i,j) = dfi / dxj,
         * where fi = (Yi - yi)/sigma[i],
         *       Yi = A * exp(-lambda * i) + b
         * and the xj are the parameters (A,lambda,b) */
        const double x = i,
                     a = 1,
                     sigma = data->sigma[i],
                     lambda = a*(x - p2)/p3,
                     exp2 = exp(-lambda),
                     exp1 = exp(-(lambda + exp2)/2)
               ;
        gsl_matrix_set( J, i, 0, exp1/sigma );
        gsl_matrix_set( J, i, 1, (p1/(2*p3*sigma))*a*(1 - exp2)*exp1 );
        gsl_matrix_set( J, i, 2, (p1/(2*p3*p3*sigma))*a*(x-p2)*(1 - exp2)*exp1 );
    }
    return GSL_SUCCESS;
}

/* Function and matrix simultaneous calculation shortcut. */
int
moyal_fdf( const gsl_vector * p,
           void * data, 
           gsl_vector * f,
           gsl_matrix * J ){
    moyal_f(p, data, f);
    moyal_df(p, data, J);
    return GSL_SUCCESS;
}

/* }}} Moyal function */


/*
 * General fitting routines
 */

/* Aux function to print out solver state */
static void
print_state( FILE * logfile, size_t iter, gsl_multifit_fdfsolver * s) {
    if( !logfile ) return;
    //uint8_t i;
    fprintf(logfile, "# iter: %3u p = % 15.8e % 15.8e % 15.8e "
           " |f(x)| = %g "
           " |f(x + dx)| = %g\n",  /* TODO that's not a |f(x + dx)| */
           (unsigned) iter,
           gsl_vector_get( s->x, 0 ),
           gsl_vector_get( s->x, 1 ),
           gsl_vector_get( s->x, 2 ),
           gsl_blas_dnrm2( s->f ),
           gsl_blas_dnrm2( s->dx )
        );
    /*fprintf(logfile, " f = {\n");
    for( i = 0; i < 32; ++i ) {
        fprintf( logfile, "%e ", gsl_vector_get( s->f, i ) );
    }
    fprintf(logfile, "}\n");*/
    fprintf(logfile, "# dx = {%e %e %e}\n",
            gsl_vector_get( s->dx, 0 ),
            gsl_vector_get( s->dx, 1 ),
            gsl_vector_get( s->dx, 2 )
        );
}


int
fit_SADC_samples(
        const struct SADCWF_FittingInput * srcData,
        SADCWF_FittingFunction fitting_function,
        union SADCWF_FittingFunctionParameters * uFitPars,
        FILE * logfile
    ) {
    gsl_multifit_fdfsolver *s;
    int status;
    unsigned int iter = 0, i;
    uint8_t p;
    gsl_matrix * covar;
    gsl_multifit_function_fdf f;
    //double p_init[3] = { 1e3, 10, 3 };
    gsl_vector_view P;

    if( moyal == fitting_function ) {
        p = 3;
        covar = gsl_matrix_alloc( p, p );
        P = gsl_vector_view_array( uFitPars->moyalPars.p, p );
        /* covar mx computation pars */
        f.f = &moyal_f;
        f.df = &moyal_df;
        f.fdf = &moyal_fdf;
        /* dimensions, etc. */
        f.n = srcData->n;
        f.p = p;
        f.params = (/*const*/ struct SADCWF_FittingInput *) srcData;
    } else {
        return -1;
    }

    s = gsl_multifit_fdfsolver_alloc( gsl_multifit_fdfsolver_lmsder, f.n, p );
    gsl_multifit_fdfsolver_set( s, &f, &P.vector );
    print_state( logfile, iter, s ); 
    do {
        iter++;
        status = gsl_multifit_fdfsolver_iterate( s );
        if( logfile ) {
            fprintf(logfile, "# multifit status = %s\n", gsl_strerror (status));
        }
        print_state(logfile, iter, s);
        /*fprintf(logfile, "XXX: %e, %e\n", s->x, s->dx );*/
        if(status)
            break;
        status = gsl_multifit_test_delta( s->dx, s->x, 1e-4, 1e-4 );
        if( logfile ) {
            fprintf(logfile, "# multifit test delta status = %s\n", gsl_strerror(status));
        }
    } while(status == GSL_CONTINUE && iter < 2000);

    gsl_multifit_covar( s->J, 0.0, covar );

    if( logfile ) {
        fprintf(logfile, "# *** Fitting exit.\n");
    }

    # define FIT(i) gsl_vector_get(s->x, i)
    # define ERR(i) sqrt(gsl_matrix_get(covar,i,i)) 
    {
        double chi = gsl_blas_dnrm2(s->f);
        double dof = f.n - p;
        double c = GSL_MAX_DBL( 1, chi/sqrt(dof) ); 
        
        if( logfile ) {
            fprintf( logfile, "# chisq/dof = %g\n",  pow(chi,2.0)/dof );
            fprintf( logfile, "# p1 = %.5f +/- %.5f\n", FIT(0), c*ERR(0) );
            fprintf( logfile, "# p2 = %.5f +/- %.5f\n", FIT(1), c*ERR(1) );
            fprintf( logfile, "# p3 = %.5f +/- %.5f\n", FIT(2), c*ERR(2) );
        }
        uFitPars->moyalPars.p[0] = FIT(0);
        uFitPars->moyalPars.p[1] = FIT(1);
        uFitPars->moyalPars.p[2] = FIT(2);
        uFitPars->moyalPars.err[0] = c*ERR(0);
        uFitPars->moyalPars.err[1] = c*ERR(1);
        uFitPars->moyalPars.err[2] = c*ERR(2);
        uFitPars->moyalPars.chisq_dof = pow(chi, 2.0)/dof;
    }

    if( logfile ) {  /* Testing printout for obtained parameters {{{ */
        gsl_vector * tmpF = gsl_vector_alloc(f.n)
                   ;
        gsl_matrix * derivCache = gsl_matrix_alloc( f.n, p );
        moyal_f( s->x, (/*const*/ void *) srcData, tmpF );
        moyal_df( s->x, (/*const*/ void *) srcData, derivCache );
        fprintf( logfile, "# i,  Y_i, |Y_i - y_i|, dY/dp1, dY/dp2, dY/dp3, Y(i)\n" );
        for( i = 0; i < srcData->n; ++i ) {
            fprintf( logfile, "%d %f %f %f %f %f %f\n",
                    (int) i,
                    srcData->samples[i],
                    gsl_vector_get(tmpF, i),
                    gsl_matrix_get( derivCache, i, 0 ),
                    gsl_matrix_get( derivCache, i, 1 ),
                    gsl_matrix_get( derivCache, i, 2 ),
                    fitting_function( uFitPars, i )
                );
        }
        gsl_vector_free( tmpF );
    }

    gsl_multifit_fdfsolver_free (s);
    gsl_matrix_free (covar);

    return 0;
}

# endif  /* ANALYSIS_ROUTINES */

