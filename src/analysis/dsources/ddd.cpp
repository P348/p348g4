/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# ifdef RPC_PROTOCOLS
# ifdef EXP_RAW_DATA_READING

# include <goo_exception.hpp>
# include "app/analysis.hpp"
# include "p348g4_uevent.hpp"
# include "p348g4_utils.h"

# include <DaqEventsManager.h>
# include <DaqEvent.h>
# include <Chip.h>
# include <ChipAPV.h>   // MM, GEM
# include <ChipSADC.h>  // Calorimeters
# include <ChipF1.h>    // Straw

# include "analysis/apvConstraints.h"

namespace p348 {
namespace dsources {
namespace DDD {

typedef void* Event;

class File : public AnalysisApplication::iEventSequence,
             public AbstractApplication::ASCII_Entry {
public:
    typedef AnalysisApplication::iEventSequence Parent;
    typedef typename AnalysisApplication::Event Event;
private:
    CS::DaqEventsManager * _manager;
    bool _lastEvReadingWasGood,
         _treatNonPhysical,
         _doDetectorNameValidation,
         _detectorsTableValidated,
         _ignoreChip_SADC,
         _ignoreChip_APV;
    PBarParameters * _pbParameters;  ///< set to nullptr when unused
    std::string _cFileName;
    std::unordered_map<unsigned, std::string> _detIDMap;
    std::unordered_map<unsigned, EnumScope::MajorDetectorsCode> _detTranslationDict;
    size_t _maxEventsNumber,
           _eventsRead,
           _eventsNonPhysicalOmitted,
           _eventsNonPhysicalProcessed,
           _eventsDecodingFailureOmitted,
           _digitsProcessed,
           _digitsIgnored,
           _digitsDataTranslationUnimplemented
           ;
protected:
    virtual bool _make_univ_event( Event *& );
    virtual bool _process_non_physical( Event *&);
    virtual bool _V_is_good() override;
    virtual void _V_next_event( Event *& ) override;
    virtual Event * _V_initialize_reading() override;
    virtual void _V_finalize_reading() override;
    virtual void _V_print_brief_summary( std::ostream & ) const override;
    virtual void _update_stat();
    virtual void _set_up_detector_mapping();
public:
    File( const std::string & filename,
          const std::string & mapFileName,
          size_t maxEvents,
          const std::vector<std::string> & omitChips,
          bool enableProgressbar=false,
          bool nonPhys=false,
          bool validateDetectorNames=true);
    virtual ~File();

    CS::DaqEvent & current_event();
    const CS::DaqEvent & current_event() const;

    CS::DaqEventsManager & get_ddd_manager();
    const CS::DaqEventsManager & get_ddd_manager() const;
};


// IMPLEMENTATION

File::File( const std::string & filename,
            const std::string & mapFileName,
            size_t maxEvents,
            const std::vector<std::string> & omitChips,
            bool enableProgressbar,
            bool nonPhys,
            bool validateDetectorNames ) :
        ASCII_Entry( &goo::app<AbstractApplication>(), 4 ),
        _manager(nullptr),
        _lastEvReadingWasGood(false),
        _treatNonPhysical(nonPhys),
        _doDetectorNameValidation( validateDetectorNames ),
        _detectorsTableValidated(false),
        _ignoreChip_SADC(false),
        _ignoreChip_APV(false),
        _pbParameters(nullptr),
        _cFileName(filename),
        _maxEventsNumber(maxEvents),
        _eventsRead(0),
        _eventsNonPhysicalOmitted(0),
        _eventsNonPhysicalProcessed(0),
        _eventsDecodingFailureOmitted(0),
        _digitsProcessed(0),
        _digitsIgnored(0),
        _digitsDataTranslationUnimplemented(0) {
    _manager = new CS::DaqEventsManager(
            /* name ............. */"p348a-reader",
            /* signal handler ... */ false );
    get_ddd_manager().SetMapsDir( mapFileName );
    // progressbar stuff
    if( enableProgressbar && _maxEventsNumber ) {
        _pbParameters = new PBarParameters;
        bzero( _pbParameters, sizeof(PBarParameters) );
        _pbParameters->mtrestrict = 250;
        _pbParameters->length = 80;
        _pbParameters->full = _maxEventsNumber;
    }
    for( auto it  = omitChips.begin(); omitChips.end() != it; ++it ) {
        if( "SADC" == *it ) {
            _ignoreChip_SADC = true;
            p348g4_log2( "Note: " ESC_CLRYELLOW "SADC" ESC_CLRCLEAR " chip information will be ignored.\n" );
        } else if( "APV" == *it ) {
            _ignoreChip_APV = true;
            p348g4_log2( "Note: " ESC_CLRYELLOW "APV" ESC_CLRCLEAR " chip information will be ignored.\n" );
        } else {
            p348g4_logw( "Unknown chip name to be omitted: %s.\n", it->c_str() );
        }
    }
}

void
File::_set_up_detector_mapping() {
    const CS::Chip::Maps & maps = get_ddd_manager().GetMaps();
    assert( maps.size() );
    p348g4_log2( "Run detector ID mapping corrections for %zu entries...\n", maps.size() );
    for( auto it = maps.begin(); maps.end() != it; ++it ) {
        const CS::Chip::Digit * d = it->second;
        // This method corrects the detector numbering table
        // according to one that read out from mapping files.
        resolve_major_detector_number_mapping( d->GetDetID().GetName().c_str(),
                                               d->GetDetID().GetNumber() );
    }
    _detectorsTableValidated = true;
}

bool
File::_process_non_physical( Event *& eventPtr ) {
    CS::DaqEvent & event = current_event();
    ::p348::events::ExperimentalEvent & expEve = *(eventPtr->mutable_experimental());
    expEve.set_is_physical( false );
    switch( event.GetType() ) {
        # define set_nphys_e_type( typeBrief )                                  \
        expEve.set_nonphyseventtype(                                            \
            ::p348::events::ExperimentalEvent_NonPhysEventType_ ## typeBrief    \
        );
        case CS::DaqEvent::START_OF_RUN : {
            //printf( "StartOfRun" );
            set_nphys_e_type( RunStart );
        } break;
        case CS::DaqEvent::END_OF_RUN : {
            //printf( "EndOfRun" ); 
            set_nphys_e_type( RunEnd );
        } break;
        case CS::DaqEvent::START_OF_RUN_FILES : {
            //printf( "StartOfRunFiles" );
            set_nphys_e_type( RunFilesStart );
        } break;
        case CS::DaqEvent::END_OF_RUN_FILES : {
            //printf( "EndOfRunFiles" );
            set_nphys_e_type( RunFilesEnd );
        } break;
        case CS::DaqEvent::START_OF_BURST : {
            //printf( "StartOfBurst" );
            set_nphys_e_type( BurstStart );
        } break;
        case CS::DaqEvent::END_OF_BURST : {
            //printf( "EndOfBurst" );
            set_nphys_e_type( BurstEnd );
        } break;
        case CS::DaqEvent::CALIBRATION_EVENT : {
            //printf( "CalibrationEvent" );
            set_nphys_e_type( Calibration );
        } break;
        case CS::DaqEvent::EVENT_FORMAT_ERROR : {
            //printf( "EventFormatError" );
            set_nphys_e_type( FormatError );
        } break;
        # if 0
        // TODO: do we need that? Defined in DATE/commonDefs/event.h
        case START_OF_DATA : {
            printf( "StartOfData" );
        } break;
        case END_OF_DATA : {
            printf( "EndOfData" );
        } break;
        case SYSTEM_SOFTWARE_TRIGGER_EVENT : {
            printf( "SystemSoftwareTriggerEvent" );
        } break
        case DETECTOR_SOFTWARE_TRIGGER_EVENT : {
            printf( "DetectorSoftwareTriggerEvent" );
        } break;
        # endif
        default:
            set_nphys_e_type( unknown );
            p348g4_loge( "Unknown DDD event type code: %d.\n", (int) event.GetType() );
            return false;
    };
    return false;
}

bool
File::_make_univ_event( Event *& eventPtr ) {
    CS::DaqEvent & event = current_event();
    // Turn uni-event pointed by eventPtr to experimental one by obtaining
    // pointer to its experimental instance (oneof GPB qualifier):
    ::p348::events::ExperimentalEvent & expEve = *(eventPtr->mutable_experimental());
    if( CS::DaqEvent::PHYSICS_EVENT != event.GetType() ) {
        ++_eventsNonPhysicalProcessed;
        if( !_treatNonPhysical ) {
            ++_eventsNonPhysicalOmitted;
            return false;  // select only events corresponds to real physics
        } else {
            return _process_non_physical( eventPtr );
        }
    } else {
        expEve.set_is_physical( true );
    }
    CS::DaqEventsManager & m = get_ddd_manager();
    // Should be called before any real data acquizition
    // of event (however, may be after determining its type).
    if( !m.DecodeEvent() ) {
        ++_eventsDecodingFailureOmitted;
        return false;  // event decoding failure
    }
    //
    for( auto it  = m.GetEventDigits().begin();
              it != m.GetEventDigits().end();
              ++it, ++_digitsProcessed ) {
        const CS::Chip::Digit * d = it->second;
        auto dddCode = (EnumScope::DDD_DetectorsCode) d->GetDetID().GetNumber();
        auto detectorMjID = det_code_ddd_to_p348( dddCode );
        if( !detectorMjID ) {
            // unresolved detector.
            ++_digitsDataTranslationUnimplemented;
            continue;
        }
        // Try to found detector id in translation dictionary:
        if( _detIDMap.find( d->GetDetID().GetNumber() ) == _detIDMap.end() ) { // if not found:
            // Obtain and remember name (for further debug):
            _detIDMap[d->GetDetID().GetNumber()] = d->GetDetID().GetName();
            if( _doDetectorNameValidation ) { // if det. table needs to be validated:
                const std::string supposedName( ddd_detector_name_by_code( dddCode ) );
                if( d->GetDetID().GetName() != supposedName ) {
                    emraise( corruption,
                        "For DDD detector code %x expected name is \"%s\" while real is \"%s\" "
                        "Det. table needs correction.",
                        d->GetDetID().GetNumber(),
                        supposedName.c_str(),
                        d->GetDetID().GetName().c_str());
                }
            }
            _detTranslationDict[d->GetDetID().GetNumber()] = detectorMjID;
        }

        if( detector_is_SADC(detectorMjID) ) {  // if SADC feature is on for this detector:
            if( _ignoreChip_SADC ) {
                ++_digitsIgnored;
                continue;
            }
            const CS::ChipSADC::Digit * sadc = static_cast<const CS::ChipSADC::Digit*>(d);
            //                          ^^^^- this is a digit from sampling ADC front-end (calorimeters)
            // following is the description of the data available in the digit
            // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipSADC.h#L150
            UniqueDetectorID did =
                    compose_cell_identifier(detectorMjID, sadc->GetX(), sadc->GetY());
            auto newSADCData = expEve.add_sadc_data();
            if(     _doDetectorNameValidation
                && !detector_is_Subd( detectorMjID )
                && ( sadc->GetX() || sadc->GetY() )) {
                emraise( corruption,
                    "Found x=%d/y=%d for detector %s which is not supposed to have transversal "
                    "segmentatation according to p348g4's internal detector table. Table needs "
                    "correction (internal name: %s, native code 0x%x, mapped mj. code 0x%x).",
                    sadc->GetX(),
                    sadc->GetY(),
                    d->GetDetID().GetName().c_str(),
                    detector_name_by_code( detectorMjID ),
                    d->GetDetID().GetNumber(),
                    det_code_ddd_to_p348( (EnumScope::DDD_DetectorsCode) d->GetDetID().GetNumber() ) );
            }
            const std::vector<CS::uint16>& wave = sadc->GetSamples();
            for( size_t i = 0; i < wave.size(); ++i ) {
                newSADCData->add_samples( wave[i] );
            }
            newSADCData->set_detectorid( did.wholenum );
            assert( newSADCData->IsInitialized() );
        } else if( detector_is_APV(detectorMjID) ) {
            if( _ignoreChip_APV ) {
                ++_digitsIgnored;
                continue;
            }
            const CS::ChipAPV::Digit * apv = static_cast<const CS::ChipAPV::Digit*>(d);
            //                         ^^^- this is a digit from APV front-end (Micromegas, GEMs)
            // following is the description of the data available in the digit
            // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipAPV.h#L50
            // APV is analog multiplexor chip originally developed in CMS experiment.
            // APV readout have 3 samples

            // In the micromegas several strips from entire set are usually
            // unioned into one wire conducting current to APV chip.
            const CS::uint16 wireNo = apv->GetChannel();
            const CS::uint32 * rawQ = apv->GetAmplitude();

            UniqueDetectorID did(0);
            did.byNumber.major = detectorMjID;

            auto newAPVData = expEve.add_apv_data();
            newAPVData->set_detectorid( did.wholenum );
            auto apvRawData = newAPVData->add_rawdata();
            apvRawData->set_wireno( wireNo );
            // APV always produces 3 sample
            for( uint8_t i = 0; i < APV_NSamples; ++i ) {
                apvRawData->add_amplitudesamples( rawQ[i] );
            }
            assert( newAPVData->IsInitialized() );
        } else {
            ++_digitsDataTranslationUnimplemented;
        }
    }
    {  // Filling basic experimental event header
        const std::pair<time_t, uint32_t> time  = event.GetTime();
        const uint32_t spillNo                  = event.GetBurstNumber(),
                       runNo                    = event.GetRunNumber(),
                       evNoInRun                = event.GetEventNumberInRun(),
                       evNoInSpill              = event.GetEventNumberInBurst(),
                       trigger                  = event.GetTrigger()
                       ;
        uint32_t triggerNo, errorCode;
        try {
            triggerNo = event.GetTriggerNumber();
        } catch ( const char * eCStr ) {
            // Apparently "DaqEvent::Header::GetTriggerNumber(): no information.":
            triggerNo = UINT32_MAX;
        }
        try {
            errorCode = event.GetErrorCode();
        } catch ( const char * eCStr ) {
            // Apparently "DaqEvent::Header::GetErrorCode(): no information.":
            errorCode = UINT32_MAX;
        }
        expEve.set_spillno(     spillNo );
        expEve.set_runno(       runNo );
        expEve.set_evnoinrun(   evNoInRun );
        expEve.set_evnoinspill( evNoInSpill );
        expEve.set_triggerno(   triggerNo );
        expEve.set_errorcode(   errorCode );
        expEve.set_trigger(     trigger );
        expEve.set_timestruct( (void *) &(time.first), sizeof(time_t) );
        expEve.set_timenum( time.second );
        _update_stat();
    }
    return true;
}

void
File::_V_print_brief_summary( std::ostream & os ) const {
    os << ESC_CLRGREEN "ddd data source" ESC_CLRCLEAR ":" << std::endl
       << "  events read ................ : " << _eventsRead << " / " << _maxEventsNumber << std::endl
       << "    digits considered ........ : " << _digitsProcessed
            << ", " << ((double)_digitsProcessed/_eventsRead) << " dig-s/ev." << std::endl
       << "    digits unknown/unused .... : " << _digitsIgnored
            << ", " << 100*((double) _digitsIgnored) / _digitsProcessed << "%" << std::endl
       << "    digits tr-n unimplem. .... : " << _digitsDataTranslationUnimplemented
            << ", " << 100*((double) _digitsDataTranslationUnimplemented) / _digitsProcessed << "%" << std::endl
       << "  non-phys. ev-s omitted ..... : " << _eventsNonPhysicalOmitted
            << ", " << 100*((double) _eventsNonPhysicalOmitted)/_eventsRead << "%" << std::endl
       << "  unable to decode ........... : " << _eventsDecodingFailureOmitted
            << ", " << 100*((double) _eventsDecodingFailureOmitted)/_eventsRead << "%" << std::endl
       ;
}

bool
File::_V_is_good() {
    if( _pbParameters ) {
        redraw_pbar(_eventsRead, _pbParameters);
    }
    return _lastEvReadingWasGood && ( _maxEventsNumber != 0 ? _eventsRead < _maxEventsNumber : true );
}

void
File::_V_next_event( Event *& ePtr ) {
    ePtr = &(p348::mixins::PBEventApp::c_event());
    ePtr->Clear();
    do {
        try {
            _lastEvReadingWasGood = get_ddd_manager().ReadEvent();
            current_event();
        } catch ( const char * cstr ) {
            _lastEvReadingWasGood = false;
            p348g4_loge( "Third-party event manager raised a C-string: \"%s\".\n", cstr );
        } catch ( ... ) {
            _lastEvReadingWasGood = false;
            p348g4_loge( "Third-party event manager raised an unrecognized exception.\n" );
        }
        if( !_lastEvReadingWasGood ) {
            return;
        }
        if( !_detectorsTableValidated ) {
            // get mapping structure
            _set_up_detector_mapping();
            if( goo::app<AnalysisApplication>().verbosity() > 1 ) {
                dump_mapping_table( stdout );
                test_detector_table( stdout );
            }
        }
    } while( !_make_univ_event( ePtr ));
    ++_eventsRead;
}

File::Event *
File::_V_initialize_reading() {
    get_ddd_manager().AddDataSource( _cFileName );
    # if 0
    if( goo::app<AnalysisApplication>().verbosity() > 2 ) {
        goo::app<ThreadedAnalysisApplication>().acquire_logstream_mutex(); {
            _manager->Print( goo::app<AnalysisApplication::Parent>().ls() );
        } goo::app<ThreadedAnalysisApplication>().release_logstream_mutex();
    }
    # endif
    Event * eventPtr;
    _V_next_event( eventPtr );
    return eventPtr;
}

void
File::_V_finalize_reading() {
    get_ddd_manager().Clear();
}

CS::DaqEventsManager &
File::get_ddd_manager() {
    return *_manager;
}

const CS::DaqEventsManager &
File::get_ddd_manager() const {
    return *_manager;
}

File::~File() {
    delete _manager;
    if( _pbParameters ) {
        delete _pbParameters;
    }
}

void
File::_update_stat() {
    # if 1
    # if 0
    /* Form:
     * Evs ...... : read (< max) / nonPhys / decodingFailure
     * Digits ... : processed / ignored / unimplemented
     * */
    printf( "\033[1K Evs ...... : %zu, read (< %zu, max), %zu non-phys, %zu dec.fail\n",
            _eventsRead, _maxEventsNumber,
            _eventsNonPhysicalOmitted,
            _eventsDecodingFailureOmitted );
    printf( "\033[1K Digits ... : %zu processed / %zu ignored / %zu unimplem\n",
            _digitsProcessed,
            _digitsIgnored,
            _digitsDataTranslationUnimplemented );
    printf( "\033[2A" );
    # else
    if( can_acquire_display_buffer() ) {
        char ** lines = my_ascii_display_buffer();
        assert( lines[0] && lines[1] && lines[2] && lines[3] && !lines[4] );

        # if 0
        expEve.set_spillno(     spillNo );
        expEve.set_runno(       runNo );
        expEve.set_evnoinrun(   evNoInRun );
        expEve.set_evnoinspill( evNoInSpill );
        expEve.set_triggerno(   triggerNo );
        expEve.set_errorcode(   errorCode );
        expEve.set_trigger(     trigger );
        expEve.set_timestruct( (void *) &(time.first), sizeof(time_t) );
        expEve.set_timenum( time.second );
        # endif

        const ::p348::events::ExperimentalEvent & expEve
            = AnalysisApplication::c_event().experimental();

        time_t timeStruct;
        memcpy( &timeStruct, expEve.timestruct().c_str(), sizeof(time_t) );

        char timeStr[32];
        strftime( timeStr, sizeof(timeStr),
              "%Y-%m-%d %H:%M:%S",
              localtime(&timeStruct) );

        snprintf( lines[0], aux::ASCII_Display::LineLength,
            " Run # ... : %d, spill # ... %d, ev# %d (%d in spill)",
            expEve.runno(), expEve.spillno(), expEve.evnoinrun(),
            expEve.evnoinspill() );
        snprintf( lines[1], aux::ASCII_Display::LineLength,
            " Event time %s : %u", timeStr, expEve.timenum() );
        snprintf( lines[2], aux::ASCII_Display::LineLength,
            " Evs ...... : %zu, read (< %zu, max), %zu non-phys, %zu dec.fail",
            _eventsRead, _maxEventsNumber,
            _eventsNonPhysicalOmitted,
            _eventsDecodingFailureOmitted );
        snprintf( lines[3], aux::ASCII_Display::LineLength,
            " Digits ... : %zu processed / %zu ignored / %zu unimplem",
            _digitsProcessed,
            _digitsIgnored,
            _digitsDataTranslationUnimplemented );
    }
    # endif
    # endif
}

CS::DaqEvent &
File::current_event() {
    return get_ddd_manager().GetEvent();
}

const CS::DaqEvent &
File::current_event() const {
    return get_ddd_manager().GetEvent();
}

p348_DEFINE_CONFIG_ARGUMENTS {
    po::options_description dddFmtCfg( "DaqDataDecoding library data source options" );
    { dddFmtCfg.add_options()
        ("ddd.map-dir",
            po::value<std::string>(),
            "The purpose of map files is to set correspondance between input channels of"
            "front-end electronics cards and experiment detectors channel"
            "(See DaqDataDecoding library docs for further explaination).")
        ("ddd.progressbar",
            po::value<bool>()->default_value(false),
            "Displays simple ASCII progressbar on stdout.")
        ("ddd.test-det-table",
            po::value<bool>()->default_value(true),
            "Tests internal detector table. Only useful for debug development.")
        ("ddd.enable-non-phys",
            po::value<bool>()->default_value(true),
            "Do show brief statistics information as ASCII-output." )
        ("ddd.omit-chip",
            po::value<std::vector<std::string> >()->multitoken()->zero_tokens(),
            "Chips to be excluded from further treatment (ignored).")
        //("ddd.list-chips",  // TODO
        //    po::value<std::vector<std::string>>(),
        //    "Prints list of supported chips/digits.")
        ;
    }
    return dddFmtCfg;
}

p348_DEFINE_DATA_SOURCE_FMT_CONSTRUCTOR( File ) {
    const std::vector<std::string> dummy;
    
    return new File(
        goo::app<AnalysisApplication>().cfg_option<std::string>("input-file"),
        goo::app<AnalysisApplication>().cfg_option<std::string>("ddd.map-dir"),
        goo::app<AnalysisApplication>().cfg_option<size_t>("max-events-to-read"),
        ( goo::app<AnalysisApplication>().co().count( "ddd.omit-chip" ) ?
          goo::app<AnalysisApplication>().cfg_option<std::vector<std::string> >("ddd.omit-chip") : dummy ),
        goo::app<AnalysisApplication>().cfg_option<bool>("ddd.progressbar"),
        goo::app<AnalysisApplication>().cfg_option<bool>("ddd.enable-non-phys"),
        goo::app<AnalysisApplication>().cfg_option<bool>("ddd.test-det-table")
    );
}
p348_REGISTER_DATA_SOURCE_FMT_CONSTRUCTOR( File, "ddd", "DaqDataDecoding library raw events." )

}  // namespace DDD
}  // namespace dsources
}  // namespace p348

# endif  // RPC_PROTOCOLS
# endif  // EXP_RAW_DATA_READING

