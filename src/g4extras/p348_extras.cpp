/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL

# include "app/abstract.hpp"
# include "g4extras/p348_extras.hpp"

# include <Geant4/G4UIdirectory.hh>
# include <Geant4/G4UIcmdWithoutParameter.hh>

namespace p348 {
namespace extras {

ExtrasMessenger::ExtrasMessenger() {
    _extrasDirectory = new G4UIdirectory("/extras/");
    _extrasDirectory->SetGuidance( "P348 related extra run-time control commands." );

    _renewStylesCmd = new G4UIcmdWithoutParameter("/extras/renew_styling", this);
    _renewStylesCmd->SetGuidance("Re-applies styling for visible touchables.");
    // ...
}

ExtrasMessenger::~ExtrasMessenger() {
    delete _extrasDirectory;
    delete _renewStylesCmd;
    // ...
}

void ExtrasMessenger::SetNewValue(
        G4UIcommand * command,
        G4String /*newValues*/ ) {  // TODO
    if( _renewStylesCmd == command ) {
        apply_styles_selector(
                goo::app<AbstractApplication>().cfg_option<std::string>("gdml.setup")
            );
    }
# if 0
  if( command==listCmd )
  { particleTable->dumpTable(); }
  else if( command==particleCmd )
  {
    G4ParticleDefinition* pd = particleTable->findParticle(newValues);
    if(pd != NULL)
    { fParticleGun->SetParticleDefinition( pd ); }
  }
  else if( command==directionCmd )
  { fParticleGun->SetParticleMomentumDirection(directionCmd->
     GetNew3VectorValue(newValues)); }
  else if( command==energyCmd )
  { fParticleGun->SetParticleEnergy(energyCmd->
     GetNewDoubleValue(newValues)); }
  else if( command==positionCmd )
  { fParticleGun->SetParticlePosition(
     directionCmd->GetNew3VectorValue(newValues)); }
  else if( command==timeCmd )
  { fParticleGun->SetParticleTime(timeCmd->
     GetNewDoubleValue(newValues)); }
# endif
}

G4String
ExtrasMessenger::GetCurrentValue(G4UIcommand * /*command*/) {  // TODO
# if 0
  G4String cv;

  if( command==directionCmd )
  { cv = directionCmd->ConvertToString(
     fParticleGun->GetParticleMomentumDirection()); }
  else if( command==energyCmd )
  { cv = energyCmd->ConvertToString(
     fParticleGun->GetParticleEnergy(),"GeV"); }
  else if( command==positionCmd )
  { cv = positionCmd->ConvertToString(
     fParticleGun->GetParticlePosition(),"cm"); }
  else if( command==timeCmd )
  { cv = timeCmd->ConvertToString(
     fParticleGun->GetParticleTime(),"ns"); }
  else if( command==particleCmd )
  { // update candidate list
    G4String candidateList;
    G4int nPtcl = particleTable->entries();
    for(G4int i=0;i<nPtcl;i++)
    {
      candidateList += particleTable->GetParticleName(i);
      candidateList += " ";
    }
    particleCmd->SetCandidates(candidateList);
  }
  return cv;
# endif
    return "";
}

}  // namespace extras
}  // namespace p348

# endif

