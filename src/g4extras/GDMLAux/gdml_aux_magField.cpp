/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL

# include <goo_ansi_escseq.h>

# if G4VERSION_NUMBER > 999
    # include <Geant4/G4AutoDelete.hh>
# endif
# include <G4FieldManager.hh>
# include <G4Field.hh>
# include <G4MagneticField.hh>
# include <Geant4/G4LogicalVolume.hh>

# include "g4extras/FieldDict.hpp"
# include "g4extras/auxInfoProcessor.hpp"
# include "g4extras/p348_extras.hpp"
# include "app/app.h"

# include <regex>

static const std::string
    srxMagValueComponent = "-?([[:digit:]]+)(\\.([[:digit:]]+)?)?",
    srxMagValue          = srxMagValueComponent + "/" + 
                           srxMagValueComponent + "/" +
                           srxMagValueComponent, 
    srxMagType           = "[[:alnum:]]+",                   
    srxMagWhole          = srxMagType + ":" + srxMagValue;

static const std::regex
    rxMagValueComponent   ( srxMagValueComponent ),
    rxMagValue            ( srxMagValue ),
    rxMagType             ( srxMagType  ),
    rxMagWhole            ( srxMagWhole )
    ;
GDML_AUXINFO_PROCESSOR_BGN( magField, volPtr, strVal)
{
    std::string strToProcess = strVal,
                magFieldType,
                magValue;
    G4ThreeVector fieldValue; 
    if (std::regex_match( strToProcess, rxMagWhole ))
        p348g4_log2("MagValue -- OK.\n");  // TODO log, errors etc.
    else p348g4_logw("Malformed argument <magField>.\n");

    std::smatch sm;

    std::regex_search( strToProcess, sm, rxMagType);  // Search for Field name
    magFieldType=sm[0];
    //std::cout << "magFieldType " << magFieldType << std::endl;

    std::regex_search( strToProcess, sm, rxMagValue);  // Search for magnetic value
    magValue=sm[0];
    //std::cout << "magValue " << magValue << std::endl;
    for (unsigned i=0; i<3; i++) {
        std::regex_search( magValue, sm, rxMagValueComponent);
        //strToProcess = sm[0];
        fieldValue[i]=std::stod(sm[0]);       
        //std::cout << "Field value " << i << " (" << fieldValue[i] << ") " << std::endl;
        magValue=magValue.substr(magValue.find("/") + 1);
        //std::cout << "magValue now " << magValue << std::endl;
    }

# if 1

    G4Field * fieldPtr = p348::FieldDictionary::self()[magFieldType]( fieldValue );
    //p348::MagneticField * MagField = new p348::MagneticField(fieldValue);
    //MagField->field_value(fieldValue);
    G4FieldManager * FieldMan = new G4FieldManager();
    
    FieldMan->SetDetectorField(fieldPtr);  
    if (magFieldType=="MagneticField") {
        G4MagneticField * magFieldPtr = dynamic_cast<G4MagneticField*>(fieldPtr);    
        FieldMan->CreateChordFinder(magFieldPtr);
    }

    G4bool ToAllDaughters = true;

    volPtr->SetFieldManager(FieldMan, ToAllDaughters);
    if ( volPtr->GetFieldManager() ) {
        p348g4_log1( ESC_CLRGREEN "TODO magField" ESC_CLRCLEAR " with magnetic field " \
                ESC_CLRBOLD "TODO value of magField" ESC_CLRCLEAR " has been associated with \"" \
                ESC_CLRBOLD "%s" ESC_CLRCLEAR "\" logical volume.\n",
                //strVal,
                //fieldValue,
                volPtr->GetName().c_str()
                );
    }
    else p348g4_logw("No FieldManager was associated with" \
            ESC_CLRBOLD "%s" ESC_CLRCLEAR "volume.\n",
            volPtr->GetName().c_str()
            ); 
    
    # if G4VERSION_NUMBER > 999
    G4AutoDelete::Register(MagField);
    G4AutoDelete::Register(FieldMan);
    # endif
   
# endif
} GDML_AUXINFO_PROCESSOR_END( magField )


# endif  // GEANT4_MC_MODEL
