/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL

# include <regex>

# include <Geant4/G4LogicalVolume.hh>
# include <Geant4/G4SDManager.hh>

# include "g4extras/SensDetDict.hpp"
# include "g4extras/auxInfoProcessor.hpp"
# include "g4extras/p348_extras.hpp"

/**@file gdml_aux_sensDet.cpp
 * @brief A GDML aux-info tag processor for "sensDet" type.
 *
 * Assigns logical volumes to certain Geant4 sensitive detector instance.
 *
 * Basically, value should consist of two parts separated with column ':' sign.
 * E.g.:
 *  ECAL_cell:/p348det/ecal
 * Will refer to SensitiveDetector subclass named 'ECAL_cell' and create an instance
 * named '/p348det/ecal'.
 *
 * TODO: there are functions for substitution based on C++11 regex, that currently unused,
 * though.
 */

static const std::string
    srxSDClass  = "((?:[[:alnum:]_])+)",
    srxVarSubst = "(\\$\\{(?:[[:alnum:]_]+)\\})",
    srxSDName   = "(/(?:[[:alnum:]_/]|" + srxVarSubst + ")+)",
    srxSDID     = "^" + srxSDClass + ":" + srxSDName + "$"
    ;

static const std::regex
    rxSDClass   ( srxSDClass ),
    rxVarSubst  ( srxVarSubst ),
    rxSDName    ( srxSDName ),
    rxSDID      ( srxSDID )
    ;

std::string
substitute_variables( const std::string & orig, std::unordered_map<std::string, std::string> varDict ) {
    std::string res = orig;
    for( auto it = varDict.begin(); varDict.end() != it; ++it ) {
        res = std::regex_replace( res, std::regex("\\$\\{" + it->first + "\\}"), it->second );
    }
    return res;
}


//
// Aux tag processor
//

GDML_AUXINFO_PROCESSOR_BGN( sensDet, volPtr, strVal ) {
    std::unordered_map<std::string, std::string> variables; {
            // TODO: this variables dictionary can be useful further:
            // ... variables["foo"]     = "__HereWasAFoo__";
            variables["PROJ"] = STRINGIFY_MACRO_ARG( PACKAGE );
        };
    // Obtain SDMananger sinleton ptr
    G4SDManager * sdm = ::G4SDManager::GetSDMpointer();

    std::string sensitiveDetectorClass,
                sensitiveDetectorName
                ;

    { // Parse sensitive detector declaration.
        std::string toProcess = strVal;
        bool needSubst = false;
        do {
            if( needSubst ) {
                toProcess = substitute_variables( toProcess, variables );
                needSubst = false;
            }
            std::smatch sm;
            if( std::regex_match( toProcess, sm, rxSDID ) ) {
                for( size_t nPiece = 0;
                     nPiece < sm.size();
                     ++nPiece ) {
                    std::ssub_match subMatch = sm[nPiece];
                    std::string piece = subMatch.str();
                    if(piece.empty()) continue;

                    if( std::regex_match( piece, rxSDID ) ) {
                        //std::cout << "Entire matching string: " << piece << std::endl;
                        continue;
                    } else if( std::regex_match( piece, rxVarSubst ) ) {
                        //std::cout << "            - Variable: " << piece << std::endl;
                        needSubst = true;
                        //std::cout << "== BREAK -- RENEW ==" << std::endl;
                    } else if( std::regex_match( piece, rxSDName ) ) {
                        //std::cout << "       - Instance name: " << piece << std::endl;
                        sensitiveDetectorName = piece;
                    } else if( std::regex_match( piece, rxSDClass ) ) {
                        sensitiveDetectorClass = piece;
                    } else {
                        emraise( badState, "Unexpected \"%s\" token in \"%s\" description.",
                                 piece.c_str(), strVal.c_str()
                                );
                    }
                }
            } else {
                emraise( malformedArguments,
                         "Wrong sensitive detector description: \"%s\".\n"
                         "\tFor detailed information, please use flag --g4.sensitiveDetectorsList",
                         strVal.c_str()
                       );
            }
        } while( needSubst );
    }

    G4VSensitiveDetector * detPtr = sdm->FindSensitiveDetector( sensitiveDetectorName );

    if( !detPtr ) {
        // Instantiate one as it still not exists:
        try {
            detPtr = 
                p348::SDDictionary::self()[ sensitiveDetectorClass ]( sensitiveDetectorName );
        } catch( goo::Exception & e ) {
            if( goo::Exception::noSuchKey == e.code() ) { // todo: make the way to strict checking
                p348g4_logw(
                "Couldn't find a \"%s\" sensitive detector constructor. Skipping detector association.\n",
                sensitiveDetectorClass.c_str() );
                detPtr = nullptr;
            } else {
                throw;
            }
        }
        if( detPtr ) {
            sdm->AddNewDetector( detPtr );
            p348g4_log1( "\"" ESC_CLRGREEN "%s" ESC_CLRCLEAR \
                      "\" instance of \"" ESC_CLRCYAN "%s" ESC_CLRCLEAR \
                      "\" sensitive detector class created.\n",
                    sensitiveDetectorName.c_str(),
                    sensitiveDetectorClass.c_str() );
        }
    }
    if( detPtr ) {
        p348g4_log2( "\"" ESC_CLRGREEN "%s" ESC_CLRCLEAR \
              "\" instance of \"" ESC_CLRCYAN "%s" ESC_CLRCLEAR \
              "\" sensitive detector class bound to volume \"" ESC_CLRBOLD "%s" ESC_CLRCLEAR "\".\n",
                sensitiveDetectorName.c_str(),
                sensitiveDetectorClass.c_str(),
                volPtr->GetName().c_str()
                );
        volPtr->SetSensitiveDetector( detPtr );
    }
} GDML_AUXINFO_PROCESSOR_END( sensDet )

# endif  // GEANT4_MC_MODEL

