/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "g4extras/physics/APhysics.hpp"

# ifdef G4_APRIME_G4_PROCESS

# include <g4extras/PhysList.hpp>

# include <Geant4/G4VProcess.hh>
# include <Geant4/G4ProcessVector.hh>
# include <Geant4/G4ProcessManager.hh>
# include <Geant4/G4HadronicProcess.hh>
# include <Geant4/G4Version.hh>
# include <Geant4/G4StepLimiter.hh>

# include <TRandom3.h>  // XXX

# include "g4extras/PhysList.hpp"
# include "g4extras/physics/AParticle.hpp"
# include "g4extras/physics/AMixinProcess.hpp"
# include "evGen/aprimeCSCache.hpp"
# include "p348g4_utils.hpp"

namespace p348 {
template<> ::p348g4::APrimePhysics *
ModularPhysicsList::construct_physics<::p348g4::APrimePhysics>() {
    p348::APrimeCSCaches::APrimeMass =
            goo::app<p348::AbstractApplication>().cfg_option<double>("aprimeEvGen.massA_GeV");
    std::set<double> energies = p348::aux::parse_aprime_tabulation_series(
                    goo::app<p348::AbstractApplication>().cfg_option<std::string>("aprimeEvGen.energyTabulationFactors"),
                    p348::APrimeCSCaches::APrimeMass,
                    goo::app<p348::AbstractApplication>().cfg_option<double>("aprimeEvGen.EBeam_GeV") );
    p348::APrimeCSCaches::MixingConstant = goo::app<p348::AbstractApplication>().cfg_option<double>("aprimeEvGen.mixingFactor");
    return new ::p348g4::APrimePhysics(
                ModularPhysicsList::physicsVerbosity,
                p348::APrimeCSCaches::APrimeMass,
                p348::APrimeCSCaches::MixingConstant,
                new TRandom3(), // TODO: unified random generator: goo::app<p348::AbstractApplication>().random_generator<TRandom*>()
                energies,
                goo::app<p348::AbstractApplication>().cfg_option<int>("extraPhysics.physicsAe.matMinAWWApprox"), // matMinAWWApprox
                goo::app<p348::AbstractApplication>().cfg_option<bool>("extraPhysics.physicsAe.considerDecay") // enableAPrimeDecay
            );
}
}

namespace p348g4 {

/**@brief Default ctr to physics module.
 *
 * Initializes tabulation range for caching container.
 * 
 * @param verbosity level of physics module (0-3).
 * @param aprimeMass assumed mass of A' particle (TODO: why it is here?).
 * @param aprimeMixing assumed mixing parameter within WW approximation.
 * @param rndGenerator pointer to random number generator.
 * @param energies a set of energies for which particular generators will be created.
 * @param matMinAWWApprox minimal atomic number for generator to be produced.
 * @param enableAPrimeDecay enables A' decay physics.
 */
APrimePhysics::APrimePhysics( int verbosity,
                              double aprimeMass,
                              double aprimeMixing,
                              TRandom * rndGenerator,
                              const std::set<double> & energies,
                              uint8_t matMinAWWApprox,
                              bool enableAPrimeDecay ) :
        G4VPhysicsConstructor( "APrimePhysics" ),
        _verbosityLevel( verbosity ),
        _aPrimeMass( aprimeMass ),
        _mixingConstant( aprimeMixing ),
        _rndGen( rndGenerator ),
        _matMinAWWApprox( matMinAWWApprox ),
        _decayEnabled( enableAPrimeDecay ) {
    p348::APrimeCSCaches::self().projectile_energy_tabulation(energies);
}

APrimePhysics::~APrimePhysics() {
    (void)(_verbosityLevel);  // XXX: suppress `unused' warning,
                              // keep field for further usage.
}

void
APrimePhysics::ConstructParticle() {
    APrime::Definition();
    G4Electron::Electron();
    //G4Positron::Positron();
    //G4MuonPlus::MuonPlus();
    //G4MuonMinus::MuonMinus();
    //G4Proton::Proton();
}

void
APrimePhysics::ConstructProcess() {
    //G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
    # if G4VERSION_NUMBER > 999
    for( aParticleIterator->reset(); (*aParticleIterator)() ; )
    # else
    for( theParticleIterator->reset(); (*theParticleIterator)() ; )
    # endif
    {
        G4ParticleDefinition & particle =
        # if G4VERSION_NUMBER > 999
        *(aParticleIterator->value());
        # else
        *(theParticleIterator->value());
        # endif

        if( !particle.GetPDGCharge() ) { continue; }  // bypass uncharged particles

        // todo: currently, only "e-" is supported:
        if( "e-" == particle.GetParticleName() ) {
            auto proc = new AMixingProcess( _rndGen, a_prime_mass(), mixing_constant() );
            proc->minimal_material_A( _matMinAWWApprox );
            proc->minimal_projectile_energy(
                    *p348::APrimeCSCaches::self().projectile_energy_tabulation().begin() );
            p348g4_log3( "Minimal considered energy for A' production is set to %e GeV.\n",
                            proc->minimal_projectile_energy() );
            p348g4_log3( "Minimal considered atomic number for A' production is set to %d.\n",
                            (int) proc->minimal_material_A() );
            G4ProcessManager & pmanager = *particle.GetProcessManager();
            pmanager.AddDiscreteProcess( proc );
            //std::cout << "XXX Process type ...... : " << proc->GetProcessType() << std::endl
            //          << "XXX PRocess subtype ... : " << proc->GetProcessSubType() << std::endl
            //          ;
            //ph->SetVerboseLevel(3); // unsupported usually
            //ph->RegisterProcess( proc, &particle );
            //todo? Do we need this:
            //ph->RegisterProcess(new G4StepLimiter,  &particle );
        }

        # if 0
        //auto * processPtr = new G4ElectronNuclearProcess(); ( TODO: obtain already associated processes )
        G4ProcessManager & procManager = *particle.GetProcessManager();
        G4ProcessVector & procVector = *procManager.GetProcessList();
        _ss << particle.GetParticleName() << ":" << std::endl;
        for( int nProcess = 0; nProcess < procVector.size(); ++nProcess ) {
            G4VProcess & process = *procVector[nProcess];
            _ss << "   ... " << process.GetProcessName();
            G4HadronicProcess * hProcPtr = dynamic_cast<G4HadronicProcess *>( &process );
            if( hProcPtr ) {
                _ss << " (" ESC_CLRBOLD "hadronic process descendant" ESC_CLRCLEAR ")";
                //hProcPtr->RegisterMe( new AnyChargedAprimeMixinModel() );  // TODO !!!
                //procManager.AddDiscreteProcess( hProcPtr ); // bad approach
            }
            _ss << std::endl;
        }
        //processPtr->RegisterMe( modelPtr );
        //ph->RegisterProcess( processPtr, &particle );
        // TODO: obtain
        # endif

        # if 0
        G4String particleName = particle->GetParticleName();
        G4VDiscreteProcess * fSRInMat = ( _inMaterials ?
                dynamic_cast<G4VDiscreteProcess *>(new G4SynchrotronRadiationInMat()) :
                dynamic_cast<G4VDiscreteProcess *>(new G4SynchrotronRadiation()) );
        # if G4VERSION_NUMBER > 999
        G4AutoDelete::Register(fSR);  // introduced since Geant 4.10
        # endif
        if( particleName=="e-" ) {
            ph->RegisterProcess(fSRInMat,           particle );
            ph->RegisterProcess(new G4StepLimiter,  particle );
        } else if( particleName=="e+" || particleName=="mu+"
                                      || particleName=="mu-"
                                      || particleName=="proton" ) {
            // XXX Previous way: procMan->AddProcess(fSR,               -1, -1, 5 );
            ph->RegisterProcess(fSRInMat,           particle );
            ph->RegisterProcess(new G4StepLimiter,  particle );
        } else if (particle->GetPDGCharge() != 0.0 &&
                 !particle->IsShortLived()) {
            ph->RegisterProcess(fSRInMat,           particle );
        }
        # endif
    }
    // TODO
    if( is_decay_enabled() ) {
        _TODO_  // TODO
    }
    p348g4_log2( "Constructed processes in A' physics module.\n" );  // todo: details?
}

REGISTER_PHYSICS_MODULE( APrimePhysics );

}  // namespace p348g4

# endif  // G4_APRIME_G4_PROCESS

