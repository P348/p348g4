/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <stdlib.h>
# include <assert.h>
# include <string.h>
# include <stdio.h>

# include "app/app.h"
# include "p348g4_detector_ids.h"

char *
snprintf_detector_name( char * buffer,
                        size_t bufferSize,
                        union UniqueDetectorID detID ) {
    const char detectorStringFmt[] = "%s",
               detectorStringFmtSbdvd[] = "%s-%dx%d"
               ;
    enum MajorDetectorsCode mjCode = (enum MajorDetectorsCode) detID.byNumber.major;
    if( detector_is_SADC( mjCode ) && detector_is_Subd( mjCode ) ) {
        snprintf( buffer, bufferSize,
                  detectorStringFmtSbdvd,
                  detector_name_by_code( mjCode ),
                  (int) detector_get_x_idx(detID),
                  (int) detector_get_y_idx(detID) );
    } else {
        snprintf( buffer, bufferSize,
                  detectorStringFmt,
                  detector_name_by_code( mjCode ) );
    }
    return buffer;
}


/** Family identifiers are listed in \ref for_all_detector_families
 * x-macro table.*/
DetectorFamilyID
detector_family_num( DetectorMajor major ) {
    return (major >> P38G4_MJ_DETID_OFFSET) & P348G4_DETID_FAMILY_MASK;
}

/** Family identifiers with its names are listed in \ref
 * for_all_detector_families x-macro table.
 * Function uses static `const char **` array.*/
const char *
detector_family_name( DetectorFamilyID fID ) {
    switch( fID ) {
    # define EX_case_dict_entry( name, code, verbName ) case code : { return verbName; }
    for_all_detector_families( EX_case_dict_entry )
    # undef EX_case_dict_entry
    default:
        return "misc";
    };
}

/**@param major a major detector ID number converted to integral value
 * (\ref MajorDetectorsCode).
 * @param xIdx an `x` index of detector cell (if available)
 * @param yIdx an `y` index of detector cell (if available)
 * @return combined unique detector identification number.
 */
DetectorSignature
compose_cell_identifier( DetectorMajor major, uint8_t xIdx, uint8_t yIdx ) {
    assert( xIdx <= 0xf );
    assert( yIdx <= 0xf );
    union UniqueDetectorID res = {{major, 0}};
    res.byNumber.major = major;
    if( detector_is_Subd((enum MajorDetectorsCode) major) ) {
        res.byNumber.minor = xIdx | (yIdx << 4);
    }
    return res.wholenum;
}

uint8_t
detector_get_x_idx( union UniqueDetectorID id ) {
    return id.byNumber.minor & 0xf;
}

uint8_t
detector_get_y_idx( union UniqueDetectorID id ) {
    return ((id.byNumber.minor & 0xf0) >> 4 );
}

uint8_t
get_hcal_cell_y( union UniqueDetectorID fullID ) {
    assert(     d_HCAL0 == fullID.byNumber.major
             || d_HCAL1 == fullID.byNumber.major
             || d_HCAL2 == fullID.byNumber.major
             || d_HCAL3 == fullID.byNumber.major);
    return (0xf0 & fullID.byNumber.minor) >> 4;
}

# if 0
# define implement_flag_checking_function(name, code, description) \
uint8_t detector_is_ ## name( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) { \
    return ((mj >> P38G4_MJ_DETID_OFFSET) & code); }
for_all_detector_features( implement_flag_checking_function )
# undef implement_flag_checking_function
# endif

uint8_t
detector_is_SADC( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_SADC);
}

uint8_t
detector_is_APV( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_APV);
}

uint8_t
detector_is_Subd( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    if( detector_is_SADC(mj) )
        return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_Subd );
    p348g4_logw( "Checking non SADC-detector for transversal segmentation.\n" );
    return 0;
}

uint8_t
detector_is_Joint( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    if( detector_is_APV(mj) )
        return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_Joint );
    p348g4_logw( "Checking non APV-detector for joint wires.\n" );
    return 0;
}

uint8_t
detector_is_XYSep( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_XYSep );
}

uint8_t
detector_is_X( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    if( detector_is_XYSep(mj) )
        return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_X );
    p348g4_logw( "Checking non X/Y-separated planes of detector for X feature.\n" );
    return 0;
}

uint8_t
detector_is_Y( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    if( detector_is_XYSep(mj) )
        return 0 == ((mj >> P38G4_MJ_DETID_OFFSET) & f_X );
    p348g4_logw( "Checking non X/Y-separated planes of detector for Y feature.\n" );
    return 0;
}

uint8_t
detector_subgroup_num( DetectorMajor major ) {
    return major & 0xf; /* TODO!!! */
}

DetectorSignature
detector_complementary_X_id( DetectorSignature uIDs ) {
    union UniqueDetectorID uID;
    uID.wholenum = uIDs;
    if( ! detector_is_XYSep( uID.byNumber.major ) ) {
        p348g4_logw( "Requesting comlementary X/Y pair for detector 0x%x without X/Y separation.\n",
                      uID.wholenum );
        return uID.wholenum;
    }
    if( detector_is_X( uID.byNumber.major ) ) {
        return uID.wholenum;
    }
    union UniqueDetectorID repID = uID;
    repID.byNumber.major |= (f_X << P38G4_MJ_DETID_OFFSET);
    return repID.wholenum;
}

DetectorSignature
detector_complementary_Y_id( DetectorSignature uIDs ) {
    union UniqueDetectorID uID;
    uID.wholenum = uIDs;
    if( ! detector_is_XYSep( uID.byNumber.major ) ) {
        p348g4_logw( "Requesting comlementary X/Y pair for detector 0x%x without X/Y separation.\n",
                      uID.wholenum );
        return uID.wholenum;
    }
    if( detector_is_Y( uID.byNumber.major ) ) {
        return uID.wholenum;
    }
    union UniqueDetectorID repID = uID;
    repID.byNumber.major &= ~(f_X << P38G4_MJ_DETID_OFFSET);
    return repID.wholenum;
}

# if 0
DetectorMajor
detector_major_by_name( const char * tName ) {
    # define return_major_number_if_matches( name, code, dddCode )  \
    if( !strcmp( # name, tName ) ){ return code; } else
    for_all_detectors( return_major_number_if_matches )
    # undef return_major_number_if_matches
    {
        return 0;  /* unknown */
    }
}
# endif

/*
 * Table testing function
 */

static int
test_codes_for_uniqueness( uint8_t cCode, const char * cName ) {
    int
    # define define_static_code_constant( name, code, dddCode ) k_ ## name = code,
    for_all_detectors(define_static_code_constant)
    # undef define_static_code_constant
    dummy = 0;

    (void)(dummy);

    # define check_code( name, code, dddCode ) \
    if( strcmp( # name, cName ) && k_ ## name == cCode ) { return -1; } else
    for_all_detectors( check_code )
    # undef check_code
    {
        return 0;
    }
}

int
test_detector_table( FILE * fle ) {
    if( 4 != sizeof(union UniqueDetectorID) ) {
        return -1;
    }

    /* Test ECAL enumeration */
    for(UByte i = 0; i < 6; ++i) {
        for(UByte j = 0; j < 6; ++j) {
            for( UByte k = 0; k < 3; ++k ) {
                union UniqueDetectorID id;
                if( !k ) {
                    id.wholenum = compose_cell_identifier( d_ECAL0,   i, j );
                } else if( k == 1 ) {
                    id.wholenum = compose_cell_identifier( d_ECAL1,   i, j );
                } else if( k == 2 ) {
                    id.wholenum = compose_cell_identifier( d_ECALSUM, i, j );
                }
                if( detector_get_x_idx(id) != i ) {
                    return -2;
                }
                if( detector_get_y_idx(id) != j ) {
                    return -3;
                }
                assert( detector_is_Subd(id.byNumber.major) );
                assert( detector_is_SADC(id.byNumber.major) );
                //assert( 0x4 == detector_family_num(id.byNumber.major) );
            }
        }
    }

    {  /* Dump detector table to stdout: */
        FILE * os = fle;
        fprintf(os, "Features:\n\ts - detector has transversal (x,y) segmentation;\n"
                    "\tW - detector is served by SADC chip;\n"
                    "\tA - detector is served by APV chip.\n"
                    "\tj - detector is served by APV chip and has joint wires.\n"
                    "\tX - detector is separated X/Y planes and represents X plane.\n"
                    "\tY - detector is separated X/Y planes and represents Y plane.\n"
                    "--------+------+----+---+----------------+--------\n"
                    " name   | code | DDD|Fam.code|   Fam.name|Features\n"
                    "--------+------+----+---+----------------+--------\n");
        /* TODO: move this stuff to string-generating function. */
        # define print_out_detector_entry( name, code, dddCode ) \
        fprintf( os, "%8s|0x%04x|%4d|0x%1x|%16s|%c %c %c %c",        \
            # name, \
            (int) code, \
            dddCode, \
            detector_family_num(code), \
            detector_family_name(detector_family_num(code)), \
            ( detector_is_SADC(code) ? 'W' : '-' ), \
            ( detector_is_APV(code) ? 'A' : '-' ), \
            ( detector_is_SADC(code) ? (( detector_is_Subd(code)  ? 's' : '-' )) :  \
                                        ( detector_is_Joint(code) ? 'j' : '-' )),   \
            ( detector_is_XYSep(code) ? (detector_is_X(code) ? 'X' : 'Y') : '-' )   \
            ); \
        if( detector_is_XYSep(code) ) {                                             \
            if( detector_is_Y( code ) ) { fprintf(os, " Y-complem. to 0x%x",        \
                detector_complementary_X_id( code ) ); }                            \
        } fprintf( os, "\n" );
        for_all_detectors( print_out_detector_entry );
        # undef print_out_detector_entry
    }
    {
        # define check_det_code( name, code, dddCode ) \
            if( test_codes_for_uniqueness( (uint8_t) (code), # name ) ) { return -6; }
        for_all_detectors(check_det_code)
        # undef check_det_code
    }
    return 0;
}

